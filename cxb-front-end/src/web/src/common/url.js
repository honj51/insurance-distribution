
const urlPrefix = '/cxb/nol/';
const WEB_DOMAIN_FULL_PATH = window.location.protocol + "//" + window.location.hostname;
// const WEB_DOMAIN_FULL_PATH = window.location.protocol + "//" + window.location.hostname + ":" + window.location.port;
export const RequestUrl = {


    //========================================企业微信运动
    //企业微信网页授权回调地址,snsapi_userinfo=静默授权，可获取成员的敏感信息，但不包含手机、邮箱；
    WECHAT_OAUTH2_ENTERPRISE_REDIRECT_URI: WEB_DOMAIN_FULL_PATH + "/cxb/nol/wechatenterprise/snsapi_userinfo",
    //企业微信【已】授权自动跳转地址
    WECHAT_OAUTH2_ENTERPRISE_AFTER_URL: WEB_DOMAIN_FULL_PATH + "/sticcxb/",
    //根据userId查询数据库的企业微信账户
    GET_USER_BY_USER_ID: urlPrefix + "wechatenterprise/getUserByUserId",
    //获取部门列表
    DEPARTMENT_LIST: urlPrefix + "wechatenterprise/departmentList",
    //获取部门下的用户列表
    GET_USERS_BYDEPARTMENT: urlPrefix + "wechatenterprise/getUsersBydepartment",
    //上传运动信息
    INSERT_SPORT: urlPrefix + "wechatenterprise/insertSport",
    //排行榜
    GET_SPORT_RANKING_LIST: urlPrefix + "wechatenterprise/getSportRankingList",


    //========================================智通
    //智通快速投保按钮地址
    ZHI_TONG_INSURE: "https://h5bp.ztwltech.com/h5bp/customer/begin?coopUserCode=E8F4E286958646FEA1CD08E9372B4D37",


    //========================================自销营平台
    BASE_URL: WEB_DOMAIN_FULL_PATH + "/sticcxb/#",
    BASE_URL_UPLOAD: WEB_DOMAIN_FULL_PATH + "/sticcxb/upload/",
    //web域名根路径
    WEB_PATH: WEB_DOMAIN_FULL_PATH + "/",
    //百度IP获取地址
    IP_BAIDU_REGION: "https://api.map.baidu.com/location/ip?ak=tcckBrhm8pX2gxbYQSQC3AMByLgeAYGC&coor=bd09ll",
    //根据IP获取地区url
    IP_REGION: "http://int.dpool.sina.com.cn/iplookup/iplookup.php?format=js",
    //客服
    WE_CHAT_FOLLOW_PUBLIC_NUMBER: "https://mp.weixin.qq.com/mp/profile_ext?action=home&__biz=MjM5MDYzNDA2MQ==&scene=124#wechat_redirect",
    //微信授权请求接口
    WECHAT_OAUTH2_URI: "https://open.weixin.qq.com/connect/oauth2/authorize",
    //微信【未】授权回调接口
    WECHAT_OAUTH2_REDIRECT_URI: WEB_DOMAIN_FULL_PATH + "/cxb/nol/wechat/snsapi_userinfo",//snsapi_base静默授权
    //微信【已】授权自动跳转地址
    WECHAT_OAUTH2_AFTER_URL: WEB_DOMAIN_FULL_PATH + "/sticcxb/",
    //根据openId获取用户信息
    FIND_USER_BY_OPEN_ID: urlPrefix + "user/findUserByOpenId",
    //微信分享请求服务地址
    WECHAT_SHARE: urlPrefix + "wechat/ticket",
    //获取地址信息
    GET_ADDRESS: urlPrefix + "addressStroe/all",
    //登录 & 手机
    LOGIN_BY_MOBILE: urlPrefix + "user/loginByMobile",
    //登录 & 手机 & 图形码
    LOGIN_BY_MOBILE_IMG: urlPrefix + "user/loginByMobile_img",
    //登录 & 密码 & 图形码
    LOGIN_BYPASSWORD: urlPrefix + "user/loginBypassword",
    //个人账户注册
    REGISTER: urlPrefix + "user/register",
    //个人账户注册 & 图形码
    REGISTER_IMG: urlPrefix + "user/registerImg",

    //查询是否存在该手机的账户
    IS_MERGE_USER: urlPrefix + "user/isMergeUser",

    //密码修改_短信验证码
    UPDATE_PWD_BY_MOBILE: urlPrefix + "user/updatePwdByMobile",
    //密码修改_短信验证码 & 图形码
    UPDATE_PWD_BY_MOBILE_IMG: urlPrefix + "user/updatePwdByMobile_img",
    //实名认证_短信验证码
    UPDATE_ID_NUMBER_BY_MOBILE: urlPrefix + "user/updateIdNumberByMobile",
    //实名认证_短信验证码 & 图形码
    UPDATE_ID_NUMBER_BY_MOBILE_IMG: urlPrefix + "user/updateIdNumberByMobile_img",
    // //解绑实名认证_短信验证码(未开发此接口)
    // UNBIND_ID_NUMBER_BY_MOBILE: urlPrefix + "user/unbindIdNumberByMobile",
    // //解绑实名认证_短信验证码 & 图形码
    // UNBIND_ID_NUMBER_BY_MOBILE_IMG: urlPrefix + "user/unbindIdNumberByMobile_img",
    //查询某用户所有有效的银行卡
    FIND_BANK_LIST_ALL: urlPrefix + "user/findBankListAll",
    //绑定银行卡=>第一步校验
    FIND_BANK_NO_OF_BINDING: urlPrefix + "user/findBankNoOfBinding",
    //绑定银行卡=>第二步校验短信验证码并绑定
    UPDATE_BANK_OF_BINDING: urlPrefix + "user/updateBankOfBinding",
    //解绑银行卡
    UPDATE_BANK_UNBUNDLING: urlPrefix + "user/updateBankUnbundling",
    //设置默认银行卡
    UPDATE_BANK_DEFAULT: urlPrefix + "user/updateBankDefault",
    //姓名修改
    // UPDATE_USER_NAME: urlPrefix + "user/updateUserName",
    //手机号修改
    UPDATE_MOBILE: urlPrefix + "user/updateMobile",
    //手机号修改 & 图形码
    UPDATE_MOBILE_IMG: urlPrefix + "user/updateMobile_img",
    //合并账户
    UPDATE_USER_FORMERGE: urlPrefix + "user/updateUserFormerge",
    //发送短信验证码
    SMS_CODE: urlPrefix + "code/smsCode",
    //发送短信验证码 & 图形码
    SMS_CODE_AND_IMG_CODE: urlPrefix + "code/smsCode_img",
    //获取图形码
    GET_VALIDATE_CODE: urlPrefix + "code/getImgCode",
    //上传头像图片
    UPLOAD_HEAD_IMG: urlPrefix + "upload/uploadHeadImg",
    //上传头像图片
    UPLOAD_HEAD_IMG_BY_CODE: urlPrefix + "upload/uploadHeadImgByCode",
    //头像默认图片获取
    HEAD_IMG_DEFAULT: "upload/uploadHeadImg/defaultHeader.jpg",
    // 按不同分类查询产品
    PRODUCT_LIST: urlPrefix + "product/productList",
    //根据产品ID获取套餐计划
    QUERY_PLANS: urlPrefix + "product/queryPlans",
    //获取服务配置
    MENU_LIST: urlPrefix + "serviceMenu/menuList",
    //首页初始化
    PRODUCT_MENU_AD: urlPrefix + "product/productMenuAd",
    //保存计划
    SAVE_PLAN: urlPrefix + "product/savePlan",
    //获取付款信息
    PAY: urlPrefix + "pay/getPayInfo",
    //支付查询成功后获取投保单
    PAY_INFO_FIND: urlPrefix + "pay/getPayInfoFind",
    //保存订单
    SAVE_ORDER: urlPrefix + "order/saveOrder",
    //获得用户订单信息
    GET_ORDER_LIST: urlPrefix + "order/getOrderList",
    //查找订单相关信息
    FIND_ORDER_LIST: urlPrefix + "order/findOrder",
    //电子保单下载
    DOWN_POLICY: urlPrefix + "order/downPolicy",
    //电子发票查询
    QUERY_EINV: urlPrefix + "order/queryEinv",
    //开具电子发票
    SEND_EINV_PRINT_DATA: urlPrefix + "order/sendEinvPrintData",
    //获得对应编码
    GET_CODE_TYPE: urlPrefix + "codeRelate/getCodeType",
    SHARE_FIND_PLAN: urlPrefix + "product/shareFindPlan",
    QUERY_SALE_PLAN: urlPrefix + "product/querySalePlan",

    QUERY_PLAN: urlPrefix + "product/queryPlan",

    FIND_ADVERT_ID: urlPrefix + "advert/findAdvertId",

    GET_SELE_COUNTRY: urlPrefix + "codeRelate/getSeleCountry",

    //历史赔案查询
    QUERY_CLAIMS: urlPrefix + "claims/queryClaims",

    //获得类型
    GET_CLIAIMS_TYPE: urlPrefix + "codeRelate/getCliaimsType",
    //马上报案-查询保单_短信验证码 & 图形码
    QUERY_POLICYS: urlPrefix + "claims/queryPolicys",
    //马上报案
    REPORT_CLAIMS: urlPrefix + "claims/reportClaims",
    //上传图片
    UPLOAD_PHOTO: urlPrefix + "claims/uploadPhoto",

    QUERY_CLAUSE: urlPrefix + "product/queryClause",

    QUERY_CLAUSE_TITLE: urlPrefix + "product/queryClauseTitle",
    //进度查询-查询报案_短信验证码 & 图形码
    QUERY_REPORTS: urlPrefix + "claims/queryReports",

    //保存分享
    SAVE_SHARE: urlPrefix + "share/saveShare",

    //获取抽奖
    ACT_SAVE_SHARE: urlPrefix + "share/actSaveShare",

    //根据id获取用户
    FIND_USER_BY_ID: urlPrefix + "user/findUserById",

    QUERY_POLICYS_AUTHEN: urlPrefix + "claims/queryPolicysAuthen",

    FIND_POLICYS: urlPrefix + "claims/findPolicys",

    PRPD_QUERY: urlPrefix + "prpd/queryCode",

    AXTX_INSURE: urlPrefix + "axtx/insure",

    //产品管理
    PRODUCT_MANAGE: urlPrefix + "shop/productManage",

    //保存
    SAVE_STORE_PRO: urlPrefix + "shop/saveStorePro",

    //保单详情查询接口
    QUERY_POLICY_DETAIL: urlPrefix + "policy/queryPolicyDetail",

    QUERY_PROPOSAL: urlPrefix + "claims/queryProposal",

    PERFORMANCE: urlPrefix + "order/performance",
    //按类型分页查询软文
    ARTICLE_LIST: urlPrefix + "article/articleList",
    FIND_SOFT_PAPER: urlPrefix + "article/findSoftPaper",
    //按类型分页查询海报
    POSTER_LIST: urlPrefix + "poster/allPoster",
    FIND_ONE_POSTER: urlPrefix + "poster/findOnePoster",

    SHOP_PRODUCT_LIST: urlPrefix + "shop/productList",
    //申请业务关系代码
    APPLY_AGREEMENT_NO: urlPrefix + "shop/applyAgreementNo",
    //查询业务关系代码 & 图形码
    FIND_BY_AGREEMENT_NO_IMG: urlPrefix + "shop/findByAgreementNo_img",
    //查询是否有专属佣金配置-店铺
    FIND_E_YONGJ_COUNT_BY_USER_CODE: urlPrefix + "shop/findEYongjCountByUserCode",
    //查询业务关系代码-店铺
    FIND_AGREEMENT_NO_OF_SHOP: urlPrefix + "shop/findAgreementNoOfShop",
    //查询机构代码代码
    FIND_SEALES_PERSOS: urlPrefix + "shop/findSealesPersos",
    //获取所有常驻地区
    FIND_ALL_HOT_AREA: urlPrefix + "shop/findAllHotArea",
    //无_业务关系代码开店
    OPEN_STORE_BY_AREA: urlPrefix + "shop/openStoreByArea",
    // 有_业务关系代码开店 <==> 业务关系代码认证
    OPEN_STORE_BY_AGREEMENT: urlPrefix + "shop/openStoreByAgreement",

    QUERY_DETAIL: urlPrefix + "policy/queryDetail",

    PERFORMANCE_LIST: urlPrefix + "order/performanceList",

    SET_STATE_TYPE: urlPrefix + "shop/setStateType",

    QUERY_PAY_LIST: urlPrefix + "pay/queryPayList",

    FIND_SHOP_INFO: urlPrefix + "shop/findShopInfo",

    SAVE_SHOP_NAME: urlPrefix + "keyword/saveShopName",

    PERFORMANCE_DETAIL: urlPrefix + "order/performanceDetail",

    CALL_BACK_URL: urlPrefix + "pay/callBackUrl",

    PLATFORM_INSURE_IMG: urlPrefix + "apply/platformInsure_img",

    PERFORMANCE_MONTH: urlPrefix + "order/performanceMonth",

    GET_CARLIMIT_NO: urlPrefix + "codeRelate/getCarlimitNo",

    //我的积分
    MY_INTEGRAL: urlPrefix + "integral/myIntegral",
    //积分收支记录
    INTEGRAL_IN_OR_OUT_LIST: urlPrefix + "integral/integralInOrOutList",
    //积分详情
    FIND_INTEGRAL_ONE: urlPrefix + "integral/findIntegralOne",
    //积分兑换提现
    INTEGRAL_CASH: urlPrefix + "integral/integralCash",
    //积分兑换记录
    FIND_CASH_ALL: urlPrefix + "integral/findCashAll",

    SHOP_RATE: urlPrefix + "shop/shopRate",

    MERGE_ACCOUNT: urlPrefix + "shop/mergeAccount",


    FIND_USERS: urlPrefix + "batch_process_q_rcode_test/findUsers",
    //店铺分享产品二维码
    SHARE_ZXING_QR_CODE: urlPrefix + "qr_code/shareZxingQrCode",

    VAL_DATA: urlPrefix + "product/valData",

    //获得当前用户的抽奖次数
    QUERY_DRAW_TIMES: urlPrefix + "activity/queryDrawTimes",

    //抽奖
    ACTIVITY_DRAW: urlPrefix + "activity/draw",

    //新春大聚惠
    GET_ACTIVITY_GREAT_BENEFIT: urlPrefix + "activity/getActivityGreatBenefit",

    //根据身份证查询客户信息
    FIND_ID_CAR: urlPrefix + "application/findIdCar",

    QUERY_VIN: urlPrefix + "axtx/queryVin",

    FIND_BY_QUE_ID: urlPrefix + "questionn/findByQueId",

    INSURE_UPLOAD: urlPrefix + "insure/insureUpload",

    GET_CONFIG_DATA: urlPrefix + "insure/getConfigData",

    FIND_ALL_FRIEND: urlPrefix + "insure/findAllFriend",

    //---------------------------------------------------------------五期 - 层级/渠道
    //根据用户usercode查询渠道账户
    FIND_USER_BY_USER_CODE: urlPrefix + "user/findUserByUserCode",
    //查询积分配置规则
    FIND_SCORE_CONFIG: urlPrefix + "integral/findScoreConfig",
    //渠道账户注册
    REGISTER_CHANNEL: urlPrefix + "user/registerChannel",
    //渠道账户注册 & 图形码
    REGISTER_CHANNEL_IMG: urlPrefix + "user/registerChannelImg",
    //邀请好友注册二维码
    REGISTER_ZXING_QR_CODE: urlPrefix + "qr_code/registerZxingQrCode",
    //查询上级用户
    FIND_USER_BY_COMCODE: urlPrefix + "user/findUserByComcode",


    //请求基本险种数据
    QUERY_INIT_RISK_DATA: urlPrefix + "product/queryInitRiskData",
    //请求菜单
    QUERY_MENU_DATA: urlPrefix + "product/queryMenuData",
    //初始化广告
    ADVERT_INIT: urlPrefix + "product/advertInit",

    //查找保存数据信息
    FIND_SAVE_DATA: urlPrefix + "insure/findSaveData",

    FIND_SAVE_DATA_ID: urlPrefix + "insure/findSaveDataId",
}
