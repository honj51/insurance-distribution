import Config from '../config/insureConfig';
import { Gender, StrLenth } from 'src/common/const';
let ValidationParam = Config.getValidationParam();

/**
 * Author		AoYiYi
 * Datetime		:	2017-09-12
 * ********************************************************************
 */
export default {
    /**
   * 判断是否为空
   * @param {*} data 
   */
    isEmpty(data) {
        return (typeof (data) != 'undefined' && data != undefined && data != null && data != "" && data != 'null' && data != 'undefined' && (data + "").length > 0) ? false : true;
    },
    isNotEmpty(data) {
        return !this.isEmpty(data);
    },
    /** 图形码是否有效 */
    isImgCode(imgCode) {
        if (this.isEmpty(imgCode)) {
            return "请输入图形码";
        }
        if (imgCode.length != StrLenth.img) {
            return "图形码必须为" + StrLenth.img + "位字符";
        }
        return null;
    },
    /** 手机验证码是否错误,true=错误 */
    isErrSmsCode(mobileCode) {
        let RegCode = new RegExp(/^\d{6}$/);
        if (RegCode.test(mobileCode)) {
            return false;
        } return true;
    },
    /** 手机验证码是否有效 */
    isSmsCode(code) {
        if (this.isEmpty(code)) {
            return "请输入短信验证码";
        }
        if (this.isErrSmsCode(code)) {
            return "短信验证码必须为" + StrLenth.sms + "位数字";
        }
        return null;
    },
    /**是手机号是否非11位,true=错误 */
    isErrPhone11(phone) {
        let RegMoblie = new RegExp(/^\d{11}$/);
        if (RegMoblie.test(phone)) {
            return false;
        }
        return true;
    },
    /**是手机号是否错误,true=错误 */
    isErrPhone(phone) {
        let RegMoblie = new RegExp(/^1[3|4|5|7|8|9][0-9]\d{8}$/);
        if (RegMoblie.test(phone)) {
            return false;
        }
        return true;
    },
    /** 是否为11位数字,null=有效 */
    isPhone11(phone) {
        if (this.isEmpty(phone)) {
            return "手机号不能为空";
        }
        if (this.isErrPhone11(phone)) {
            return "手机号格式错误";
        }
        return null;
    },
    /** 是否为手机号,null=有效 */
    isPhone(phone) {
        if (this.isEmpty(phone)) {
            return "手机号不能为空";
        }
        if (this.isErrPhone(phone)) {
            return "手机号格式错误";
        }
        return null;
    },
    //是否有特殊字符,return true=有特殊字符
    isSpecialCharacters(name) {
        let RegName = new RegExp("[`~!@#$^&*+()=|{}\"':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
        if (RegName.test(name)) {
            return true;
        }
        return false;
    },
    //是否有特殊字符1,return true=有特殊字符
    isSpecialCharacters1(name) {
        let RegName = new RegExp("[`~!@#$%^&*()\\_+|}{\":<>?，./;''\\[\\]=\\-。]");
        if (RegName.test(name)) {
            return true;
        }
        return false;
    },
    //是否有特殊字符2,return true=有特殊字符
    isSpecialCharacters2(name) {
        let RegName = new RegExp("[`~!@#$%^&*+|}{\":<>?/;''\\[\\]=。]");
        if (RegName.test(name)) {
            return true;
        }
        return false;
    },
    //是否实名认证,姓名和证件号都有效=true
    isRealName(name, idNo, type) {
        //证件类型：1身份证，2社会信用代码
        if (this.isNotEmpty(type)) {
            switch (type) {
                case "1":
                    return this.isChNameA_9(name) && this.isIdNumber(idNo);//已实名
                case "2":
                    return this.isNotEmpty(name) && this.isNotEmpty(idNo);//已实名
                default:
                    break;
            }
        }
        return false;
    },
    //验证中文姓名是否有效,return true=有效
    isChNameA_9(name) {
        // debugger
        if (!this.isEmpty(name) && name.length > 1 && name.length <= StrLenth.userNameMax) {
            if (/^[A-Za-z0-9\u4E00-\u9FA5]+$/.test(name)) {
                return true;
            }
        }
        return false;
    },
    //验证中文姓名是否有效,return true=有效
    isChName(name) {
        // debugger
        if (!this.isEmpty(name) && name.length > 1 && name.length <= StrLenth.userNameMax) {
            if (/^[\u4E00-\u9FA5]+$/.test(name)) {
                return true;
            }
        }
        return false;
    },
    /** 真实姓名是否有效（含字母数字） */
    isUserNameA_9(name) {
        if (this.isEmpty(name)) {
            return "姓名不能为空";
        }
        if (!this.isChNameA_9(name)) {
            return "姓名必须是" + StrLenth.userNameMin + "-" + StrLenth.userNameMax + "位";
        }
        return null;
    },
    /** 真实姓名是否有效 */
    isUserName(name) {
        if (this.isEmpty(name)) {
            return "姓名不能为空";
        }
        if (!this.isChName(name)) {
            return "姓名必须是" + StrLenth.userNameMin + "-" + StrLenth.userNameMax + "位中文";
        }
        return null;
    },
    /** 用户昵称是否有效 */
    isNickName(nickName) {
        if (this.isEmpty(nickName)) {
            return "用户名不能为空";
        }
        if (nickName.length < StrLenth.nickNameMin || nickName.length > StrLenth.nickNameMax) {
            return "用户名必须为" + StrLenth.nickNameMin + "-" + StrLenth.nickNameMax + "位";
        }
        return null;
    },
    /** 密码是否有效 */
    isPassword(password) {
        if (this.isEmpty(password)) {
            return "请输入密码";
        }
        if (password.length < StrLenth.pwdMin || password.length > StrLenth.pwdMax) {
            return "密码必须为" + StrLenth.pwdMin + "-" + StrLenth.pwdMax + "位";
        }
        if (this.isSpecialCharacters(password)) {
            return "密码不能含有特殊字符";
        }
        return null;
    },
    /** 员工编码是否有效 */
    isEmployeeCode(employeeCode) {
        if (this.isEmpty(employeeCode)) {
            return "请输入员工编码";
        }
        if (employeeCode.length < StrLenth.eCodeMin || employeeCode.length > StrLenth.eCodeMax) {
            return "员工编码必须为" + StrLenth.eCodeMin + "-" + StrLenth.eCodeMax + "位";
        }
        if (this.isSpecialCharacters(employeeCode)) {
            return "员工编码不能含有特殊字符";
        }
        return null;
    },
    /** */
    vinNoVal(vinNo) {
        let reg = new RegExp(/^[a-hj-npr-zA-HJ-NPR-Z0-9\s]+$/);
        return !reg.test(vinNo);
    },
    isEmail(email) {
        let reg = new RegExp("^[a-z0-9]+([._\\-]*[a-z0-9])*@([a-z0-9]+[-a-z0-9]*[a-z0-9]+.){1,63}[a-z0-9]+$");
        // let obj = document.getElementById(email); //要验证的对象
        // if (obj.value === "") { //输入不能为空
        //     return false;
        // } else 
        if (!reg.test(email)) { //正则验证不通过，格式不对
            return false;
        } else {
            return true;
        }
    },
    //验证统一社会信用代码是否错误 true=错误
    isErrTrinityIdNo(idCardNo) {
        let RegTrinityIdNo = new RegExp(/^([A-Z]|[a-z]|[0-9]){8,18}$/);
        if (RegTrinityIdNo.test(idCardNo)) {
            return false;
        }
        return true;
    },
    //验证18位身份证号是否有效,return true=有效
    isIdNumber(idNo) {
        if (this.isEmpty(idNo)) {
            return false;
        }
        // 前17位
        let code17 = idNo.substring(0, 17);
        // 第18位
        let code18 = idNo.substring(17, idNo.length);
        //前17位纯数字
        if (this.isNum(code17)) {
            //加权因子相乘之和
            let sum17 = this.getPowerSum(code17);
            // 获取校验位
            let val18 = this.getCheckCode18(sum17);
            //校验成功
            if (val18.length > 0 && val18.toLocaleUpperCase() === code18.toLocaleUpperCase()) {
                return true;
            }
        }
        return false;
    },
    //根据身份编号获取年龄数
    getAgeByidCardNumber(idNo) {
        if (this.isIdNumber(idNo)) {
            let year = parseInt(idNo.substring(6, 10));
            let nowDate = new Date(parseInt(sessionStorage["dataTime"]));
            return nowDate.getFullYear() - year;
        }
        return 0;
    },
    //根据身份编号获取生日,return yyyy-MM-dd
    getBirthByidCardNumber(idNo) {
        if (this.isIdNumber(idNo)) {
            let birth = idNo.substring(6, 14);
            return birth.substring(0, 4) + "-" + birth.substring(4, 6) + "-" + birth.substring(6);
        }
        return '';
    },
    //根据身份编号获取性别,return 性别(M-男，F-女，N-未知)
    getGenderByidCardNumber(idNo) {
        let sGender = Gender.UNKNOWN;
        if (this.isIdNumber(idNo)) {
            let sCardNum = idNo.substring(16, 17);
            if (parseInt(sCardNum) % 2 != 0) {
                sGender = Gender.MALE;
            } else {
                sGender = Gender.WOMEN;
            }
        }
        return sGender;
    },
    //根据身份编号获取户籍省份,return 省份名称
    getProvinceByidCardNumber(idNo) {
        if (this.isIdNumber(idNo)) {
            let sProvinNum = idNo.substring(0, 2);
            return ValidationParam.CITYCODES[sProvinNum];
        }
        return null;
    },
    //判断参数是否纯数字,true=number
    isNum(code17) {
        if (("" + code17) != "" && /^[0-9]*$/.test(code17)) {
            return true;
        }
        return false;
    },
    //将身份证的每位和对应位的加权因子相乘之后，再得到和值
    getPowerSum(code17) {
        let sum17 = 0;
        if (ValidationParam.POWER.length == code17.length) {
            for (let i = 0; i < code17.length; i++) {
                let codeCh = parseInt(code17.substring(i, i + 1));
                for (let j = 0; j < ValidationParam.POWER.length; j++) {
                    if (i == j) {
                        sum17 += codeCh * ValidationParam.POWER[j];
                    }
                }
            }
        }
        return sum17;
    },
    //将power和值与11取模获得余数进行校验码判断
    getCheckCode18(sum17) {
        let sCode = "";
        switch (sum17 % 11) {
            case 10:
                sCode = "2";
                break;
            case 9:
                sCode = "3";
                break;
            case 8:
                sCode = "4";
                break;
            case 7:
                sCode = "5";
                break;
            case 6:
                sCode = "6";
                break;
            case 5:
                sCode = "7";
                break;
            case 4:
                sCode = "8";
                break;
            case 3:
                sCode = "9";
                break;
            case 2:
                sCode = "x";
                break;
            case 1:
                sCode = "0";
                break;
            case 0:
                sCode = "1";
                break;
        }
        return sCode;
    },
    //获取屏幕宽高
    geWHByScreen() {
        let w = screen.width;
        let h = screen.height;
        w = w > 0 ? w : screen.availWidth;
        h = h > 0 ? h : screen.availHeight;
        w = w > 0 ? w : window.screen.width;
        h = h > 0 ? h : window.screen.height;
        w = w > 0 ? w : window.screen.availWidth;
        h = h > 0 ? h : window.screen.availHeight;
        return { w: w, h: h }
    },
    //根据屏幕分辨率获取英寸,3.5,4.0,4.7,5.5,5.8,6.0,9.7,12.9,1.0
    geInchByScreen() {
        let screen = this.geWHByScreen();
        if (screen.w == 320 && screen.h == 480) {//iphone4
            return 3.5;
        } else if (screen.w == 320 && screen.h == 568) {//iPhone5
            return 4.0;
        } else if (screen.w == 375 && screen.h == 667) {//iPhone6,7,8
            return 4.7;
        } else if (screen.w == 412 && screen.h == 732) {//Nexus5x,6p
            return 5.0;
        } else if (screen.w == 360 && screen.h == 640) {//GalaxyS5
            return 5.1;
        } else if (screen.w == 414 && screen.h == 736) {//iPhone6,7,8Plus
            return 5.5;
        } else if (screen.w == 375 && screen.h == 812) {//iPhoneX
            return 5.8;
        } else if (screen.w > 414 || screen.h > 812) {
            return 6.0;
        } else if (screen.w > 320 && screen.w < 375) {
            return 4.0;
        } else if (screen.w > 375 && screen.w < 414) {
            return 4.7;
        } else if (screen.w >= 414) {
            return 5.5;
        } else if (screen.w >= 768) {
            return 9.7;
        } else if (screen.w >= 1024) {
            return 12.9;
        } else {
            return 1.0;
        }
    },
}