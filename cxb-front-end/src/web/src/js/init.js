import {
  PopState,
  FooterShow,
  Mutations,
  RouteUrl
} from 'src/common/const';

export default {
  indexInit(_this, indexState) {
    // document.body.scrollTop = 0
    // document.documentElement.scrollTop = 0
    $('html,body').animate({
      scrollTop: '0px'
    }, 10);
    // let iframe = {
    //   isIframe: false,
    // };
    // _this.$store.commit(Mutations.SET_IFRAME_DATA, iframe);
    // if(){

    // }
    if (_this.$route.path != RouteUrl.SERVICE) {
      _this.$common.storeCommit(_this, Mutations.SET_IS_SHOP, false);
    }
    _this.$store.commit(Mutations.POP_STATE, PopState.VUX_POP_NULL);
    _this.$store.commit(Mutations.FOOTER_STATE, indexState);
    _this.$store.commit(Mutations.FOOTER_SHOW, FooterShow.TRUE);
    $("body").css("overflow-y", "auto");
  },
  subPageInit(_this) {
    // document.body.scrollTop = 0
    // document.documentElement.scrollTop = 0
    $('html,body').animate({
      scrollTop: '0px'
    }, 10);
    // let iframe = {
    //   isIframe: false,
    // };
    // _this.$store.commit(Mutations.SET_IFRAME_DATA, iframe);
    // _this.$common.storeCommit(_this, Mutations.INIT_TYPE_INSURANCE, null);
    _this.$store.commit(Mutations.POP_STATE, PopState.VUX_POP_NULL);
    _this.$store.commit(Mutations.FOOTER_SHOW, FooterShow.FALSE);
    $("body").css("overflow-y", "auto");
  },
  subPageInitFooter(_this) {
    // document.body.scrollTop = 0
    // document.documentElement.scrollTop = 0
    $('html,body').animate({
      scrollTop: '0px'
    }, 10);
    // let iframe = {
    //   isIframe: false,
    // };
    // _this.$store.commit(Mutations.SET_IFRAME_DATA, iframe);
    _this.$store.commit(Mutations.POP_STATE, PopState.VUX_POP_NULL);
    _this.$store.commit(Mutations.FOOTER_SHOW, FooterShow.TRUE);
    $("body").css("overflow-y", "auto");
  },
}
