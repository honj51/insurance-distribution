export const TRANSLATIONS_EN = {
    "content": "This is some {type} content",
    "index": "Index",
    "insurance": "Insurance",
    "service": "Service",
    "my": "My",
    "henghua": "Henghua",
    "more": "more",
    "hotActivity": "Hot Activity",
    "moreInsurance": "More Insurance",
    "moreService": "More Service"
};