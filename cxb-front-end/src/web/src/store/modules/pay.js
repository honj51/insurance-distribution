import { Mutations } from 'src/common/const';
import OrderConfig from 'src/common/config/orderConfig';
import {
    RequestUrl
} from 'src/common/url';
export default {
    state: {
        paymentNo: '',
        riskCode: '',
        showPayData: {
            policyno: "",
            proposalNo: null,
            applicode: "",
            appliIdentifynumber: "",
            appliname: "",
            insuredcode: "",
            insuredIdentifynumber: "",
            insuredname: "",
            licenseno: "",
            brandname: "",
            seatcount: "",
            sumpremium: "",
            riskcode: "",
            startdate: "",
            enddate: "",
            status: "",
            riskcname: "",
            polisource: "",
            underwriteflag: null,
            itemKinds: [
                {
                    kindCodeMain: "",
                    kindNameMain: "",
                    modeCode: "",
                    modeName: "",
                    premiumMain: 0,
                    amountMain: 0
                },
            ]
        },
        payStyle: "background:#636365;",
        state: "正在查询..",
        isPay: false,
        // showPayData: {
        //     policyno: "9131055010170000085000",
        //     proposalNo: null,
        //     applicode: "5110001000028232",
        //     appliIdentifynumber: "null",
        //     appliname: "QQL",
        //     insuredcode: "5110001000028232",
        //     insuredIdentifynumber: "350521199501257577",
        //     insuredname: "QQL",
        //     licenseno: "null",
        //     brandname: "null",
        //     seatcount: "null",
        //     sumpremium: "400",
        //     riskcode: "3105",
        //     startdate: "2017-12-21 0:00",
        //     enddate: "2018-12-20 24:00",
        //     status: "8",
        //     riskcname: "尊驰无忧",
        //     polisource: "0040000006",
        //     underwriteflag: null,
        //     itemKinds: [
        //         {
        //             kindCodeMain: "27M0029",
        //             kindNameMain: "个人意外伤害保险(A款)条款",
        //             modeCode: "null",
        //             modeName: "null",
        //             premiumMain: 120,
        //             amountMain: 200000
        //         },
        //         {
        //             kindCodeMain: "27S0096",
        //             kindNameMain: "附加个人意外伤害医疗保险(A款)条款",
        //             modeCode: "null",
        //             modeName: "null",
        //             premiumMain: 120,
        //             amountMain: 20000
        //         },
        //         {
        //             kindCodeMain: "27S0097",
        //             kindNameMain: "附加个人意外伤害住院津贴保险(A款)条款",
        //             modeCode: "null",
        //             modeName: "null",
        //             premiumMain: 20,
        //             amountMain: 9000
        //         },
        //         {
        //             kindCodeMain: "27S0092",
        //             kindNameMain: "附加个人交通意外伤害保险(A款)条款",
        //             modeCode: "null",
        //             modeName: "null",
        //             premiumMain: 80,
        //             amountMain: 300000
        //         },
        //     ]
        // },

        payOrder: {},

    },
    mutations: {
        [Mutations.PAY_SET_DATA](state, payData) {
            // state.paymentNo = payData.paymentNo;
            // state.riskCode = payData.riskCode;
            let payFormData = {
                documentNo: payData.paymentNo,
                riskCode: payData.riskCode
            };
            let _this = payData._this;
            _this.$http.post(RequestUrl.PAY, payFormData)
                .then(function (res) {
                    let payInfo = res.result;

                    $("#payForm").attr("action", payInfo.paymentURL)
                    $("#redirectUrl").val(payInfo.redirectUrl);
                    // $("#redirectUrl").val("http://10.132.28.177:8080/cxb/nol/pay/payRed");
                    $("#callBackUrl").val(payInfo.callbackURL);
                    // $("#callBackUrl").val("ttps://uat-lm.libertymutual.com.cn/cxb/nol/pay/paySuccess");
                    $("#agreementNo").val(payInfo.agreementNo);
                    $("#partnerAccountCode").val(payInfo.partnerAccountCode);
                    $("#documentNo").val(payInfo.documentNo);
                    $("#recordCode").val(payInfo.recordCode);
                    $("#flowId").val(payInfo.flowId);
                    $("#riskCode").val(payInfo.riskCode);
                    $("#operatorDate").val(new Date());
                    //提交表单，进入支付界面
                    $("#payForm").submit();

                })
        },
        [Mutations.SET_SHOW_PAY_DATA](state, payData) {
            // state.paymentNo = payData.paymentNo;
            // state.riskCode = payData.riskCode;
            let payFormData = {
                policyNo: payData.order.proposalNo,
                TYPE: "FORM"
            };
            state.payOrder = payData.order;
            let _this = payData._this;
            _this.$http.post(RequestUrl.QUERY_DETAIL, payFormData)
                .then(function (res) {
                    state.payStyle = "background:#C8161D;"
                    state.state = "支付";
                    state.showPayData = res.policys[0];
                    state.isPay = true;
                })
        },
        [Mutations.SET_SHOW_PAY_DEFAULT](state, payData) {
            state.payStyle = "background:#636365;"
            state.state = "正在查询中..";
            state.isPay = false;
        },
    },
    actions: {

    },
    getters: {

    }
}