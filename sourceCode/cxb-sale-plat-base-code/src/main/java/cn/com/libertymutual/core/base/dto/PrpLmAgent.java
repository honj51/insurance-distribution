package cn.com.libertymutual.core.base.dto;

import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Created by Ryan on 2016-09-09.
 */
@Entity
@javax.persistence.Table(name = "tb_prp_lm_agent")
public class PrpLmAgent implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4224578422982630692L;
	private String id;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@javax.persistence.Column(name = "ID", nullable = false, length = 25)
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	private String branch;

	/**Remarks: 所属机构编码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年12月13日下午3:17:04<br>
	 * Project：liberty_sale_plat<br>
	 * @return
	 */
	@Basic
	@javax.persistence.Column(name = "BRANCH", nullable = true, length = 100)
	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	private String agreementNo;

	/**Remarks: 业务关系代码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年12月13日下午3:17:19<br>
	 * Project：liberty_sale_plat<br>
	 * @return
	 */
	@Basic
	@javax.persistence.Column(name = "AGREEMENT_NO", nullable = false, length = 20)
	public String getAgreementNo() {
		return agreementNo;
	}

	public void setAgreementNo(String agreementNo) {
		this.agreementNo = agreementNo;
	}

	private String product;

	@Basic
	@javax.persistence.Column(name = "PRODUCT", nullable = false, length = 100)
	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	private String carInsuranceType;

	@Basic
	@javax.persistence.Column(name = "CAR_INSURANCE_TYPE", nullable = true, length = 3)
	public String getCarInsuranceType() {
		return carInsuranceType;
	}

	public void setCarInsuranceType(String carInsuranceType) {
		this.carInsuranceType = carInsuranceType;
	}

	private String accType;

	/**Remarks: 佣金类型<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年12月13日下午3:15:39<br>
	 * Project：liberty_sale_plat<br>
	 * @return 1=代理人，2=提佣，3=绩效
	 */
	@Basic
	@javax.persistence.Column(name = "ACC_TYPE", nullable = true, length = 1)
	public String getAccType() {
		return accType;
	}

	public void setAccType(String accType) {
		this.accType = accType;
	}

	private Double comm1Rate;

	@Basic
	@javax.persistence.Column(name = "COMM1_RATE", nullable = true, precision = 0)
	public Double getComm1Rate() {
		return comm1Rate;
	}

	public void setComm1Rate(Double comm1Rate) {
		this.comm1Rate = comm1Rate;
	}

	private String agentCode;

	/**Remarks: 代理编码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年12月13日下午3:17:37<br>
	 * Project：liberty_sale_plat<br>
	 * @return
	 */
	@Basic
	@javax.persistence.Column(name = "AGENT_CODE", nullable = true, length = 100)
	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	private String agentName;

	/**Remarks: 代理名称<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年12月13日下午3:17:48<br>
	 * Project：liberty_sale_plat<br>
	 * @return
	 */
	@Basic
	@javax.persistence.Column(name = "AGENT_NAME", nullable = true, length = 255)
	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	private String handlerCode;

	@Basic
	@javax.persistence.Column(name = "HANDLER_CODE", nullable = true, length = 100)
	public String getHandlerCode() {
		return handlerCode;
	}

	public void setHandlerCode(String handlerCode) {
		this.handlerCode = handlerCode;
	}

	private String handlerName;

	@Basic
	@javax.persistence.Column(name = "HANDLER_NAME", nullable = true, length = 255)
	public String getHandlerName() {
		return handlerName;
	}

	public void setHandlerName(String handlerName) {
		this.handlerName = handlerName;
	}

	private String channel;

	/**Remarks: 销售渠道编码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年12月13日下午3:18:02<br>
	 * Project：liberty_sale_plat<br>
	 * @return
	 */
	@Basic
	@javax.persistence.Column(name = "CHANNEL", nullable = true, length = 100)
	public String getChannel() {
		return channel;
	}

	public void setChannel(String channel) {
		this.channel = channel;
	}

	private String channelName;

	/**Remarks: 销售渠道名称<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年12月13日下午3:18:11<br>
	 * Project：liberty_sale_plat<br>
	 * @return
	 */
	@Basic
	@javax.persistence.Column(name = "CHANNEL_NAME", nullable = true, length = 255)
	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	private String isPolicommFlag;

	@Basic
	@javax.persistence.Column(name = "IS_POLICOMM_FLAG", nullable = true, length = 1)
	public String getIsPolicommFlag() {
		return isPolicommFlag;
	}

	public void setIsPolicommFlag(String isPolicommFlag) {
		this.isPolicommFlag = isPolicommFlag;
	}

	private String validStatus;

	@Basic
	@javax.persistence.Column(name = "VALID_STATUS", nullable = false, length = 10)
	public String getValidStatus() {
		return validStatus;
	}

	public void setValidStatus(String validStatus) {
		this.validStatus = validStatus;
	}

	private Date startDate;

	@Basic
	@javax.persistence.Column(name = "START_DATE", nullable = true)
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	private java.util.Date endDate;

	@Basic
	@javax.persistence.Column(name = "END_DATE", nullable = true)
	public java.util.Date getEndDate() {
		return endDate;
	}

	public void setEndDate(java.util.Date endDate) {
		this.endDate = endDate;
	}

	private String remark;

	@Basic
	@javax.persistence.Column(name = "REMARK", nullable = true, length = 100)
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	private String motorcade;

	@Basic
	@javax.persistence.Column(name = "MOTORCADE", nullable = true, length = 1)
	public String getMotorcade() {
		return motorcade;
	}

	public void setMotorcade(String motorcade) {
		this.motorcade = motorcade;
	}

	private String dumpcar;

	@Basic
	@javax.persistence.Column(name = "DUMPCAR", nullable = true, length = 1)
	public String getDumpcar() {
		return dumpcar;
	}

	public void setDumpcar(String dumpcar) {
		this.dumpcar = dumpcar;
	}

	private Double baseUpperLimit;

	@Basic
	@javax.persistence.Column(name = "BASE_UPPER_LIMIT", nullable = true, precision = 0)
	public Double getBaseUpperLimit() {
		return baseUpperLimit;
	}

	public void setBaseUpperLimit(Double baseUpperLimit) {
		this.baseUpperLimit = baseUpperLimit;
	}

	private String carProperetyType;

	@Basic
	@javax.persistence.Column(name = "CAR_PROPERETY_TYPE", nullable = true, length = 255)
	public String getCarProperetyType() {
		return carProperetyType;
	}

	public void setCarProperetyType(String carProperetyType) {
		this.carProperetyType = carProperetyType;
	}

	private Double configSetValueName1;

	@Basic
	@javax.persistence.Column(name = "CONFIG_SET_VALUE_NAME1", nullable = true, precision = 0)
	public Double getConfigSetValueName1() {
		return configSetValueName1;
	}

	public void setConfigSetValueName1(Double configSetValueName1) {
		this.configSetValueName1 = configSetValueName1;
	}

	private Double configSetValueName3;

	@Basic
	@javax.persistence.Column(name = "CONFIG_SET_VALUE_NAME3", nullable = true, precision = 0)
	public Double getConfigSetValueName3() {
		return configSetValueName3;
	}

	public void setConfigSetValueName3(Double configSetValueName3) {
		this.configSetValueName3 = configSetValueName3;
	}

	private String credentialsNo;

	@Basic
	@javax.persistence.Column(name = "CREDENTIALS_NO", nullable = true, length = 100)
	public String getCredentialsNo() {
		return credentialsNo;
	}

	public void setCredentialsNo(String credentialsNo) {
		this.credentialsNo = credentialsNo;
	}

	private String permitNo;

	@Basic
	@javax.persistence.Column(name = "PERMIT_NO", nullable = true, length = 100)
	public String getPermitNo() {
		return permitNo;
	}

	public void setPermitNo(String permitNo) {
		this.permitNo = permitNo;
	}

	private Double commissionUpperLimit;

	@Basic
	@javax.persistence.Column(name = "COMMISSION_UPPER_LIMIT", nullable = true, precision = 0)
	public Double getCommissionUpperLimit() {
		return commissionUpperLimit;
	}

	public void setCommissionUpperLimit(Double commissionUpperLimit) {
		this.commissionUpperLimit = commissionUpperLimit;
	}

	private String alternate1;

	/**Remarks: 业务关系代码类型<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年12月13日下午3:15:16<br>
	 * Project：liberty_sale_plat<br>
	 * @return 0=内部员工,1=个贷,2=专业代理,3=兼业代理,4=经纪业务,5=电销,6=网销,i=邮政代理,k=银保代理
	 */
	@Basic
	@javax.persistence.Column(name = "ALTERNATE1", nullable = true, length = 64)
	public String getAlternate1() {
		return alternate1;
	}

	public void setAlternate1(String alternate1) {
		this.alternate1 = alternate1;
	}

	private String alternate2;

	@Basic
	@javax.persistence.Column(name = "ALTERNATE2", nullable = true, length = 64)
	public String getAlternate2() {
		return alternate2;
	}

	public void setAlternate2(String alternate2) {
		this.alternate2 = alternate2;
	}

	private String alternate3;

	@Basic
	@javax.persistence.Column(name = "ALTERNATE3", nullable = true, length = 64)
	public String getAlternate3() {
		return alternate3;
	}

	public void setAlternate3(String alternate3) {
		this.alternate3 = alternate3;
	}

	private String alternate4;

	@Basic
	@javax.persistence.Column(name = "ALTERNATE4", nullable = true, length = 64)
	public String getAlternate4() {
		return alternate4;
	}

	public void setAlternate4(String alternate4) {
		this.alternate4 = alternate4;
	}

	private String alternate5;
	private String alternate6;
	private String alternate7;
	private String alternate8;
	private String alternate9;
	private String alternate10;

	@Basic
	@javax.persistence.Column(name = "ALTERNATE5", nullable = true, length = 64)
	public String getAlternate5() {
		return alternate5;
	}

	public void setAlternate5(String alternate5) {
		this.alternate5 = alternate5;
	}

	/**Remarks: 组织机构代码<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年12月14日下午3:03:38<br>
	 * Project：liberty_sale_plat<br>
	 * @return
	 */
	@Basic
	@javax.persistence.Column(name = "ALTERNATE6", nullable = true, length = 64)
	public String getAlternate6() {
		return alternate6;
	}

	public void setAlternate6(String alternate6) {
		this.alternate6 = alternate6;
	}

	/**Remarks: 营业执照号<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2017年12月14日下午3:03:30<br>
	 * Project：liberty_sale_plat<br>
	 * @return
	 */
	@Basic
	@javax.persistence.Column(name = "ALTERNATE7", nullable = true, length = 64)
	public String getAlternate7() {
		return alternate7;
	}

	public void setAlternate7(String alternate7) {
		this.alternate7 = alternate7;
	}

	@Basic
	@javax.persistence.Column(name = "ALTERNATE8", nullable = true, length = 64)
	public String getAlternate8() {
		return alternate8;
	}

	// 客户单位类型
	public void setAlternate8(String alternate8) {
		this.alternate8 = alternate8;
	}

	// 客户单位名称
	@Basic
	@javax.persistence.Column(name = "ALTERNATE9", nullable = true, length = 64)
	public String getAlternate9() {
		return alternate9;
	}

	public void setAlternate9(String alternate9) {
		this.alternate9 = alternate9;
	}

	@Basic
	@javax.persistence.Column(name = "ALTERNATE10", nullable = true, length = 64)
	public String getAlternate10() {
		return alternate10;
	}

	public void setAlternate10(String alternate10) {
		this.alternate10 = alternate10;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		PrpLmAgent that = (PrpLmAgent) o;

		if (id != null ? !id.equals(that.id) : that.id != null)
			return false;
		if (branch != null ? !branch.equals(that.branch) : that.branch != null)
			return false;
		if (agreementNo != null ? !agreementNo.equals(that.agreementNo) : that.agreementNo != null)
			return false;
		if (product != null ? !product.equals(that.product) : that.product != null)
			return false;
		if (carInsuranceType != null ? !carInsuranceType.equals(that.carInsuranceType) : that.carInsuranceType != null)
			return false;
		if (accType != null ? !accType.equals(that.accType) : that.accType != null)
			return false;
		if (comm1Rate != null ? !comm1Rate.equals(that.comm1Rate) : that.comm1Rate != null)
			return false;
		if (agentCode != null ? !agentCode.equals(that.agentCode) : that.agentCode != null)
			return false;
		if (agentName != null ? !agentName.equals(that.agentName) : that.agentName != null)
			return false;
		if (handlerCode != null ? !handlerCode.equals(that.handlerCode) : that.handlerCode != null)
			return false;
		if (handlerName != null ? !handlerName.equals(that.handlerName) : that.handlerName != null)
			return false;
		if (channel != null ? !channel.equals(that.channel) : that.channel != null)
			return false;
		if (channelName != null ? !channelName.equals(that.channelName) : that.channelName != null)
			return false;
		if (isPolicommFlag != null ? !isPolicommFlag.equals(that.isPolicommFlag) : that.isPolicommFlag != null)
			return false;
		if (validStatus != null ? !validStatus.equals(that.validStatus) : that.validStatus != null)
			return false;
		if (startDate != null ? !startDate.equals(that.startDate) : that.startDate != null)
			return false;
		if (endDate != null ? !endDate.equals(that.endDate) : that.endDate != null)
			return false;
		if (remark != null ? !remark.equals(that.remark) : that.remark != null)
			return false;
		if (motorcade != null ? !motorcade.equals(that.motorcade) : that.motorcade != null)
			return false;
		if (dumpcar != null ? !dumpcar.equals(that.dumpcar) : that.dumpcar != null)
			return false;
		if (baseUpperLimit != null ? !baseUpperLimit.equals(that.baseUpperLimit) : that.baseUpperLimit != null)
			return false;
		if (carProperetyType != null ? !carProperetyType.equals(that.carProperetyType) : that.carProperetyType != null)
			return false;
		if (configSetValueName1 != null ? !configSetValueName1.equals(that.configSetValueName1) : that.configSetValueName1 != null)
			return false;
		if (configSetValueName3 != null ? !configSetValueName3.equals(that.configSetValueName3) : that.configSetValueName3 != null)
			return false;
		if (credentialsNo != null ? !credentialsNo.equals(that.credentialsNo) : that.credentialsNo != null)
			return false;
		if (permitNo != null ? !permitNo.equals(that.permitNo) : that.permitNo != null)
			return false;
		if (commissionUpperLimit != null ? !commissionUpperLimit.equals(that.commissionUpperLimit) : that.commissionUpperLimit != null)
			return false;
		if (alternate1 != null ? !alternate1.equals(that.alternate1) : that.alternate1 != null)
			return false;
		if (alternate2 != null ? !alternate2.equals(that.alternate2) : that.alternate2 != null)
			return false;
		if (alternate3 != null ? !alternate3.equals(that.alternate3) : that.alternate3 != null)
			return false;
		if (alternate4 != null ? !alternate4.equals(that.alternate4) : that.alternate4 != null)
			return false;
		if (alternate5 != null ? !alternate5.equals(that.alternate5) : that.alternate5 != null)
			return false;
		if (alternate6 != null ? !alternate6.equals(that.alternate6) : that.alternate6 != null)
			return false;
		if (alternate7 != null ? !alternate7.equals(that.alternate7) : that.alternate7 != null)
			return false;
		if (alternate8 != null ? !alternate8.equals(that.alternate8) : that.alternate8 != null)
			return false;
		if (alternate9 != null ? !alternate9.equals(that.alternate9) : that.alternate9 != null)
			return false;
		if (alternate10 != null ? !alternate10.equals(that.alternate10) : that.alternate10 != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = id != null ? id.hashCode() : 0;
		result = 31 * result + (branch != null ? branch.hashCode() : 0);
		result = 31 * result + (agreementNo != null ? agreementNo.hashCode() : 0);
		result = 31 * result + (product != null ? product.hashCode() : 0);
		result = 31 * result + (carInsuranceType != null ? carInsuranceType.hashCode() : 0);
		result = 31 * result + (accType != null ? accType.hashCode() : 0);
		result = 31 * result + (comm1Rate != null ? comm1Rate.hashCode() : 0);
		result = 31 * result + (agentCode != null ? agentCode.hashCode() : 0);
		result = 31 * result + (agentName != null ? agentName.hashCode() : 0);
		result = 31 * result + (handlerCode != null ? handlerCode.hashCode() : 0);
		result = 31 * result + (handlerName != null ? handlerName.hashCode() : 0);
		result = 31 * result + (channel != null ? channel.hashCode() : 0);
		result = 31 * result + (channelName != null ? channelName.hashCode() : 0);
		result = 31 * result + (isPolicommFlag != null ? isPolicommFlag.hashCode() : 0);
		result = 31 * result + (validStatus != null ? validStatus.hashCode() : 0);
		result = 31 * result + (startDate != null ? startDate.hashCode() : 0);
		result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
		result = 31 * result + (remark != null ? remark.hashCode() : 0);
		result = 31 * result + (motorcade != null ? motorcade.hashCode() : 0);
		result = 31 * result + (dumpcar != null ? dumpcar.hashCode() : 0);
		result = 31 * result + (baseUpperLimit != null ? baseUpperLimit.hashCode() : 0);
		result = 31 * result + (carProperetyType != null ? carProperetyType.hashCode() : 0);
		result = 31 * result + (configSetValueName1 != null ? configSetValueName1.hashCode() : 0);
		result = 31 * result + (configSetValueName3 != null ? configSetValueName3.hashCode() : 0);
		result = 31 * result + (credentialsNo != null ? credentialsNo.hashCode() : 0);
		result = 31 * result + (permitNo != null ? permitNo.hashCode() : 0);
		result = 31 * result + (commissionUpperLimit != null ? commissionUpperLimit.hashCode() : 0);
		result = 31 * result + (alternate1 != null ? alternate1.hashCode() : 0);
		result = 31 * result + (alternate2 != null ? alternate2.hashCode() : 0);
		result = 31 * result + (alternate3 != null ? alternate3.hashCode() : 0);
		result = 31 * result + (alternate4 != null ? alternate4.hashCode() : 0);
		result = 31 * result + (alternate5 != null ? alternate5.hashCode() : 0);
		result = 31 * result + (alternate6 != null ? alternate6.hashCode() : 0);
		result = 31 * result + (alternate7 != null ? alternate7.hashCode() : 0);
		result = 31 * result + (alternate8 != null ? alternate8.hashCode() : 0);
		result = 31 * result + (alternate9 != null ? alternate9.hashCode() : 0);
		result = 31 * result + (alternate10 != null ? alternate10.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "PrpLmAgent [id=" + id + ", branch=" + branch + ", agreementNo=" + agreementNo + ", product=" + product + ", carInsuranceType="
				+ carInsuranceType + ", accType=" + accType + ", comm1Rate=" + comm1Rate + ", agentCode=" + agentCode + ", agentName=" + agentName
				+ ", handlerCode=" + handlerCode + ", handlerName=" + handlerName + ", channel=" + channel + ", channelName=" + channelName
				+ ", isPolicommFlag=" + isPolicommFlag + ", validStatus=" + validStatus + ", startDate=" + startDate + ", endDate=" + endDate
				+ ", remark=" + remark + ", motorcade=" + motorcade + ", dumpcar=" + dumpcar + ", baseUpperLimit=" + baseUpperLimit
				+ ", carProperetyType=" + carProperetyType + ", configSetValueName1=" + configSetValueName1 + ", configSetValueName3="
				+ configSetValueName3 + ", credentialsNo=" + credentialsNo + ", permitNo=" + permitNo + ", commissionUpperLimit="
				+ commissionUpperLimit + ", alternate1=" + alternate1 + ", alternate2=" + alternate2 + ", alternate3=" + alternate3 + ", alternate4="
				+ alternate4 + ", alternate5=" + alternate5 + ", alternate6=" + alternate6 + ", alternate7=" + alternate7 + ", alternate8="
				+ alternate8 + ", alternate9=" + alternate9 + ", alternate10=" + alternate10 + "]";
	}

}
