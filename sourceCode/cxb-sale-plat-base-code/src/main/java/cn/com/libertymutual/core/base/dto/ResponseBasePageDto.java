package cn.com.libertymutual.core.base.dto;

public class ResponseBasePageDto extends ResponseBaseDto {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** 
	 * @ClassName: BasePageDto 
	 * @author pengmin
	 * @date 2017年8月22日 下午2:15:16 
	 * 
	 */
	
	private int pageNumber=1;// 当前页
	
	private int pageSize=50;// 每页显示多少条 
	 
	private int totalPages; // 总页数
	
	private int totalCount; // 总条数

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalCount) {
		this.totalCount = totalCount;
	}

	public int getPageNumber() {
		return pageNumber;
	}

	public void setPageNumber(int pageNumber) {
		this.pageNumber = pageNumber;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getTotalPages() {
		return totalPages;
	}

	public void setTotalPages(int totalPages) {
		this.totalPages = totalPages;
	}
}
