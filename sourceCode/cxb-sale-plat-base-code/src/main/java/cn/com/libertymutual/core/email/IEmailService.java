package cn.com.libertymutual.core.email;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;


public interface IEmailService {

	public boolean sendEmail( String subject, String content, List<String> toEmils,List<String> ccEmails,
    		List<String> bccEmails,List<String> paths,boolean html) throws MessagingException, UnsupportedEncodingException;
	public boolean sendEmail( String subject, String content, List<String> toEmils,List<String> ccEmails,
			List<String> bccEmails,Map<String,byte[] > attachments ,boolean html) throws MessagingException, UnsupportedEncodingException;

	public boolean sendEmail( String subject, String content, List<String> toEmils,List<String> ccEmails, List<String> paths ) throws MessagingException, UnsupportedEncodingException;
	public boolean sendEmail( String subject, String content, List<String> toEmils,List<String> paths ) throws MessagingException, UnsupportedEncodingException;
	public boolean sendEmail( String subject, String content, List<String> toEmils) throws MessagingException, UnsupportedEncodingException;
	public void setEmailEntity(EmailEntity emailEntity);
	public boolean sendEmail(String subject, String content, List<String> toEmils,
			List<String> ccEmails, List<String> bccEmails, File file,
			boolean html) throws MessagingException,
			UnsupportedEncodingException;
}
