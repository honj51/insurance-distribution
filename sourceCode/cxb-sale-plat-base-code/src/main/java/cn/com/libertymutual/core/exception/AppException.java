package cn.com.libertymutual.core.exception;



/**
 * 平台异常全部统一抛出此异常
 * @author bob.kuang
 *
 */

public class AppException extends CustomLangException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5659041175845853736L;

	private int errorCode = 9997;
	private String errorMessage;
	
	//添加枚举类型的属性  author:tracy.liao date:2016-4-27
	private ExceptionEnums exceptionEnums;
	
	public AppException() {
		super();
	}
	public AppException( Throwable e ) {
		super( e );
	}
	
	@Override
	public Throwable fillInStackTrace() {
		return this;
	}

	public AppException( String message ) {
		super( message );
		errorMessage = message;
	}
	public AppException( String message, Throwable e ) {
		super( message, e );
		errorMessage = message;
	}

	public AppException( int errorCode, String message ) {
		super(message);
		this.errorCode = errorCode;
		this.errorMessage = message;
	}
	public AppException( int errorCode, String message, Throwable e ) {
		super(message, e);
		this.errorCode = errorCode;
		this.errorMessage = message;
	}
	
	
	
	/**
	 * 添加枚举类型构造方法
	 * @author tracy.liao
	 * @date 2016-4-27
	 * @param exceptionEnums
	 * @param message
	 */
	public AppException(ExceptionEnums exceptionEnums, String message){
		super(message);
		this.exceptionEnums=exceptionEnums;
		this.errorCode=exceptionEnums.getCode();
		this.errorMessage=exceptionEnums.getMessage()+";"+ message;
	}
	/**
	 * 添加枚举类型构造方法
	 * @author tracy.liao
	 * @date 2016-4-27
	 * @param exceptionEnums
	 * @param message
	 */
	public AppException(ExceptionEnums exceptionEnums){
		super(exceptionEnums.getMessage());
		this.exceptionEnums=exceptionEnums;
		this.errorCode=exceptionEnums.getCode();
		this.errorMessage=exceptionEnums.getMessage();
	}

	/**
	 * @return the exceptionEnums
	 */
	@Override
	public ExceptionEnums getExceptionEnums() {
		return exceptionEnums;
	}

	/**
	 * @param exceptionEnums the exceptionEnums to set
	 */
	@Override
	public void setExceptionEnums(ExceptionEnums exceptionEnums) {
		this.exceptionEnums = exceptionEnums;
	}
	@Override
	public int getErrorCode() {
		return errorCode;
	}
	
	@Override
	public String getErrorMessage() {
		return errorMessage;
	}
}
