package cn.com.libertymutual.core.util;

import java.util.Date;
import java.util.List;

import cn.com.libertymutual.core.base.SysUserBase;
import cn.com.libertymutual.core.base.UserInfoBase;
import cn.com.libertymutual.core.common.mobile.util.ClientOsInfo;

/**
 * 
 * @author suxu
 *
 */
public class Current {
	public static ThreadLocal<String> uuid = new ThreadLocal<String>();

	/**
	 * 系统用户 对应tb_sys_user 表
	 */
	/**  Current userId */
	public static ThreadLocal<String> userId = new ThreadLocal<String>();
	/**  Current flowId */
	public static ThreadLocal<String> flowId = new ThreadLocal<String>();
	/**  Current IP */
	public static ThreadLocal<String> IP = new ThreadLocal<String>();
	/**  Current date */
	public static ThreadLocal<Date> date = new ThreadLocal<Date>();
	/**  Current time */
	public static ThreadLocal<Long> time = new ThreadLocal<Long>();

	public static ThreadLocal<String> userCode = new ThreadLocal<String>();
	public static ThreadLocal<String> userCodeBs = new ThreadLocal<String>();

	public static ThreadLocal<ClientOsInfo> clientOs = new ThreadLocal<ClientOsInfo>();
	public static ThreadLocal<UserInfoBase> userInfo = new ThreadLocal<UserInfoBase>();
	public static ThreadLocal<List<Integer>> roles = new ThreadLocal<List<Integer>>();
	public static ThreadLocal<SysUserBase> sysuser = new ThreadLocal<SysUserBase>();
	public static ThreadLocal<String> requestParameters = new ThreadLocal<String>();
	/**
	 */
	/**
	 * 第三方请求账户信息
	 */
	/**
	 * 第三方请求 业务关系代码配置信息
	 */
	/**
	 * 是否推送日志到服务器
	 */
	public static ThreadLocal<Boolean> pushLog = new ThreadLocal<Boolean>();

	// 返回内容
	public static ThreadLocal<StringBuilder> responseMsg = new ThreadLocal<StringBuilder>();

	/**
	 * 
	 */
	public static void remove() {

		uuid.remove();
		userId.remove();
		flowId.remove();
		IP.remove();
		userInfo.remove();
		date.remove();
		time.remove();
		roles.remove();
		clientOs.remove();
		responseMsg.remove();
		sysuser.remove();
		requestParameters.remove();
		userCode.remove();
		userCodeBs.remove();
	}
}
