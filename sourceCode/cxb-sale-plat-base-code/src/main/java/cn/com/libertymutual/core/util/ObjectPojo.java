package cn.com.libertymutual.core.util;

public class ObjectPojo {
	//用于区别菜单
	public static final String SUBMIT_TYPE_POLICY = "policy";//保单查询
	public static final String SUBMIT_TYPE_CLAIMS = "claims";//理赔查询
	public static final String SUBMIT_TYPE_CLIENT = "client";//尊客会查询
	public static final String SUBMIT_TYPE_REPORT = "report";//马上报案
	public static final String SUBMIT_TYPE_UPLOAD = "upload";//资料上传
	public static final String SUBMIT_TYPE_LIBERTY = "henghua";//微门店
	public static final String SUBMIT_TYPE_IDENTITY = "identity";//身份验证
	//身份验证操作区分
	public static final String SUBMIT_TYPE_VALIDATION = "validation";//证件验证
	public static final String SUBMIT_TYPE_PHONECODE = "phoneCode";//手机验证码
	//用于区别险种
	public static final String LIBERTY_TYPE_AUTOMOBILE = "A";//利宝保险类型--车险保单
	public static final String LIBERTY_TYPE_FAMILY = "F";//利宝保险类型--家庭财产险保单
	public static final String LIBERTY_TYPE_PERSONAL_ACCIDENT = "PA";//利宝保险类型--个人意外险保单
	//保单
	public static final String POLICY_REQUEST_AUTOMOBILE = "policyController/doQueryAutomobile.action";//车险请求
	public static final String POLICY_REQUEST_FAMILY = "policyController/doQueryFamily.action";//家庭财产险
	public static final String POLICY_REQUEST_PERSONAL_ACCIDENT = "policyController/doQueryPersonalAccident.action";//个人意外险
	//理赔
	public static final String CLAIMS_REQUEST_AUTOMOBILE = "claimsController/doQueryAutomobile.action";//车险请求
	public static final String CLAIMS_REQUEST_FAMILY = "claimsController/doQueryFamily.action";//家庭财产险
	public static final String CLAIMS_REQUEST_PERSONAL_ACCIDENT = "claimsController/doQueryPersonalAccident.action";//个人意外险
	//理赔状态（1：新建 ，2 ：打开， 3：重开 ，0：关闭）
	public static final String CLAIMS_STATUS_CREATE = "1";
	public static final String CLAIMS_STATUS_OPEN = "2";
	public static final String CLAIMS_STATUS_RESET = "3";
	public static final String CLAIMS_STATUS_CLOSE = "0";
	//尊客会等级
	public static final String CLIENT_LEVEL_DIAMONDS_TWO = "2";//2是铂金卡
	public static final String CLIENT_LEVEL_GOLD_ONE = "1";//1是金卡
	public static final String CLIENT_LEVEL_GOLD = "gold";//金卡会员
	public static final String CLIENT_LEVEL_DIAMONDS = "diamonds";//钻石卡会员
	public static final String CLIENT_LEVEL_URL_GOLD = "clientController/doClientGold.action";//金卡会员URL
	public static final String CLIENT_LEVEL_URL_DIAMONDS = "clientController/doClientDiamonds.action";//钻石卡会员URL
	//理赔类型是否选择
	public static final String CLIAIMS_TYPE_OFF = "1";//是
	public static final String CLIAIMS_TYPE_NO = "0";//否
	//在期与不在期保单状态
	public static final String POLICY_YES = "1";//在期
	public static final String POLICY_NO = "0";//不在期
	//案件状态
	public static final String CLAIMS_STATUS_YES = "1";//已支付
	public static final String CLAIMS_STATUS_NO = "0";//未支付
	//车险保单的续保按钮是否显示
	public static final String POLICY_BUTTON_YES = "1";//是
	public static final String POLICY_BUTTON_NO = "0";//否
	//请求返回状态
	public static final String REQUEST_STATUS_TYPE_SUCCESS = "success";
	public static final String REQUEST_STATUS_TYPE_ERROR = "error";
	//用户逻辑删除状态
	public static final String LIBERTY_DELETE_STATUS_YES = "1";//已删除
	public static final String LIBERTY_DELETE_STATUS_NO = "0";//未删除
	//用户验证状态
	public static final String LIBERTY_VALIDATION_STATUS_YES = "1";//已验证
	public static final String LIBERTY_VALIDATION_STATUS_NO = "0";//未验证
	//响应报文code
	public static final String LIBERTY_RESULT_STATUS_ONE = "1";//成功
	public static final String LIBERTY_RESULT_STATUS_TWO = "-1";//失败
	public static final String LIBERTY_RESULT_STATUS_THREE = "500";//服务器错误
	public static final String LIBERTY_RESULT_STATUS_FOUR = "404";//找不到资源
	//非车险险别编号
	public static final String POLICY_NONCAR_FAMILY = "0301";//家庭财产险
	public static final String POLICY_NONCAR_PERSON = "2725";//个人意外险
	//保单车险险别编号
	public static final String POLICY_CAR_COMMERCIAL = "0501";//商业险
	public static final String POLICY_CAR_COMPULSORY = "0508";//交强险
	
	//=================================微信相关配置==================================================================
	public static final String LIBERTY_WEIXIN_APPID = "APPID";
	public static final String LIBERTY_WEIXIN_APPSECRET = "SECRET";
	public static final String LIBERTY_WEIXIN_CODE = "CODE";
	public static final String LIBERTY_WEIXIN_ACCESS_TOKEN = "ACCESS_TOKEN";
	public static final String LIBERTY_WEIXIN_UPPERCASE_OPENID = "OPENID";
	public static final String LIBERTY_WEIXIN_LOWERCASE_OPENID = "openid";
	
	//=================================保单查询--查询所有保单==================================================================
	public static final String POLICY_ALL_CODE = "root-code";//代码
	public static final String POLICY_ALL_MESSAGE = "root-message";//提示信息
	//车险
	public static final String POLICY_ALL_CAR = "root-car_insurances";//车险
	public static final String POLICY_ALL_CAR_A = "root-car_insurances-car_insurance-plates";//车牌号
	public static final String POLICY_ALL_CAR_B = "root-car_insurances-car_insurance-commercial_insurance_num";//商业险保单号
	public static final String POLICY_ALL_CAR_C = "root-car_insurances-car_insurance-commercial_user";//被保人
	public static final String POLICY_ALL_CAR_D = "root-car_insurances-car_insurance-com_begin_date";//保单开始时间
	public static final String POLICY_ALL_CAR_E = "root-car_insurances-car_insurance-com_end_date";//保单结束时间
	public static final String POLICY_ALL_CAR_F = "root-car_insurances-car_insurance-compulsory_insurance_num";//交强险保单号
	public static final String POLICY_ALL_CAR_G = "root-car_insurances-car_insurance-compulsory_user";//被保险人
	public static final String POLICY_ALL_CAR_H = "root-car_insurances-car_insurance-cps_begin_date";//交强险保单开始时间
	public static final String POLICY_ALL_CAR_I = "root-car_insurances-car_insurance-cps_end_date";//交强险保单结束时间
	//家财
	public static final String POLICY_ALL_FAMILY = "root-family_insurances";//家财
	public static final String POLICY_ALL_FAMILY_A = "root-family_insurances-family_insurance-insurance_num";//保单号
	public static final String POLICY_ALL_FAMILY_B = "root-family_insurances-family_insurance-user";//被保人
	public static final String POLICY_ALL_FAMILY_C = "root-family_insurances-family_insurance-begin_date";//保单开始时间
	public static final String POLICY_ALL_FAMILY_D = "root-family_insurances-family_insurance-end_date";//保单结束时间
	//个人
	public static final String POLICY_ALL_PERSON = "root-person_insurances";//个人
	public static final String POLICY_ALL_PERSON_A = "root-person_insurances-person_insurance-insurance_num";//保单号
	public static final String POLICY_ALL_PERSON_B = "root-person_insurances-person_insurance-user";//被保人
	public static final String POLICY_ALL_PERSON_C = "root-person_insurances-person_insurance-begin_date";//保单开始时间
	public static final String POLICY_ALL_PERSON_D = "root-person_insurances-person_insurance-end_date";//保单结束时间
	
	//=================================保单查询--查询车险保单==================================================================
	public static final String POLICY_CAR_CODE = "root-code";//代码
	public static final String POLICY_CAR_MESSAGE = "root-message";//消息
	//商业险
	public static final String POLICY_CAR_COMMERCIAL_A = "root-commercial_insurance_detail-com_num";//保单号
	public static final String POLICY_CAR_COMMERCIAL_B = "root-commercial_insurance_detail-com_plates";//车牌号
	public static final String POLICY_CAR_COMMERCIAL_C = "root-commercial_insurance_detail-com_insured";//被保人
	public static final String POLICY_CAR_COMMERCIAL_D = "root-commercial_insurance_detail-com_policyholder";//投保人
	public static final String POLICY_CAR_COMMERCIAL_E = "root-commercial_insurance_detail-com_begin_date";//保单开始时间
	public static final String POLICY_CAR_COMMERCIAL_F = "root-commercial_insurance_detail-com_end_date";//保单结束时间
	public static final String POLICY_CAR_COMMERCIAL_G = "root-commercial_insurance_detail-engine_num";//发动机号
	public static final String POLICY_CAR_COMMERCIAL_H = "root-commercial_insurance_detail-frame_num";//车架号
	public static final String POLICY_CAR_COMMERCIAL_I = "root-commercial_insurance_detail-ratified";//核定载质量
	public static final String POLICY_CAR_COMMERCIAL_J = "root-commercial_insurance_detail-model";//厂牌车型
	public static final String POLICY_CAR_COMMERCIAL_K = "root-commercial_insurance_detail-car_type";//车辆种类
	public static final String POLICY_CAR_COMMERCIAL_L = "root-commercial_insurance_detail-seats";//座位数
	public static final String POLICY_CAR_COMMERCIAL_M = "root-commercial_insurance_detail-tonnage";//吨位
	public static final String POLICY_CAR_COMMERCIAL_N = "root-commercial_insurance_detail-premium";//保费
	public static final String POLICY_CAR_COMMERCIAL_O = "root-commercial_insurance_detail-underwriting_details";//承保详情
	public static final String POLICY_CAR_COMMERCIAL_P = "root-commercial_insurance_detail-underwriting_details-underwriting_detail-type";//承保险别
	public static final String POLICY_CAR_COMMERCIAL_Q = "root-commercial_insurance_detail-underwriting_details-underwriting_detail-insured_amount";//保险金额
	public static final String POLICY_CAR_COMMERCIAL_R = "root-commercial_insurance_detail-underwriting_details-underwriting_detail-premium_amount";//保费金额
	//交强险
	public static final String POLICY_CAR_COMPULSORY_A = "root-compulsory_insurance_detail-com_num";//保单号
	public static final String POLICY_CAR_COMPULSORY_B = "root-compulsory_insurance_detail-cps_plates";//车牌号
	public static final String POLICY_CAR_COMPULSORY_C = "root-compulsory_insurance_detail-cps_insured";//被保人
	public static final String POLICY_CAR_COMPULSORY_D = "root-compulsory_insurance_detail-cps_policyholder";//投保人
	public static final String POLICY_CAR_COMPULSORY_E = "root-compulsory_insurance_detail-cps_begin_date";//保单开始时间
	public static final String POLICY_CAR_COMPULSORY_F = "root-compulsory_insurance_detail-cps_end_date";//保单结束时间
	public static final String POLICY_CAR_COMPULSORY_G = "root-compulsory_insurance_detail-engine_num";//发动机号
	public static final String POLICY_CAR_COMPULSORY_H = "root-compulsory_insurance_detail-frame_num";//车架号
	public static final String POLICY_CAR_COMPULSORY_I = "root-compulsory_insurance_detail-ratified";//核定载质量
	public static final String POLICY_CAR_COMPULSORY_J = "root-compulsory_insurance_detail-model";//厂牌车型
	public static final String POLICY_CAR_COMPULSORY_K = "root-compulsory_insurance_detail-car_type";//车辆种类
	public static final String POLICY_CAR_COMPULSORY_L = "root-compulsory_insurance_detail-seats";//座位数
	public static final String POLICY_CAR_COMPULSORY_M = "root-compulsory_insurance_detail-tonnage";//吨位
	public static final String POLICY_CAR_COMPULSORY_N = "root-compulsory_insurance_detail-underwriting_details";//承保详情
	public static final String POLICY_CAR_COMPULSORY_O = "root-compulsory_insurance_detail-underwriting_details-underwriting_detail-type";//承保险别
	public static final String POLICY_CAR_COMPULSORY_P = "root-compulsory_insurance_detail-underwriting_details-underwriting_detail-insured_amount";//保险金额
	public static final String POLICY_CAR_COMPULSORY_Q = "root-compulsory_insurance_detail-underwriting_details-underwriting_detail-premium_amount";//保费金额
	public static final String POLICY_CAR_COMPULSORY_R = "root-compulsory_insurance_detail-underwriting_details-underwriting_detail-vessel_tax_amount";//车船税金额
	
	//=================================保单查询--查询家财险保单==================================================================
	public static final String POLICY_FAMILY_CODE = "root-code";//代码
	public static final String POLICY_FAMILY_MESSAGE = "root-message";//提示信息
	public static final String POLICY_FAMILY_NUM = "root-family_insurance_detail-num";//保单号
	public static final String POLICY_FAMILY_INSURED = "root-family_insurance_detail-insured";//被保人
	public static final String POLICY_FAMILY_POLICYHOLDER = "root-family_insurance_detail-policyholder";//投保人
	public static final String POLICY_FAMILY_BEGINDATE = "root-family_insurance_detail-begin_date";//保单开始时间
	public static final String POLICY_FAMILY_ENDDATE = "root-family_insurance_detail-end_date";//保单结束时间
	public static final String POLICY_FAMILY_PREMIUMAMOUNT = "root-family_insurance_detail-premium_amount";//保费金额
	public static final String POLICY_FAMILY_DETAIL = "root-family_insurance_detail-underwriting_details";//详情
	public static final String POLICY_FAMILY_DETAIL_NAME = "root-family_insurance_detail-underwriting_details-underwriting_detail-name";//责任名称
	public static final String POLICY_FAMILY_DETAIL_AMOUNT = "root-family_insurance_detail-underwriting_details-underwriting_detail-amount";//保险金额
	
	//=================================保单查询--查询个人意外保单==================================================================
	public static final String POLICY_PERSON_CODE = "root-code";//代码
	public static final String POLICY_PERSON_MESSAGE = "root-message";//提示信息
	public static final String POLICY_PERSON_COMNUM = "root-person_insurance_detail-com_num";//保单号
	public static final String POLICY_PERSON_COMINSURED = "root-person_insurance_detail-com_insured";//被保人
	public static final String POLICY_PERSON_COMPOLICYHOLDER = "root-person_insurance_detail-com_policyholder";//投保人
	public static final String POLICY_PERSON_COMBEGINDATE = "root-person_insurance_detail-com_begin_date";//保单开始时间
	public static final String POLICY_PERSON_COMENDDATE = "root-person_insurance_detail-com_end_date";//保单结束时间
	public static final String POLICY_PERSON_PREMIUMAMOUNT = "root-person_insurance_detail-premium_amount";//保费金额
	public static final String POLICY_PERSON_DETAIL = "root-person_insurance_detail-underwriting_details";//详情
	public static final String POLICY_PERSON_DETAIL_NAME = "root-person_insurance_detail-underwriting_details-underwriting_detail-name";//责任名称
	public static final String POLICY_PERSON_DETAIL_AMOUNT = "root-person_insurance_detail-underwriting_details-underwriting_detail-amount";//保险金额
	
	//=================================理赔查询--查询所有理赔==================================================================
	public static final String CLAIMS_ALL_CODE = "root-code";//代码
	public static final String CLAIMS_ALL_MESSAGE = "root-message";//提示信息
	//车险
	public static final String CLAIMS_ALL_CARS = "root-cars";//理赔车险详情
	public static final String CLAIMS_ALL_CARS_PLATES = "root-cars-car-plates";//车牌号
	public static final String CLAIMS_ALL_CARS_REPORTNO = "root-cars-car-report_no";//报案号
	public static final String CLAIMS_ALL_CARS_REPORTTIME = "root-cars-car-report_time";//报案时间
	public static final String CLAIMS_ALL_CARS_STATUS = "root-cars-car-status";//案件状态
	public static final String CLAIMS_ALL_CARS_EMAIL = "root-cars-car-email";//邮件地址
	//家财险
	public static final String CLAIMS_ALL_FAMILYS = "root-familys";//理赔家财险详情
	public static final String CLAIMS_ALL_FAMILYS_REPORTNO = "root-familys-family-report_no";//报案号
	public static final String CLAIMS_ALL_FAMILYS_REPORTTIME = "root-familys-family-report_time";//报案时间
	public static final String CLAIMS_ALL_FAMILYS_STATUS = "root-familys-family-status";//案件状态
	public static final String CLAIMS_ALL_FAMILYS_EMAIL = "root-familys-family-email";//邮件地址
	//个人意外险
	public static final String CLAIMS_ALL_PERSONS = "root-persons";//理赔家财险详情
	public static final String CLAIMS_ALL_PERSONS_REPORTNO = "root-persons-person-report_no";//报案号
	public static final String CLAIMS_ALL_PERSONS_REPORTTIME = "root-persons-person-report_time";//报案时间
	public static final String CLAIMS_ALL_PERSONS_STATUS = "root-persons-person-status";//案件状态
	public static final String CLAIMS_ALL_PERSONS_EMAIL = "root-persons-person-email";//邮件地址
	
	//=================================理赔查询--车险理赔==================================================================
	public static final String CLAIMS_CAR_CODE = "root-code";//返回码
	public static final String CLAIMS_CAR_MESSAGE = "root-message";//提示信息
	public static final String CLAIMS_CAR_REPORTNO = "root-report_no";//报案号
	public static final String CLAIMS_CAR_REPORTUSER = "root-report_user";//报案人
	public static final String CLAIMS_CAR_REPORTDATE = "root-report_date";//报案时间
	public static final String CLAIMS_CAR_STATUS = "root-status";//案件状态
	public static final String CLAIMS_CAR_LOSSCASE_NAME = "root-losscase_name";//损失原因
	public static final String CLAIMS_CAR_LOSSCASE_TIME = "root-accident_time";//损失时间
	public static final String CLAIMS_CAR_LOSSCASE_PLACE = "root-accident_place";//损失地点
	public static final String CLAIMS_CAR_LOSSCASE_DESC = "root-accident_desc";//报案环节备注
	//标的车
	public static final String CLAIMS_CAR_ONE_CARNO = "root-car_one-car_no";//车牌号
	public static final String CLAIMS_CAR_ONE_ALLAMOUNT = "root-car_one-all_amount";//总费用
	public static final String CLAIMS_CAR_ONE_PAYDATE = "root-car_one-pay_date";//赔付时间
	public static final String CLAIMS_CAR_ONE_PAYAMOUNT = "root-car_one-pay_amount";//赔付金额
	public static final String CLAIMS_CAR_ONE_DETAILS = "root-car_one-details";//损失详情
	public static final String CLAIMS_CAR_ONE_DETAILS_NAME = "root-car_one-details-detail-name";//部件名称
	public static final String CLAIMS_CAR_ONE_DETAILS_AMOUNT = "root-car_one-details-detail-amount";//费用
	//三者车
	public static final String CLAIMS_CAR_THREE_CARNO = "root-car_three-car_no";//车牌号
	public static final String CLAIMS_CAR_THREE_ALLAMOUNT = "root-car_three-all_amount";//总费用
	public static final String CLAIMS_CAR_THREE_PAYDATE = "root-car_three-pay_date";//赔付时间
	public static final String CLAIMS_CAR_THREE_PAYAMOUNT = "root-car_three-pay_amount";//赔付金额
	public static final String CLAIMS_CAR_THREE_DETAILS = "root-car_three-details";//损失详情
	public static final String CLAIMS_CAR_THREE_DETAILS_NAME = "root-car_three-details-detail-name";//部件名称
	public static final String CLAIMS_CAR_THREE_DETAILS_AMOUNT = "root-car_three-details-detail-amount";//费用
	//财产
	public static final String CLAIMS_CAR_PROPERTY_AMOUNT = "root-property-amount";//总赔付金额
	public static final String CLAIMS_CAR_PROPERTY_DETAILS = "root-property-details";//赔付详情
	public static final String CLAIMS_CAR_PROPERTY_DETAILS_NAME = "root-property-details-detail-name";//费用名称
	public static final String CLAIMS_CAR_PROPERTY_DETAILS_AMOUNT = "root-property-details-detail-amount";//费用
	//人身
	public static final String CLAIMS_CAR_PERSON_AMOUNT = "root-person-amount";//赔付总金额
	public static final String CLAIMS_CAR_PERSON_DETAILS = "root-person-details";//赔付详情
	public static final String CLAIMS_CAR_PERSON_DETAILS_NAME = "root-person-details-detail-name";//费用名称
	public static final String CLAIMS_CAR_PERSON_DETAILS_AMOUNT = "root-person-details-detail-amount";//费用
	
	//=================================理赔查询--家财险理赔==================================================================
	public static final String CLAIMS_FAMILY_CODE = "root-code";//返回码
	public static final String CLAIMS_FAMILY_MESSAGE = "root-message";//提示信息
	public static final String CLAIMS_FAMILY_REPORTNO = "root-report_no";//报案号
	public static final String CLAIMS_FAMILY_REPORTUSER = "root-report_user";//报案人
	public static final String CLAIMS_FAMILY_REPORTDATE = "root-report_date";//报案时间
	public static final String CLAIMS_FAMILY_STATUS = "root-status";//案件状态
	public static final String CLAIMS_FAMILY_ALLAMOUNT = "root-all_amount";//总费用
	public static final String CLAIMS_FAMILY_PAYDATE = "root-pay_date";//赔付时间
	public static final String CLAIMS_FAMILY_PAYAMOUNT = "root-pay_amount";//赔付金额
	public static final String CLAIMS_FAMILY_DETAILS = "root-details";//家财险理赔详情
	public static final String CLAIMS_FAMILY_LOSSCASE_NAME = "root-losscase_name";//损失原因
	public static final String CLAIMS_FAMILY_LOSSCASE_TIME = "root-accident_time";//损失时间
	public static final String CLAIMS_FAMILY_LOSSCASE_PLACE = "root-accident_place";//损失地点
	public static final String CLAIMS_FAMILY_LOSSCASE_DESC = "root-accident_desc";//报案环节备注
	public static final String CLAIMS_FAMILY_DETAILS_NAME = "root-details-detail-name";//损失项目
	public static final String CLAIMS_FAMILY_DETAILS_AMOUNT = "root-details-detail-amount";//费用
	
	//=================================理赔查询--个人意外险理赔==================================================================
	public static final String CLAIMS_PERSONS_CODE = "root-code";//返回码
	public static final String CLAIMS_PERSONS_MESSAGE = "root-message";//提示信息
	public static final String CLAIMS_PERSONS_REPORTNO = "root-report_no";//报案号
	public static final String CLAIMS_PERSONS_REPORTUSER = "root-report_user";//报案人
	public static final String CLAIMS_PERSONS_REPORTDATE = "root-report_date";//报案时间
	public static final String CLAIMS_PERSONS_STATUS = "root-status";//案件状态
	public static final String CLAIMS_PERSONS_ALLAMOUNT = "root-all_amount";//总费用
	public static final String CLAIMS_PERSONS_PAYDATE = "root-pay_date";//赔付时间
	public static final String CLAIMS_PERSONS_PAYAMOUNT = "root-pay_amount";//赔付金额
	public static final String CLAIMS_PERSONS_DETAILS = "root-details";//家财险理赔详情
	public static final String CLAIMS_PERSONS_LOSSCASE_NAME = "root-losscase_name";//损失原因
	public static final String CLAIMS_PERSONS_LOSSCASE_TIME = "root-accident_time";//损失时间
	public static final String CLAIMS_PERSONS_LOSSCASE_PLACE = "root-accident_place";//损失地点
	public static final String CLAIMS_PERSONS_LOSSCASE_DESC = "root-accident_desc";//报案环节备注
	public static final String CLAIMS_PERSONS_DETAILS_NAME = "root-details-detail-name";//损失项目
	public static final String CLAIMS_PERSONS_DETAILS_AMOUNT = "root-details-detail-amount";//费用
	
	//=================================尊客会--保单列表==================================================================
	public static final String CLIENT_CODE = "root-code";//代码
	public static final String CLIENT_MESSAGE = "root-message";//提示信息
	public static final String CLIENT_OWNERS = "root-owners";//尊可会保单详情
	public static final String CLIENT_OWNERS_NAME = "root-owners-owner-name";//被保人姓名
	public static final String CLIENT_OWNERS_CARNO = "root-owners-owner-car_no";//车牌号
	public static final String CLIENT_OWNERS_INSURANCENUM = "root-owners-owner-insurance_num";//保单号
	public static final String CLIENT_OWNERS_BEGINDATE = "root-owners-owner-begin_date";//保单开始时间
	public static final String CLIENT_OWNERS_ENDDATE = "root-owners-owner-end_date";//保单结束时间
	public static final String CLIENT_OWNERS_LEVEL = "root-owners-owner-level";//尊客会等级
	
	//=================================用户验证--身份信息==================================================================
	public static final String LIBERTY_USER_CODE = "root-code";//代码
	public static final String LIBERTY_USER_MESSAGE = "root-message";//消息
	public static final String LIBERTY_USER_NAME = "root-name";//用户名
	
	//=================================短信验证--验证码==================================================================
	public static final String LIBERTY_PHONE_VERIFY_TYPE = "PACKET-HEAD-REQUEST_TYPE";//消息类型
	public static final String LIBERTY_PHONE_VERIFY_CODE = "PACKET-HEAD-RESPONSE_CODE";//状态编码
	public static final String LIBERTY_PHONE_VERIFY_MESSAGE = "PACKET-HEAD-ERROR_MESSAGE";//消息内容
}