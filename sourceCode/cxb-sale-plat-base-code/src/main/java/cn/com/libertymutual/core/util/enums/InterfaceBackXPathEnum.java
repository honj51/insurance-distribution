package cn.com.libertymutual.core.util.enums;

public enum InterfaceBackXPathEnum {
	/**
	 * 慎用
	 */
	默认响应码("//*[local-name()='success']"),
	/**
	 * 慎用
	 */
	默认响应信息("//*[local-name()='message']"),
	
	用户登录响应码("/*[local-name()='userLoginInfoQueryResponse']/*[local-name()='userLoginInfoDto']/success"),
	用户登录响应信息("/*[local-name()='userLoginInfoQueryResponse']/*[local-name()='userLoginInfoDto']/message"),
	
	角色信息响应码("/*[local-name()='roleInfoQueryResponse']/*[local-name()='roleInfoQueryResult']/success"),
	角色信息响应信息("/*[local-name()='roleInfoQueryResponse']/*[local-name()='roleInfoQueryResult']/message"),

	打印状态查询响应码("/*[local-name()='printStautsQueryResponse']/*[local-name()='MotorPrintStautsQueryResponseDto']/success"),
	打印状态查询响应信息("/*[local-name()='printStautsQueryResponse']/*[local-name()='MotorPrintStautsQueryResponseDto']/message"),
	
	支付状态查询响应码("/*[local-name()='statusUpdateResponse']/*[local-name()='PaymentStatusUpdateResponseDto']/success"),
	支付状态查询响应信息("/*[local-name()='statusUpdateResponse']/*[local-name()='PaymentStatusUpdateResponseDto']/message"),

	支付方式修改响应码("/*[local-name()='methodUpdateResponse']/*[local-name()='ResponseBaseDto']/success"),
	支付方式修改响应信息("/*[local-name()='methodUpdateResponse']/*[local-name()='ResponseBaseDto']/message"),

	支付信息查询响应码("/*[local-name()='queryResponse']/*[local-name()='PaymentInformationQueryResponseDto']/success"),
	支付信息查询响应信息("/*[local-name()='queryResponse']/*[local-name()='PaymentInformationQueryResponseDto']/message"),

	客户信息查询ID响应码("/*[local-name()='customerInfoQueryByCodeResponse']/*[local-name()='customerInfoQueryResponseDto']/success"),
	客户信息查询ID响应信息("/*[local-name()='customerInfoQueryByCodeResponse']/*[local-name()='customerInfoQueryResponseDto']/message"),

	客户信息查询响应码("/*[local-name()='customerInfoQueryResponse']/*[local-name()='roleInfoQueryResult']/success"),
	客户信息查询响应信息("/*[local-name()='customerInfoQueryResponse']/*[local-name()='roleInfoQueryResult']/message"),

	客户信息修改响应码("/*[local-name()='customerInfoUpdateResponse']/*[local-name()='roleInfoQueryResult']/success"),
	客户信息修改响应信息("/*[local-name()='customerInfoUpdateResponse']/*[local-name()='roleInfoQueryResult']/message"),

	撤回保单响应码("/*[local-name()='proposalRecallResponse']/*[local-name()='ResponseBaseDto']/success"),
	撤回保单响应信息("/*[local-name()='proposalRecallResponse']/*[local-name()='ResponseBaseDto']/message"),

	续保查询响应码("/*[local-name()='renewalQueryResponse']/*[local-name()='RenewalQueryResponseDto']/success"),
	续保查询响应信息("/*[local-name()='renewalQueryResponse']/*[local-name()='RenewalQueryResponseDto']/message"),


	保费计算响应码("/*[local-name()='premiumCalResponse']/*[local-name()='PremiumCalculationResponseDTO']/success"),
	保费计算响应信息("/*[local-name()='premiumCalResponse']/*[local-name()='PremiumCalculationResponseDTO']/message"),

	商业险转保响应码("/*[local-name()='motorProposalVerifyResponse']/*[local-name()='MotorProposalVerifyResponseDto']/success"),
	商业险转保响应信息("/*[local-name()='motorProposalVerifyResponse']/*[local-name()='MotorProposalVerifyResponseDto']/message"),

	交强险转保响应码("/*[local-name()='mtplProposalVerifyResponse']/*[local-name()='MtplProposalVerifyResponseDto']/success"),
	交强险转保响应信息("/*[local-name()='mtplProposalVerifyResponse']/*[local-name()='MtplProposalVerifyResponseDto']/message"),

	
	平台交互响应码("/*[local-name()='interactResponse']/*[local-name()='proposalQueryResponseDto']/success"),
	平台交互响应信息("/*[local-name()='interactResponse']/*[local-name()='proposalQueryResponseDto']/message"),

	投保响应码("/*[local-name()='saveResponse']/*[local-name()='proposalSaveResponseDTO']/success"),
	投保响应信息("/*[local-name()='saveResponse']/*[local-name()='proposalSaveResponseDTO']/message"),

	签单查询响应码("/*[local-name()='productionQueryResponse']/*[local-name()='ProductionQueryResponseDto']/success"),
	签单查询响应信息("/*[local-name()='productionQueryResponse']/*[local-name()='ProductionQueryResponseDto']/message"),
	

	VIN查询响应码("/*[local-name()='vinnoQueryResponse']/*[local-name()='MotorVinnoQueryResponseDto']/success"),
	VIN查询响应信息("/*[local-name()='vinnoQueryResponse']/*[local-name()='MotorVinnoQueryResponseDto']/message"),
	车型查询响应码("/*[local-name()='modelcodeQueryResponse']/*[local-name()='MotorVinnoQueryResponseDto']/success"),
	车型查询响应信息("/*[local-name()='modelcodeQueryResponse']/*[local-name()='MotorVinnoQueryResponseDto']/message"),

	保单查询响应码("/*[local-name()='prpallVisaInfoQueryResponse']/*[local-name()='prpallVisaInfoResult']/success"),
	保单查询响应信息("/*[local-name()='prpallVisaInfoQueryResponse']/*[local-name()='prpallVisaInfoResult']/message"),

	保单详情查询响应码("/*[local-name()='motorPolicyDetailInqueryResponse']/*[local-name()='MotorPolicyDetailInquiryResponseDTO']/success"),
	保单详情查询响应信息("/*[local-name()='motorPolicyDetailInqueryResponse']/*[local-name()='MotorPolicyDetailInquiryResponseDTO']/message"),
	
	保单打印响应码("/*[local-name()='printResponse']/*[local-name()='MotorPrintResponseDto']/success"),
	保单打印响应信息("/*[local-name()='printResponse']/*[local-name()='MotorPrintResponseDto']/message");

	
	
	
	private String xpath;
	private InterfaceBackXPathEnum( String xpath ) {
		this.xpath = xpath;
	}
	
	public String getXpath() {
		return xpath;
	}
}
