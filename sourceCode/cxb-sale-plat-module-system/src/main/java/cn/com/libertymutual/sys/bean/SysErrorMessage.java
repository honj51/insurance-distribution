package cn.com.libertymutual.sys.bean;

import javax.persistence.*;

import java.io.Serializable;
import java.sql.Date;

/**
 * Created by Ryan on 2016-09-09.
 */
@Entity
@Table(name = "tb_sys_error_message",  catalog = "")
public class SysErrorMessage  implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 1892121967283458364L;
	private String errorCode;
    private String errorShowMessage;
    private String erroeDetail;
    private Date updateTime;
    private String updateUser;

    @Id
    @Column(name = "ERROR_CODE", nullable = false, length = 10)
    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    @Basic
    @Column(name = "ERROR_SHOW_MESSAGE", nullable = false, length = 100)
    public String getErrorShowMessage() {
        return errorShowMessage;
    }

    public void setErrorShowMessage(String errorShowMessage) {
        this.errorShowMessage = errorShowMessage;
    }

    @Basic
    @Column(name = "ERROE_DETAIL", nullable = true, length = 200)
    public String getErroeDetail() {
        return erroeDetail;
    }

    public void setErroeDetail(String erroeDetail) {
        this.erroeDetail = erroeDetail;
    }

    @Basic
    @Column(name = "UPDATE_TIME", nullable = true)
    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Basic
    @Column(name = "UPDATE_USER", nullable = true, length = 20)
    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysErrorMessage that = (SysErrorMessage) o;

        if (errorCode != null ? !errorCode.equals(that.errorCode) : that.errorCode != null) return false;
        if (errorShowMessage != null ? !errorShowMessage.equals(that.errorShowMessage) : that.errorShowMessage != null)
            return false;
        if (erroeDetail != null ? !erroeDetail.equals(that.erroeDetail) : that.erroeDetail != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;
        if (updateUser != null ? !updateUser.equals(that.updateUser) : that.updateUser != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = errorCode != null ? errorCode.hashCode() : 0;
        result = 31 * result + (errorShowMessage != null ? errorShowMessage.hashCode() : 0);
        result = 31 * result + (erroeDetail != null ? erroeDetail.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        result = 31 * result + (updateUser != null ? updateUser.hashCode() : 0);
        return result;
    }

	@Override
	public String toString() {
		return "SysErrorMessage [errorCode=" + errorCode
				+ ", errorShowMessage=" + errorShowMessage + ", erroeDetail="
				+ erroeDetail + ", updateTime=" + updateTime + ", updateUser="
				+ updateUser + "]";
	}
}
