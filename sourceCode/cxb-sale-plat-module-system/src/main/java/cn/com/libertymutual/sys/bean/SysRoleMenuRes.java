package cn.com.libertymutual.sys.bean;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Created by Ryan on 2016-09-09.
 */
@Entity
@Table(name = "tb_sys_role_menu_res",  catalog = "")
@IdClass(SysRoleMenuResPK.class)
public class SysRoleMenuRes  implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 4008003744710332216L;
	private String roleid;
    private int menuid;
    private String resid;

    @Id
    @Column(name = "ROLEID", nullable = false, length = 16)
    public String getRoleid() {
        return roleid;
    }

    public void setRoleid(String roleid) {
        this.roleid = roleid;
    }

    @Id
    @Column(name = "MENUID", nullable = false)
    public int getMenuid() {
        return menuid;
    }

    public void setMenuid(int menuid) {
        this.menuid = menuid;
    }

    @Id
    @Column(name = "RESID", nullable = false, length = 32)
    public String getResid() {
        return resid;
    }

    public void setResid(String resid) {
        this.resid = resid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysRoleMenuRes that = (SysRoleMenuRes) o;

        if (menuid != that.menuid) return false;
        if (roleid != null ? !roleid.equals(that.roleid) : that.roleid != null) return false;
        if (resid != null ? !resid.equals(that.resid) : that.resid != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = roleid != null ? roleid.hashCode() : 0;
        result = 31 * result + menuid;
        result = 31 * result + (resid != null ? resid.hashCode() : 0);
        return result;
    }

	@Override
	public String toString() {
		return "SysRoleMenuRes [roleid=" + roleid + ", menuid=" + menuid
				+ ", resid=" + resid + "]";
	}
}
