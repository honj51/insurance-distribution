package cn.com.libertymutual.sys.bean;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

/**
 * Created by Ryan on 2016-09-09.
 */
public class SysRoleUrlPK implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 6134201210073814267L;
	private String roleid;
    private String url;

    @Column(name = "ROLEID", nullable = false, length = 16)
    @Id
    public String getRoleid() {
        return roleid;
    }

    public void setRoleid(String roleid) {
        this.roleid = roleid;
    }

    @Column(name = "URL", nullable = false, length = 64)
    @Id
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SysRoleUrlPK that = (SysRoleUrlPK) o;

        if (roleid != null ? !roleid.equals(that.roleid) : that.roleid != null) return false;
        if (url != null ? !url.equals(that.url) : that.url != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = roleid != null ? roleid.hashCode() : 0;
        result = 31 * result + (url != null ? url.hashCode() : 0);
        return result;
    }

	@Override
	public String toString() {
		return "SysRoleUrlPK [roleid=" + roleid + ", url=" + url + "]";
	}
}
