package cn.com.libertymutual.sys.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.*;

/**
 * Created by Ryan on 2016-12-21.
 */
@Entity
@Table(name = "tb_sys_role_user", catalog = "")
// @IdClass(SysRoleUserPK.class)
public class SysRoleUser implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -9030299871487721458L;
	private Integer id;
	private Integer roleid;
	private String userid;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "ROLEID", nullable = false)
	public Integer getRoleid() {
		return roleid;
	}

	public void setRoleid(Integer roleid) {
		this.roleid = roleid;
	}

	// @Id
	@Column(name = "USERID", nullable = false, length = 16)
	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		SysRoleUser that = (SysRoleUser) o;

		if (roleid != null ? !roleid.equals(that.roleid) : that.roleid != null)
			return false;
		if (userid != null ? !userid.equals(that.userid) : that.userid != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = roleid != null ? roleid.hashCode() : 0;
		result = 31 * result + (userid != null ? userid.hashCode() : 0);
		return result;
	}
}
