package cn.com.libertymutual.sys.bean;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;

import cn.com.libertymutual.core.base.SysUserBase;

import java.util.Date;

/**
 * Created by Ryan on 2016-12-21.
 */
@Entity
@javax.persistence.Table(name = "tb_sys_user", catalog = "")
public class SysUser extends SysUserBase {
	/**
	 * 
	 */
	private static final long serialVersionUID = -1836524808359140520L;
	private String userid;

	@Id
	@javax.persistence.Column(name = "USERID", nullable = false, length = 16)
	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	private String userCode;
	
	@Basic
	@javax.persistence.Column(name = "USER_CODE", nullable = false, length = 32)
	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	private String username;

	@Basic
	@javax.persistence.Column(name = "USERNAME", nullable = false, length = 32)
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	private String password;

	@Basic
	@javax.persistence.Column(name = "PASSWORD", nullable = false, length = 32)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	private String branchcode;

	@Basic
	@javax.persistence.Column(name = "BRANCHCODE", nullable = true, length = 12)
	public String getBranchcode() {
		return branchcode;
	}

	public void setBranchcode(String branchcode) {
		this.branchcode = branchcode;
	}
	private String branchName;
	@Basic
	@javax.persistence.Column(name = "BRANCHNAME", nullable = true, length = 30)
	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	private String deptcode;

	@Basic
	@javax.persistence.Column(name = "DEPTCODE", nullable = true, length = 12)
	public String getDeptcode() {
		return deptcode;
	}

	public void setDeptcode(String deptcode) {
		this.deptcode = deptcode;
	}

	private String termcode;

	@Basic
	@javax.persistence.Column(name = "TERMCODE", nullable = true, length = 12)
	public String getTermcode() {
		return termcode;
	}

	public void setTermcode(String termcode) {
		this.termcode = termcode;
	}

	private String clevel;

	@Basic
	@javax.persistence.Column(name = "CLEVEL", nullable = true, length = 1)
	public String getClevel() {
		return clevel;
	}

	public void setClevel(String clevel) {
		this.clevel = clevel;
	}

	private String sex;

	@Basic
	@javax.persistence.Column(name = "SEX", nullable = true, length = 1)
	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	private String phoneno;

	@Basic
	@javax.persistence.Column(name = "PHONENO", nullable = true, length = 20)
	public String getPhoneno() {
		return phoneno;
	}

	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}

	private String mobileno;

	@Basic
	@javax.persistence.Column(name = "MOBILENO", nullable = true, length = 12)
	public String getMobileno() {
		return mobileno;
	}

	public void setMobileno(String mobileno) {
		this.mobileno = mobileno;
	}

	private String email;

	@Basic
	@javax.persistence.Column(name = "EMAIL", nullable = true, length = 32)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	private Integer loginnum;

	@Basic
	@javax.persistence.Column(name = "LOGINNUM", nullable = false, precision = 0)
	public Integer getLoginnum() {
		return loginnum;
	}

	public void setLoginnum(Integer loginnum) {
		this.loginnum = loginnum;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date lastlogindate;

	@Basic
	@javax.persistence.Column(name = "LASTLOGINDATE", nullable = true)
	public Date getLastlogindate() {
		return lastlogindate;
	}

	public void setLastlogindate(Date lastlogindate) {
		this.lastlogindate = lastlogindate;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date logoutdate;

	@Basic
	@javax.persistence.Column(name = "LOGOUTDATE", nullable = true)
	public Date getLogoutdate() {
		return logoutdate;
	}

	public void setLogoutdate(Date logoutdate) {
		this.logoutdate = logoutdate;
	}

	private String loginip;

	@Basic
	@javax.persistence.Column(name = "LOGINIP", nullable = true, length = 20)
	public String getLoginip() {
		return loginip;
	}

	public void setLoginip(String loginip) {
		this.loginip = loginip;
	}

	private String remarks;

	@Basic
	@javax.persistence.Column(name = "REMARKS", nullable = true, length = 100)
	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date created;

	@Basic
	@javax.persistence.Column(name = "CREATED", nullable = false)
	public Date getCreated() {
		return created;
	}

	public void setCreated(Date created) {
		this.created = created;
	}

	private String creater;

	@Basic
	@javax.persistence.Column(name = "CREATER", nullable = false, length = 16)
	public String getCreater() {
		return creater;
	}

	public void setCreater(String creater) {
		this.creater = creater;
	}

	private String authtype;

	@Basic
	@javax.persistence.Column(name = "AUTHTYPE", nullable = false, length = 1)
	public String getAuthtype() {
		return authtype;
	}

	public void setAuthtype(String authtype) {
		this.authtype = authtype;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date disableddate;

	@Basic
	@javax.persistence.Column(name = "DISABLEDDATE", nullable = true)
	public Date getDisableddate() {
		return disableddate;
	}

	public void setDisableddate(Date disableddate) {
		this.disableddate = disableddate;
	}

	private String status;

	@Basic
	@javax.persistence.Column(name = "STATUS", nullable = false, length = 1)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	private String type;

	@Basic
	@javax.persistence.Column(name = "type", nullable = false, length = 1)
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	private String roleCode;

	@Basic
	@javax.persistence.Column(name = "ROLE_CODE", nullable = false, length = 32)
	public String getRoleCode() {
		return roleCode;
	}

	public void setRoleCode(String roleCode) {
		this.roleCode = roleCode;
	}

	private String authLevel;

	@Basic
	@javax.persistence.Column(name = "AUTH_LEVEL", nullable = false, length = 1)
	public String getAuthLevel() {
		return authLevel;
	}

	public void setAuthLevel(String authLevel) {
		this.authLevel = authLevel;
	}

	private SysRole role;

	@Transient
	public SysRole getRole() {
		return role;
	}

	public void setRole(SysRole role) {
		this.role = role;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		SysUser sysUser = (SysUser) o;

		if (userid != null ? !userid.equals(sysUser.userid) : sysUser.userid != null)
			return false;
		if (username != null ? !username.equals(sysUser.username) : sysUser.username != null)
			return false;
		if (password != null ? !password.equals(sysUser.password) : sysUser.password != null)
			return false;
		if (branchcode != null ? !branchcode.equals(sysUser.branchcode) : sysUser.branchcode != null)
			return false;
		if (deptcode != null ? !deptcode.equals(sysUser.deptcode) : sysUser.deptcode != null)
			return false;
		if (termcode != null ? !termcode.equals(sysUser.termcode) : sysUser.termcode != null)
			return false;
		if (clevel != null ? !clevel.equals(sysUser.clevel) : sysUser.clevel != null)
			return false;
		if (sex != null ? !sex.equals(sysUser.sex) : sysUser.sex != null)
			return false;
		if (phoneno != null ? !phoneno.equals(sysUser.phoneno) : sysUser.phoneno != null)
			return false;
		if (mobileno != null ? !mobileno.equals(sysUser.mobileno) : sysUser.mobileno != null)
			return false;
		if (email != null ? !email.equals(sysUser.email) : sysUser.email != null)
			return false;
		if (loginnum != null ? !loginnum.equals(sysUser.loginnum) : sysUser.loginnum != null)
			return false;
		if (lastlogindate != null ? !lastlogindate.equals(sysUser.lastlogindate) : sysUser.lastlogindate != null)
			return false;
		if (logoutdate != null ? !logoutdate.equals(sysUser.logoutdate) : sysUser.logoutdate != null)
			return false;
		if (loginip != null ? !loginip.equals(sysUser.loginip) : sysUser.loginip != null)
			return false;
		if (remarks != null ? !remarks.equals(sysUser.remarks) : sysUser.remarks != null)
			return false;
		if (created != null ? !created.equals(sysUser.created) : sysUser.created != null)
			return false;
		if (creater != null ? !creater.equals(sysUser.creater) : sysUser.creater != null)
			return false;
		if (authtype != null ? !authtype.equals(sysUser.authtype) : sysUser.authtype != null)
			return false;
		if (disableddate != null ? !disableddate.equals(sysUser.disableddate) : sysUser.disableddate != null)
			return false;
		if (status != null ? !status.equals(sysUser.status) : sysUser.status != null)
			return false;
		if (authLevel != null ? !authLevel.equals(sysUser.authLevel) : sysUser.authLevel != null)
			return false;

		return true;
	}

	@Override
	public int hashCode() {
		int result = userid != null ? userid.hashCode() : 0;
		result = 31 * result + (username != null ? username.hashCode() : 0);
		result = 31 * result + (password != null ? password.hashCode() : 0);
		result = 31 * result + (branchcode != null ? branchcode.hashCode() : 0);
		result = 31 * result + (deptcode != null ? deptcode.hashCode() : 0);
		result = 31 * result + (termcode != null ? termcode.hashCode() : 0);
		result = 31 * result + (clevel != null ? clevel.hashCode() : 0);
		result = 31 * result + (sex != null ? sex.hashCode() : 0);
		result = 31 * result + (phoneno != null ? phoneno.hashCode() : 0);
		result = 31 * result + (mobileno != null ? mobileno.hashCode() : 0);
		result = 31 * result + (email != null ? email.hashCode() : 0);
		result = 31 * result + (loginnum != null ? loginnum.hashCode() : 0);
		result = 31 * result + (lastlogindate != null ? lastlogindate.hashCode() : 0);
		result = 31 * result + (logoutdate != null ? logoutdate.hashCode() : 0);
		result = 31 * result + (loginip != null ? loginip.hashCode() : 0);
		result = 31 * result + (remarks != null ? remarks.hashCode() : 0);
		result = 31 * result + (created != null ? created.hashCode() : 0);
		result = 31 * result + (creater != null ? creater.hashCode() : 0);
		result = 31 * result + (authtype != null ? authtype.hashCode() : 0);
		result = 31 * result + (disableddate != null ? disableddate.hashCode() : 0);
		result = 31 * result + (status != null ? status.hashCode() : 0);
		result = 31 * result + (authLevel != null ? authLevel.hashCode() : 0);
		return result;
	}
}
