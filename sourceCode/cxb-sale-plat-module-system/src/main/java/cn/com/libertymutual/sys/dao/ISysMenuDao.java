package cn.com.libertymutual.sys.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cn.com.libertymutual.sys.bean.SysMenu;

@Repository
public interface ISysMenuDao  extends JpaRepository<SysMenu, Integer> ,JpaSpecificationExecutor<SysMenu> 
{
    @Transactional
    @Modifying
    @Query("update SysMenu set orderid = :orderid  where menuid = :menuid")
    public void updateMenuStatus(@Param("orderid") int state, @Param("menuid") int menuid);
   
    /**
     * 查询相关状态的菜单
     * @param status
     */
    @Query("from SysMenu   where status=:status")	    
    public void findByMenuStatus(@Param("status")  String status);

    @Query("select m from SysMenu m where m.status = '1' and m.menuid in( select r from SysRoleMenu r where r.roleid = ?1)")	    
	public List<SysMenu> findByRoleid(String roleId);

    @Query("select m from SysMenu m where m.status = '1' ")
	public List<SysMenu> findAllMenu();
    
    @Query("select m from SysMenu m where m.fmenuid = ?1  and m.status = '1' ")
	public List<SysMenu> findByFmenuid(int fmenuid);

    @Query("select sm from SysMenu sm where sm.menuid = (select max(m.menuid) from SysMenu m where m.fmenuid = ?1  and m.status = '1' )")
    SysMenu findMaxMenuid(int fmenuid);
    
    @Query("select m from SysMenu m where m.menuid in (?1)  and m.status = '1' ")
	public List<SysMenu> findByMenuIds(List<Integer> msd);
    
    @Query("select m from SysMenu m where m.menuid = ?1  and m.status = '1' ")
   	public SysMenu findByMenuId(int menuId);

    
    @Modifying
	@Transactional
	@Query("update SysMenu set status = ?1 where menuid = ?2")
	public void updateStatus(String status,Integer menuId);
    
//    @Modifying
//	@Transactional
//	@Query("update SysMenu set menuname = ?1,menunameen = ?1 where menuid = ?2")
//	void updateMenuName(String name, Integer menuid);
    
}
