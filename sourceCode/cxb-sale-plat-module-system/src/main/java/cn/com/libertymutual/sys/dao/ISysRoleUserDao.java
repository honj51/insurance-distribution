package cn.com.libertymutual.sys.dao;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.com.libertymutual.sys.bean.SysRoleUser;
import cn.com.libertymutual.sys.bean.SysRoleUserPK;


@Repository
public interface ISysRoleUserDao extends JpaRepository<SysRoleUser, Integer> 
{

	List<SysRoleUser> findByRoleid(int roleId);

	@Modifying
	@Transactional
	@Query("delete from SysRoleUser where roleid = :roleId")
	void deleteRole(@Param("roleId")String roleId);

	SysRoleUser findByUserid(String userid);

	@Modifying
	@Transactional
	@Query("update SysRoleUser set roleid = ?1 where userid = ?2")
	void updateRole(Integer roleId, String userId);
	
	
	//@Query( "SELECT DISTINCT t1.menuid FROM tb_sys_role_menu t1 INNER JOIN tb_sys_menu t2 ON t1.menuid=t2.menuid WHERE t1.roleid IN (?1) GROUP BY t1.menuid ORDER BY t2.orderid")
	//List<Integer> findRoleMenu(List<String> roleList, Integer fmenuId) ;
	
	
	//@Query("select menuid from tb_sys_role_menu where roleid=:roleId order by menuid")
	//List<Integer> findMenuIdList( String roleId );
}
