package cn.com.libertymutual.sys.exception;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

//import org.springframework.boot.web.reactive.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
///import org.springframework.boot.autoconfigure.web.ErrorProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.context.request.WebRequest;

import com.beust.jcommander.internal.Maps;

@Configuration
public class CustomErrorAttributes implements ErrorAttributes {

	//@Override
	public Map<String, Object> getErrorAttributes(
			RequestAttributes requestAttributes, boolean includeStackTrace) {
		
		Map<String, Object> map = Maps.newHashMap();
		ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes)requestAttributes;
		HttpServletRequest request = servletRequestAttributes.getRequest();
		/*
		Enumeration<String> enm = request.getRequest().getAttributeNames();
		while( enm.hasMoreElements() ) {
			String ss = enm.nextElement();
			
			System.out.println( ss +"=" + request.getRequest().getAttribute(ss));
		}*/
		Integer errorCode = (Integer)request.getAttribute("javax.servlet.error.status_code");
		
		map.put("state", errorCode);
		map.put("resCode", errorCode.toString());
		map.put("result", request.getAttribute("javax.servlet.error.message"));
		map.put("success", false);
		
		return map;
	}

	@Override
	public Map<String, Object> getErrorAttributes(WebRequest webRequest,
			boolean includeStackTrace) {

		Map<String, Object> map = Maps.newHashMap();
		
		ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes)webRequest;
		HttpServletRequest request = servletRequestAttributes.getRequest();
		
		/*
		Enumeration<String> enm = request.getRequest().getAttributeNames();
		while( enm.hasMoreElements() ) {
			String ss = enm.nextElement();
			
			System.out.println( ss +"=" + request.getRequest().getAttribute(ss));
		}*/
		Integer errorCode = (Integer)request.getAttribute("javax.servlet.error.status_code");
		
		map.put("state", errorCode);
		map.put("resCode", errorCode.toString());
		map.put("result", request.getAttribute("javax.servlet.error.message"));
		map.put("success", false);
		
		return map;
		
	}

	@Override
	public Throwable getError(WebRequest webRequest) {
		// TODO Auto-generated method stub
		return null;
	}

}
