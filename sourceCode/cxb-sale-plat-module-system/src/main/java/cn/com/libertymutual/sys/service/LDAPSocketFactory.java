package cn.com.libertymutual.sys.service;


import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;

import javax.net.SocketFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public final class LDAPSocketFactory extends SocketFactory{
	
private static SocketFactory blindFactory = null;
private static Logger log = LoggerFactory.getLogger(LDAPSocketFactory.class);
	/**
	 * Builds an all trusting "blind" ssl socket factory.
	 */
	static {
		// create a trust manager that will purposefully fall down on the
		TrustManager[] blindTrustMan = new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() { return null; }
			public void checkClientTrusted(X509Certificate[] c, String a) { }
			public void checkServerTrusted(X509Certificate[] c, String a) { }
		} };

		// create our ssl socket factory with our lazy trust manager
		try {
			SSLContext sc = SSLContext.getInstance("SSL");
			sc.init(null, blindTrustMan, new java.security.SecureRandom());
			blindFactory = sc.getSocketFactory();
		} catch (GeneralSecurityException e) {
			log.error(e.getMessage(),e);
			e.printStackTrace();
		}
	}
	
	public static SocketFactory getDefault() {
		return new LDAPSocketFactory();
	}
	
	@Override
	public Socket createSocket() throws IOException {
		return blindFactory.createSocket();
	}
	
	@Override
	public Socket createSocket(InetAddress arg0, int arg1) throws IOException {
		return blindFactory.createSocket(arg0, arg1);
	}

	@Override
	public Socket createSocket(InetAddress arg0, int arg1, InetAddress arg2,
			int arg3) throws IOException {
		return blindFactory.createSocket(arg0, arg1, arg2, arg3);
	}
	@Override
	public Socket createSocket(String arg0, int arg1) throws IOException,
			UnknownHostException {
		return blindFactory.createSocket(arg0, arg1);
	}

	@Override
	public Socket createSocket(String arg0, int arg1, InetAddress arg2, int arg3)
			throws IOException, UnknownHostException {
		 return blindFactory.createSocket(arg0, arg1, arg2, arg3);
	}

	

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
	}

}
