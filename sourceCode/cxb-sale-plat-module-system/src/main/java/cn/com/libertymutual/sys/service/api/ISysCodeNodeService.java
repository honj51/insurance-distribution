package cn.com.libertymutual.sys.service.api;

import java.util.List;
import java.util.Map;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sys.bean.SysCodeNode;

public interface ISysCodeNodeService {

	public	List<SysCodeNode> findCodeInfoForValid(String codeType);

	public	List<SysCodeNode> findCodeInfoByRiskCode(String codeType, String riskCode, Map<String, SysCodeNode> nodeMap);

	public	List<SysCodeNode> findAreaSortList();

	public	List<SysCodeNode> findAreaSortList(	String code); 
	public  List<SysCodeNode> findSysCodeNode(String codetype, String sorttype);


}
 