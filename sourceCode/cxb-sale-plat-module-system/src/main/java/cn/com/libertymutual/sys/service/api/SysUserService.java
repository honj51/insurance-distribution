package cn.com.libertymutual.sys.service.api;

import cn.com.libertymutual.core.web.ServiceResult;

public interface SysUserService {
	ServiceResult  findAll(boolean isLookup,String lookupDataOne,String lookupDataTwo,int pageNumber, int pageSize, String sortfield,String sorttype);
}
