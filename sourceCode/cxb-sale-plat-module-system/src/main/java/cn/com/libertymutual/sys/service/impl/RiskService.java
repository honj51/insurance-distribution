package cn.com.libertymutual.sys.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.libertymutual.sys.bean.PrpRisk;
import cn.com.libertymutual.sys.dao.IRiskDao;
import cn.com.libertymutual.sys.service.api.IRiskService;

import com.google.common.collect.Lists;

@Service("riskService")
public class RiskService implements IRiskService {

	@Autowired private IRiskDao riskDao;
	@Override
	public List<PrpRisk> listAll( ) {
		Iterable<PrpRisk> risks= riskDao.findAll();
		List<PrpRisk> relist = Lists.newArrayList();
		for( PrpRisk rs :risks ){
			relist.add(rs);
		}
		return relist;
	}

	 

}
