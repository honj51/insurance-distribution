package cn.com.libertymutual.sys.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.libertymutual.sys.bean.SysServiceInfo;
import cn.com.libertymutual.sys.dao.ISysServiceInfoDao;
import cn.com.libertymutual.sys.service.api.ISysServiceInfoService;

@Service("sysServiceInfoService")
public class SysServiceInfoService implements ISysServiceInfoService {

	@Autowired private ISysServiceInfoDao sysServiceInfoDao;
	
	@Override
	public Iterable<SysServiceInfo> getsysServiceInfo() {
		Iterable<SysServiceInfo> ir = sysServiceInfoDao.findAll();
		return ir;
	}

}