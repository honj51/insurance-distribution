package cn.com.libertymutual.sys.vo;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;

import cn.com.libertymutual.sys.bean.SysMenu;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

public class MenuConvert {

	public MenuConvert() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * 
	* @Title genMenuTree 
	* @Description 生成树状结构菜单
	* @param menuMap
	* @param menuIdList
	* @param menuMapRel
	* @param mResIdMap
	* @param fmenuId
	* @return    
	* @return List<MenuInfoVO>
	 */
	public List<MenuInfoVO> genMenuTree( Map<String, SysMenu> menuMap, List<Integer> menuIdList, Map<String, List<Integer>> menuMapRel, 
			Map<Integer,List<String>> mResIdMap, Integer fmenuId ) { 

		List<MenuInfoVO> mvoList = Lists.newArrayList();
		
		if( menuIdList.contains(fmenuId) && menuMapRel.containsKey(fmenuId.toString()) ) {
			menuIdList.remove(fmenuId);
			SysMenu menuTmp = null;
			List<Integer> menuIdRelList = menuMapRel.get(fmenuId.toString());
			MenuInfoVO mvo = null;
			List<MenuInfoVO> subMvoList = null;
			for (Integer menuId : menuIdRelList) {
				if( !menuIdList.contains( menuId ) ) continue;
				
				menuTmp = menuMap.get( menuId.toString() );
				
				mvo = new MenuInfoVO();
				try {
					BeanUtils.copyProperties(mvo, menuTmp);
					if( mResIdMap.containsKey( mvo.getMenuid() ) ) {
						mvo.setResIds( mResIdMap.get(mvo.getMenuid()) );
					}
				} catch (IllegalAccessException | InvocationTargetException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				subMvoList = genMenuTree(menuMap, menuIdList, menuMapRel, mResIdMap, menuId);
				
				if( subMvoList.size() > 0 ) {
					mvo.setChild( subMvoList );
					mvo.setIsleaf("false"); 
				}else{
					mvo.setIsleaf("true"); 
					
				}
				
				mvoList.add( mvo );
			}
		}
		
		/*
		MenuVO mvo = null;
	
		MenuInfo menuTmp = null;
		Integer menuId = null;
		for ( int i=0; menuIdList.size() > 0; i++) {
			//if( i > menuIdList.size() ) break;
			menuId = menuIdList.get(0);
			menuTmp = menuMap.get( menuId );

			menuIdList.remove(menuId);
			if( menuTmp.getFmenuid().intValue() != menuId2.intValue() && menuTmp.getFmenuid().intValue() != fmenuId.intValue() && menuTmp.getFmenuid().intValue() != menuId2.intValue())
				continue;

			
			mvo = new MenuVO();
			try {
				BeanUtils.copyProperties(mvo, menuTmp);
			} catch (IllegalAccessException | InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			if( Constants.TRUE.equals( menuTmp.getIsLeaf() ) && menuIdList.size() > 0 && !menuIdList.contains( menuTmp.getFmenuid() ) ) {
				
				mvo.setChild( genMenuTree(menuMap, menuIdList, fmenuId, menuTmp.getFmenuid() ) );
				
			}

			mvoList.add( mvo );
			
			

		}*/
		
		return mvoList;
	}

	public Map<Integer,List<String>> convertResList( List<Map<String,Object>> resList ) {
		Map<Integer,List<String>> mapList = Maps.newHashMap();
		Integer menuIdTmp = 0;
		List<String> resId = Lists.newArrayList();
		for (Map<String, Object> map : resList) {
			Object obj = map.get("MENUID");
			Integer menuId = 0;
			if( obj instanceof BigDecimal )
				menuId = ((BigDecimal) obj).intValue();
			else if( obj instanceof Integer )
				menuId = (Integer) obj;
			else menuId = Integer.parseInt( (String) obj );
			
			if( menuIdTmp.intValue() > 0 && menuId.intValue() != menuIdTmp.intValue() ) {
				mapList.put( menuIdTmp.intValue(), resId);
				resId = Lists.newArrayList();
			}
			
			resId.add( (String) map.get("RESID") );
			
			menuIdTmp = menuId;
		}
		if( menuIdTmp > 0 ) {
			mapList.put( menuIdTmp.intValue(), resId);
		}
		return mapList;
	}
	
	@Deprecated
	public Map<Integer,List<String>> convertMenuResList( List<Map<String,Object>> resList ) {
		Map<Integer,List<String>> mapList = Maps.newHashMap();
		Integer menuIdTmp = -2;
		Integer menuId = 0;
		String resId;
		boolean resBool = false;
		List<String> resIdList = Lists.newArrayList();
		for (Map<String, Object> map : resList) {
			Object obj = map.get("MENUID");
			if( obj instanceof BigDecimal )
				menuId = ((BigDecimal) obj).intValue();
			else if( obj instanceof Integer )
				menuId = (Integer) obj;
			else menuId = Integer.parseInt( (String) obj );
			
			if( resBool && menuIdTmp.intValue() == menuId.intValue() ) {
				menuIdTmp = menuId;
				continue;
			}
			resBool = false;
			
			resId = (String) map.get("RESID");
			if( Strings.isNullOrEmpty(resId) ) {
				resBool = true;
				menuIdTmp = menuId;
				resIdList.clear();
				continue;
			}
			
			if( menuIdTmp.intValue() > 0 && menuId.intValue() != menuIdTmp.intValue() ) {
				mapList.put( menuIdTmp.intValue(), resIdList);
				resIdList = Lists.newArrayList();
			}
			
					
			resIdList.add( resId );
			
			menuIdTmp = menuId;
		}
		if( menuIdTmp > 0 && resIdList.size() > 0 ) {
			mapList.put( menuIdTmp.intValue(), resIdList);
		}
		return mapList;
	}
	
	/**
	 * 转换菜单层级
	 * @param menuInfoMap
	 * @return
	 */
	public Map<Integer,List<SysMenu>> convertyMenuLevel( Map<Integer, SysMenu> menuInfoMap ) {
		List<SysMenu> flist = Lists.newArrayList();
		Map<Integer,List<SysMenu>> menuMap = Maps.newHashMap();
		int itmp = 0;
		
		Set<Entry<Integer, SysMenu>> menuSet = menuInfoMap.entrySet();
		Iterator<Entry<Integer, SysMenu>> menuIt = menuSet.iterator();
		while( menuIt.hasNext() ) {
			Entry<Integer, SysMenu> menu = menuIt.next();
			SysMenu menuInfo = menu.getValue();
			if( menuInfo.getFmenuid() != itmp ) {
				if( menuMap.containsKey(itmp) ) {
					menuMap.get(itmp).addAll( flist );
					//menuMap.put( itmp , flist);
				}
				else menuMap.put( itmp , flist);
				flist = Lists.newArrayList();
			}
			flist.add( menuInfo );
			itmp = menuInfo.getFmenuid();
		}
		
		if( menuMap.containsKey(itmp) ) {
			menuMap.get(itmp).addAll( flist );
		}
		else menuMap.put( itmp , flist);
		
		return menuMap;
	}
	
	public JSONArray getAllChildrenMenuInfo( Map<Integer,List<SysMenu>> levelMenuMap, String uiType, Integer fmenuId, boolean isOpen, Integer menuId, List<Integer> menuIdList, Map<Integer,List<String>> menuResIdListMap ) {
		if( menuId != null && menuId.equals( fmenuId ) ) return null;
		List<SysMenu> menuList = levelMenuMap.get( fmenuId );
		if( menuList == null ) return null;
		JSONArray b = new JSONArray();
		
		for (SysMenu menuInfo : menuList) {
			if( menuId != null && menuId.equals( menuInfo.getFmenuid() ) ) continue;
			JSONObject obj = new JSONObject();
			obj.put("id", menuInfo.getFmenuid());
			obj.put("ztree".equals(uiType)?"name":"text", menuInfo.getMenuname());
			obj.put("ztree".equals(uiType)?"pId":"parentId", menuInfo.getFmenuid());
			if( "1".equals( menuInfo.getIsleaf() ) ) {
				if( !"ztree".equals( uiType ) )
					obj.put("state", isOpen ? "open" : "closed");
				else obj.put("open", false);
				
				if( menuIdList != null && menuIdList.contains( menuInfo.getFmenuid() ) )
					obj.put("checked", true);
			}
			else {
				//obj.put("iconCls", "leaf");
				obj.put("icon", "/img/menuFile.gif");
				obj.put("iconSkin", "menuIcon");
				obj.put("isMenu", "1");

				if( menuIdList != null && menuIdList.contains( menuInfo.getFmenuid() ) )
					obj.put("checked", true);
				
				//显示菜单下的资源信息
				///List<MenuRes> menuResList = CacheUtil.getCacheMenuResMap().get( menuInfo.getFmenuid() );
			//	System.out.println( menuInfo.getFmenuid() + "\t" + menuResList);
				/*if( menuResList != null && "ztree".equals(uiType) ) {	//如果前端显示太慢，可考虑异步加载

					JSONArray mRes = new JSONArray();
					for (MenuRes menuRes : menuResList) {
						JSONObject resObj = new JSONObject();
						String _id = String.format("%s_%s", menuInfo.getFmenuid(), menuRes.getResId());
						resObj.put( "id", _id );
						resObj.put( "pId", menuInfo.getFmenuid() );
						resObj.put( "isPageFun", true );
						resObj.put( "isParent", false );
						resObj.put( "resId", menuRes.getResId() );
						resObj.put( "name", menuRes.getResName() );
						resObj.put( "resType", menuRes.getResType() );
						resObj.put( "drag", false );
						resObj.put( "icon", "../img/simpletype_obj.gif" );

						if( menuResIdListMap != null && obj.getBoolean("checked") != null && obj.getBoolean("checked") ) {
							List<String> resList = menuResIdListMap.get( menuInfo.getFmenuid() );
							if( resList != null && resList.size() > 0 && !resList.contains( menuRes.getResId() ) )
								resObj.put( "checked", true );
							else if( resList == null ) resObj.put( "checked", true );
						}
							
						
						mRes.add( resObj );
					}

					obj.put("children", mRes);
				}*/
			}

			JSONArray jsonTmp = getAllChildrenMenuInfo( levelMenuMap, uiType, menuInfo.getFmenuid(), isOpen, menuId, menuIdList, menuResIdListMap );
			if( jsonTmp != null ) {
				obj.put("children", jsonTmp);
			}
			b.add( obj );
		}
		
		return b;
	}
}
