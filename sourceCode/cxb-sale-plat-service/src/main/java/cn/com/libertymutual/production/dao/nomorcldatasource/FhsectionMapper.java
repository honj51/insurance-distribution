package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.com.libertymutual.production.model.nomorcldatasource.Fhsection;
import cn.com.libertymutual.production.model.nomorcldatasource.FhsectionExample;
import cn.com.libertymutual.production.model.nomorcldatasource.FhsectionKey;
@Mapper
public interface FhsectionMapper {
    int countByExample(FhsectionExample example);

    int deleteByExample(FhsectionExample example);

    int deleteByPrimaryKey(FhsectionKey key);

    int insert(Fhsection record);

    int insertSelective(Fhsection record);

    List<Fhsection> selectByExample(FhsectionExample example);

    Fhsection selectByPrimaryKey(FhsectionKey key);

    int updateByExampleSelective(@Param("record") Fhsection record, @Param("example") FhsectionExample example);

    int updateByExample(@Param("record") Fhsection record, @Param("example") FhsectionExample example);

    int updateByPrimaryKeySelective(Fhsection record);

    int updateByPrimaryKey(Fhsection record);
}