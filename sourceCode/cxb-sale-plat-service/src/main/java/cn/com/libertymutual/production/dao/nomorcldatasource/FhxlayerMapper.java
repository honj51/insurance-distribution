package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.com.libertymutual.production.model.nomorcldatasource.Fhxlayer;
import cn.com.libertymutual.production.model.nomorcldatasource.FhxlayerExample;
import cn.com.libertymutual.production.model.nomorcldatasource.FhxlayerKey;
@Mapper
public interface FhxlayerMapper {
    int countByExample(FhxlayerExample example);

    int deleteByExample(FhxlayerExample example);

    int deleteByPrimaryKey(FhxlayerKey key);

    int insert(Fhxlayer record);

    int insertSelective(Fhxlayer record);

    List<Fhxlayer> selectByExample(FhxlayerExample example);

    Fhxlayer selectByPrimaryKey(FhxlayerKey key);

    int updateByExampleSelective(@Param("record") Fhxlayer record, @Param("example") FhxlayerExample example);

    int updateByExample(@Param("record") Fhxlayer record, @Param("example") FhxlayerExample example);

    int updateByPrimaryKeySelective(Fhxlayer record);

    int updateByPrimaryKey(Fhxlayer record);
}