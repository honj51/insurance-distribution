package cn.com.libertymutual.production.dao.nomorcldatasource;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdpolicypay;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdpolicypayExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdpolicypayKey;

import java.util.List;

@Mapper
public interface PrpdpolicypayMapper {
    int countByExample(PrpdpolicypayExample example);

    int deleteByExample(PrpdpolicypayExample example);

    int deleteByPrimaryKey(PrpdpolicypayKey key);

    int insert(Prpdpolicypay record);

    int insertSelective(Prpdpolicypay record);

    List<Prpdpolicypay> selectByExample(PrpdpolicypayExample example);

    Prpdpolicypay selectByPrimaryKey(PrpdpolicypayKey key);

    int updateByExampleSelective(@Param("record") Prpdpolicypay record, @Param("example") PrpdpolicypayExample example);

    int updateByExample(@Param("record") Prpdpolicypay record, @Param("example") PrpdpolicypayExample example);

    int updateByPrimaryKeySelective(Prpdpolicypay record);

    int updateByPrimaryKey(Prpdpolicypay record);
}