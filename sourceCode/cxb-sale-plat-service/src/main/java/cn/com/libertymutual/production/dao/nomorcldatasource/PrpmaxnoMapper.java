package cn.com.libertymutual.production.dao.nomorcldatasource;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpmaxno;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpmaxnoExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpmaxnoKey;
@Mapper
public interface PrpmaxnoMapper {
    int countByExample(PrpmaxnoExample example);

    int deleteByExample(PrpmaxnoExample example);

    int deleteByPrimaryKey(PrpmaxnoKey key);

    int insert(Prpmaxno record);

    int insertSelective(Prpmaxno record);

    List<Prpmaxno> selectByExample(PrpmaxnoExample example);

    Prpmaxno selectByPrimaryKey(PrpmaxnoKey key);

    int updateByExampleSelective(@Param("record") Prpmaxno record, @Param("example") PrpmaxnoExample example);

    int updateByExample(@Param("record") Prpmaxno record, @Param("example") PrpmaxnoExample example);

    int updateByPrimaryKeySelective(Prpmaxno record);

    int updateByPrimaryKey(Prpmaxno record);
}