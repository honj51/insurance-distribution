package cn.com.libertymutual.production.model.nomorcldatasource;

import java.math.BigDecimal;

public class Fhsection extends FhsectionKey {
    private String classcode;

    private String sectioncdesc;

    private String sectionedesc;

    private String currency;

    private BigDecimal retentionvalue;

    private BigDecimal reinsurerate;

    private BigDecimal insharerate;

    private BigDecimal lines;

    private BigDecimal limitvalue;

    private BigDecimal lowerlimitvalue;

    private String commflag;

    private BigDecimal largelossvalue;

    private BigDecimal cashlossvalue;

    private String cleanmode;

    private BigDecimal maincoinsrate;

    private BigDecimal subcoinsrate;

    private BigDecimal shareholderrate;

    private BigDecimal shareholderlines;

    private BigDecimal inrate;

    private String pccleanmode;

    private String accmode;

    private BigDecimal oslossrate;

    private BigDecimal expenserate;

    private Short carriedyrs;

    private Short pcstartmths;

    private String expenseind;

    private String pcadjustmeth;

    private BigDecimal pcminrate;

    private BigDecimal pcmaxrate;

    private BigDecimal pccompbase;

    private BigDecimal pccompstep;

    private BigDecimal pcratestep;

    private String flag;

    private String cashlossflag;

    private BigDecimal cleanyear;

    private BigDecimal lowpaidrate;

    private BigDecimal upperpaidrate;

    private BigDecimal basepayrate;

    private BigDecimal adjustcommrate;

    private BigDecimal adjustrate;

    private String inratetype;

    private String cleancutremark;

    private String cleancutstatus;

    private BigDecimal maincoinsratege;

    private BigDecimal maincoinsratebelow;

    private String vatflag;

    private BigDecimal addvatrate;

    public String getClasscode() {
        return classcode;
    }

    public void setClasscode(String classcode) {
        this.classcode = classcode == null ? null : classcode.trim();
    }

    public String getSectioncdesc() {
        return sectioncdesc;
    }

    public void setSectioncdesc(String sectioncdesc) {
        this.sectioncdesc = sectioncdesc == null ? null : sectioncdesc.trim();
    }

    public String getSectionedesc() {
        return sectionedesc;
    }

    public void setSectionedesc(String sectionedesc) {
        this.sectionedesc = sectionedesc == null ? null : sectionedesc.trim();
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency == null ? null : currency.trim();
    }

    public BigDecimal getRetentionvalue() {
        return retentionvalue;
    }

    public void setRetentionvalue(BigDecimal retentionvalue) {
        this.retentionvalue = retentionvalue;
    }

    public BigDecimal getReinsurerate() {
        return reinsurerate;
    }

    public void setReinsurerate(BigDecimal reinsurerate) {
        this.reinsurerate = reinsurerate;
    }

    public BigDecimal getInsharerate() {
        return insharerate;
    }

    public void setInsharerate(BigDecimal insharerate) {
        this.insharerate = insharerate;
    }

    public BigDecimal getLines() {
        return lines;
    }

    public void setLines(BigDecimal lines) {
        this.lines = lines;
    }

    public BigDecimal getLimitvalue() {
        return limitvalue;
    }

    public void setLimitvalue(BigDecimal limitvalue) {
        this.limitvalue = limitvalue;
    }

    public BigDecimal getLowerlimitvalue() {
        return lowerlimitvalue;
    }

    public void setLowerlimitvalue(BigDecimal lowerlimitvalue) {
        this.lowerlimitvalue = lowerlimitvalue;
    }

    public String getCommflag() {
        return commflag;
    }

    public void setCommflag(String commflag) {
        this.commflag = commflag == null ? null : commflag.trim();
    }

    public BigDecimal getLargelossvalue() {
        return largelossvalue;
    }

    public void setLargelossvalue(BigDecimal largelossvalue) {
        this.largelossvalue = largelossvalue;
    }

    public BigDecimal getCashlossvalue() {
        return cashlossvalue;
    }

    public void setCashlossvalue(BigDecimal cashlossvalue) {
        this.cashlossvalue = cashlossvalue;
    }

    public String getCleanmode() {
        return cleanmode;
    }

    public void setCleanmode(String cleanmode) {
        this.cleanmode = cleanmode == null ? null : cleanmode.trim();
    }

    public BigDecimal getMaincoinsrate() {
        return maincoinsrate;
    }

    public void setMaincoinsrate(BigDecimal maincoinsrate) {
        this.maincoinsrate = maincoinsrate;
    }

    public BigDecimal getSubcoinsrate() {
        return subcoinsrate;
    }

    public void setSubcoinsrate(BigDecimal subcoinsrate) {
        this.subcoinsrate = subcoinsrate;
    }

    public BigDecimal getShareholderrate() {
        return shareholderrate;
    }

    public void setShareholderrate(BigDecimal shareholderrate) {
        this.shareholderrate = shareholderrate;
    }

    public BigDecimal getShareholderlines() {
        return shareholderlines;
    }

    public void setShareholderlines(BigDecimal shareholderlines) {
        this.shareholderlines = shareholderlines;
    }

    public BigDecimal getInrate() {
        return inrate;
    }

    public void setInrate(BigDecimal inrate) {
        this.inrate = inrate;
    }

    public String getPccleanmode() {
        return pccleanmode;
    }

    public void setPccleanmode(String pccleanmode) {
        this.pccleanmode = pccleanmode == null ? null : pccleanmode.trim();
    }

    public String getAccmode() {
        return accmode;
    }

    public void setAccmode(String accmode) {
        this.accmode = accmode == null ? null : accmode.trim();
    }

    public BigDecimal getOslossrate() {
        return oslossrate;
    }

    public void setOslossrate(BigDecimal oslossrate) {
        this.oslossrate = oslossrate;
    }

    public BigDecimal getExpenserate() {
        return expenserate;
    }

    public void setExpenserate(BigDecimal expenserate) {
        this.expenserate = expenserate;
    }

    public Short getCarriedyrs() {
        return carriedyrs;
    }

    public void setCarriedyrs(Short carriedyrs) {
        this.carriedyrs = carriedyrs;
    }

    public Short getPcstartmths() {
        return pcstartmths;
    }

    public void setPcstartmths(Short pcstartmths) {
        this.pcstartmths = pcstartmths;
    }

    public String getExpenseind() {
        return expenseind;
    }

    public void setExpenseind(String expenseind) {
        this.expenseind = expenseind == null ? null : expenseind.trim();
    }

    public String getPcadjustmeth() {
        return pcadjustmeth;
    }

    public void setPcadjustmeth(String pcadjustmeth) {
        this.pcadjustmeth = pcadjustmeth == null ? null : pcadjustmeth.trim();
    }

    public BigDecimal getPcminrate() {
        return pcminrate;
    }

    public void setPcminrate(BigDecimal pcminrate) {
        this.pcminrate = pcminrate;
    }

    public BigDecimal getPcmaxrate() {
        return pcmaxrate;
    }

    public void setPcmaxrate(BigDecimal pcmaxrate) {
        this.pcmaxrate = pcmaxrate;
    }

    public BigDecimal getPccompbase() {
        return pccompbase;
    }

    public void setPccompbase(BigDecimal pccompbase) {
        this.pccompbase = pccompbase;
    }

    public BigDecimal getPccompstep() {
        return pccompstep;
    }

    public void setPccompstep(BigDecimal pccompstep) {
        this.pccompstep = pccompstep;
    }

    public BigDecimal getPcratestep() {
        return pcratestep;
    }

    public void setPcratestep(BigDecimal pcratestep) {
        this.pcratestep = pcratestep;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag == null ? null : flag.trim();
    }

    public String getCashlossflag() {
        return cashlossflag;
    }

    public void setCashlossflag(String cashlossflag) {
        this.cashlossflag = cashlossflag == null ? null : cashlossflag.trim();
    }

    public BigDecimal getCleanyear() {
        return cleanyear;
    }

    public void setCleanyear(BigDecimal cleanyear) {
        this.cleanyear = cleanyear;
    }

    public BigDecimal getLowpaidrate() {
        return lowpaidrate;
    }

    public void setLowpaidrate(BigDecimal lowpaidrate) {
        this.lowpaidrate = lowpaidrate;
    }

    public BigDecimal getUpperpaidrate() {
        return upperpaidrate;
    }

    public void setUpperpaidrate(BigDecimal upperpaidrate) {
        this.upperpaidrate = upperpaidrate;
    }

    public BigDecimal getBasepayrate() {
        return basepayrate;
    }

    public void setBasepayrate(BigDecimal basepayrate) {
        this.basepayrate = basepayrate;
    }

    public BigDecimal getAdjustcommrate() {
        return adjustcommrate;
    }

    public void setAdjustcommrate(BigDecimal adjustcommrate) {
        this.adjustcommrate = adjustcommrate;
    }

    public BigDecimal getAdjustrate() {
        return adjustrate;
    }

    public void setAdjustrate(BigDecimal adjustrate) {
        this.adjustrate = adjustrate;
    }

    public String getInratetype() {
        return inratetype;
    }

    public void setInratetype(String inratetype) {
        this.inratetype = inratetype == null ? null : inratetype.trim();
    }

    public String getCleancutremark() {
        return cleancutremark;
    }

    public void setCleancutremark(String cleancutremark) {
        this.cleancutremark = cleancutremark == null ? null : cleancutremark.trim();
    }

    public String getCleancutstatus() {
        return cleancutstatus;
    }

    public void setCleancutstatus(String cleancutstatus) {
        this.cleancutstatus = cleancutstatus == null ? null : cleancutstatus.trim();
    }

    public BigDecimal getMaincoinsratege() {
        return maincoinsratege;
    }

    public void setMaincoinsratege(BigDecimal maincoinsratege) {
        this.maincoinsratege = maincoinsratege;
    }

    public BigDecimal getMaincoinsratebelow() {
        return maincoinsratebelow;
    }

    public void setMaincoinsratebelow(BigDecimal maincoinsratebelow) {
        this.maincoinsratebelow = maincoinsratebelow;
    }

    public String getVatflag() {
        return vatflag;
    }

    public void setVatflag(String vatflag) {
        this.vatflag = vatflag == null ? null : vatflag.trim();
    }

    public BigDecimal getAddvatrate() {
        return addvatrate;
    }

    public void setAddvatrate(BigDecimal addvatrate) {
        this.addvatrate = addvatrate;
    }
}