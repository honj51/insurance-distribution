package cn.com.libertymutual.production.model.nomorcldatasource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class FhsectionExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public FhsectionExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andTreatynoIsNull() {
            addCriterion("TREATYNO is null");
            return (Criteria) this;
        }

        public Criteria andTreatynoIsNotNull() {
            addCriterion("TREATYNO is not null");
            return (Criteria) this;
        }

        public Criteria andTreatynoEqualTo(String value) {
            addCriterion("TREATYNO =", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoNotEqualTo(String value) {
            addCriterion("TREATYNO <>", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoGreaterThan(String value) {
            addCriterion("TREATYNO >", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoGreaterThanOrEqualTo(String value) {
            addCriterion("TREATYNO >=", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoLessThan(String value) {
            addCriterion("TREATYNO <", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoLessThanOrEqualTo(String value) {
            addCriterion("TREATYNO <=", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoLike(String value) {
            addCriterion("TREATYNO like", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoNotLike(String value) {
            addCriterion("TREATYNO not like", value, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoIn(List<String> values) {
            addCriterion("TREATYNO in", values, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoNotIn(List<String> values) {
            addCriterion("TREATYNO not in", values, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoBetween(String value1, String value2) {
            addCriterion("TREATYNO between", value1, value2, "treatyno");
            return (Criteria) this;
        }

        public Criteria andTreatynoNotBetween(String value1, String value2) {
            addCriterion("TREATYNO not between", value1, value2, "treatyno");
            return (Criteria) this;
        }

        public Criteria andSectionnoIsNull() {
            addCriterion("SECTIONNO is null");
            return (Criteria) this;
        }

        public Criteria andSectionnoIsNotNull() {
            addCriterion("SECTIONNO is not null");
            return (Criteria) this;
        }

        public Criteria andSectionnoEqualTo(String value) {
            addCriterion("SECTIONNO =", value, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoNotEqualTo(String value) {
            addCriterion("SECTIONNO <>", value, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoGreaterThan(String value) {
            addCriterion("SECTIONNO >", value, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoGreaterThanOrEqualTo(String value) {
            addCriterion("SECTIONNO >=", value, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoLessThan(String value) {
            addCriterion("SECTIONNO <", value, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoLessThanOrEqualTo(String value) {
            addCriterion("SECTIONNO <=", value, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoLike(String value) {
            addCriterion("SECTIONNO like", value, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoNotLike(String value) {
            addCriterion("SECTIONNO not like", value, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoIn(List<String> values) {
            addCriterion("SECTIONNO in", values, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoNotIn(List<String> values) {
            addCriterion("SECTIONNO not in", values, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoBetween(String value1, String value2) {
            addCriterion("SECTIONNO between", value1, value2, "sectionno");
            return (Criteria) this;
        }

        public Criteria andSectionnoNotBetween(String value1, String value2) {
            addCriterion("SECTIONNO not between", value1, value2, "sectionno");
            return (Criteria) this;
        }

        public Criteria andClasscodeIsNull() {
            addCriterion("CLASSCODE is null");
            return (Criteria) this;
        }

        public Criteria andClasscodeIsNotNull() {
            addCriterion("CLASSCODE is not null");
            return (Criteria) this;
        }

        public Criteria andClasscodeEqualTo(String value) {
            addCriterion("CLASSCODE =", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeNotEqualTo(String value) {
            addCriterion("CLASSCODE <>", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeGreaterThan(String value) {
            addCriterion("CLASSCODE >", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeGreaterThanOrEqualTo(String value) {
            addCriterion("CLASSCODE >=", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeLessThan(String value) {
            addCriterion("CLASSCODE <", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeLessThanOrEqualTo(String value) {
            addCriterion("CLASSCODE <=", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeLike(String value) {
            addCriterion("CLASSCODE like", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeNotLike(String value) {
            addCriterion("CLASSCODE not like", value, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeIn(List<String> values) {
            addCriterion("CLASSCODE in", values, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeNotIn(List<String> values) {
            addCriterion("CLASSCODE not in", values, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeBetween(String value1, String value2) {
            addCriterion("CLASSCODE between", value1, value2, "classcode");
            return (Criteria) this;
        }

        public Criteria andClasscodeNotBetween(String value1, String value2) {
            addCriterion("CLASSCODE not between", value1, value2, "classcode");
            return (Criteria) this;
        }

        public Criteria andSectioncdescIsNull() {
            addCriterion("SECTIONCDESC is null");
            return (Criteria) this;
        }

        public Criteria andSectioncdescIsNotNull() {
            addCriterion("SECTIONCDESC is not null");
            return (Criteria) this;
        }

        public Criteria andSectioncdescEqualTo(String value) {
            addCriterion("SECTIONCDESC =", value, "sectioncdesc");
            return (Criteria) this;
        }

        public Criteria andSectioncdescNotEqualTo(String value) {
            addCriterion("SECTIONCDESC <>", value, "sectioncdesc");
            return (Criteria) this;
        }

        public Criteria andSectioncdescGreaterThan(String value) {
            addCriterion("SECTIONCDESC >", value, "sectioncdesc");
            return (Criteria) this;
        }

        public Criteria andSectioncdescGreaterThanOrEqualTo(String value) {
            addCriterion("SECTIONCDESC >=", value, "sectioncdesc");
            return (Criteria) this;
        }

        public Criteria andSectioncdescLessThan(String value) {
            addCriterion("SECTIONCDESC <", value, "sectioncdesc");
            return (Criteria) this;
        }

        public Criteria andSectioncdescLessThanOrEqualTo(String value) {
            addCriterion("SECTIONCDESC <=", value, "sectioncdesc");
            return (Criteria) this;
        }

        public Criteria andSectioncdescLike(String value) {
            addCriterion("SECTIONCDESC like", value, "sectioncdesc");
            return (Criteria) this;
        }

        public Criteria andSectioncdescNotLike(String value) {
            addCriterion("SECTIONCDESC not like", value, "sectioncdesc");
            return (Criteria) this;
        }

        public Criteria andSectioncdescIn(List<String> values) {
            addCriterion("SECTIONCDESC in", values, "sectioncdesc");
            return (Criteria) this;
        }

        public Criteria andSectioncdescNotIn(List<String> values) {
            addCriterion("SECTIONCDESC not in", values, "sectioncdesc");
            return (Criteria) this;
        }

        public Criteria andSectioncdescBetween(String value1, String value2) {
            addCriterion("SECTIONCDESC between", value1, value2, "sectioncdesc");
            return (Criteria) this;
        }

        public Criteria andSectioncdescNotBetween(String value1, String value2) {
            addCriterion("SECTIONCDESC not between", value1, value2, "sectioncdesc");
            return (Criteria) this;
        }

        public Criteria andSectionedescIsNull() {
            addCriterion("SECTIONEDESC is null");
            return (Criteria) this;
        }

        public Criteria andSectionedescIsNotNull() {
            addCriterion("SECTIONEDESC is not null");
            return (Criteria) this;
        }

        public Criteria andSectionedescEqualTo(String value) {
            addCriterion("SECTIONEDESC =", value, "sectionedesc");
            return (Criteria) this;
        }

        public Criteria andSectionedescNotEqualTo(String value) {
            addCriterion("SECTIONEDESC <>", value, "sectionedesc");
            return (Criteria) this;
        }

        public Criteria andSectionedescGreaterThan(String value) {
            addCriterion("SECTIONEDESC >", value, "sectionedesc");
            return (Criteria) this;
        }

        public Criteria andSectionedescGreaterThanOrEqualTo(String value) {
            addCriterion("SECTIONEDESC >=", value, "sectionedesc");
            return (Criteria) this;
        }

        public Criteria andSectionedescLessThan(String value) {
            addCriterion("SECTIONEDESC <", value, "sectionedesc");
            return (Criteria) this;
        }

        public Criteria andSectionedescLessThanOrEqualTo(String value) {
            addCriterion("SECTIONEDESC <=", value, "sectionedesc");
            return (Criteria) this;
        }

        public Criteria andSectionedescLike(String value) {
            addCriterion("SECTIONEDESC like", value, "sectionedesc");
            return (Criteria) this;
        }

        public Criteria andSectionedescNotLike(String value) {
            addCriterion("SECTIONEDESC not like", value, "sectionedesc");
            return (Criteria) this;
        }

        public Criteria andSectionedescIn(List<String> values) {
            addCriterion("SECTIONEDESC in", values, "sectionedesc");
            return (Criteria) this;
        }

        public Criteria andSectionedescNotIn(List<String> values) {
            addCriterion("SECTIONEDESC not in", values, "sectionedesc");
            return (Criteria) this;
        }

        public Criteria andSectionedescBetween(String value1, String value2) {
            addCriterion("SECTIONEDESC between", value1, value2, "sectionedesc");
            return (Criteria) this;
        }

        public Criteria andSectionedescNotBetween(String value1, String value2) {
            addCriterion("SECTIONEDESC not between", value1, value2, "sectionedesc");
            return (Criteria) this;
        }

        public Criteria andCurrencyIsNull() {
            addCriterion("CURRENCY is null");
            return (Criteria) this;
        }

        public Criteria andCurrencyIsNotNull() {
            addCriterion("CURRENCY is not null");
            return (Criteria) this;
        }

        public Criteria andCurrencyEqualTo(String value) {
            addCriterion("CURRENCY =", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyNotEqualTo(String value) {
            addCriterion("CURRENCY <>", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyGreaterThan(String value) {
            addCriterion("CURRENCY >", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyGreaterThanOrEqualTo(String value) {
            addCriterion("CURRENCY >=", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyLessThan(String value) {
            addCriterion("CURRENCY <", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyLessThanOrEqualTo(String value) {
            addCriterion("CURRENCY <=", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyLike(String value) {
            addCriterion("CURRENCY like", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyNotLike(String value) {
            addCriterion("CURRENCY not like", value, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyIn(List<String> values) {
            addCriterion("CURRENCY in", values, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyNotIn(List<String> values) {
            addCriterion("CURRENCY not in", values, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyBetween(String value1, String value2) {
            addCriterion("CURRENCY between", value1, value2, "currency");
            return (Criteria) this;
        }

        public Criteria andCurrencyNotBetween(String value1, String value2) {
            addCriterion("CURRENCY not between", value1, value2, "currency");
            return (Criteria) this;
        }

        public Criteria andRetentionvalueIsNull() {
            addCriterion("RETENTIONVALUE is null");
            return (Criteria) this;
        }

        public Criteria andRetentionvalueIsNotNull() {
            addCriterion("RETENTIONVALUE is not null");
            return (Criteria) this;
        }

        public Criteria andRetentionvalueEqualTo(BigDecimal value) {
            addCriterion("RETENTIONVALUE =", value, "retentionvalue");
            return (Criteria) this;
        }

        public Criteria andRetentionvalueNotEqualTo(BigDecimal value) {
            addCriterion("RETENTIONVALUE <>", value, "retentionvalue");
            return (Criteria) this;
        }

        public Criteria andRetentionvalueGreaterThan(BigDecimal value) {
            addCriterion("RETENTIONVALUE >", value, "retentionvalue");
            return (Criteria) this;
        }

        public Criteria andRetentionvalueGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("RETENTIONVALUE >=", value, "retentionvalue");
            return (Criteria) this;
        }

        public Criteria andRetentionvalueLessThan(BigDecimal value) {
            addCriterion("RETENTIONVALUE <", value, "retentionvalue");
            return (Criteria) this;
        }

        public Criteria andRetentionvalueLessThanOrEqualTo(BigDecimal value) {
            addCriterion("RETENTIONVALUE <=", value, "retentionvalue");
            return (Criteria) this;
        }

        public Criteria andRetentionvalueIn(List<BigDecimal> values) {
            addCriterion("RETENTIONVALUE in", values, "retentionvalue");
            return (Criteria) this;
        }

        public Criteria andRetentionvalueNotIn(List<BigDecimal> values) {
            addCriterion("RETENTIONVALUE not in", values, "retentionvalue");
            return (Criteria) this;
        }

        public Criteria andRetentionvalueBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("RETENTIONVALUE between", value1, value2, "retentionvalue");
            return (Criteria) this;
        }

        public Criteria andRetentionvalueNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("RETENTIONVALUE not between", value1, value2, "retentionvalue");
            return (Criteria) this;
        }

        public Criteria andReinsurerateIsNull() {
            addCriterion("REINSURERATE is null");
            return (Criteria) this;
        }

        public Criteria andReinsurerateIsNotNull() {
            addCriterion("REINSURERATE is not null");
            return (Criteria) this;
        }

        public Criteria andReinsurerateEqualTo(BigDecimal value) {
            addCriterion("REINSURERATE =", value, "reinsurerate");
            return (Criteria) this;
        }

        public Criteria andReinsurerateNotEqualTo(BigDecimal value) {
            addCriterion("REINSURERATE <>", value, "reinsurerate");
            return (Criteria) this;
        }

        public Criteria andReinsurerateGreaterThan(BigDecimal value) {
            addCriterion("REINSURERATE >", value, "reinsurerate");
            return (Criteria) this;
        }

        public Criteria andReinsurerateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("REINSURERATE >=", value, "reinsurerate");
            return (Criteria) this;
        }

        public Criteria andReinsurerateLessThan(BigDecimal value) {
            addCriterion("REINSURERATE <", value, "reinsurerate");
            return (Criteria) this;
        }

        public Criteria andReinsurerateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("REINSURERATE <=", value, "reinsurerate");
            return (Criteria) this;
        }

        public Criteria andReinsurerateIn(List<BigDecimal> values) {
            addCriterion("REINSURERATE in", values, "reinsurerate");
            return (Criteria) this;
        }

        public Criteria andReinsurerateNotIn(List<BigDecimal> values) {
            addCriterion("REINSURERATE not in", values, "reinsurerate");
            return (Criteria) this;
        }

        public Criteria andReinsurerateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("REINSURERATE between", value1, value2, "reinsurerate");
            return (Criteria) this;
        }

        public Criteria andReinsurerateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("REINSURERATE not between", value1, value2, "reinsurerate");
            return (Criteria) this;
        }

        public Criteria andInsharerateIsNull() {
            addCriterion("INSHARERATE is null");
            return (Criteria) this;
        }

        public Criteria andInsharerateIsNotNull() {
            addCriterion("INSHARERATE is not null");
            return (Criteria) this;
        }

        public Criteria andInsharerateEqualTo(BigDecimal value) {
            addCriterion("INSHARERATE =", value, "insharerate");
            return (Criteria) this;
        }

        public Criteria andInsharerateNotEqualTo(BigDecimal value) {
            addCriterion("INSHARERATE <>", value, "insharerate");
            return (Criteria) this;
        }

        public Criteria andInsharerateGreaterThan(BigDecimal value) {
            addCriterion("INSHARERATE >", value, "insharerate");
            return (Criteria) this;
        }

        public Criteria andInsharerateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("INSHARERATE >=", value, "insharerate");
            return (Criteria) this;
        }

        public Criteria andInsharerateLessThan(BigDecimal value) {
            addCriterion("INSHARERATE <", value, "insharerate");
            return (Criteria) this;
        }

        public Criteria andInsharerateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("INSHARERATE <=", value, "insharerate");
            return (Criteria) this;
        }

        public Criteria andInsharerateIn(List<BigDecimal> values) {
            addCriterion("INSHARERATE in", values, "insharerate");
            return (Criteria) this;
        }

        public Criteria andInsharerateNotIn(List<BigDecimal> values) {
            addCriterion("INSHARERATE not in", values, "insharerate");
            return (Criteria) this;
        }

        public Criteria andInsharerateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("INSHARERATE between", value1, value2, "insharerate");
            return (Criteria) this;
        }

        public Criteria andInsharerateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("INSHARERATE not between", value1, value2, "insharerate");
            return (Criteria) this;
        }

        public Criteria andLinesIsNull() {
            addCriterion("LINES is null");
            return (Criteria) this;
        }

        public Criteria andLinesIsNotNull() {
            addCriterion("LINES is not null");
            return (Criteria) this;
        }

        public Criteria andLinesEqualTo(BigDecimal value) {
            addCriterion("LINES =", value, "lines");
            return (Criteria) this;
        }

        public Criteria andLinesNotEqualTo(BigDecimal value) {
            addCriterion("LINES <>", value, "lines");
            return (Criteria) this;
        }

        public Criteria andLinesGreaterThan(BigDecimal value) {
            addCriterion("LINES >", value, "lines");
            return (Criteria) this;
        }

        public Criteria andLinesGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("LINES >=", value, "lines");
            return (Criteria) this;
        }

        public Criteria andLinesLessThan(BigDecimal value) {
            addCriterion("LINES <", value, "lines");
            return (Criteria) this;
        }

        public Criteria andLinesLessThanOrEqualTo(BigDecimal value) {
            addCriterion("LINES <=", value, "lines");
            return (Criteria) this;
        }

        public Criteria andLinesIn(List<BigDecimal> values) {
            addCriterion("LINES in", values, "lines");
            return (Criteria) this;
        }

        public Criteria andLinesNotIn(List<BigDecimal> values) {
            addCriterion("LINES not in", values, "lines");
            return (Criteria) this;
        }

        public Criteria andLinesBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("LINES between", value1, value2, "lines");
            return (Criteria) this;
        }

        public Criteria andLinesNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("LINES not between", value1, value2, "lines");
            return (Criteria) this;
        }

        public Criteria andLimitvalueIsNull() {
            addCriterion("LIMITVALUE is null");
            return (Criteria) this;
        }

        public Criteria andLimitvalueIsNotNull() {
            addCriterion("LIMITVALUE is not null");
            return (Criteria) this;
        }

        public Criteria andLimitvalueEqualTo(BigDecimal value) {
            addCriterion("LIMITVALUE =", value, "limitvalue");
            return (Criteria) this;
        }

        public Criteria andLimitvalueNotEqualTo(BigDecimal value) {
            addCriterion("LIMITVALUE <>", value, "limitvalue");
            return (Criteria) this;
        }

        public Criteria andLimitvalueGreaterThan(BigDecimal value) {
            addCriterion("LIMITVALUE >", value, "limitvalue");
            return (Criteria) this;
        }

        public Criteria andLimitvalueGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("LIMITVALUE >=", value, "limitvalue");
            return (Criteria) this;
        }

        public Criteria andLimitvalueLessThan(BigDecimal value) {
            addCriterion("LIMITVALUE <", value, "limitvalue");
            return (Criteria) this;
        }

        public Criteria andLimitvalueLessThanOrEqualTo(BigDecimal value) {
            addCriterion("LIMITVALUE <=", value, "limitvalue");
            return (Criteria) this;
        }

        public Criteria andLimitvalueIn(List<BigDecimal> values) {
            addCriterion("LIMITVALUE in", values, "limitvalue");
            return (Criteria) this;
        }

        public Criteria andLimitvalueNotIn(List<BigDecimal> values) {
            addCriterion("LIMITVALUE not in", values, "limitvalue");
            return (Criteria) this;
        }

        public Criteria andLimitvalueBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("LIMITVALUE between", value1, value2, "limitvalue");
            return (Criteria) this;
        }

        public Criteria andLimitvalueNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("LIMITVALUE not between", value1, value2, "limitvalue");
            return (Criteria) this;
        }

        public Criteria andLowerlimitvalueIsNull() {
            addCriterion("LOWERLIMITVALUE is null");
            return (Criteria) this;
        }

        public Criteria andLowerlimitvalueIsNotNull() {
            addCriterion("LOWERLIMITVALUE is not null");
            return (Criteria) this;
        }

        public Criteria andLowerlimitvalueEqualTo(BigDecimal value) {
            addCriterion("LOWERLIMITVALUE =", value, "lowerlimitvalue");
            return (Criteria) this;
        }

        public Criteria andLowerlimitvalueNotEqualTo(BigDecimal value) {
            addCriterion("LOWERLIMITVALUE <>", value, "lowerlimitvalue");
            return (Criteria) this;
        }

        public Criteria andLowerlimitvalueGreaterThan(BigDecimal value) {
            addCriterion("LOWERLIMITVALUE >", value, "lowerlimitvalue");
            return (Criteria) this;
        }

        public Criteria andLowerlimitvalueGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("LOWERLIMITVALUE >=", value, "lowerlimitvalue");
            return (Criteria) this;
        }

        public Criteria andLowerlimitvalueLessThan(BigDecimal value) {
            addCriterion("LOWERLIMITVALUE <", value, "lowerlimitvalue");
            return (Criteria) this;
        }

        public Criteria andLowerlimitvalueLessThanOrEqualTo(BigDecimal value) {
            addCriterion("LOWERLIMITVALUE <=", value, "lowerlimitvalue");
            return (Criteria) this;
        }

        public Criteria andLowerlimitvalueIn(List<BigDecimal> values) {
            addCriterion("LOWERLIMITVALUE in", values, "lowerlimitvalue");
            return (Criteria) this;
        }

        public Criteria andLowerlimitvalueNotIn(List<BigDecimal> values) {
            addCriterion("LOWERLIMITVALUE not in", values, "lowerlimitvalue");
            return (Criteria) this;
        }

        public Criteria andLowerlimitvalueBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("LOWERLIMITVALUE between", value1, value2, "lowerlimitvalue");
            return (Criteria) this;
        }

        public Criteria andLowerlimitvalueNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("LOWERLIMITVALUE not between", value1, value2, "lowerlimitvalue");
            return (Criteria) this;
        }

        public Criteria andCommflagIsNull() {
            addCriterion("COMMFLAG is null");
            return (Criteria) this;
        }

        public Criteria andCommflagIsNotNull() {
            addCriterion("COMMFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andCommflagEqualTo(String value) {
            addCriterion("COMMFLAG =", value, "commflag");
            return (Criteria) this;
        }

        public Criteria andCommflagNotEqualTo(String value) {
            addCriterion("COMMFLAG <>", value, "commflag");
            return (Criteria) this;
        }

        public Criteria andCommflagGreaterThan(String value) {
            addCriterion("COMMFLAG >", value, "commflag");
            return (Criteria) this;
        }

        public Criteria andCommflagGreaterThanOrEqualTo(String value) {
            addCriterion("COMMFLAG >=", value, "commflag");
            return (Criteria) this;
        }

        public Criteria andCommflagLessThan(String value) {
            addCriterion("COMMFLAG <", value, "commflag");
            return (Criteria) this;
        }

        public Criteria andCommflagLessThanOrEqualTo(String value) {
            addCriterion("COMMFLAG <=", value, "commflag");
            return (Criteria) this;
        }

        public Criteria andCommflagLike(String value) {
            addCriterion("COMMFLAG like", value, "commflag");
            return (Criteria) this;
        }

        public Criteria andCommflagNotLike(String value) {
            addCriterion("COMMFLAG not like", value, "commflag");
            return (Criteria) this;
        }

        public Criteria andCommflagIn(List<String> values) {
            addCriterion("COMMFLAG in", values, "commflag");
            return (Criteria) this;
        }

        public Criteria andCommflagNotIn(List<String> values) {
            addCriterion("COMMFLAG not in", values, "commflag");
            return (Criteria) this;
        }

        public Criteria andCommflagBetween(String value1, String value2) {
            addCriterion("COMMFLAG between", value1, value2, "commflag");
            return (Criteria) this;
        }

        public Criteria andCommflagNotBetween(String value1, String value2) {
            addCriterion("COMMFLAG not between", value1, value2, "commflag");
            return (Criteria) this;
        }

        public Criteria andLargelossvalueIsNull() {
            addCriterion("LARGELOSSVALUE is null");
            return (Criteria) this;
        }

        public Criteria andLargelossvalueIsNotNull() {
            addCriterion("LARGELOSSVALUE is not null");
            return (Criteria) this;
        }

        public Criteria andLargelossvalueEqualTo(BigDecimal value) {
            addCriterion("LARGELOSSVALUE =", value, "largelossvalue");
            return (Criteria) this;
        }

        public Criteria andLargelossvalueNotEqualTo(BigDecimal value) {
            addCriterion("LARGELOSSVALUE <>", value, "largelossvalue");
            return (Criteria) this;
        }

        public Criteria andLargelossvalueGreaterThan(BigDecimal value) {
            addCriterion("LARGELOSSVALUE >", value, "largelossvalue");
            return (Criteria) this;
        }

        public Criteria andLargelossvalueGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("LARGELOSSVALUE >=", value, "largelossvalue");
            return (Criteria) this;
        }

        public Criteria andLargelossvalueLessThan(BigDecimal value) {
            addCriterion("LARGELOSSVALUE <", value, "largelossvalue");
            return (Criteria) this;
        }

        public Criteria andLargelossvalueLessThanOrEqualTo(BigDecimal value) {
            addCriterion("LARGELOSSVALUE <=", value, "largelossvalue");
            return (Criteria) this;
        }

        public Criteria andLargelossvalueIn(List<BigDecimal> values) {
            addCriterion("LARGELOSSVALUE in", values, "largelossvalue");
            return (Criteria) this;
        }

        public Criteria andLargelossvalueNotIn(List<BigDecimal> values) {
            addCriterion("LARGELOSSVALUE not in", values, "largelossvalue");
            return (Criteria) this;
        }

        public Criteria andLargelossvalueBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("LARGELOSSVALUE between", value1, value2, "largelossvalue");
            return (Criteria) this;
        }

        public Criteria andLargelossvalueNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("LARGELOSSVALUE not between", value1, value2, "largelossvalue");
            return (Criteria) this;
        }

        public Criteria andCashlossvalueIsNull() {
            addCriterion("CASHLOSSVALUE is null");
            return (Criteria) this;
        }

        public Criteria andCashlossvalueIsNotNull() {
            addCriterion("CASHLOSSVALUE is not null");
            return (Criteria) this;
        }

        public Criteria andCashlossvalueEqualTo(BigDecimal value) {
            addCriterion("CASHLOSSVALUE =", value, "cashlossvalue");
            return (Criteria) this;
        }

        public Criteria andCashlossvalueNotEqualTo(BigDecimal value) {
            addCriterion("CASHLOSSVALUE <>", value, "cashlossvalue");
            return (Criteria) this;
        }

        public Criteria andCashlossvalueGreaterThan(BigDecimal value) {
            addCriterion("CASHLOSSVALUE >", value, "cashlossvalue");
            return (Criteria) this;
        }

        public Criteria andCashlossvalueGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("CASHLOSSVALUE >=", value, "cashlossvalue");
            return (Criteria) this;
        }

        public Criteria andCashlossvalueLessThan(BigDecimal value) {
            addCriterion("CASHLOSSVALUE <", value, "cashlossvalue");
            return (Criteria) this;
        }

        public Criteria andCashlossvalueLessThanOrEqualTo(BigDecimal value) {
            addCriterion("CASHLOSSVALUE <=", value, "cashlossvalue");
            return (Criteria) this;
        }

        public Criteria andCashlossvalueIn(List<BigDecimal> values) {
            addCriterion("CASHLOSSVALUE in", values, "cashlossvalue");
            return (Criteria) this;
        }

        public Criteria andCashlossvalueNotIn(List<BigDecimal> values) {
            addCriterion("CASHLOSSVALUE not in", values, "cashlossvalue");
            return (Criteria) this;
        }

        public Criteria andCashlossvalueBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("CASHLOSSVALUE between", value1, value2, "cashlossvalue");
            return (Criteria) this;
        }

        public Criteria andCashlossvalueNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("CASHLOSSVALUE not between", value1, value2, "cashlossvalue");
            return (Criteria) this;
        }

        public Criteria andCleanmodeIsNull() {
            addCriterion("CLEANMODE is null");
            return (Criteria) this;
        }

        public Criteria andCleanmodeIsNotNull() {
            addCriterion("CLEANMODE is not null");
            return (Criteria) this;
        }

        public Criteria andCleanmodeEqualTo(String value) {
            addCriterion("CLEANMODE =", value, "cleanmode");
            return (Criteria) this;
        }

        public Criteria andCleanmodeNotEqualTo(String value) {
            addCriterion("CLEANMODE <>", value, "cleanmode");
            return (Criteria) this;
        }

        public Criteria andCleanmodeGreaterThan(String value) {
            addCriterion("CLEANMODE >", value, "cleanmode");
            return (Criteria) this;
        }

        public Criteria andCleanmodeGreaterThanOrEqualTo(String value) {
            addCriterion("CLEANMODE >=", value, "cleanmode");
            return (Criteria) this;
        }

        public Criteria andCleanmodeLessThan(String value) {
            addCriterion("CLEANMODE <", value, "cleanmode");
            return (Criteria) this;
        }

        public Criteria andCleanmodeLessThanOrEqualTo(String value) {
            addCriterion("CLEANMODE <=", value, "cleanmode");
            return (Criteria) this;
        }

        public Criteria andCleanmodeLike(String value) {
            addCriterion("CLEANMODE like", value, "cleanmode");
            return (Criteria) this;
        }

        public Criteria andCleanmodeNotLike(String value) {
            addCriterion("CLEANMODE not like", value, "cleanmode");
            return (Criteria) this;
        }

        public Criteria andCleanmodeIn(List<String> values) {
            addCriterion("CLEANMODE in", values, "cleanmode");
            return (Criteria) this;
        }

        public Criteria andCleanmodeNotIn(List<String> values) {
            addCriterion("CLEANMODE not in", values, "cleanmode");
            return (Criteria) this;
        }

        public Criteria andCleanmodeBetween(String value1, String value2) {
            addCriterion("CLEANMODE between", value1, value2, "cleanmode");
            return (Criteria) this;
        }

        public Criteria andCleanmodeNotBetween(String value1, String value2) {
            addCriterion("CLEANMODE not between", value1, value2, "cleanmode");
            return (Criteria) this;
        }

        public Criteria andMaincoinsrateIsNull() {
            addCriterion("MAINCOINSRATE is null");
            return (Criteria) this;
        }

        public Criteria andMaincoinsrateIsNotNull() {
            addCriterion("MAINCOINSRATE is not null");
            return (Criteria) this;
        }

        public Criteria andMaincoinsrateEqualTo(BigDecimal value) {
            addCriterion("MAINCOINSRATE =", value, "maincoinsrate");
            return (Criteria) this;
        }

        public Criteria andMaincoinsrateNotEqualTo(BigDecimal value) {
            addCriterion("MAINCOINSRATE <>", value, "maincoinsrate");
            return (Criteria) this;
        }

        public Criteria andMaincoinsrateGreaterThan(BigDecimal value) {
            addCriterion("MAINCOINSRATE >", value, "maincoinsrate");
            return (Criteria) this;
        }

        public Criteria andMaincoinsrateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("MAINCOINSRATE >=", value, "maincoinsrate");
            return (Criteria) this;
        }

        public Criteria andMaincoinsrateLessThan(BigDecimal value) {
            addCriterion("MAINCOINSRATE <", value, "maincoinsrate");
            return (Criteria) this;
        }

        public Criteria andMaincoinsrateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("MAINCOINSRATE <=", value, "maincoinsrate");
            return (Criteria) this;
        }

        public Criteria andMaincoinsrateIn(List<BigDecimal> values) {
            addCriterion("MAINCOINSRATE in", values, "maincoinsrate");
            return (Criteria) this;
        }

        public Criteria andMaincoinsrateNotIn(List<BigDecimal> values) {
            addCriterion("MAINCOINSRATE not in", values, "maincoinsrate");
            return (Criteria) this;
        }

        public Criteria andMaincoinsrateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("MAINCOINSRATE between", value1, value2, "maincoinsrate");
            return (Criteria) this;
        }

        public Criteria andMaincoinsrateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("MAINCOINSRATE not between", value1, value2, "maincoinsrate");
            return (Criteria) this;
        }

        public Criteria andSubcoinsrateIsNull() {
            addCriterion("SUBCOINSRATE is null");
            return (Criteria) this;
        }

        public Criteria andSubcoinsrateIsNotNull() {
            addCriterion("SUBCOINSRATE is not null");
            return (Criteria) this;
        }

        public Criteria andSubcoinsrateEqualTo(BigDecimal value) {
            addCriterion("SUBCOINSRATE =", value, "subcoinsrate");
            return (Criteria) this;
        }

        public Criteria andSubcoinsrateNotEqualTo(BigDecimal value) {
            addCriterion("SUBCOINSRATE <>", value, "subcoinsrate");
            return (Criteria) this;
        }

        public Criteria andSubcoinsrateGreaterThan(BigDecimal value) {
            addCriterion("SUBCOINSRATE >", value, "subcoinsrate");
            return (Criteria) this;
        }

        public Criteria andSubcoinsrateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("SUBCOINSRATE >=", value, "subcoinsrate");
            return (Criteria) this;
        }

        public Criteria andSubcoinsrateLessThan(BigDecimal value) {
            addCriterion("SUBCOINSRATE <", value, "subcoinsrate");
            return (Criteria) this;
        }

        public Criteria andSubcoinsrateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("SUBCOINSRATE <=", value, "subcoinsrate");
            return (Criteria) this;
        }

        public Criteria andSubcoinsrateIn(List<BigDecimal> values) {
            addCriterion("SUBCOINSRATE in", values, "subcoinsrate");
            return (Criteria) this;
        }

        public Criteria andSubcoinsrateNotIn(List<BigDecimal> values) {
            addCriterion("SUBCOINSRATE not in", values, "subcoinsrate");
            return (Criteria) this;
        }

        public Criteria andSubcoinsrateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("SUBCOINSRATE between", value1, value2, "subcoinsrate");
            return (Criteria) this;
        }

        public Criteria andSubcoinsrateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("SUBCOINSRATE not between", value1, value2, "subcoinsrate");
            return (Criteria) this;
        }

        public Criteria andShareholderrateIsNull() {
            addCriterion("SHAREHOLDERRATE is null");
            return (Criteria) this;
        }

        public Criteria andShareholderrateIsNotNull() {
            addCriterion("SHAREHOLDERRATE is not null");
            return (Criteria) this;
        }

        public Criteria andShareholderrateEqualTo(BigDecimal value) {
            addCriterion("SHAREHOLDERRATE =", value, "shareholderrate");
            return (Criteria) this;
        }

        public Criteria andShareholderrateNotEqualTo(BigDecimal value) {
            addCriterion("SHAREHOLDERRATE <>", value, "shareholderrate");
            return (Criteria) this;
        }

        public Criteria andShareholderrateGreaterThan(BigDecimal value) {
            addCriterion("SHAREHOLDERRATE >", value, "shareholderrate");
            return (Criteria) this;
        }

        public Criteria andShareholderrateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("SHAREHOLDERRATE >=", value, "shareholderrate");
            return (Criteria) this;
        }

        public Criteria andShareholderrateLessThan(BigDecimal value) {
            addCriterion("SHAREHOLDERRATE <", value, "shareholderrate");
            return (Criteria) this;
        }

        public Criteria andShareholderrateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("SHAREHOLDERRATE <=", value, "shareholderrate");
            return (Criteria) this;
        }

        public Criteria andShareholderrateIn(List<BigDecimal> values) {
            addCriterion("SHAREHOLDERRATE in", values, "shareholderrate");
            return (Criteria) this;
        }

        public Criteria andShareholderrateNotIn(List<BigDecimal> values) {
            addCriterion("SHAREHOLDERRATE not in", values, "shareholderrate");
            return (Criteria) this;
        }

        public Criteria andShareholderrateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("SHAREHOLDERRATE between", value1, value2, "shareholderrate");
            return (Criteria) this;
        }

        public Criteria andShareholderrateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("SHAREHOLDERRATE not between", value1, value2, "shareholderrate");
            return (Criteria) this;
        }

        public Criteria andShareholderlinesIsNull() {
            addCriterion("SHAREHOLDERLINES is null");
            return (Criteria) this;
        }

        public Criteria andShareholderlinesIsNotNull() {
            addCriterion("SHAREHOLDERLINES is not null");
            return (Criteria) this;
        }

        public Criteria andShareholderlinesEqualTo(BigDecimal value) {
            addCriterion("SHAREHOLDERLINES =", value, "shareholderlines");
            return (Criteria) this;
        }

        public Criteria andShareholderlinesNotEqualTo(BigDecimal value) {
            addCriterion("SHAREHOLDERLINES <>", value, "shareholderlines");
            return (Criteria) this;
        }

        public Criteria andShareholderlinesGreaterThan(BigDecimal value) {
            addCriterion("SHAREHOLDERLINES >", value, "shareholderlines");
            return (Criteria) this;
        }

        public Criteria andShareholderlinesGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("SHAREHOLDERLINES >=", value, "shareholderlines");
            return (Criteria) this;
        }

        public Criteria andShareholderlinesLessThan(BigDecimal value) {
            addCriterion("SHAREHOLDERLINES <", value, "shareholderlines");
            return (Criteria) this;
        }

        public Criteria andShareholderlinesLessThanOrEqualTo(BigDecimal value) {
            addCriterion("SHAREHOLDERLINES <=", value, "shareholderlines");
            return (Criteria) this;
        }

        public Criteria andShareholderlinesIn(List<BigDecimal> values) {
            addCriterion("SHAREHOLDERLINES in", values, "shareholderlines");
            return (Criteria) this;
        }

        public Criteria andShareholderlinesNotIn(List<BigDecimal> values) {
            addCriterion("SHAREHOLDERLINES not in", values, "shareholderlines");
            return (Criteria) this;
        }

        public Criteria andShareholderlinesBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("SHAREHOLDERLINES between", value1, value2, "shareholderlines");
            return (Criteria) this;
        }

        public Criteria andShareholderlinesNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("SHAREHOLDERLINES not between", value1, value2, "shareholderlines");
            return (Criteria) this;
        }

        public Criteria andInrateIsNull() {
            addCriterion("INRATE is null");
            return (Criteria) this;
        }

        public Criteria andInrateIsNotNull() {
            addCriterion("INRATE is not null");
            return (Criteria) this;
        }

        public Criteria andInrateEqualTo(BigDecimal value) {
            addCriterion("INRATE =", value, "inrate");
            return (Criteria) this;
        }

        public Criteria andInrateNotEqualTo(BigDecimal value) {
            addCriterion("INRATE <>", value, "inrate");
            return (Criteria) this;
        }

        public Criteria andInrateGreaterThan(BigDecimal value) {
            addCriterion("INRATE >", value, "inrate");
            return (Criteria) this;
        }

        public Criteria andInrateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("INRATE >=", value, "inrate");
            return (Criteria) this;
        }

        public Criteria andInrateLessThan(BigDecimal value) {
            addCriterion("INRATE <", value, "inrate");
            return (Criteria) this;
        }

        public Criteria andInrateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("INRATE <=", value, "inrate");
            return (Criteria) this;
        }

        public Criteria andInrateIn(List<BigDecimal> values) {
            addCriterion("INRATE in", values, "inrate");
            return (Criteria) this;
        }

        public Criteria andInrateNotIn(List<BigDecimal> values) {
            addCriterion("INRATE not in", values, "inrate");
            return (Criteria) this;
        }

        public Criteria andInrateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("INRATE between", value1, value2, "inrate");
            return (Criteria) this;
        }

        public Criteria andInrateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("INRATE not between", value1, value2, "inrate");
            return (Criteria) this;
        }

        public Criteria andPccleanmodeIsNull() {
            addCriterion("PCCLEANMODE is null");
            return (Criteria) this;
        }

        public Criteria andPccleanmodeIsNotNull() {
            addCriterion("PCCLEANMODE is not null");
            return (Criteria) this;
        }

        public Criteria andPccleanmodeEqualTo(String value) {
            addCriterion("PCCLEANMODE =", value, "pccleanmode");
            return (Criteria) this;
        }

        public Criteria andPccleanmodeNotEqualTo(String value) {
            addCriterion("PCCLEANMODE <>", value, "pccleanmode");
            return (Criteria) this;
        }

        public Criteria andPccleanmodeGreaterThan(String value) {
            addCriterion("PCCLEANMODE >", value, "pccleanmode");
            return (Criteria) this;
        }

        public Criteria andPccleanmodeGreaterThanOrEqualTo(String value) {
            addCriterion("PCCLEANMODE >=", value, "pccleanmode");
            return (Criteria) this;
        }

        public Criteria andPccleanmodeLessThan(String value) {
            addCriterion("PCCLEANMODE <", value, "pccleanmode");
            return (Criteria) this;
        }

        public Criteria andPccleanmodeLessThanOrEqualTo(String value) {
            addCriterion("PCCLEANMODE <=", value, "pccleanmode");
            return (Criteria) this;
        }

        public Criteria andPccleanmodeLike(String value) {
            addCriterion("PCCLEANMODE like", value, "pccleanmode");
            return (Criteria) this;
        }

        public Criteria andPccleanmodeNotLike(String value) {
            addCriterion("PCCLEANMODE not like", value, "pccleanmode");
            return (Criteria) this;
        }

        public Criteria andPccleanmodeIn(List<String> values) {
            addCriterion("PCCLEANMODE in", values, "pccleanmode");
            return (Criteria) this;
        }

        public Criteria andPccleanmodeNotIn(List<String> values) {
            addCriterion("PCCLEANMODE not in", values, "pccleanmode");
            return (Criteria) this;
        }

        public Criteria andPccleanmodeBetween(String value1, String value2) {
            addCriterion("PCCLEANMODE between", value1, value2, "pccleanmode");
            return (Criteria) this;
        }

        public Criteria andPccleanmodeNotBetween(String value1, String value2) {
            addCriterion("PCCLEANMODE not between", value1, value2, "pccleanmode");
            return (Criteria) this;
        }

        public Criteria andAccmodeIsNull() {
            addCriterion("ACCMODE is null");
            return (Criteria) this;
        }

        public Criteria andAccmodeIsNotNull() {
            addCriterion("ACCMODE is not null");
            return (Criteria) this;
        }

        public Criteria andAccmodeEqualTo(String value) {
            addCriterion("ACCMODE =", value, "accmode");
            return (Criteria) this;
        }

        public Criteria andAccmodeNotEqualTo(String value) {
            addCriterion("ACCMODE <>", value, "accmode");
            return (Criteria) this;
        }

        public Criteria andAccmodeGreaterThan(String value) {
            addCriterion("ACCMODE >", value, "accmode");
            return (Criteria) this;
        }

        public Criteria andAccmodeGreaterThanOrEqualTo(String value) {
            addCriterion("ACCMODE >=", value, "accmode");
            return (Criteria) this;
        }

        public Criteria andAccmodeLessThan(String value) {
            addCriterion("ACCMODE <", value, "accmode");
            return (Criteria) this;
        }

        public Criteria andAccmodeLessThanOrEqualTo(String value) {
            addCriterion("ACCMODE <=", value, "accmode");
            return (Criteria) this;
        }

        public Criteria andAccmodeLike(String value) {
            addCriterion("ACCMODE like", value, "accmode");
            return (Criteria) this;
        }

        public Criteria andAccmodeNotLike(String value) {
            addCriterion("ACCMODE not like", value, "accmode");
            return (Criteria) this;
        }

        public Criteria andAccmodeIn(List<String> values) {
            addCriterion("ACCMODE in", values, "accmode");
            return (Criteria) this;
        }

        public Criteria andAccmodeNotIn(List<String> values) {
            addCriterion("ACCMODE not in", values, "accmode");
            return (Criteria) this;
        }

        public Criteria andAccmodeBetween(String value1, String value2) {
            addCriterion("ACCMODE between", value1, value2, "accmode");
            return (Criteria) this;
        }

        public Criteria andAccmodeNotBetween(String value1, String value2) {
            addCriterion("ACCMODE not between", value1, value2, "accmode");
            return (Criteria) this;
        }

        public Criteria andOslossrateIsNull() {
            addCriterion("OSLOSSRATE is null");
            return (Criteria) this;
        }

        public Criteria andOslossrateIsNotNull() {
            addCriterion("OSLOSSRATE is not null");
            return (Criteria) this;
        }

        public Criteria andOslossrateEqualTo(BigDecimal value) {
            addCriterion("OSLOSSRATE =", value, "oslossrate");
            return (Criteria) this;
        }

        public Criteria andOslossrateNotEqualTo(BigDecimal value) {
            addCriterion("OSLOSSRATE <>", value, "oslossrate");
            return (Criteria) this;
        }

        public Criteria andOslossrateGreaterThan(BigDecimal value) {
            addCriterion("OSLOSSRATE >", value, "oslossrate");
            return (Criteria) this;
        }

        public Criteria andOslossrateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("OSLOSSRATE >=", value, "oslossrate");
            return (Criteria) this;
        }

        public Criteria andOslossrateLessThan(BigDecimal value) {
            addCriterion("OSLOSSRATE <", value, "oslossrate");
            return (Criteria) this;
        }

        public Criteria andOslossrateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("OSLOSSRATE <=", value, "oslossrate");
            return (Criteria) this;
        }

        public Criteria andOslossrateIn(List<BigDecimal> values) {
            addCriterion("OSLOSSRATE in", values, "oslossrate");
            return (Criteria) this;
        }

        public Criteria andOslossrateNotIn(List<BigDecimal> values) {
            addCriterion("OSLOSSRATE not in", values, "oslossrate");
            return (Criteria) this;
        }

        public Criteria andOslossrateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("OSLOSSRATE between", value1, value2, "oslossrate");
            return (Criteria) this;
        }

        public Criteria andOslossrateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("OSLOSSRATE not between", value1, value2, "oslossrate");
            return (Criteria) this;
        }

        public Criteria andExpenserateIsNull() {
            addCriterion("EXPENSERATE is null");
            return (Criteria) this;
        }

        public Criteria andExpenserateIsNotNull() {
            addCriterion("EXPENSERATE is not null");
            return (Criteria) this;
        }

        public Criteria andExpenserateEqualTo(BigDecimal value) {
            addCriterion("EXPENSERATE =", value, "expenserate");
            return (Criteria) this;
        }

        public Criteria andExpenserateNotEqualTo(BigDecimal value) {
            addCriterion("EXPENSERATE <>", value, "expenserate");
            return (Criteria) this;
        }

        public Criteria andExpenserateGreaterThan(BigDecimal value) {
            addCriterion("EXPENSERATE >", value, "expenserate");
            return (Criteria) this;
        }

        public Criteria andExpenserateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("EXPENSERATE >=", value, "expenserate");
            return (Criteria) this;
        }

        public Criteria andExpenserateLessThan(BigDecimal value) {
            addCriterion("EXPENSERATE <", value, "expenserate");
            return (Criteria) this;
        }

        public Criteria andExpenserateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("EXPENSERATE <=", value, "expenserate");
            return (Criteria) this;
        }

        public Criteria andExpenserateIn(List<BigDecimal> values) {
            addCriterion("EXPENSERATE in", values, "expenserate");
            return (Criteria) this;
        }

        public Criteria andExpenserateNotIn(List<BigDecimal> values) {
            addCriterion("EXPENSERATE not in", values, "expenserate");
            return (Criteria) this;
        }

        public Criteria andExpenserateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("EXPENSERATE between", value1, value2, "expenserate");
            return (Criteria) this;
        }

        public Criteria andExpenserateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("EXPENSERATE not between", value1, value2, "expenserate");
            return (Criteria) this;
        }

        public Criteria andCarriedyrsIsNull() {
            addCriterion("CARRIEDYRS is null");
            return (Criteria) this;
        }

        public Criteria andCarriedyrsIsNotNull() {
            addCriterion("CARRIEDYRS is not null");
            return (Criteria) this;
        }

        public Criteria andCarriedyrsEqualTo(Short value) {
            addCriterion("CARRIEDYRS =", value, "carriedyrs");
            return (Criteria) this;
        }

        public Criteria andCarriedyrsNotEqualTo(Short value) {
            addCriterion("CARRIEDYRS <>", value, "carriedyrs");
            return (Criteria) this;
        }

        public Criteria andCarriedyrsGreaterThan(Short value) {
            addCriterion("CARRIEDYRS >", value, "carriedyrs");
            return (Criteria) this;
        }

        public Criteria andCarriedyrsGreaterThanOrEqualTo(Short value) {
            addCriterion("CARRIEDYRS >=", value, "carriedyrs");
            return (Criteria) this;
        }

        public Criteria andCarriedyrsLessThan(Short value) {
            addCriterion("CARRIEDYRS <", value, "carriedyrs");
            return (Criteria) this;
        }

        public Criteria andCarriedyrsLessThanOrEqualTo(Short value) {
            addCriterion("CARRIEDYRS <=", value, "carriedyrs");
            return (Criteria) this;
        }

        public Criteria andCarriedyrsIn(List<Short> values) {
            addCriterion("CARRIEDYRS in", values, "carriedyrs");
            return (Criteria) this;
        }

        public Criteria andCarriedyrsNotIn(List<Short> values) {
            addCriterion("CARRIEDYRS not in", values, "carriedyrs");
            return (Criteria) this;
        }

        public Criteria andCarriedyrsBetween(Short value1, Short value2) {
            addCriterion("CARRIEDYRS between", value1, value2, "carriedyrs");
            return (Criteria) this;
        }

        public Criteria andCarriedyrsNotBetween(Short value1, Short value2) {
            addCriterion("CARRIEDYRS not between", value1, value2, "carriedyrs");
            return (Criteria) this;
        }

        public Criteria andPcstartmthsIsNull() {
            addCriterion("PCSTARTMTHS is null");
            return (Criteria) this;
        }

        public Criteria andPcstartmthsIsNotNull() {
            addCriterion("PCSTARTMTHS is not null");
            return (Criteria) this;
        }

        public Criteria andPcstartmthsEqualTo(Short value) {
            addCriterion("PCSTARTMTHS =", value, "pcstartmths");
            return (Criteria) this;
        }

        public Criteria andPcstartmthsNotEqualTo(Short value) {
            addCriterion("PCSTARTMTHS <>", value, "pcstartmths");
            return (Criteria) this;
        }

        public Criteria andPcstartmthsGreaterThan(Short value) {
            addCriterion("PCSTARTMTHS >", value, "pcstartmths");
            return (Criteria) this;
        }

        public Criteria andPcstartmthsGreaterThanOrEqualTo(Short value) {
            addCriterion("PCSTARTMTHS >=", value, "pcstartmths");
            return (Criteria) this;
        }

        public Criteria andPcstartmthsLessThan(Short value) {
            addCriterion("PCSTARTMTHS <", value, "pcstartmths");
            return (Criteria) this;
        }

        public Criteria andPcstartmthsLessThanOrEqualTo(Short value) {
            addCriterion("PCSTARTMTHS <=", value, "pcstartmths");
            return (Criteria) this;
        }

        public Criteria andPcstartmthsIn(List<Short> values) {
            addCriterion("PCSTARTMTHS in", values, "pcstartmths");
            return (Criteria) this;
        }

        public Criteria andPcstartmthsNotIn(List<Short> values) {
            addCriterion("PCSTARTMTHS not in", values, "pcstartmths");
            return (Criteria) this;
        }

        public Criteria andPcstartmthsBetween(Short value1, Short value2) {
            addCriterion("PCSTARTMTHS between", value1, value2, "pcstartmths");
            return (Criteria) this;
        }

        public Criteria andPcstartmthsNotBetween(Short value1, Short value2) {
            addCriterion("PCSTARTMTHS not between", value1, value2, "pcstartmths");
            return (Criteria) this;
        }

        public Criteria andExpenseindIsNull() {
            addCriterion("EXPENSEIND is null");
            return (Criteria) this;
        }

        public Criteria andExpenseindIsNotNull() {
            addCriterion("EXPENSEIND is not null");
            return (Criteria) this;
        }

        public Criteria andExpenseindEqualTo(String value) {
            addCriterion("EXPENSEIND =", value, "expenseind");
            return (Criteria) this;
        }

        public Criteria andExpenseindNotEqualTo(String value) {
            addCriterion("EXPENSEIND <>", value, "expenseind");
            return (Criteria) this;
        }

        public Criteria andExpenseindGreaterThan(String value) {
            addCriterion("EXPENSEIND >", value, "expenseind");
            return (Criteria) this;
        }

        public Criteria andExpenseindGreaterThanOrEqualTo(String value) {
            addCriterion("EXPENSEIND >=", value, "expenseind");
            return (Criteria) this;
        }

        public Criteria andExpenseindLessThan(String value) {
            addCriterion("EXPENSEIND <", value, "expenseind");
            return (Criteria) this;
        }

        public Criteria andExpenseindLessThanOrEqualTo(String value) {
            addCriterion("EXPENSEIND <=", value, "expenseind");
            return (Criteria) this;
        }

        public Criteria andExpenseindLike(String value) {
            addCriterion("EXPENSEIND like", value, "expenseind");
            return (Criteria) this;
        }

        public Criteria andExpenseindNotLike(String value) {
            addCriterion("EXPENSEIND not like", value, "expenseind");
            return (Criteria) this;
        }

        public Criteria andExpenseindIn(List<String> values) {
            addCriterion("EXPENSEIND in", values, "expenseind");
            return (Criteria) this;
        }

        public Criteria andExpenseindNotIn(List<String> values) {
            addCriterion("EXPENSEIND not in", values, "expenseind");
            return (Criteria) this;
        }

        public Criteria andExpenseindBetween(String value1, String value2) {
            addCriterion("EXPENSEIND between", value1, value2, "expenseind");
            return (Criteria) this;
        }

        public Criteria andExpenseindNotBetween(String value1, String value2) {
            addCriterion("EXPENSEIND not between", value1, value2, "expenseind");
            return (Criteria) this;
        }

        public Criteria andPcadjustmethIsNull() {
            addCriterion("PCADJUSTMETH is null");
            return (Criteria) this;
        }

        public Criteria andPcadjustmethIsNotNull() {
            addCriterion("PCADJUSTMETH is not null");
            return (Criteria) this;
        }

        public Criteria andPcadjustmethEqualTo(String value) {
            addCriterion("PCADJUSTMETH =", value, "pcadjustmeth");
            return (Criteria) this;
        }

        public Criteria andPcadjustmethNotEqualTo(String value) {
            addCriterion("PCADJUSTMETH <>", value, "pcadjustmeth");
            return (Criteria) this;
        }

        public Criteria andPcadjustmethGreaterThan(String value) {
            addCriterion("PCADJUSTMETH >", value, "pcadjustmeth");
            return (Criteria) this;
        }

        public Criteria andPcadjustmethGreaterThanOrEqualTo(String value) {
            addCriterion("PCADJUSTMETH >=", value, "pcadjustmeth");
            return (Criteria) this;
        }

        public Criteria andPcadjustmethLessThan(String value) {
            addCriterion("PCADJUSTMETH <", value, "pcadjustmeth");
            return (Criteria) this;
        }

        public Criteria andPcadjustmethLessThanOrEqualTo(String value) {
            addCriterion("PCADJUSTMETH <=", value, "pcadjustmeth");
            return (Criteria) this;
        }

        public Criteria andPcadjustmethLike(String value) {
            addCriterion("PCADJUSTMETH like", value, "pcadjustmeth");
            return (Criteria) this;
        }

        public Criteria andPcadjustmethNotLike(String value) {
            addCriterion("PCADJUSTMETH not like", value, "pcadjustmeth");
            return (Criteria) this;
        }

        public Criteria andPcadjustmethIn(List<String> values) {
            addCriterion("PCADJUSTMETH in", values, "pcadjustmeth");
            return (Criteria) this;
        }

        public Criteria andPcadjustmethNotIn(List<String> values) {
            addCriterion("PCADJUSTMETH not in", values, "pcadjustmeth");
            return (Criteria) this;
        }

        public Criteria andPcadjustmethBetween(String value1, String value2) {
            addCriterion("PCADJUSTMETH between", value1, value2, "pcadjustmeth");
            return (Criteria) this;
        }

        public Criteria andPcadjustmethNotBetween(String value1, String value2) {
            addCriterion("PCADJUSTMETH not between", value1, value2, "pcadjustmeth");
            return (Criteria) this;
        }

        public Criteria andPcminrateIsNull() {
            addCriterion("PCMINRATE is null");
            return (Criteria) this;
        }

        public Criteria andPcminrateIsNotNull() {
            addCriterion("PCMINRATE is not null");
            return (Criteria) this;
        }

        public Criteria andPcminrateEqualTo(BigDecimal value) {
            addCriterion("PCMINRATE =", value, "pcminrate");
            return (Criteria) this;
        }

        public Criteria andPcminrateNotEqualTo(BigDecimal value) {
            addCriterion("PCMINRATE <>", value, "pcminrate");
            return (Criteria) this;
        }

        public Criteria andPcminrateGreaterThan(BigDecimal value) {
            addCriterion("PCMINRATE >", value, "pcminrate");
            return (Criteria) this;
        }

        public Criteria andPcminrateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("PCMINRATE >=", value, "pcminrate");
            return (Criteria) this;
        }

        public Criteria andPcminrateLessThan(BigDecimal value) {
            addCriterion("PCMINRATE <", value, "pcminrate");
            return (Criteria) this;
        }

        public Criteria andPcminrateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("PCMINRATE <=", value, "pcminrate");
            return (Criteria) this;
        }

        public Criteria andPcminrateIn(List<BigDecimal> values) {
            addCriterion("PCMINRATE in", values, "pcminrate");
            return (Criteria) this;
        }

        public Criteria andPcminrateNotIn(List<BigDecimal> values) {
            addCriterion("PCMINRATE not in", values, "pcminrate");
            return (Criteria) this;
        }

        public Criteria andPcminrateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("PCMINRATE between", value1, value2, "pcminrate");
            return (Criteria) this;
        }

        public Criteria andPcminrateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("PCMINRATE not between", value1, value2, "pcminrate");
            return (Criteria) this;
        }

        public Criteria andPcmaxrateIsNull() {
            addCriterion("PCMAXRATE is null");
            return (Criteria) this;
        }

        public Criteria andPcmaxrateIsNotNull() {
            addCriterion("PCMAXRATE is not null");
            return (Criteria) this;
        }

        public Criteria andPcmaxrateEqualTo(BigDecimal value) {
            addCriterion("PCMAXRATE =", value, "pcmaxrate");
            return (Criteria) this;
        }

        public Criteria andPcmaxrateNotEqualTo(BigDecimal value) {
            addCriterion("PCMAXRATE <>", value, "pcmaxrate");
            return (Criteria) this;
        }

        public Criteria andPcmaxrateGreaterThan(BigDecimal value) {
            addCriterion("PCMAXRATE >", value, "pcmaxrate");
            return (Criteria) this;
        }

        public Criteria andPcmaxrateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("PCMAXRATE >=", value, "pcmaxrate");
            return (Criteria) this;
        }

        public Criteria andPcmaxrateLessThan(BigDecimal value) {
            addCriterion("PCMAXRATE <", value, "pcmaxrate");
            return (Criteria) this;
        }

        public Criteria andPcmaxrateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("PCMAXRATE <=", value, "pcmaxrate");
            return (Criteria) this;
        }

        public Criteria andPcmaxrateIn(List<BigDecimal> values) {
            addCriterion("PCMAXRATE in", values, "pcmaxrate");
            return (Criteria) this;
        }

        public Criteria andPcmaxrateNotIn(List<BigDecimal> values) {
            addCriterion("PCMAXRATE not in", values, "pcmaxrate");
            return (Criteria) this;
        }

        public Criteria andPcmaxrateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("PCMAXRATE between", value1, value2, "pcmaxrate");
            return (Criteria) this;
        }

        public Criteria andPcmaxrateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("PCMAXRATE not between", value1, value2, "pcmaxrate");
            return (Criteria) this;
        }

        public Criteria andPccompbaseIsNull() {
            addCriterion("PCCOMPBASE is null");
            return (Criteria) this;
        }

        public Criteria andPccompbaseIsNotNull() {
            addCriterion("PCCOMPBASE is not null");
            return (Criteria) this;
        }

        public Criteria andPccompbaseEqualTo(BigDecimal value) {
            addCriterion("PCCOMPBASE =", value, "pccompbase");
            return (Criteria) this;
        }

        public Criteria andPccompbaseNotEqualTo(BigDecimal value) {
            addCriterion("PCCOMPBASE <>", value, "pccompbase");
            return (Criteria) this;
        }

        public Criteria andPccompbaseGreaterThan(BigDecimal value) {
            addCriterion("PCCOMPBASE >", value, "pccompbase");
            return (Criteria) this;
        }

        public Criteria andPccompbaseGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("PCCOMPBASE >=", value, "pccompbase");
            return (Criteria) this;
        }

        public Criteria andPccompbaseLessThan(BigDecimal value) {
            addCriterion("PCCOMPBASE <", value, "pccompbase");
            return (Criteria) this;
        }

        public Criteria andPccompbaseLessThanOrEqualTo(BigDecimal value) {
            addCriterion("PCCOMPBASE <=", value, "pccompbase");
            return (Criteria) this;
        }

        public Criteria andPccompbaseIn(List<BigDecimal> values) {
            addCriterion("PCCOMPBASE in", values, "pccompbase");
            return (Criteria) this;
        }

        public Criteria andPccompbaseNotIn(List<BigDecimal> values) {
            addCriterion("PCCOMPBASE not in", values, "pccompbase");
            return (Criteria) this;
        }

        public Criteria andPccompbaseBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("PCCOMPBASE between", value1, value2, "pccompbase");
            return (Criteria) this;
        }

        public Criteria andPccompbaseNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("PCCOMPBASE not between", value1, value2, "pccompbase");
            return (Criteria) this;
        }

        public Criteria andPccompstepIsNull() {
            addCriterion("PCCOMPSTEP is null");
            return (Criteria) this;
        }

        public Criteria andPccompstepIsNotNull() {
            addCriterion("PCCOMPSTEP is not null");
            return (Criteria) this;
        }

        public Criteria andPccompstepEqualTo(BigDecimal value) {
            addCriterion("PCCOMPSTEP =", value, "pccompstep");
            return (Criteria) this;
        }

        public Criteria andPccompstepNotEqualTo(BigDecimal value) {
            addCriterion("PCCOMPSTEP <>", value, "pccompstep");
            return (Criteria) this;
        }

        public Criteria andPccompstepGreaterThan(BigDecimal value) {
            addCriterion("PCCOMPSTEP >", value, "pccompstep");
            return (Criteria) this;
        }

        public Criteria andPccompstepGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("PCCOMPSTEP >=", value, "pccompstep");
            return (Criteria) this;
        }

        public Criteria andPccompstepLessThan(BigDecimal value) {
            addCriterion("PCCOMPSTEP <", value, "pccompstep");
            return (Criteria) this;
        }

        public Criteria andPccompstepLessThanOrEqualTo(BigDecimal value) {
            addCriterion("PCCOMPSTEP <=", value, "pccompstep");
            return (Criteria) this;
        }

        public Criteria andPccompstepIn(List<BigDecimal> values) {
            addCriterion("PCCOMPSTEP in", values, "pccompstep");
            return (Criteria) this;
        }

        public Criteria andPccompstepNotIn(List<BigDecimal> values) {
            addCriterion("PCCOMPSTEP not in", values, "pccompstep");
            return (Criteria) this;
        }

        public Criteria andPccompstepBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("PCCOMPSTEP between", value1, value2, "pccompstep");
            return (Criteria) this;
        }

        public Criteria andPccompstepNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("PCCOMPSTEP not between", value1, value2, "pccompstep");
            return (Criteria) this;
        }

        public Criteria andPcratestepIsNull() {
            addCriterion("PCRATESTEP is null");
            return (Criteria) this;
        }

        public Criteria andPcratestepIsNotNull() {
            addCriterion("PCRATESTEP is not null");
            return (Criteria) this;
        }

        public Criteria andPcratestepEqualTo(BigDecimal value) {
            addCriterion("PCRATESTEP =", value, "pcratestep");
            return (Criteria) this;
        }

        public Criteria andPcratestepNotEqualTo(BigDecimal value) {
            addCriterion("PCRATESTEP <>", value, "pcratestep");
            return (Criteria) this;
        }

        public Criteria andPcratestepGreaterThan(BigDecimal value) {
            addCriterion("PCRATESTEP >", value, "pcratestep");
            return (Criteria) this;
        }

        public Criteria andPcratestepGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("PCRATESTEP >=", value, "pcratestep");
            return (Criteria) this;
        }

        public Criteria andPcratestepLessThan(BigDecimal value) {
            addCriterion("PCRATESTEP <", value, "pcratestep");
            return (Criteria) this;
        }

        public Criteria andPcratestepLessThanOrEqualTo(BigDecimal value) {
            addCriterion("PCRATESTEP <=", value, "pcratestep");
            return (Criteria) this;
        }

        public Criteria andPcratestepIn(List<BigDecimal> values) {
            addCriterion("PCRATESTEP in", values, "pcratestep");
            return (Criteria) this;
        }

        public Criteria andPcratestepNotIn(List<BigDecimal> values) {
            addCriterion("PCRATESTEP not in", values, "pcratestep");
            return (Criteria) this;
        }

        public Criteria andPcratestepBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("PCRATESTEP between", value1, value2, "pcratestep");
            return (Criteria) this;
        }

        public Criteria andPcratestepNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("PCRATESTEP not between", value1, value2, "pcratestep");
            return (Criteria) this;
        }

        public Criteria andFlagIsNull() {
            addCriterion("FLAG is null");
            return (Criteria) this;
        }

        public Criteria andFlagIsNotNull() {
            addCriterion("FLAG is not null");
            return (Criteria) this;
        }

        public Criteria andFlagEqualTo(String value) {
            addCriterion("FLAG =", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotEqualTo(String value) {
            addCriterion("FLAG <>", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThan(String value) {
            addCriterion("FLAG >", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThanOrEqualTo(String value) {
            addCriterion("FLAG >=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThan(String value) {
            addCriterion("FLAG <", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThanOrEqualTo(String value) {
            addCriterion("FLAG <=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLike(String value) {
            addCriterion("FLAG like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotLike(String value) {
            addCriterion("FLAG not like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagIn(List<String> values) {
            addCriterion("FLAG in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotIn(List<String> values) {
            addCriterion("FLAG not in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagBetween(String value1, String value2) {
            addCriterion("FLAG between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotBetween(String value1, String value2) {
            addCriterion("FLAG not between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andCashlossflagIsNull() {
            addCriterion("CASHLOSSFLAG is null");
            return (Criteria) this;
        }

        public Criteria andCashlossflagIsNotNull() {
            addCriterion("CASHLOSSFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andCashlossflagEqualTo(String value) {
            addCriterion("CASHLOSSFLAG =", value, "cashlossflag");
            return (Criteria) this;
        }

        public Criteria andCashlossflagNotEqualTo(String value) {
            addCriterion("CASHLOSSFLAG <>", value, "cashlossflag");
            return (Criteria) this;
        }

        public Criteria andCashlossflagGreaterThan(String value) {
            addCriterion("CASHLOSSFLAG >", value, "cashlossflag");
            return (Criteria) this;
        }

        public Criteria andCashlossflagGreaterThanOrEqualTo(String value) {
            addCriterion("CASHLOSSFLAG >=", value, "cashlossflag");
            return (Criteria) this;
        }

        public Criteria andCashlossflagLessThan(String value) {
            addCriterion("CASHLOSSFLAG <", value, "cashlossflag");
            return (Criteria) this;
        }

        public Criteria andCashlossflagLessThanOrEqualTo(String value) {
            addCriterion("CASHLOSSFLAG <=", value, "cashlossflag");
            return (Criteria) this;
        }

        public Criteria andCashlossflagLike(String value) {
            addCriterion("CASHLOSSFLAG like", value, "cashlossflag");
            return (Criteria) this;
        }

        public Criteria andCashlossflagNotLike(String value) {
            addCriterion("CASHLOSSFLAG not like", value, "cashlossflag");
            return (Criteria) this;
        }

        public Criteria andCashlossflagIn(List<String> values) {
            addCriterion("CASHLOSSFLAG in", values, "cashlossflag");
            return (Criteria) this;
        }

        public Criteria andCashlossflagNotIn(List<String> values) {
            addCriterion("CASHLOSSFLAG not in", values, "cashlossflag");
            return (Criteria) this;
        }

        public Criteria andCashlossflagBetween(String value1, String value2) {
            addCriterion("CASHLOSSFLAG between", value1, value2, "cashlossflag");
            return (Criteria) this;
        }

        public Criteria andCashlossflagNotBetween(String value1, String value2) {
            addCriterion("CASHLOSSFLAG not between", value1, value2, "cashlossflag");
            return (Criteria) this;
        }

        public Criteria andCleanyearIsNull() {
            addCriterion("CLEANYEAR is null");
            return (Criteria) this;
        }

        public Criteria andCleanyearIsNotNull() {
            addCriterion("CLEANYEAR is not null");
            return (Criteria) this;
        }

        public Criteria andCleanyearEqualTo(BigDecimal value) {
            addCriterion("CLEANYEAR =", value, "cleanyear");
            return (Criteria) this;
        }

        public Criteria andCleanyearNotEqualTo(BigDecimal value) {
            addCriterion("CLEANYEAR <>", value, "cleanyear");
            return (Criteria) this;
        }

        public Criteria andCleanyearGreaterThan(BigDecimal value) {
            addCriterion("CLEANYEAR >", value, "cleanyear");
            return (Criteria) this;
        }

        public Criteria andCleanyearGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("CLEANYEAR >=", value, "cleanyear");
            return (Criteria) this;
        }

        public Criteria andCleanyearLessThan(BigDecimal value) {
            addCriterion("CLEANYEAR <", value, "cleanyear");
            return (Criteria) this;
        }

        public Criteria andCleanyearLessThanOrEqualTo(BigDecimal value) {
            addCriterion("CLEANYEAR <=", value, "cleanyear");
            return (Criteria) this;
        }

        public Criteria andCleanyearIn(List<BigDecimal> values) {
            addCriterion("CLEANYEAR in", values, "cleanyear");
            return (Criteria) this;
        }

        public Criteria andCleanyearNotIn(List<BigDecimal> values) {
            addCriterion("CLEANYEAR not in", values, "cleanyear");
            return (Criteria) this;
        }

        public Criteria andCleanyearBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("CLEANYEAR between", value1, value2, "cleanyear");
            return (Criteria) this;
        }

        public Criteria andCleanyearNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("CLEANYEAR not between", value1, value2, "cleanyear");
            return (Criteria) this;
        }

        public Criteria andLowpaidrateIsNull() {
            addCriterion("LOWPAIDRATE is null");
            return (Criteria) this;
        }

        public Criteria andLowpaidrateIsNotNull() {
            addCriterion("LOWPAIDRATE is not null");
            return (Criteria) this;
        }

        public Criteria andLowpaidrateEqualTo(BigDecimal value) {
            addCriterion("LOWPAIDRATE =", value, "lowpaidrate");
            return (Criteria) this;
        }

        public Criteria andLowpaidrateNotEqualTo(BigDecimal value) {
            addCriterion("LOWPAIDRATE <>", value, "lowpaidrate");
            return (Criteria) this;
        }

        public Criteria andLowpaidrateGreaterThan(BigDecimal value) {
            addCriterion("LOWPAIDRATE >", value, "lowpaidrate");
            return (Criteria) this;
        }

        public Criteria andLowpaidrateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("LOWPAIDRATE >=", value, "lowpaidrate");
            return (Criteria) this;
        }

        public Criteria andLowpaidrateLessThan(BigDecimal value) {
            addCriterion("LOWPAIDRATE <", value, "lowpaidrate");
            return (Criteria) this;
        }

        public Criteria andLowpaidrateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("LOWPAIDRATE <=", value, "lowpaidrate");
            return (Criteria) this;
        }

        public Criteria andLowpaidrateIn(List<BigDecimal> values) {
            addCriterion("LOWPAIDRATE in", values, "lowpaidrate");
            return (Criteria) this;
        }

        public Criteria andLowpaidrateNotIn(List<BigDecimal> values) {
            addCriterion("LOWPAIDRATE not in", values, "lowpaidrate");
            return (Criteria) this;
        }

        public Criteria andLowpaidrateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("LOWPAIDRATE between", value1, value2, "lowpaidrate");
            return (Criteria) this;
        }

        public Criteria andLowpaidrateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("LOWPAIDRATE not between", value1, value2, "lowpaidrate");
            return (Criteria) this;
        }

        public Criteria andUpperpaidrateIsNull() {
            addCriterion("UPPERPAIDRATE is null");
            return (Criteria) this;
        }

        public Criteria andUpperpaidrateIsNotNull() {
            addCriterion("UPPERPAIDRATE is not null");
            return (Criteria) this;
        }

        public Criteria andUpperpaidrateEqualTo(BigDecimal value) {
            addCriterion("UPPERPAIDRATE =", value, "upperpaidrate");
            return (Criteria) this;
        }

        public Criteria andUpperpaidrateNotEqualTo(BigDecimal value) {
            addCriterion("UPPERPAIDRATE <>", value, "upperpaidrate");
            return (Criteria) this;
        }

        public Criteria andUpperpaidrateGreaterThan(BigDecimal value) {
            addCriterion("UPPERPAIDRATE >", value, "upperpaidrate");
            return (Criteria) this;
        }

        public Criteria andUpperpaidrateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("UPPERPAIDRATE >=", value, "upperpaidrate");
            return (Criteria) this;
        }

        public Criteria andUpperpaidrateLessThan(BigDecimal value) {
            addCriterion("UPPERPAIDRATE <", value, "upperpaidrate");
            return (Criteria) this;
        }

        public Criteria andUpperpaidrateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("UPPERPAIDRATE <=", value, "upperpaidrate");
            return (Criteria) this;
        }

        public Criteria andUpperpaidrateIn(List<BigDecimal> values) {
            addCriterion("UPPERPAIDRATE in", values, "upperpaidrate");
            return (Criteria) this;
        }

        public Criteria andUpperpaidrateNotIn(List<BigDecimal> values) {
            addCriterion("UPPERPAIDRATE not in", values, "upperpaidrate");
            return (Criteria) this;
        }

        public Criteria andUpperpaidrateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("UPPERPAIDRATE between", value1, value2, "upperpaidrate");
            return (Criteria) this;
        }

        public Criteria andUpperpaidrateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("UPPERPAIDRATE not between", value1, value2, "upperpaidrate");
            return (Criteria) this;
        }

        public Criteria andBasepayrateIsNull() {
            addCriterion("BASEPAYRATE is null");
            return (Criteria) this;
        }

        public Criteria andBasepayrateIsNotNull() {
            addCriterion("BASEPAYRATE is not null");
            return (Criteria) this;
        }

        public Criteria andBasepayrateEqualTo(BigDecimal value) {
            addCriterion("BASEPAYRATE =", value, "basepayrate");
            return (Criteria) this;
        }

        public Criteria andBasepayrateNotEqualTo(BigDecimal value) {
            addCriterion("BASEPAYRATE <>", value, "basepayrate");
            return (Criteria) this;
        }

        public Criteria andBasepayrateGreaterThan(BigDecimal value) {
            addCriterion("BASEPAYRATE >", value, "basepayrate");
            return (Criteria) this;
        }

        public Criteria andBasepayrateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("BASEPAYRATE >=", value, "basepayrate");
            return (Criteria) this;
        }

        public Criteria andBasepayrateLessThan(BigDecimal value) {
            addCriterion("BASEPAYRATE <", value, "basepayrate");
            return (Criteria) this;
        }

        public Criteria andBasepayrateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("BASEPAYRATE <=", value, "basepayrate");
            return (Criteria) this;
        }

        public Criteria andBasepayrateIn(List<BigDecimal> values) {
            addCriterion("BASEPAYRATE in", values, "basepayrate");
            return (Criteria) this;
        }

        public Criteria andBasepayrateNotIn(List<BigDecimal> values) {
            addCriterion("BASEPAYRATE not in", values, "basepayrate");
            return (Criteria) this;
        }

        public Criteria andBasepayrateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("BASEPAYRATE between", value1, value2, "basepayrate");
            return (Criteria) this;
        }

        public Criteria andBasepayrateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("BASEPAYRATE not between", value1, value2, "basepayrate");
            return (Criteria) this;
        }

        public Criteria andAdjustcommrateIsNull() {
            addCriterion("ADJUSTCOMMRATE is null");
            return (Criteria) this;
        }

        public Criteria andAdjustcommrateIsNotNull() {
            addCriterion("ADJUSTCOMMRATE is not null");
            return (Criteria) this;
        }

        public Criteria andAdjustcommrateEqualTo(BigDecimal value) {
            addCriterion("ADJUSTCOMMRATE =", value, "adjustcommrate");
            return (Criteria) this;
        }

        public Criteria andAdjustcommrateNotEqualTo(BigDecimal value) {
            addCriterion("ADJUSTCOMMRATE <>", value, "adjustcommrate");
            return (Criteria) this;
        }

        public Criteria andAdjustcommrateGreaterThan(BigDecimal value) {
            addCriterion("ADJUSTCOMMRATE >", value, "adjustcommrate");
            return (Criteria) this;
        }

        public Criteria andAdjustcommrateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("ADJUSTCOMMRATE >=", value, "adjustcommrate");
            return (Criteria) this;
        }

        public Criteria andAdjustcommrateLessThan(BigDecimal value) {
            addCriterion("ADJUSTCOMMRATE <", value, "adjustcommrate");
            return (Criteria) this;
        }

        public Criteria andAdjustcommrateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("ADJUSTCOMMRATE <=", value, "adjustcommrate");
            return (Criteria) this;
        }

        public Criteria andAdjustcommrateIn(List<BigDecimal> values) {
            addCriterion("ADJUSTCOMMRATE in", values, "adjustcommrate");
            return (Criteria) this;
        }

        public Criteria andAdjustcommrateNotIn(List<BigDecimal> values) {
            addCriterion("ADJUSTCOMMRATE not in", values, "adjustcommrate");
            return (Criteria) this;
        }

        public Criteria andAdjustcommrateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("ADJUSTCOMMRATE between", value1, value2, "adjustcommrate");
            return (Criteria) this;
        }

        public Criteria andAdjustcommrateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("ADJUSTCOMMRATE not between", value1, value2, "adjustcommrate");
            return (Criteria) this;
        }

        public Criteria andAdjustrateIsNull() {
            addCriterion("ADJUSTRATE is null");
            return (Criteria) this;
        }

        public Criteria andAdjustrateIsNotNull() {
            addCriterion("ADJUSTRATE is not null");
            return (Criteria) this;
        }

        public Criteria andAdjustrateEqualTo(BigDecimal value) {
            addCriterion("ADJUSTRATE =", value, "adjustrate");
            return (Criteria) this;
        }

        public Criteria andAdjustrateNotEqualTo(BigDecimal value) {
            addCriterion("ADJUSTRATE <>", value, "adjustrate");
            return (Criteria) this;
        }

        public Criteria andAdjustrateGreaterThan(BigDecimal value) {
            addCriterion("ADJUSTRATE >", value, "adjustrate");
            return (Criteria) this;
        }

        public Criteria andAdjustrateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("ADJUSTRATE >=", value, "adjustrate");
            return (Criteria) this;
        }

        public Criteria andAdjustrateLessThan(BigDecimal value) {
            addCriterion("ADJUSTRATE <", value, "adjustrate");
            return (Criteria) this;
        }

        public Criteria andAdjustrateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("ADJUSTRATE <=", value, "adjustrate");
            return (Criteria) this;
        }

        public Criteria andAdjustrateIn(List<BigDecimal> values) {
            addCriterion("ADJUSTRATE in", values, "adjustrate");
            return (Criteria) this;
        }

        public Criteria andAdjustrateNotIn(List<BigDecimal> values) {
            addCriterion("ADJUSTRATE not in", values, "adjustrate");
            return (Criteria) this;
        }

        public Criteria andAdjustrateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("ADJUSTRATE between", value1, value2, "adjustrate");
            return (Criteria) this;
        }

        public Criteria andAdjustrateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("ADJUSTRATE not between", value1, value2, "adjustrate");
            return (Criteria) this;
        }

        public Criteria andInratetypeIsNull() {
            addCriterion("INRATETYPE is null");
            return (Criteria) this;
        }

        public Criteria andInratetypeIsNotNull() {
            addCriterion("INRATETYPE is not null");
            return (Criteria) this;
        }

        public Criteria andInratetypeEqualTo(String value) {
            addCriterion("INRATETYPE =", value, "inratetype");
            return (Criteria) this;
        }

        public Criteria andInratetypeNotEqualTo(String value) {
            addCriterion("INRATETYPE <>", value, "inratetype");
            return (Criteria) this;
        }

        public Criteria andInratetypeGreaterThan(String value) {
            addCriterion("INRATETYPE >", value, "inratetype");
            return (Criteria) this;
        }

        public Criteria andInratetypeGreaterThanOrEqualTo(String value) {
            addCriterion("INRATETYPE >=", value, "inratetype");
            return (Criteria) this;
        }

        public Criteria andInratetypeLessThan(String value) {
            addCriterion("INRATETYPE <", value, "inratetype");
            return (Criteria) this;
        }

        public Criteria andInratetypeLessThanOrEqualTo(String value) {
            addCriterion("INRATETYPE <=", value, "inratetype");
            return (Criteria) this;
        }

        public Criteria andInratetypeLike(String value) {
            addCriterion("INRATETYPE like", value, "inratetype");
            return (Criteria) this;
        }

        public Criteria andInratetypeNotLike(String value) {
            addCriterion("INRATETYPE not like", value, "inratetype");
            return (Criteria) this;
        }

        public Criteria andInratetypeIn(List<String> values) {
            addCriterion("INRATETYPE in", values, "inratetype");
            return (Criteria) this;
        }

        public Criteria andInratetypeNotIn(List<String> values) {
            addCriterion("INRATETYPE not in", values, "inratetype");
            return (Criteria) this;
        }

        public Criteria andInratetypeBetween(String value1, String value2) {
            addCriterion("INRATETYPE between", value1, value2, "inratetype");
            return (Criteria) this;
        }

        public Criteria andInratetypeNotBetween(String value1, String value2) {
            addCriterion("INRATETYPE not between", value1, value2, "inratetype");
            return (Criteria) this;
        }

        public Criteria andCleancutremarkIsNull() {
            addCriterion("CLEANCUTREMARK is null");
            return (Criteria) this;
        }

        public Criteria andCleancutremarkIsNotNull() {
            addCriterion("CLEANCUTREMARK is not null");
            return (Criteria) this;
        }

        public Criteria andCleancutremarkEqualTo(String value) {
            addCriterion("CLEANCUTREMARK =", value, "cleancutremark");
            return (Criteria) this;
        }

        public Criteria andCleancutremarkNotEqualTo(String value) {
            addCriterion("CLEANCUTREMARK <>", value, "cleancutremark");
            return (Criteria) this;
        }

        public Criteria andCleancutremarkGreaterThan(String value) {
            addCriterion("CLEANCUTREMARK >", value, "cleancutremark");
            return (Criteria) this;
        }

        public Criteria andCleancutremarkGreaterThanOrEqualTo(String value) {
            addCriterion("CLEANCUTREMARK >=", value, "cleancutremark");
            return (Criteria) this;
        }

        public Criteria andCleancutremarkLessThan(String value) {
            addCriterion("CLEANCUTREMARK <", value, "cleancutremark");
            return (Criteria) this;
        }

        public Criteria andCleancutremarkLessThanOrEqualTo(String value) {
            addCriterion("CLEANCUTREMARK <=", value, "cleancutremark");
            return (Criteria) this;
        }

        public Criteria andCleancutremarkLike(String value) {
            addCriterion("CLEANCUTREMARK like", value, "cleancutremark");
            return (Criteria) this;
        }

        public Criteria andCleancutremarkNotLike(String value) {
            addCriterion("CLEANCUTREMARK not like", value, "cleancutremark");
            return (Criteria) this;
        }

        public Criteria andCleancutremarkIn(List<String> values) {
            addCriterion("CLEANCUTREMARK in", values, "cleancutremark");
            return (Criteria) this;
        }

        public Criteria andCleancutremarkNotIn(List<String> values) {
            addCriterion("CLEANCUTREMARK not in", values, "cleancutremark");
            return (Criteria) this;
        }

        public Criteria andCleancutremarkBetween(String value1, String value2) {
            addCriterion("CLEANCUTREMARK between", value1, value2, "cleancutremark");
            return (Criteria) this;
        }

        public Criteria andCleancutremarkNotBetween(String value1, String value2) {
            addCriterion("CLEANCUTREMARK not between", value1, value2, "cleancutremark");
            return (Criteria) this;
        }

        public Criteria andCleancutstatusIsNull() {
            addCriterion("CLEANCUTSTATUS is null");
            return (Criteria) this;
        }

        public Criteria andCleancutstatusIsNotNull() {
            addCriterion("CLEANCUTSTATUS is not null");
            return (Criteria) this;
        }

        public Criteria andCleancutstatusEqualTo(String value) {
            addCriterion("CLEANCUTSTATUS =", value, "cleancutstatus");
            return (Criteria) this;
        }

        public Criteria andCleancutstatusNotEqualTo(String value) {
            addCriterion("CLEANCUTSTATUS <>", value, "cleancutstatus");
            return (Criteria) this;
        }

        public Criteria andCleancutstatusGreaterThan(String value) {
            addCriterion("CLEANCUTSTATUS >", value, "cleancutstatus");
            return (Criteria) this;
        }

        public Criteria andCleancutstatusGreaterThanOrEqualTo(String value) {
            addCriterion("CLEANCUTSTATUS >=", value, "cleancutstatus");
            return (Criteria) this;
        }

        public Criteria andCleancutstatusLessThan(String value) {
            addCriterion("CLEANCUTSTATUS <", value, "cleancutstatus");
            return (Criteria) this;
        }

        public Criteria andCleancutstatusLessThanOrEqualTo(String value) {
            addCriterion("CLEANCUTSTATUS <=", value, "cleancutstatus");
            return (Criteria) this;
        }

        public Criteria andCleancutstatusLike(String value) {
            addCriterion("CLEANCUTSTATUS like", value, "cleancutstatus");
            return (Criteria) this;
        }

        public Criteria andCleancutstatusNotLike(String value) {
            addCriterion("CLEANCUTSTATUS not like", value, "cleancutstatus");
            return (Criteria) this;
        }

        public Criteria andCleancutstatusIn(List<String> values) {
            addCriterion("CLEANCUTSTATUS in", values, "cleancutstatus");
            return (Criteria) this;
        }

        public Criteria andCleancutstatusNotIn(List<String> values) {
            addCriterion("CLEANCUTSTATUS not in", values, "cleancutstatus");
            return (Criteria) this;
        }

        public Criteria andCleancutstatusBetween(String value1, String value2) {
            addCriterion("CLEANCUTSTATUS between", value1, value2, "cleancutstatus");
            return (Criteria) this;
        }

        public Criteria andCleancutstatusNotBetween(String value1, String value2) {
            addCriterion("CLEANCUTSTATUS not between", value1, value2, "cleancutstatus");
            return (Criteria) this;
        }

        public Criteria andMaincoinsrategeIsNull() {
            addCriterion("MAINCOINSRATEGE is null");
            return (Criteria) this;
        }

        public Criteria andMaincoinsrategeIsNotNull() {
            addCriterion("MAINCOINSRATEGE is not null");
            return (Criteria) this;
        }

        public Criteria andMaincoinsrategeEqualTo(BigDecimal value) {
            addCriterion("MAINCOINSRATEGE =", value, "maincoinsratege");
            return (Criteria) this;
        }

        public Criteria andMaincoinsrategeNotEqualTo(BigDecimal value) {
            addCriterion("MAINCOINSRATEGE <>", value, "maincoinsratege");
            return (Criteria) this;
        }

        public Criteria andMaincoinsrategeGreaterThan(BigDecimal value) {
            addCriterion("MAINCOINSRATEGE >", value, "maincoinsratege");
            return (Criteria) this;
        }

        public Criteria andMaincoinsrategeGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("MAINCOINSRATEGE >=", value, "maincoinsratege");
            return (Criteria) this;
        }

        public Criteria andMaincoinsrategeLessThan(BigDecimal value) {
            addCriterion("MAINCOINSRATEGE <", value, "maincoinsratege");
            return (Criteria) this;
        }

        public Criteria andMaincoinsrategeLessThanOrEqualTo(BigDecimal value) {
            addCriterion("MAINCOINSRATEGE <=", value, "maincoinsratege");
            return (Criteria) this;
        }

        public Criteria andMaincoinsrategeIn(List<BigDecimal> values) {
            addCriterion("MAINCOINSRATEGE in", values, "maincoinsratege");
            return (Criteria) this;
        }

        public Criteria andMaincoinsrategeNotIn(List<BigDecimal> values) {
            addCriterion("MAINCOINSRATEGE not in", values, "maincoinsratege");
            return (Criteria) this;
        }

        public Criteria andMaincoinsrategeBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("MAINCOINSRATEGE between", value1, value2, "maincoinsratege");
            return (Criteria) this;
        }

        public Criteria andMaincoinsrategeNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("MAINCOINSRATEGE not between", value1, value2, "maincoinsratege");
            return (Criteria) this;
        }

        public Criteria andMaincoinsratebelowIsNull() {
            addCriterion("MAINCOINSRATEBELOW is null");
            return (Criteria) this;
        }

        public Criteria andMaincoinsratebelowIsNotNull() {
            addCriterion("MAINCOINSRATEBELOW is not null");
            return (Criteria) this;
        }

        public Criteria andMaincoinsratebelowEqualTo(BigDecimal value) {
            addCriterion("MAINCOINSRATEBELOW =", value, "maincoinsratebelow");
            return (Criteria) this;
        }

        public Criteria andMaincoinsratebelowNotEqualTo(BigDecimal value) {
            addCriterion("MAINCOINSRATEBELOW <>", value, "maincoinsratebelow");
            return (Criteria) this;
        }

        public Criteria andMaincoinsratebelowGreaterThan(BigDecimal value) {
            addCriterion("MAINCOINSRATEBELOW >", value, "maincoinsratebelow");
            return (Criteria) this;
        }

        public Criteria andMaincoinsratebelowGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("MAINCOINSRATEBELOW >=", value, "maincoinsratebelow");
            return (Criteria) this;
        }

        public Criteria andMaincoinsratebelowLessThan(BigDecimal value) {
            addCriterion("MAINCOINSRATEBELOW <", value, "maincoinsratebelow");
            return (Criteria) this;
        }

        public Criteria andMaincoinsratebelowLessThanOrEqualTo(BigDecimal value) {
            addCriterion("MAINCOINSRATEBELOW <=", value, "maincoinsratebelow");
            return (Criteria) this;
        }

        public Criteria andMaincoinsratebelowIn(List<BigDecimal> values) {
            addCriterion("MAINCOINSRATEBELOW in", values, "maincoinsratebelow");
            return (Criteria) this;
        }

        public Criteria andMaincoinsratebelowNotIn(List<BigDecimal> values) {
            addCriterion("MAINCOINSRATEBELOW not in", values, "maincoinsratebelow");
            return (Criteria) this;
        }

        public Criteria andMaincoinsratebelowBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("MAINCOINSRATEBELOW between", value1, value2, "maincoinsratebelow");
            return (Criteria) this;
        }

        public Criteria andMaincoinsratebelowNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("MAINCOINSRATEBELOW not between", value1, value2, "maincoinsratebelow");
            return (Criteria) this;
        }

        public Criteria andVatflagIsNull() {
            addCriterion("VATFLAG is null");
            return (Criteria) this;
        }

        public Criteria andVatflagIsNotNull() {
            addCriterion("VATFLAG is not null");
            return (Criteria) this;
        }

        public Criteria andVatflagEqualTo(String value) {
            addCriterion("VATFLAG =", value, "vatflag");
            return (Criteria) this;
        }

        public Criteria andVatflagNotEqualTo(String value) {
            addCriterion("VATFLAG <>", value, "vatflag");
            return (Criteria) this;
        }

        public Criteria andVatflagGreaterThan(String value) {
            addCriterion("VATFLAG >", value, "vatflag");
            return (Criteria) this;
        }

        public Criteria andVatflagGreaterThanOrEqualTo(String value) {
            addCriterion("VATFLAG >=", value, "vatflag");
            return (Criteria) this;
        }

        public Criteria andVatflagLessThan(String value) {
            addCriterion("VATFLAG <", value, "vatflag");
            return (Criteria) this;
        }

        public Criteria andVatflagLessThanOrEqualTo(String value) {
            addCriterion("VATFLAG <=", value, "vatflag");
            return (Criteria) this;
        }

        public Criteria andVatflagLike(String value) {
            addCriterion("VATFLAG like", value, "vatflag");
            return (Criteria) this;
        }

        public Criteria andVatflagNotLike(String value) {
            addCriterion("VATFLAG not like", value, "vatflag");
            return (Criteria) this;
        }

        public Criteria andVatflagIn(List<String> values) {
            addCriterion("VATFLAG in", values, "vatflag");
            return (Criteria) this;
        }

        public Criteria andVatflagNotIn(List<String> values) {
            addCriterion("VATFLAG not in", values, "vatflag");
            return (Criteria) this;
        }

        public Criteria andVatflagBetween(String value1, String value2) {
            addCriterion("VATFLAG between", value1, value2, "vatflag");
            return (Criteria) this;
        }

        public Criteria andVatflagNotBetween(String value1, String value2) {
            addCriterion("VATFLAG not between", value1, value2, "vatflag");
            return (Criteria) this;
        }

        public Criteria andAddvatrateIsNull() {
            addCriterion("ADDVATRATE is null");
            return (Criteria) this;
        }

        public Criteria andAddvatrateIsNotNull() {
            addCriterion("ADDVATRATE is not null");
            return (Criteria) this;
        }

        public Criteria andAddvatrateEqualTo(BigDecimal value) {
            addCriterion("ADDVATRATE =", value, "addvatrate");
            return (Criteria) this;
        }

        public Criteria andAddvatrateNotEqualTo(BigDecimal value) {
            addCriterion("ADDVATRATE <>", value, "addvatrate");
            return (Criteria) this;
        }

        public Criteria andAddvatrateGreaterThan(BigDecimal value) {
            addCriterion("ADDVATRATE >", value, "addvatrate");
            return (Criteria) this;
        }

        public Criteria andAddvatrateGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("ADDVATRATE >=", value, "addvatrate");
            return (Criteria) this;
        }

        public Criteria andAddvatrateLessThan(BigDecimal value) {
            addCriterion("ADDVATRATE <", value, "addvatrate");
            return (Criteria) this;
        }

        public Criteria andAddvatrateLessThanOrEqualTo(BigDecimal value) {
            addCriterion("ADDVATRATE <=", value, "addvatrate");
            return (Criteria) this;
        }

        public Criteria andAddvatrateIn(List<BigDecimal> values) {
            addCriterion("ADDVATRATE in", values, "addvatrate");
            return (Criteria) this;
        }

        public Criteria andAddvatrateNotIn(List<BigDecimal> values) {
            addCriterion("ADDVATRATE not in", values, "addvatrate");
            return (Criteria) this;
        }

        public Criteria andAddvatrateBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("ADDVATRATE between", value1, value2, "addvatrate");
            return (Criteria) this;
        }

        public Criteria andAddvatrateNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("ADDVATRATE not between", value1, value2, "addvatrate");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}