package cn.com.libertymutual.production.model.nomorcldatasource;

import java.math.BigDecimal;
import java.util.Date;

public class Fhxlayer extends FhxlayerKey {
    private String layertype;

    private String layercdesc;

    private String layeredesc;

    private String currency;

    private BigDecimal excessloss;

    private BigDecimal layerquota;

    private BigDecimal totalquota;

    private BigDecimal egnpi;

    private BigDecimal gnpi;

    private BigDecimal rate;

    private BigDecimal rol;

    private BigDecimal mdprate;

    private BigDecimal mdp;

    private BigDecimal layerpremium;

    private BigDecimal sharerate;

    private BigDecimal reinsttimes;

    private BigDecimal reinstrate;

    private BigDecimal residualreinstsum;

    private String reinsttype;

    private String remarks;

    private String updatercode;

    private Date updaterdate;

    private String flag;

    private BigDecimal rwp;

    private BigDecimal expireloss;

    private String remarks1;

    public String getLayertype() {
        return layertype;
    }

    public void setLayertype(String layertype) {
        this.layertype = layertype == null ? null : layertype.trim();
    }

    public String getLayercdesc() {
        return layercdesc;
    }

    public void setLayercdesc(String layercdesc) {
        this.layercdesc = layercdesc == null ? null : layercdesc.trim();
    }

    public String getLayeredesc() {
        return layeredesc;
    }

    public void setLayeredesc(String layeredesc) {
        this.layeredesc = layeredesc == null ? null : layeredesc.trim();
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency == null ? null : currency.trim();
    }

    public BigDecimal getExcessloss() {
        return excessloss;
    }

    public void setExcessloss(BigDecimal excessloss) {
        this.excessloss = excessloss;
    }

    public BigDecimal getLayerquota() {
        return layerquota;
    }

    public void setLayerquota(BigDecimal layerquota) {
        this.layerquota = layerquota;
    }

    public BigDecimal getTotalquota() {
        return totalquota;
    }

    public void setTotalquota(BigDecimal totalquota) {
        this.totalquota = totalquota;
    }

    public BigDecimal getEgnpi() {
        return egnpi;
    }

    public void setEgnpi(BigDecimal egnpi) {
        this.egnpi = egnpi;
    }

    public BigDecimal getGnpi() {
        return gnpi;
    }

    public void setGnpi(BigDecimal gnpi) {
        this.gnpi = gnpi;
    }

    public BigDecimal getRate() {
        return rate;
    }

    public void setRate(BigDecimal rate) {
        this.rate = rate;
    }

    public BigDecimal getRol() {
        return rol;
    }

    public void setRol(BigDecimal rol) {
        this.rol = rol;
    }

    public BigDecimal getMdprate() {
        return mdprate;
    }

    public void setMdprate(BigDecimal mdprate) {
        this.mdprate = mdprate;
    }

    public BigDecimal getMdp() {
        return mdp;
    }

    public void setMdp(BigDecimal mdp) {
        this.mdp = mdp;
    }

    public BigDecimal getLayerpremium() {
        return layerpremium;
    }

    public void setLayerpremium(BigDecimal layerpremium) {
        this.layerpremium = layerpremium;
    }

    public BigDecimal getSharerate() {
        return sharerate;
    }

    public void setSharerate(BigDecimal sharerate) {
        this.sharerate = sharerate;
    }

    public BigDecimal getReinsttimes() {
        return reinsttimes;
    }

    public void setReinsttimes(BigDecimal reinsttimes) {
        this.reinsttimes = reinsttimes;
    }

    public BigDecimal getReinstrate() {
        return reinstrate;
    }

    public void setReinstrate(BigDecimal reinstrate) {
        this.reinstrate = reinstrate;
    }

    public BigDecimal getResidualreinstsum() {
        return residualreinstsum;
    }

    public void setResidualreinstsum(BigDecimal residualreinstsum) {
        this.residualreinstsum = residualreinstsum;
    }

    public String getReinsttype() {
        return reinsttype;
    }

    public void setReinsttype(String reinsttype) {
        this.reinsttype = reinsttype == null ? null : reinsttype.trim();
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks == null ? null : remarks.trim();
    }

    public String getUpdatercode() {
        return updatercode;
    }

    public void setUpdatercode(String updatercode) {
        this.updatercode = updatercode == null ? null : updatercode.trim();
    }

    public Date getUpdaterdate() {
        return updaterdate;
    }

    public void setUpdaterdate(Date updaterdate) {
        this.updaterdate = updaterdate;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag == null ? null : flag.trim();
    }

    public BigDecimal getRwp() {
        return rwp;
    }

    public void setRwp(BigDecimal rwp) {
        this.rwp = rwp;
    }

    public BigDecimal getExpireloss() {
        return expireloss;
    }

    public void setExpireloss(BigDecimal expireloss) {
        this.expireloss = expireloss;
    }

    public String getRemarks1() {
        return remarks1;
    }

    public void setRemarks1(String remarks1) {
        this.remarks1 = remarks1 == null ? null : remarks1.trim();
    }
}