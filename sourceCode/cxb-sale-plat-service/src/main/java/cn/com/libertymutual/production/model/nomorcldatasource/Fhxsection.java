package cn.com.libertymutual.production.model.nomorcldatasource;

import java.math.BigDecimal;

public class Fhxsection extends FhxsectionKey {
    private String sectioncdesc;

    private String sectionedesc;

    private BigDecimal gnpi;

    private BigDecimal excessloss;

    private BigDecimal sectionquota;

    private BigDecimal expireloss;

    private BigDecimal exconamount;

    public String getSectioncdesc() {
        return sectioncdesc;
    }

    public void setSectioncdesc(String sectioncdesc) {
        this.sectioncdesc = sectioncdesc == null ? null : sectioncdesc.trim();
    }

    public String getSectionedesc() {
        return sectionedesc;
    }

    public void setSectionedesc(String sectionedesc) {
        this.sectionedesc = sectionedesc == null ? null : sectionedesc.trim();
    }

    public BigDecimal getGnpi() {
        return gnpi;
    }

    public void setGnpi(BigDecimal gnpi) {
        this.gnpi = gnpi;
    }

    public BigDecimal getExcessloss() {
        return excessloss;
    }

    public void setExcessloss(BigDecimal excessloss) {
        this.excessloss = excessloss;
    }

    public BigDecimal getSectionquota() {
        return sectionquota;
    }

    public void setSectionquota(BigDecimal sectionquota) {
        this.sectionquota = sectionquota;
    }

    public BigDecimal getExpireloss() {
        return expireloss;
    }

    public void setExpireloss(BigDecimal expireloss) {
        this.expireloss = expireloss;
    }

    public BigDecimal getExconamount() {
        return exconamount;
    }

    public void setExconamount(BigDecimal exconamount) {
        this.exconamount = exconamount;
    }
}