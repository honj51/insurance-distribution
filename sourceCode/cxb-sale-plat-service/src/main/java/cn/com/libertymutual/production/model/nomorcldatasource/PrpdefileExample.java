package cn.com.libertymutual.production.model.nomorcldatasource;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PrpdefileExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PrpdefileExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andEfilecodeIsNull() {
            addCriterion("EFILECODE is null");
            return (Criteria) this;
        }

        public Criteria andEfilecodeIsNotNull() {
            addCriterion("EFILECODE is not null");
            return (Criteria) this;
        }

        public Criteria andEfilecodeEqualTo(String value) {
            addCriterion("EFILECODE =", value, "efilecode");
            return (Criteria) this;
        }

        public Criteria andEfilecodeNotEqualTo(String value) {
            addCriterion("EFILECODE <>", value, "efilecode");
            return (Criteria) this;
        }

        public Criteria andEfilecodeGreaterThan(String value) {
            addCriterion("EFILECODE >", value, "efilecode");
            return (Criteria) this;
        }

        public Criteria andEfilecodeGreaterThanOrEqualTo(String value) {
            addCriterion("EFILECODE >=", value, "efilecode");
            return (Criteria) this;
        }

        public Criteria andEfilecodeLessThan(String value) {
            addCriterion("EFILECODE <", value, "efilecode");
            return (Criteria) this;
        }

        public Criteria andEfilecodeLessThanOrEqualTo(String value) {
            addCriterion("EFILECODE <=", value, "efilecode");
            return (Criteria) this;
        }

        public Criteria andEfilecodeLike(String value) {
            addCriterion("EFILECODE like", value, "efilecode");
            return (Criteria) this;
        }

        public Criteria andEfilecodeNotLike(String value) {
            addCriterion("EFILECODE not like", value, "efilecode");
            return (Criteria) this;
        }

        public Criteria andEfilecodeIn(List<String> values) {
            addCriterion("EFILECODE in", values, "efilecode");
            return (Criteria) this;
        }

        public Criteria andEfilecodeNotIn(List<String> values) {
            addCriterion("EFILECODE not in", values, "efilecode");
            return (Criteria) this;
        }

        public Criteria andEfilecodeBetween(String value1, String value2) {
            addCriterion("EFILECODE between", value1, value2, "efilecode");
            return (Criteria) this;
        }

        public Criteria andEfilecodeNotBetween(String value1, String value2) {
            addCriterion("EFILECODE not between", value1, value2, "efilecode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeIsNull() {
            addCriterion("RISKCODE is null");
            return (Criteria) this;
        }

        public Criteria andRiskcodeIsNotNull() {
            addCriterion("RISKCODE is not null");
            return (Criteria) this;
        }

        public Criteria andRiskcodeEqualTo(String value) {
            addCriterion("RISKCODE =", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeNotEqualTo(String value) {
            addCriterion("RISKCODE <>", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeGreaterThan(String value) {
            addCriterion("RISKCODE >", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeGreaterThanOrEqualTo(String value) {
            addCriterion("RISKCODE >=", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeLessThan(String value) {
            addCriterion("RISKCODE <", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeLessThanOrEqualTo(String value) {
            addCriterion("RISKCODE <=", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeLike(String value) {
            addCriterion("RISKCODE like", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeNotLike(String value) {
            addCriterion("RISKCODE not like", value, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeIn(List<String> values) {
            addCriterion("RISKCODE in", values, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeNotIn(List<String> values) {
            addCriterion("RISKCODE not in", values, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeBetween(String value1, String value2) {
            addCriterion("RISKCODE between", value1, value2, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskcodeNotBetween(String value1, String value2) {
            addCriterion("RISKCODE not between", value1, value2, "riskcode");
            return (Criteria) this;
        }

        public Criteria andRiskversionIsNull() {
            addCriterion("RISKVERSION is null");
            return (Criteria) this;
        }

        public Criteria andRiskversionIsNotNull() {
            addCriterion("RISKVERSION is not null");
            return (Criteria) this;
        }

        public Criteria andRiskversionEqualTo(String value) {
            addCriterion("RISKVERSION =", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionNotEqualTo(String value) {
            addCriterion("RISKVERSION <>", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionGreaterThan(String value) {
            addCriterion("RISKVERSION >", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionGreaterThanOrEqualTo(String value) {
            addCriterion("RISKVERSION >=", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionLessThan(String value) {
            addCriterion("RISKVERSION <", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionLessThanOrEqualTo(String value) {
            addCriterion("RISKVERSION <=", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionLike(String value) {
            addCriterion("RISKVERSION like", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionNotLike(String value) {
            addCriterion("RISKVERSION not like", value, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionIn(List<String> values) {
            addCriterion("RISKVERSION in", values, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionNotIn(List<String> values) {
            addCriterion("RISKVERSION not in", values, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionBetween(String value1, String value2) {
            addCriterion("RISKVERSION between", value1, value2, "riskversion");
            return (Criteria) this;
        }

        public Criteria andRiskversionNotBetween(String value1, String value2) {
            addCriterion("RISKVERSION not between", value1, value2, "riskversion");
            return (Criteria) this;
        }

        public Criteria andKindcodeIsNull() {
            addCriterion("KINDCODE is null");
            return (Criteria) this;
        }

        public Criteria andKindcodeIsNotNull() {
            addCriterion("KINDCODE is not null");
            return (Criteria) this;
        }

        public Criteria andKindcodeEqualTo(String value) {
            addCriterion("KINDCODE =", value, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeNotEqualTo(String value) {
            addCriterion("KINDCODE <>", value, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeGreaterThan(String value) {
            addCriterion("KINDCODE >", value, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeGreaterThanOrEqualTo(String value) {
            addCriterion("KINDCODE >=", value, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeLessThan(String value) {
            addCriterion("KINDCODE <", value, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeLessThanOrEqualTo(String value) {
            addCriterion("KINDCODE <=", value, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeLike(String value) {
            addCriterion("KINDCODE like", value, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeNotLike(String value) {
            addCriterion("KINDCODE not like", value, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeIn(List<String> values) {
            addCriterion("KINDCODE in", values, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeNotIn(List<String> values) {
            addCriterion("KINDCODE not in", values, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeBetween(String value1, String value2) {
            addCriterion("KINDCODE between", value1, value2, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindcodeNotBetween(String value1, String value2) {
            addCriterion("KINDCODE not between", value1, value2, "kindcode");
            return (Criteria) this;
        }

        public Criteria andKindversionIsNull() {
            addCriterion("KINDVERSION is null");
            return (Criteria) this;
        }

        public Criteria andKindversionIsNotNull() {
            addCriterion("KINDVERSION is not null");
            return (Criteria) this;
        }

        public Criteria andKindversionEqualTo(String value) {
            addCriterion("KINDVERSION =", value, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionNotEqualTo(String value) {
            addCriterion("KINDVERSION <>", value, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionGreaterThan(String value) {
            addCriterion("KINDVERSION >", value, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionGreaterThanOrEqualTo(String value) {
            addCriterion("KINDVERSION >=", value, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionLessThan(String value) {
            addCriterion("KINDVERSION <", value, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionLessThanOrEqualTo(String value) {
            addCriterion("KINDVERSION <=", value, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionLike(String value) {
            addCriterion("KINDVERSION like", value, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionNotLike(String value) {
            addCriterion("KINDVERSION not like", value, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionIn(List<String> values) {
            addCriterion("KINDVERSION in", values, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionNotIn(List<String> values) {
            addCriterion("KINDVERSION not in", values, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionBetween(String value1, String value2) {
            addCriterion("KINDVERSION between", value1, value2, "kindversion");
            return (Criteria) this;
        }

        public Criteria andKindversionNotBetween(String value1, String value2) {
            addCriterion("KINDVERSION not between", value1, value2, "kindversion");
            return (Criteria) this;
        }

        public Criteria andMatchlevelIsNull() {
            addCriterion("MATCHLEVEL is null");
            return (Criteria) this;
        }

        public Criteria andMatchlevelIsNotNull() {
            addCriterion("MATCHLEVEL is not null");
            return (Criteria) this;
        }

        public Criteria andMatchlevelEqualTo(String value) {
            addCriterion("MATCHLEVEL =", value, "matchlevel");
            return (Criteria) this;
        }

        public Criteria andMatchlevelNotEqualTo(String value) {
            addCriterion("MATCHLEVEL <>", value, "matchlevel");
            return (Criteria) this;
        }

        public Criteria andMatchlevelGreaterThan(String value) {
            addCriterion("MATCHLEVEL >", value, "matchlevel");
            return (Criteria) this;
        }

        public Criteria andMatchlevelGreaterThanOrEqualTo(String value) {
            addCriterion("MATCHLEVEL >=", value, "matchlevel");
            return (Criteria) this;
        }

        public Criteria andMatchlevelLessThan(String value) {
            addCriterion("MATCHLEVEL <", value, "matchlevel");
            return (Criteria) this;
        }

        public Criteria andMatchlevelLessThanOrEqualTo(String value) {
            addCriterion("MATCHLEVEL <=", value, "matchlevel");
            return (Criteria) this;
        }

        public Criteria andMatchlevelLike(String value) {
            addCriterion("MATCHLEVEL like", value, "matchlevel");
            return (Criteria) this;
        }

        public Criteria andMatchlevelNotLike(String value) {
            addCriterion("MATCHLEVEL not like", value, "matchlevel");
            return (Criteria) this;
        }

        public Criteria andMatchlevelIn(List<String> values) {
            addCriterion("MATCHLEVEL in", values, "matchlevel");
            return (Criteria) this;
        }

        public Criteria andMatchlevelNotIn(List<String> values) {
            addCriterion("MATCHLEVEL not in", values, "matchlevel");
            return (Criteria) this;
        }

        public Criteria andMatchlevelBetween(String value1, String value2) {
            addCriterion("MATCHLEVEL between", value1, value2, "matchlevel");
            return (Criteria) this;
        }

        public Criteria andMatchlevelNotBetween(String value1, String value2) {
            addCriterion("MATCHLEVEL not between", value1, value2, "matchlevel");
            return (Criteria) this;
        }

        public Criteria andEfilecnameIsNull() {
            addCriterion("EFILECNAME is null");
            return (Criteria) this;
        }

        public Criteria andEfilecnameIsNotNull() {
            addCriterion("EFILECNAME is not null");
            return (Criteria) this;
        }

        public Criteria andEfilecnameEqualTo(String value) {
            addCriterion("EFILECNAME =", value, "efilecname");
            return (Criteria) this;
        }

        public Criteria andEfilecnameNotEqualTo(String value) {
            addCriterion("EFILECNAME <>", value, "efilecname");
            return (Criteria) this;
        }

        public Criteria andEfilecnameGreaterThan(String value) {
            addCriterion("EFILECNAME >", value, "efilecname");
            return (Criteria) this;
        }

        public Criteria andEfilecnameGreaterThanOrEqualTo(String value) {
            addCriterion("EFILECNAME >=", value, "efilecname");
            return (Criteria) this;
        }

        public Criteria andEfilecnameLessThan(String value) {
            addCriterion("EFILECNAME <", value, "efilecname");
            return (Criteria) this;
        }

        public Criteria andEfilecnameLessThanOrEqualTo(String value) {
            addCriterion("EFILECNAME <=", value, "efilecname");
            return (Criteria) this;
        }

        public Criteria andEfilecnameLike(String value) {
            addCriterion("EFILECNAME like", value, "efilecname");
            return (Criteria) this;
        }

        public Criteria andEfilecnameNotLike(String value) {
            addCriterion("EFILECNAME not like", value, "efilecname");
            return (Criteria) this;
        }

        public Criteria andEfilecnameIn(List<String> values) {
            addCriterion("EFILECNAME in", values, "efilecname");
            return (Criteria) this;
        }

        public Criteria andEfilecnameNotIn(List<String> values) {
            addCriterion("EFILECNAME not in", values, "efilecname");
            return (Criteria) this;
        }

        public Criteria andEfilecnameBetween(String value1, String value2) {
            addCriterion("EFILECNAME between", value1, value2, "efilecname");
            return (Criteria) this;
        }

        public Criteria andEfilecnameNotBetween(String value1, String value2) {
            addCriterion("EFILECNAME not between", value1, value2, "efilecname");
            return (Criteria) this;
        }

        public Criteria andEfileenameIsNull() {
            addCriterion("EFILEENAME is null");
            return (Criteria) this;
        }

        public Criteria andEfileenameIsNotNull() {
            addCriterion("EFILEENAME is not null");
            return (Criteria) this;
        }

        public Criteria andEfileenameEqualTo(String value) {
            addCriterion("EFILEENAME =", value, "efileename");
            return (Criteria) this;
        }

        public Criteria andEfileenameNotEqualTo(String value) {
            addCriterion("EFILEENAME <>", value, "efileename");
            return (Criteria) this;
        }

        public Criteria andEfileenameGreaterThan(String value) {
            addCriterion("EFILEENAME >", value, "efileename");
            return (Criteria) this;
        }

        public Criteria andEfileenameGreaterThanOrEqualTo(String value) {
            addCriterion("EFILEENAME >=", value, "efileename");
            return (Criteria) this;
        }

        public Criteria andEfileenameLessThan(String value) {
            addCriterion("EFILEENAME <", value, "efileename");
            return (Criteria) this;
        }

        public Criteria andEfileenameLessThanOrEqualTo(String value) {
            addCriterion("EFILEENAME <=", value, "efileename");
            return (Criteria) this;
        }

        public Criteria andEfileenameLike(String value) {
            addCriterion("EFILEENAME like", value, "efileename");
            return (Criteria) this;
        }

        public Criteria andEfileenameNotLike(String value) {
            addCriterion("EFILEENAME not like", value, "efileename");
            return (Criteria) this;
        }

        public Criteria andEfileenameIn(List<String> values) {
            addCriterion("EFILEENAME in", values, "efileename");
            return (Criteria) this;
        }

        public Criteria andEfileenameNotIn(List<String> values) {
            addCriterion("EFILEENAME not in", values, "efileename");
            return (Criteria) this;
        }

        public Criteria andEfileenameBetween(String value1, String value2) {
            addCriterion("EFILEENAME between", value1, value2, "efileename");
            return (Criteria) this;
        }

        public Criteria andEfileenameNotBetween(String value1, String value2) {
            addCriterion("EFILEENAME not between", value1, value2, "efileename");
            return (Criteria) this;
        }

        public Criteria andPlancodeIsNull() {
            addCriterion("PLANCODE is null");
            return (Criteria) this;
        }

        public Criteria andPlancodeIsNotNull() {
            addCriterion("PLANCODE is not null");
            return (Criteria) this;
        }

        public Criteria andPlancodeEqualTo(String value) {
            addCriterion("PLANCODE =", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeNotEqualTo(String value) {
            addCriterion("PLANCODE <>", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeGreaterThan(String value) {
            addCriterion("PLANCODE >", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeGreaterThanOrEqualTo(String value) {
            addCriterion("PLANCODE >=", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeLessThan(String value) {
            addCriterion("PLANCODE <", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeLessThanOrEqualTo(String value) {
            addCriterion("PLANCODE <=", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeLike(String value) {
            addCriterion("PLANCODE like", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeNotLike(String value) {
            addCriterion("PLANCODE not like", value, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeIn(List<String> values) {
            addCriterion("PLANCODE in", values, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeNotIn(List<String> values) {
            addCriterion("PLANCODE not in", values, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeBetween(String value1, String value2) {
            addCriterion("PLANCODE between", value1, value2, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlancodeNotBetween(String value1, String value2) {
            addCriterion("PLANCODE not between", value1, value2, "plancode");
            return (Criteria) this;
        }

        public Criteria andPlannameIsNull() {
            addCriterion("PLANNAME is null");
            return (Criteria) this;
        }

        public Criteria andPlannameIsNotNull() {
            addCriterion("PLANNAME is not null");
            return (Criteria) this;
        }

        public Criteria andPlannameEqualTo(String value) {
            addCriterion("PLANNAME =", value, "planname");
            return (Criteria) this;
        }

        public Criteria andPlannameNotEqualTo(String value) {
            addCriterion("PLANNAME <>", value, "planname");
            return (Criteria) this;
        }

        public Criteria andPlannameGreaterThan(String value) {
            addCriterion("PLANNAME >", value, "planname");
            return (Criteria) this;
        }

        public Criteria andPlannameGreaterThanOrEqualTo(String value) {
            addCriterion("PLANNAME >=", value, "planname");
            return (Criteria) this;
        }

        public Criteria andPlannameLessThan(String value) {
            addCriterion("PLANNAME <", value, "planname");
            return (Criteria) this;
        }

        public Criteria andPlannameLessThanOrEqualTo(String value) {
            addCriterion("PLANNAME <=", value, "planname");
            return (Criteria) this;
        }

        public Criteria andPlannameLike(String value) {
            addCriterion("PLANNAME like", value, "planname");
            return (Criteria) this;
        }

        public Criteria andPlannameNotLike(String value) {
            addCriterion("PLANNAME not like", value, "planname");
            return (Criteria) this;
        }

        public Criteria andPlannameIn(List<String> values) {
            addCriterion("PLANNAME in", values, "planname");
            return (Criteria) this;
        }

        public Criteria andPlannameNotIn(List<String> values) {
            addCriterion("PLANNAME not in", values, "planname");
            return (Criteria) this;
        }

        public Criteria andPlannameBetween(String value1, String value2) {
            addCriterion("PLANNAME between", value1, value2, "planname");
            return (Criteria) this;
        }

        public Criteria andPlannameNotBetween(String value1, String value2) {
            addCriterion("PLANNAME not between", value1, value2, "planname");
            return (Criteria) this;
        }

        public Criteria andRisknameIsNull() {
            addCriterion("RISKNAME is null");
            return (Criteria) this;
        }

        public Criteria andRisknameIsNotNull() {
            addCriterion("RISKNAME is not null");
            return (Criteria) this;
        }

        public Criteria andRisknameEqualTo(String value) {
            addCriterion("RISKNAME =", value, "riskname");
            return (Criteria) this;
        }

        public Criteria andRisknameNotEqualTo(String value) {
            addCriterion("RISKNAME <>", value, "riskname");
            return (Criteria) this;
        }

        public Criteria andRisknameGreaterThan(String value) {
            addCriterion("RISKNAME >", value, "riskname");
            return (Criteria) this;
        }

        public Criteria andRisknameGreaterThanOrEqualTo(String value) {
            addCriterion("RISKNAME >=", value, "riskname");
            return (Criteria) this;
        }

        public Criteria andRisknameLessThan(String value) {
            addCriterion("RISKNAME <", value, "riskname");
            return (Criteria) this;
        }

        public Criteria andRisknameLessThanOrEqualTo(String value) {
            addCriterion("RISKNAME <=", value, "riskname");
            return (Criteria) this;
        }

        public Criteria andRisknameLike(String value) {
            addCriterion("RISKNAME like", value, "riskname");
            return (Criteria) this;
        }

        public Criteria andRisknameNotLike(String value) {
            addCriterion("RISKNAME not like", value, "riskname");
            return (Criteria) this;
        }

        public Criteria andRisknameIn(List<String> values) {
            addCriterion("RISKNAME in", values, "riskname");
            return (Criteria) this;
        }

        public Criteria andRisknameNotIn(List<String> values) {
            addCriterion("RISKNAME not in", values, "riskname");
            return (Criteria) this;
        }

        public Criteria andRisknameBetween(String value1, String value2) {
            addCriterion("RISKNAME between", value1, value2, "riskname");
            return (Criteria) this;
        }

        public Criteria andRisknameNotBetween(String value1, String value2) {
            addCriterion("RISKNAME not between", value1, value2, "riskname");
            return (Criteria) this;
        }

        public Criteria andKindcnameIsNull() {
            addCriterion("KINDCNAME is null");
            return (Criteria) this;
        }

        public Criteria andKindcnameIsNotNull() {
            addCriterion("KINDCNAME is not null");
            return (Criteria) this;
        }

        public Criteria andKindcnameEqualTo(String value) {
            addCriterion("KINDCNAME =", value, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameNotEqualTo(String value) {
            addCriterion("KINDCNAME <>", value, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameGreaterThan(String value) {
            addCriterion("KINDCNAME >", value, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameGreaterThanOrEqualTo(String value) {
            addCriterion("KINDCNAME >=", value, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameLessThan(String value) {
            addCriterion("KINDCNAME <", value, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameLessThanOrEqualTo(String value) {
            addCriterion("KINDCNAME <=", value, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameLike(String value) {
            addCriterion("KINDCNAME like", value, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameNotLike(String value) {
            addCriterion("KINDCNAME not like", value, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameIn(List<String> values) {
            addCriterion("KINDCNAME in", values, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameNotIn(List<String> values) {
            addCriterion("KINDCNAME not in", values, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameBetween(String value1, String value2) {
            addCriterion("KINDCNAME between", value1, value2, "kindcname");
            return (Criteria) this;
        }

        public Criteria andKindcnameNotBetween(String value1, String value2) {
            addCriterion("KINDCNAME not between", value1, value2, "kindcname");
            return (Criteria) this;
        }

        public Criteria andStartdateIsNull() {
            addCriterion("STARTDATE is null");
            return (Criteria) this;
        }

        public Criteria andStartdateIsNotNull() {
            addCriterion("STARTDATE is not null");
            return (Criteria) this;
        }

        public Criteria andStartdateEqualTo(Date value) {
            addCriterion("STARTDATE =", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateNotEqualTo(Date value) {
            addCriterion("STARTDATE <>", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateGreaterThan(Date value) {
            addCriterion("STARTDATE >", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateGreaterThanOrEqualTo(Date value) {
            addCriterion("STARTDATE >=", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateLessThan(Date value) {
            addCriterion("STARTDATE <", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateLessThanOrEqualTo(Date value) {
            addCriterion("STARTDATE <=", value, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateIn(List<Date> values) {
            addCriterion("STARTDATE in", values, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateNotIn(List<Date> values) {
            addCriterion("STARTDATE not in", values, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateBetween(Date value1, Date value2) {
            addCriterion("STARTDATE between", value1, value2, "startdate");
            return (Criteria) this;
        }

        public Criteria andStartdateNotBetween(Date value1, Date value2) {
            addCriterion("STARTDATE not between", value1, value2, "startdate");
            return (Criteria) this;
        }

        public Criteria andEnddateIsNull() {
            addCriterion("ENDDATE is null");
            return (Criteria) this;
        }

        public Criteria andEnddateIsNotNull() {
            addCriterion("ENDDATE is not null");
            return (Criteria) this;
        }

        public Criteria andEnddateEqualTo(Date value) {
            addCriterion("ENDDATE =", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateNotEqualTo(Date value) {
            addCriterion("ENDDATE <>", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateGreaterThan(Date value) {
            addCriterion("ENDDATE >", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateGreaterThanOrEqualTo(Date value) {
            addCriterion("ENDDATE >=", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateLessThan(Date value) {
            addCriterion("ENDDATE <", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateLessThanOrEqualTo(Date value) {
            addCriterion("ENDDATE <=", value, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateIn(List<Date> values) {
            addCriterion("ENDDATE in", values, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateNotIn(List<Date> values) {
            addCriterion("ENDDATE not in", values, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateBetween(Date value1, Date value2) {
            addCriterion("ENDDATE between", value1, value2, "enddate");
            return (Criteria) this;
        }

        public Criteria andEnddateNotBetween(Date value1, Date value2) {
            addCriterion("ENDDATE not between", value1, value2, "enddate");
            return (Criteria) this;
        }

        public Criteria andPathIsNull() {
            addCriterion("PATH is null");
            return (Criteria) this;
        }

        public Criteria andPathIsNotNull() {
            addCriterion("PATH is not null");
            return (Criteria) this;
        }

        public Criteria andPathEqualTo(String value) {
            addCriterion("PATH =", value, "path");
            return (Criteria) this;
        }

        public Criteria andPathNotEqualTo(String value) {
            addCriterion("PATH <>", value, "path");
            return (Criteria) this;
        }

        public Criteria andPathGreaterThan(String value) {
            addCriterion("PATH >", value, "path");
            return (Criteria) this;
        }

        public Criteria andPathGreaterThanOrEqualTo(String value) {
            addCriterion("PATH >=", value, "path");
            return (Criteria) this;
        }

        public Criteria andPathLessThan(String value) {
            addCriterion("PATH <", value, "path");
            return (Criteria) this;
        }

        public Criteria andPathLessThanOrEqualTo(String value) {
            addCriterion("PATH <=", value, "path");
            return (Criteria) this;
        }

        public Criteria andPathLike(String value) {
            addCriterion("PATH like", value, "path");
            return (Criteria) this;
        }

        public Criteria andPathNotLike(String value) {
            addCriterion("PATH not like", value, "path");
            return (Criteria) this;
        }

        public Criteria andPathIn(List<String> values) {
            addCriterion("PATH in", values, "path");
            return (Criteria) this;
        }

        public Criteria andPathNotIn(List<String> values) {
            addCriterion("PATH not in", values, "path");
            return (Criteria) this;
        }

        public Criteria andPathBetween(String value1, String value2) {
            addCriterion("PATH between", value1, value2, "path");
            return (Criteria) this;
        }

        public Criteria andPathNotBetween(String value1, String value2) {
            addCriterion("PATH not between", value1, value2, "path");
            return (Criteria) this;
        }

        public Criteria andImgnameIsNull() {
            addCriterion("IMGNAME is null");
            return (Criteria) this;
        }

        public Criteria andImgnameIsNotNull() {
            addCriterion("IMGNAME is not null");
            return (Criteria) this;
        }

        public Criteria andImgnameEqualTo(String value) {
            addCriterion("IMGNAME =", value, "imgname");
            return (Criteria) this;
        }

        public Criteria andImgnameNotEqualTo(String value) {
            addCriterion("IMGNAME <>", value, "imgname");
            return (Criteria) this;
        }

        public Criteria andImgnameGreaterThan(String value) {
            addCriterion("IMGNAME >", value, "imgname");
            return (Criteria) this;
        }

        public Criteria andImgnameGreaterThanOrEqualTo(String value) {
            addCriterion("IMGNAME >=", value, "imgname");
            return (Criteria) this;
        }

        public Criteria andImgnameLessThan(String value) {
            addCriterion("IMGNAME <", value, "imgname");
            return (Criteria) this;
        }

        public Criteria andImgnameLessThanOrEqualTo(String value) {
            addCriterion("IMGNAME <=", value, "imgname");
            return (Criteria) this;
        }

        public Criteria andImgnameLike(String value) {
            addCriterion("IMGNAME like", value, "imgname");
            return (Criteria) this;
        }

        public Criteria andImgnameNotLike(String value) {
            addCriterion("IMGNAME not like", value, "imgname");
            return (Criteria) this;
        }

        public Criteria andImgnameIn(List<String> values) {
            addCriterion("IMGNAME in", values, "imgname");
            return (Criteria) this;
        }

        public Criteria andImgnameNotIn(List<String> values) {
            addCriterion("IMGNAME not in", values, "imgname");
            return (Criteria) this;
        }

        public Criteria andImgnameBetween(String value1, String value2) {
            addCriterion("IMGNAME between", value1, value2, "imgname");
            return (Criteria) this;
        }

        public Criteria andImgnameNotBetween(String value1, String value2) {
            addCriterion("IMGNAME not between", value1, value2, "imgname");
            return (Criteria) this;
        }

        public Criteria andImglengthIsNull() {
            addCriterion("IMGLENGTH is null");
            return (Criteria) this;
        }

        public Criteria andImglengthIsNotNull() {
            addCriterion("IMGLENGTH is not null");
            return (Criteria) this;
        }

        public Criteria andImglengthEqualTo(BigDecimal value) {
            addCriterion("IMGLENGTH =", value, "imglength");
            return (Criteria) this;
        }

        public Criteria andImglengthNotEqualTo(BigDecimal value) {
            addCriterion("IMGLENGTH <>", value, "imglength");
            return (Criteria) this;
        }

        public Criteria andImglengthGreaterThan(BigDecimal value) {
            addCriterion("IMGLENGTH >", value, "imglength");
            return (Criteria) this;
        }

        public Criteria andImglengthGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("IMGLENGTH >=", value, "imglength");
            return (Criteria) this;
        }

        public Criteria andImglengthLessThan(BigDecimal value) {
            addCriterion("IMGLENGTH <", value, "imglength");
            return (Criteria) this;
        }

        public Criteria andImglengthLessThanOrEqualTo(BigDecimal value) {
            addCriterion("IMGLENGTH <=", value, "imglength");
            return (Criteria) this;
        }

        public Criteria andImglengthIn(List<BigDecimal> values) {
            addCriterion("IMGLENGTH in", values, "imglength");
            return (Criteria) this;
        }

        public Criteria andImglengthNotIn(List<BigDecimal> values) {
            addCriterion("IMGLENGTH not in", values, "imglength");
            return (Criteria) this;
        }

        public Criteria andImglengthBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("IMGLENGTH between", value1, value2, "imglength");
            return (Criteria) this;
        }

        public Criteria andImglengthNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("IMGLENGTH not between", value1, value2, "imglength");
            return (Criteria) this;
        }

        public Criteria andImgwidthIsNull() {
            addCriterion("IMGWIDTH is null");
            return (Criteria) this;
        }

        public Criteria andImgwidthIsNotNull() {
            addCriterion("IMGWIDTH is not null");
            return (Criteria) this;
        }

        public Criteria andImgwidthEqualTo(BigDecimal value) {
            addCriterion("IMGWIDTH =", value, "imgwidth");
            return (Criteria) this;
        }

        public Criteria andImgwidthNotEqualTo(BigDecimal value) {
            addCriterion("IMGWIDTH <>", value, "imgwidth");
            return (Criteria) this;
        }

        public Criteria andImgwidthGreaterThan(BigDecimal value) {
            addCriterion("IMGWIDTH >", value, "imgwidth");
            return (Criteria) this;
        }

        public Criteria andImgwidthGreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("IMGWIDTH >=", value, "imgwidth");
            return (Criteria) this;
        }

        public Criteria andImgwidthLessThan(BigDecimal value) {
            addCriterion("IMGWIDTH <", value, "imgwidth");
            return (Criteria) this;
        }

        public Criteria andImgwidthLessThanOrEqualTo(BigDecimal value) {
            addCriterion("IMGWIDTH <=", value, "imgwidth");
            return (Criteria) this;
        }

        public Criteria andImgwidthIn(List<BigDecimal> values) {
            addCriterion("IMGWIDTH in", values, "imgwidth");
            return (Criteria) this;
        }

        public Criteria andImgwidthNotIn(List<BigDecimal> values) {
            addCriterion("IMGWIDTH not in", values, "imgwidth");
            return (Criteria) this;
        }

        public Criteria andImgwidthBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("IMGWIDTH between", value1, value2, "imgwidth");
            return (Criteria) this;
        }

        public Criteria andImgwidthNotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("IMGWIDTH not between", value1, value2, "imgwidth");
            return (Criteria) this;
        }

        public Criteria andFlagIsNull() {
            addCriterion("FLAG is null");
            return (Criteria) this;
        }

        public Criteria andFlagIsNotNull() {
            addCriterion("FLAG is not null");
            return (Criteria) this;
        }

        public Criteria andFlagEqualTo(String value) {
            addCriterion("FLAG =", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotEqualTo(String value) {
            addCriterion("FLAG <>", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThan(String value) {
            addCriterion("FLAG >", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagGreaterThanOrEqualTo(String value) {
            addCriterion("FLAG >=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThan(String value) {
            addCriterion("FLAG <", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLessThanOrEqualTo(String value) {
            addCriterion("FLAG <=", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagLike(String value) {
            addCriterion("FLAG like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotLike(String value) {
            addCriterion("FLAG not like", value, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagIn(List<String> values) {
            addCriterion("FLAG in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotIn(List<String> values) {
            addCriterion("FLAG not in", values, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagBetween(String value1, String value2) {
            addCriterion("FLAG between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andFlagNotBetween(String value1, String value2) {
            addCriterion("FLAG not between", value1, value2, "flag");
            return (Criteria) this;
        }

        public Criteria andValidatestatusIsNull() {
            addCriterion("VALIDATESTATUS is null");
            return (Criteria) this;
        }

        public Criteria andValidatestatusIsNotNull() {
            addCriterion("VALIDATESTATUS is not null");
            return (Criteria) this;
        }

        public Criteria andValidatestatusEqualTo(String value) {
            addCriterion("VALIDATESTATUS =", value, "validatestatus");
            return (Criteria) this;
        }

        public Criteria andValidatestatusNotEqualTo(String value) {
            addCriterion("VALIDATESTATUS <>", value, "validatestatus");
            return (Criteria) this;
        }

        public Criteria andValidatestatusGreaterThan(String value) {
            addCriterion("VALIDATESTATUS >", value, "validatestatus");
            return (Criteria) this;
        }

        public Criteria andValidatestatusGreaterThanOrEqualTo(String value) {
            addCriterion("VALIDATESTATUS >=", value, "validatestatus");
            return (Criteria) this;
        }

        public Criteria andValidatestatusLessThan(String value) {
            addCriterion("VALIDATESTATUS <", value, "validatestatus");
            return (Criteria) this;
        }

        public Criteria andValidatestatusLessThanOrEqualTo(String value) {
            addCriterion("VALIDATESTATUS <=", value, "validatestatus");
            return (Criteria) this;
        }

        public Criteria andValidatestatusLike(String value) {
            addCriterion("VALIDATESTATUS like", value, "validatestatus");
            return (Criteria) this;
        }

        public Criteria andValidatestatusNotLike(String value) {
            addCriterion("VALIDATESTATUS not like", value, "validatestatus");
            return (Criteria) this;
        }

        public Criteria andValidatestatusIn(List<String> values) {
            addCriterion("VALIDATESTATUS in", values, "validatestatus");
            return (Criteria) this;
        }

        public Criteria andValidatestatusNotIn(List<String> values) {
            addCriterion("VALIDATESTATUS not in", values, "validatestatus");
            return (Criteria) this;
        }

        public Criteria andValidatestatusBetween(String value1, String value2) {
            addCriterion("VALIDATESTATUS between", value1, value2, "validatestatus");
            return (Criteria) this;
        }

        public Criteria andValidatestatusNotBetween(String value1, String value2) {
            addCriterion("VALIDATESTATUS not between", value1, value2, "validatestatus");
            return (Criteria) this;
        }

        public Criteria andAttribute1IsNull() {
            addCriterion("ATTRIBUTE1 is null");
            return (Criteria) this;
        }

        public Criteria andAttribute1IsNotNull() {
            addCriterion("ATTRIBUTE1 is not null");
            return (Criteria) this;
        }

        public Criteria andAttribute1EqualTo(String value) {
            addCriterion("ATTRIBUTE1 =", value, "attribute1");
            return (Criteria) this;
        }

        public Criteria andAttribute1NotEqualTo(String value) {
            addCriterion("ATTRIBUTE1 <>", value, "attribute1");
            return (Criteria) this;
        }

        public Criteria andAttribute1GreaterThan(String value) {
            addCriterion("ATTRIBUTE1 >", value, "attribute1");
            return (Criteria) this;
        }

        public Criteria andAttribute1GreaterThanOrEqualTo(String value) {
            addCriterion("ATTRIBUTE1 >=", value, "attribute1");
            return (Criteria) this;
        }

        public Criteria andAttribute1LessThan(String value) {
            addCriterion("ATTRIBUTE1 <", value, "attribute1");
            return (Criteria) this;
        }

        public Criteria andAttribute1LessThanOrEqualTo(String value) {
            addCriterion("ATTRIBUTE1 <=", value, "attribute1");
            return (Criteria) this;
        }

        public Criteria andAttribute1Like(String value) {
            addCriterion("ATTRIBUTE1 like", value, "attribute1");
            return (Criteria) this;
        }

        public Criteria andAttribute1NotLike(String value) {
            addCriterion("ATTRIBUTE1 not like", value, "attribute1");
            return (Criteria) this;
        }

        public Criteria andAttribute1In(List<String> values) {
            addCriterion("ATTRIBUTE1 in", values, "attribute1");
            return (Criteria) this;
        }

        public Criteria andAttribute1NotIn(List<String> values) {
            addCriterion("ATTRIBUTE1 not in", values, "attribute1");
            return (Criteria) this;
        }

        public Criteria andAttribute1Between(String value1, String value2) {
            addCriterion("ATTRIBUTE1 between", value1, value2, "attribute1");
            return (Criteria) this;
        }

        public Criteria andAttribute1NotBetween(String value1, String value2) {
            addCriterion("ATTRIBUTE1 not between", value1, value2, "attribute1");
            return (Criteria) this;
        }

        public Criteria andAttribute2IsNull() {
            addCriterion("ATTRIBUTE2 is null");
            return (Criteria) this;
        }

        public Criteria andAttribute2IsNotNull() {
            addCriterion("ATTRIBUTE2 is not null");
            return (Criteria) this;
        }

        public Criteria andAttribute2EqualTo(String value) {
            addCriterion("ATTRIBUTE2 =", value, "attribute2");
            return (Criteria) this;
        }

        public Criteria andAttribute2NotEqualTo(String value) {
            addCriterion("ATTRIBUTE2 <>", value, "attribute2");
            return (Criteria) this;
        }

        public Criteria andAttribute2GreaterThan(String value) {
            addCriterion("ATTRIBUTE2 >", value, "attribute2");
            return (Criteria) this;
        }

        public Criteria andAttribute2GreaterThanOrEqualTo(String value) {
            addCriterion("ATTRIBUTE2 >=", value, "attribute2");
            return (Criteria) this;
        }

        public Criteria andAttribute2LessThan(String value) {
            addCriterion("ATTRIBUTE2 <", value, "attribute2");
            return (Criteria) this;
        }

        public Criteria andAttribute2LessThanOrEqualTo(String value) {
            addCriterion("ATTRIBUTE2 <=", value, "attribute2");
            return (Criteria) this;
        }

        public Criteria andAttribute2Like(String value) {
            addCriterion("ATTRIBUTE2 like", value, "attribute2");
            return (Criteria) this;
        }

        public Criteria andAttribute2NotLike(String value) {
            addCriterion("ATTRIBUTE2 not like", value, "attribute2");
            return (Criteria) this;
        }

        public Criteria andAttribute2In(List<String> values) {
            addCriterion("ATTRIBUTE2 in", values, "attribute2");
            return (Criteria) this;
        }

        public Criteria andAttribute2NotIn(List<String> values) {
            addCriterion("ATTRIBUTE2 not in", values, "attribute2");
            return (Criteria) this;
        }

        public Criteria andAttribute2Between(String value1, String value2) {
            addCriterion("ATTRIBUTE2 between", value1, value2, "attribute2");
            return (Criteria) this;
        }

        public Criteria andAttribute2NotBetween(String value1, String value2) {
            addCriterion("ATTRIBUTE2 not between", value1, value2, "attribute2");
            return (Criteria) this;
        }

        public Criteria andAttribute3IsNull() {
            addCriterion("ATTRIBUTE3 is null");
            return (Criteria) this;
        }

        public Criteria andAttribute3IsNotNull() {
            addCriterion("ATTRIBUTE3 is not null");
            return (Criteria) this;
        }

        public Criteria andAttribute3EqualTo(String value) {
            addCriterion("ATTRIBUTE3 =", value, "attribute3");
            return (Criteria) this;
        }

        public Criteria andAttribute3NotEqualTo(String value) {
            addCriterion("ATTRIBUTE3 <>", value, "attribute3");
            return (Criteria) this;
        }

        public Criteria andAttribute3GreaterThan(String value) {
            addCriterion("ATTRIBUTE3 >", value, "attribute3");
            return (Criteria) this;
        }

        public Criteria andAttribute3GreaterThanOrEqualTo(String value) {
            addCriterion("ATTRIBUTE3 >=", value, "attribute3");
            return (Criteria) this;
        }

        public Criteria andAttribute3LessThan(String value) {
            addCriterion("ATTRIBUTE3 <", value, "attribute3");
            return (Criteria) this;
        }

        public Criteria andAttribute3LessThanOrEqualTo(String value) {
            addCriterion("ATTRIBUTE3 <=", value, "attribute3");
            return (Criteria) this;
        }

        public Criteria andAttribute3Like(String value) {
            addCriterion("ATTRIBUTE3 like", value, "attribute3");
            return (Criteria) this;
        }

        public Criteria andAttribute3NotLike(String value) {
            addCriterion("ATTRIBUTE3 not like", value, "attribute3");
            return (Criteria) this;
        }

        public Criteria andAttribute3In(List<String> values) {
            addCriterion("ATTRIBUTE3 in", values, "attribute3");
            return (Criteria) this;
        }

        public Criteria andAttribute3NotIn(List<String> values) {
            addCriterion("ATTRIBUTE3 not in", values, "attribute3");
            return (Criteria) this;
        }

        public Criteria andAttribute3Between(String value1, String value2) {
            addCriterion("ATTRIBUTE3 between", value1, value2, "attribute3");
            return (Criteria) this;
        }

        public Criteria andAttribute3NotBetween(String value1, String value2) {
            addCriterion("ATTRIBUTE3 not between", value1, value2, "attribute3");
            return (Criteria) this;
        }

        public Criteria andAttribute4IsNull() {
            addCriterion("ATTRIBUTE4 is null");
            return (Criteria) this;
        }

        public Criteria andAttribute4IsNotNull() {
            addCriterion("ATTRIBUTE4 is not null");
            return (Criteria) this;
        }

        public Criteria andAttribute4EqualTo(String value) {
            addCriterion("ATTRIBUTE4 =", value, "attribute4");
            return (Criteria) this;
        }

        public Criteria andAttribute4NotEqualTo(String value) {
            addCriterion("ATTRIBUTE4 <>", value, "attribute4");
            return (Criteria) this;
        }

        public Criteria andAttribute4GreaterThan(String value) {
            addCriterion("ATTRIBUTE4 >", value, "attribute4");
            return (Criteria) this;
        }

        public Criteria andAttribute4GreaterThanOrEqualTo(String value) {
            addCriterion("ATTRIBUTE4 >=", value, "attribute4");
            return (Criteria) this;
        }

        public Criteria andAttribute4LessThan(String value) {
            addCriterion("ATTRIBUTE4 <", value, "attribute4");
            return (Criteria) this;
        }

        public Criteria andAttribute4LessThanOrEqualTo(String value) {
            addCriterion("ATTRIBUTE4 <=", value, "attribute4");
            return (Criteria) this;
        }

        public Criteria andAttribute4Like(String value) {
            addCriterion("ATTRIBUTE4 like", value, "attribute4");
            return (Criteria) this;
        }

        public Criteria andAttribute4NotLike(String value) {
            addCriterion("ATTRIBUTE4 not like", value, "attribute4");
            return (Criteria) this;
        }

        public Criteria andAttribute4In(List<String> values) {
            addCriterion("ATTRIBUTE4 in", values, "attribute4");
            return (Criteria) this;
        }

        public Criteria andAttribute4NotIn(List<String> values) {
            addCriterion("ATTRIBUTE4 not in", values, "attribute4");
            return (Criteria) this;
        }

        public Criteria andAttribute4Between(String value1, String value2) {
            addCriterion("ATTRIBUTE4 between", value1, value2, "attribute4");
            return (Criteria) this;
        }

        public Criteria andAttribute4NotBetween(String value1, String value2) {
            addCriterion("ATTRIBUTE4 not between", value1, value2, "attribute4");
            return (Criteria) this;
        }

        public Criteria andAttribute5IsNull() {
            addCriterion("ATTRIBUTE5 is null");
            return (Criteria) this;
        }

        public Criteria andAttribute5IsNotNull() {
            addCriterion("ATTRIBUTE5 is not null");
            return (Criteria) this;
        }

        public Criteria andAttribute5EqualTo(String value) {
            addCriterion("ATTRIBUTE5 =", value, "attribute5");
            return (Criteria) this;
        }

        public Criteria andAttribute5NotEqualTo(String value) {
            addCriterion("ATTRIBUTE5 <>", value, "attribute5");
            return (Criteria) this;
        }

        public Criteria andAttribute5GreaterThan(String value) {
            addCriterion("ATTRIBUTE5 >", value, "attribute5");
            return (Criteria) this;
        }

        public Criteria andAttribute5GreaterThanOrEqualTo(String value) {
            addCriterion("ATTRIBUTE5 >=", value, "attribute5");
            return (Criteria) this;
        }

        public Criteria andAttribute5LessThan(String value) {
            addCriterion("ATTRIBUTE5 <", value, "attribute5");
            return (Criteria) this;
        }

        public Criteria andAttribute5LessThanOrEqualTo(String value) {
            addCriterion("ATTRIBUTE5 <=", value, "attribute5");
            return (Criteria) this;
        }

        public Criteria andAttribute5Like(String value) {
            addCriterion("ATTRIBUTE5 like", value, "attribute5");
            return (Criteria) this;
        }

        public Criteria andAttribute5NotLike(String value) {
            addCriterion("ATTRIBUTE5 not like", value, "attribute5");
            return (Criteria) this;
        }

        public Criteria andAttribute5In(List<String> values) {
            addCriterion("ATTRIBUTE5 in", values, "attribute5");
            return (Criteria) this;
        }

        public Criteria andAttribute5NotIn(List<String> values) {
            addCriterion("ATTRIBUTE5 not in", values, "attribute5");
            return (Criteria) this;
        }

        public Criteria andAttribute5Between(String value1, String value2) {
            addCriterion("ATTRIBUTE5 between", value1, value2, "attribute5");
            return (Criteria) this;
        }

        public Criteria andAttribute5NotBetween(String value1, String value2) {
            addCriterion("ATTRIBUTE5 not between", value1, value2, "attribute5");
            return (Criteria) this;
        }

        public Criteria andAttribute6IsNull() {
            addCriterion("ATTRIBUTE6 is null");
            return (Criteria) this;
        }

        public Criteria andAttribute6IsNotNull() {
            addCriterion("ATTRIBUTE6 is not null");
            return (Criteria) this;
        }

        public Criteria andAttribute6EqualTo(String value) {
            addCriterion("ATTRIBUTE6 =", value, "attribute6");
            return (Criteria) this;
        }

        public Criteria andAttribute6NotEqualTo(String value) {
            addCriterion("ATTRIBUTE6 <>", value, "attribute6");
            return (Criteria) this;
        }

        public Criteria andAttribute6GreaterThan(String value) {
            addCriterion("ATTRIBUTE6 >", value, "attribute6");
            return (Criteria) this;
        }

        public Criteria andAttribute6GreaterThanOrEqualTo(String value) {
            addCriterion("ATTRIBUTE6 >=", value, "attribute6");
            return (Criteria) this;
        }

        public Criteria andAttribute6LessThan(String value) {
            addCriterion("ATTRIBUTE6 <", value, "attribute6");
            return (Criteria) this;
        }

        public Criteria andAttribute6LessThanOrEqualTo(String value) {
            addCriterion("ATTRIBUTE6 <=", value, "attribute6");
            return (Criteria) this;
        }

        public Criteria andAttribute6Like(String value) {
            addCriterion("ATTRIBUTE6 like", value, "attribute6");
            return (Criteria) this;
        }

        public Criteria andAttribute6NotLike(String value) {
            addCriterion("ATTRIBUTE6 not like", value, "attribute6");
            return (Criteria) this;
        }

        public Criteria andAttribute6In(List<String> values) {
            addCriterion("ATTRIBUTE6 in", values, "attribute6");
            return (Criteria) this;
        }

        public Criteria andAttribute6NotIn(List<String> values) {
            addCriterion("ATTRIBUTE6 not in", values, "attribute6");
            return (Criteria) this;
        }

        public Criteria andAttribute6Between(String value1, String value2) {
            addCriterion("ATTRIBUTE6 between", value1, value2, "attribute6");
            return (Criteria) this;
        }

        public Criteria andAttribute6NotBetween(String value1, String value2) {
            addCriterion("ATTRIBUTE6 not between", value1, value2, "attribute6");
            return (Criteria) this;
        }

        public Criteria andInserttimeIsNull() {
            addCriterion("INSERTTIME is null");
            return (Criteria) this;
        }

        public Criteria andInserttimeIsNotNull() {
            addCriterion("INSERTTIME is not null");
            return (Criteria) this;
        }

        public Criteria andInserttimeEqualTo(Date value) {
            addCriterion("INSERTTIME =", value, "inserttime");
            return (Criteria) this;
        }

        public Criteria andInserttimeNotEqualTo(Date value) {
            addCriterion("INSERTTIME <>", value, "inserttime");
            return (Criteria) this;
        }

        public Criteria andInserttimeGreaterThan(Date value) {
            addCriterion("INSERTTIME >", value, "inserttime");
            return (Criteria) this;
        }

        public Criteria andInserttimeGreaterThanOrEqualTo(Date value) {
            addCriterion("INSERTTIME >=", value, "inserttime");
            return (Criteria) this;
        }

        public Criteria andInserttimeLessThan(Date value) {
            addCriterion("INSERTTIME <", value, "inserttime");
            return (Criteria) this;
        }

        public Criteria andInserttimeLessThanOrEqualTo(Date value) {
            addCriterion("INSERTTIME <=", value, "inserttime");
            return (Criteria) this;
        }

        public Criteria andInserttimeIn(List<Date> values) {
            addCriterion("INSERTTIME in", values, "inserttime");
            return (Criteria) this;
        }

        public Criteria andInserttimeNotIn(List<Date> values) {
            addCriterion("INSERTTIME not in", values, "inserttime");
            return (Criteria) this;
        }

        public Criteria andInserttimeBetween(Date value1, Date value2) {
            addCriterion("INSERTTIME between", value1, value2, "inserttime");
            return (Criteria) this;
        }

        public Criteria andInserttimeNotBetween(Date value1, Date value2) {
            addCriterion("INSERTTIME not between", value1, value2, "inserttime");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNull() {
            addCriterion("UPDATEDATE is null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIsNotNull() {
            addCriterion("UPDATEDATE is not null");
            return (Criteria) this;
        }

        public Criteria andUpdatedateEqualTo(Date value) {
            addCriterion("UPDATEDATE =", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotEqualTo(Date value) {
            addCriterion("UPDATEDATE <>", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThan(Date value) {
            addCriterion("UPDATEDATE >", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateGreaterThanOrEqualTo(Date value) {
            addCriterion("UPDATEDATE >=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThan(Date value) {
            addCriterion("UPDATEDATE <", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateLessThanOrEqualTo(Date value) {
            addCriterion("UPDATEDATE <=", value, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateIn(List<Date> values) {
            addCriterion("UPDATEDATE in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotIn(List<Date> values) {
            addCriterion("UPDATEDATE not in", values, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateBetween(Date value1, Date value2) {
            addCriterion("UPDATEDATE between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andUpdatedateNotBetween(Date value1, Date value2) {
            addCriterion("UPDATEDATE not between", value1, value2, "updatedate");
            return (Criteria) this;
        }

        public Criteria andImgname2IsNull() {
            addCriterion("IMGNAME2 is null");
            return (Criteria) this;
        }

        public Criteria andImgname2IsNotNull() {
            addCriterion("IMGNAME2 is not null");
            return (Criteria) this;
        }

        public Criteria andImgname2EqualTo(String value) {
            addCriterion("IMGNAME2 =", value, "imgname2");
            return (Criteria) this;
        }

        public Criteria andImgname2NotEqualTo(String value) {
            addCriterion("IMGNAME2 <>", value, "imgname2");
            return (Criteria) this;
        }

        public Criteria andImgname2GreaterThan(String value) {
            addCriterion("IMGNAME2 >", value, "imgname2");
            return (Criteria) this;
        }

        public Criteria andImgname2GreaterThanOrEqualTo(String value) {
            addCriterion("IMGNAME2 >=", value, "imgname2");
            return (Criteria) this;
        }

        public Criteria andImgname2LessThan(String value) {
            addCriterion("IMGNAME2 <", value, "imgname2");
            return (Criteria) this;
        }

        public Criteria andImgname2LessThanOrEqualTo(String value) {
            addCriterion("IMGNAME2 <=", value, "imgname2");
            return (Criteria) this;
        }

        public Criteria andImgname2Like(String value) {
            addCriterion("IMGNAME2 like", value, "imgname2");
            return (Criteria) this;
        }

        public Criteria andImgname2NotLike(String value) {
            addCriterion("IMGNAME2 not like", value, "imgname2");
            return (Criteria) this;
        }

        public Criteria andImgname2In(List<String> values) {
            addCriterion("IMGNAME2 in", values, "imgname2");
            return (Criteria) this;
        }

        public Criteria andImgname2NotIn(List<String> values) {
            addCriterion("IMGNAME2 not in", values, "imgname2");
            return (Criteria) this;
        }

        public Criteria andImgname2Between(String value1, String value2) {
            addCriterion("IMGNAME2 between", value1, value2, "imgname2");
            return (Criteria) this;
        }

        public Criteria andImgname2NotBetween(String value1, String value2) {
            addCriterion("IMGNAME2 not between", value1, value2, "imgname2");
            return (Criteria) this;
        }

        public Criteria andImglength2IsNull() {
            addCriterion("IMGLENGTH2 is null");
            return (Criteria) this;
        }

        public Criteria andImglength2IsNotNull() {
            addCriterion("IMGLENGTH2 is not null");
            return (Criteria) this;
        }

        public Criteria andImglength2EqualTo(BigDecimal value) {
            addCriterion("IMGLENGTH2 =", value, "imglength2");
            return (Criteria) this;
        }

        public Criteria andImglength2NotEqualTo(BigDecimal value) {
            addCriterion("IMGLENGTH2 <>", value, "imglength2");
            return (Criteria) this;
        }

        public Criteria andImglength2GreaterThan(BigDecimal value) {
            addCriterion("IMGLENGTH2 >", value, "imglength2");
            return (Criteria) this;
        }

        public Criteria andImglength2GreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("IMGLENGTH2 >=", value, "imglength2");
            return (Criteria) this;
        }

        public Criteria andImglength2LessThan(BigDecimal value) {
            addCriterion("IMGLENGTH2 <", value, "imglength2");
            return (Criteria) this;
        }

        public Criteria andImglength2LessThanOrEqualTo(BigDecimal value) {
            addCriterion("IMGLENGTH2 <=", value, "imglength2");
            return (Criteria) this;
        }

        public Criteria andImglength2In(List<BigDecimal> values) {
            addCriterion("IMGLENGTH2 in", values, "imglength2");
            return (Criteria) this;
        }

        public Criteria andImglength2NotIn(List<BigDecimal> values) {
            addCriterion("IMGLENGTH2 not in", values, "imglength2");
            return (Criteria) this;
        }

        public Criteria andImglength2Between(BigDecimal value1, BigDecimal value2) {
            addCriterion("IMGLENGTH2 between", value1, value2, "imglength2");
            return (Criteria) this;
        }

        public Criteria andImglength2NotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("IMGLENGTH2 not between", value1, value2, "imglength2");
            return (Criteria) this;
        }

        public Criteria andImgwidth2IsNull() {
            addCriterion("IMGWIDTH2 is null");
            return (Criteria) this;
        }

        public Criteria andImgwidth2IsNotNull() {
            addCriterion("IMGWIDTH2 is not null");
            return (Criteria) this;
        }

        public Criteria andImgwidth2EqualTo(BigDecimal value) {
            addCriterion("IMGWIDTH2 =", value, "imgwidth2");
            return (Criteria) this;
        }

        public Criteria andImgwidth2NotEqualTo(BigDecimal value) {
            addCriterion("IMGWIDTH2 <>", value, "imgwidth2");
            return (Criteria) this;
        }

        public Criteria andImgwidth2GreaterThan(BigDecimal value) {
            addCriterion("IMGWIDTH2 >", value, "imgwidth2");
            return (Criteria) this;
        }

        public Criteria andImgwidth2GreaterThanOrEqualTo(BigDecimal value) {
            addCriterion("IMGWIDTH2 >=", value, "imgwidth2");
            return (Criteria) this;
        }

        public Criteria andImgwidth2LessThan(BigDecimal value) {
            addCriterion("IMGWIDTH2 <", value, "imgwidth2");
            return (Criteria) this;
        }

        public Criteria andImgwidth2LessThanOrEqualTo(BigDecimal value) {
            addCriterion("IMGWIDTH2 <=", value, "imgwidth2");
            return (Criteria) this;
        }

        public Criteria andImgwidth2In(List<BigDecimal> values) {
            addCriterion("IMGWIDTH2 in", values, "imgwidth2");
            return (Criteria) this;
        }

        public Criteria andImgwidth2NotIn(List<BigDecimal> values) {
            addCriterion("IMGWIDTH2 not in", values, "imgwidth2");
            return (Criteria) this;
        }

        public Criteria andImgwidth2Between(BigDecimal value1, BigDecimal value2) {
            addCriterion("IMGWIDTH2 between", value1, value2, "imgwidth2");
            return (Criteria) this;
        }

        public Criteria andImgwidth2NotBetween(BigDecimal value1, BigDecimal value2) {
            addCriterion("IMGWIDTH2 not between", value1, value2, "imgwidth2");
            return (Criteria) this;
        }

        public Criteria andChannelIsNull() {
            addCriterion("CHANNEL is null");
            return (Criteria) this;
        }

        public Criteria andChannelIsNotNull() {
            addCriterion("CHANNEL is not null");
            return (Criteria) this;
        }

        public Criteria andChannelEqualTo(String value) {
            addCriterion("CHANNEL =", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelNotEqualTo(String value) {
            addCriterion("CHANNEL <>", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelGreaterThan(String value) {
            addCriterion("CHANNEL >", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelGreaterThanOrEqualTo(String value) {
            addCriterion("CHANNEL >=", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelLessThan(String value) {
            addCriterion("CHANNEL <", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelLessThanOrEqualTo(String value) {
            addCriterion("CHANNEL <=", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelLike(String value) {
            addCriterion("CHANNEL like", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelNotLike(String value) {
            addCriterion("CHANNEL not like", value, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelIn(List<String> values) {
            addCriterion("CHANNEL in", values, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelNotIn(List<String> values) {
            addCriterion("CHANNEL not in", values, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelBetween(String value1, String value2) {
            addCriterion("CHANNEL between", value1, value2, "channel");
            return (Criteria) this;
        }

        public Criteria andChannelNotBetween(String value1, String value2) {
            addCriterion("CHANNEL not between", value1, value2, "channel");
            return (Criteria) this;
        }

        public Criteria andSubmitcomIsNull() {
            addCriterion("SUBMITCOM is null");
            return (Criteria) this;
        }

        public Criteria andSubmitcomIsNotNull() {
            addCriterion("SUBMITCOM is not null");
            return (Criteria) this;
        }

        public Criteria andSubmitcomEqualTo(String value) {
            addCriterion("SUBMITCOM =", value, "submitcom");
            return (Criteria) this;
        }

        public Criteria andSubmitcomNotEqualTo(String value) {
            addCriterion("SUBMITCOM <>", value, "submitcom");
            return (Criteria) this;
        }

        public Criteria andSubmitcomGreaterThan(String value) {
            addCriterion("SUBMITCOM >", value, "submitcom");
            return (Criteria) this;
        }

        public Criteria andSubmitcomGreaterThanOrEqualTo(String value) {
            addCriterion("SUBMITCOM >=", value, "submitcom");
            return (Criteria) this;
        }

        public Criteria andSubmitcomLessThan(String value) {
            addCriterion("SUBMITCOM <", value, "submitcom");
            return (Criteria) this;
        }

        public Criteria andSubmitcomLessThanOrEqualTo(String value) {
            addCriterion("SUBMITCOM <=", value, "submitcom");
            return (Criteria) this;
        }

        public Criteria andSubmitcomLike(String value) {
            addCriterion("SUBMITCOM like", value, "submitcom");
            return (Criteria) this;
        }

        public Criteria andSubmitcomNotLike(String value) {
            addCriterion("SUBMITCOM not like", value, "submitcom");
            return (Criteria) this;
        }

        public Criteria andSubmitcomIn(List<String> values) {
            addCriterion("SUBMITCOM in", values, "submitcom");
            return (Criteria) this;
        }

        public Criteria andSubmitcomNotIn(List<String> values) {
            addCriterion("SUBMITCOM not in", values, "submitcom");
            return (Criteria) this;
        }

        public Criteria andSubmitcomBetween(String value1, String value2) {
            addCriterion("SUBMITCOM between", value1, value2, "submitcom");
            return (Criteria) this;
        }

        public Criteria andSubmitcomNotBetween(String value1, String value2) {
            addCriterion("SUBMITCOM not between", value1, value2, "submitcom");
            return (Criteria) this;
        }

        public Criteria andProductnameIsNull() {
            addCriterion("PRODUCTNAME is null");
            return (Criteria) this;
        }

        public Criteria andProductnameIsNotNull() {
            addCriterion("PRODUCTNAME is not null");
            return (Criteria) this;
        }

        public Criteria andProductnameEqualTo(String value) {
            addCriterion("PRODUCTNAME =", value, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameNotEqualTo(String value) {
            addCriterion("PRODUCTNAME <>", value, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameGreaterThan(String value) {
            addCriterion("PRODUCTNAME >", value, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameGreaterThanOrEqualTo(String value) {
            addCriterion("PRODUCTNAME >=", value, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameLessThan(String value) {
            addCriterion("PRODUCTNAME <", value, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameLessThanOrEqualTo(String value) {
            addCriterion("PRODUCTNAME <=", value, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameLike(String value) {
            addCriterion("PRODUCTNAME like", value, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameNotLike(String value) {
            addCriterion("PRODUCTNAME not like", value, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameIn(List<String> values) {
            addCriterion("PRODUCTNAME in", values, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameNotIn(List<String> values) {
            addCriterion("PRODUCTNAME not in", values, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameBetween(String value1, String value2) {
            addCriterion("PRODUCTNAME between", value1, value2, "productname");
            return (Criteria) this;
        }

        public Criteria andProductnameNotBetween(String value1, String value2) {
            addCriterion("PRODUCTNAME not between", value1, value2, "productname");
            return (Criteria) this;
        }

        public Criteria andSubmittypeIsNull() {
            addCriterion("SUBMITTYPE is null");
            return (Criteria) this;
        }

        public Criteria andSubmittypeIsNotNull() {
            addCriterion("SUBMITTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andSubmittypeEqualTo(String value) {
            addCriterion("SUBMITTYPE =", value, "submittype");
            return (Criteria) this;
        }

        public Criteria andSubmittypeNotEqualTo(String value) {
            addCriterion("SUBMITTYPE <>", value, "submittype");
            return (Criteria) this;
        }

        public Criteria andSubmittypeGreaterThan(String value) {
            addCriterion("SUBMITTYPE >", value, "submittype");
            return (Criteria) this;
        }

        public Criteria andSubmittypeGreaterThanOrEqualTo(String value) {
            addCriterion("SUBMITTYPE >=", value, "submittype");
            return (Criteria) this;
        }

        public Criteria andSubmittypeLessThan(String value) {
            addCriterion("SUBMITTYPE <", value, "submittype");
            return (Criteria) this;
        }

        public Criteria andSubmittypeLessThanOrEqualTo(String value) {
            addCriterion("SUBMITTYPE <=", value, "submittype");
            return (Criteria) this;
        }

        public Criteria andSubmittypeLike(String value) {
            addCriterion("SUBMITTYPE like", value, "submittype");
            return (Criteria) this;
        }

        public Criteria andSubmittypeNotLike(String value) {
            addCriterion("SUBMITTYPE not like", value, "submittype");
            return (Criteria) this;
        }

        public Criteria andSubmittypeIn(List<String> values) {
            addCriterion("SUBMITTYPE in", values, "submittype");
            return (Criteria) this;
        }

        public Criteria andSubmittypeNotIn(List<String> values) {
            addCriterion("SUBMITTYPE not in", values, "submittype");
            return (Criteria) this;
        }

        public Criteria andSubmittypeBetween(String value1, String value2) {
            addCriterion("SUBMITTYPE between", value1, value2, "submittype");
            return (Criteria) this;
        }

        public Criteria andSubmittypeNotBetween(String value1, String value2) {
            addCriterion("SUBMITTYPE not between", value1, value2, "submittype");
            return (Criteria) this;
        }

        public Criteria andDevtypeIsNull() {
            addCriterion("DEVTYPE is null");
            return (Criteria) this;
        }

        public Criteria andDevtypeIsNotNull() {
            addCriterion("DEVTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andDevtypeEqualTo(String value) {
            addCriterion("DEVTYPE =", value, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeNotEqualTo(String value) {
            addCriterion("DEVTYPE <>", value, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeGreaterThan(String value) {
            addCriterion("DEVTYPE >", value, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeGreaterThanOrEqualTo(String value) {
            addCriterion("DEVTYPE >=", value, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeLessThan(String value) {
            addCriterion("DEVTYPE <", value, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeLessThanOrEqualTo(String value) {
            addCriterion("DEVTYPE <=", value, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeLike(String value) {
            addCriterion("DEVTYPE like", value, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeNotLike(String value) {
            addCriterion("DEVTYPE not like", value, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeIn(List<String> values) {
            addCriterion("DEVTYPE in", values, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeNotIn(List<String> values) {
            addCriterion("DEVTYPE not in", values, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeBetween(String value1, String value2) {
            addCriterion("DEVTYPE between", value1, value2, "devtype");
            return (Criteria) this;
        }

        public Criteria andDevtypeNotBetween(String value1, String value2) {
            addCriterion("DEVTYPE not between", value1, value2, "devtype");
            return (Criteria) this;
        }

        public Criteria andProducttypeIsNull() {
            addCriterion("PRODUCTTYPE is null");
            return (Criteria) this;
        }

        public Criteria andProducttypeIsNotNull() {
            addCriterion("PRODUCTTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andProducttypeEqualTo(String value) {
            addCriterion("PRODUCTTYPE =", value, "producttype");
            return (Criteria) this;
        }

        public Criteria andProducttypeNotEqualTo(String value) {
            addCriterion("PRODUCTTYPE <>", value, "producttype");
            return (Criteria) this;
        }

        public Criteria andProducttypeGreaterThan(String value) {
            addCriterion("PRODUCTTYPE >", value, "producttype");
            return (Criteria) this;
        }

        public Criteria andProducttypeGreaterThanOrEqualTo(String value) {
            addCriterion("PRODUCTTYPE >=", value, "producttype");
            return (Criteria) this;
        }

        public Criteria andProducttypeLessThan(String value) {
            addCriterion("PRODUCTTYPE <", value, "producttype");
            return (Criteria) this;
        }

        public Criteria andProducttypeLessThanOrEqualTo(String value) {
            addCriterion("PRODUCTTYPE <=", value, "producttype");
            return (Criteria) this;
        }

        public Criteria andProducttypeLike(String value) {
            addCriterion("PRODUCTTYPE like", value, "producttype");
            return (Criteria) this;
        }

        public Criteria andProducttypeNotLike(String value) {
            addCriterion("PRODUCTTYPE not like", value, "producttype");
            return (Criteria) this;
        }

        public Criteria andProducttypeIn(List<String> values) {
            addCriterion("PRODUCTTYPE in", values, "producttype");
            return (Criteria) this;
        }

        public Criteria andProducttypeNotIn(List<String> values) {
            addCriterion("PRODUCTTYPE not in", values, "producttype");
            return (Criteria) this;
        }

        public Criteria andProducttypeBetween(String value1, String value2) {
            addCriterion("PRODUCTTYPE between", value1, value2, "producttype");
            return (Criteria) this;
        }

        public Criteria andProducttypeNotBetween(String value1, String value2) {
            addCriterion("PRODUCTTYPE not between", value1, value2, "producttype");
            return (Criteria) this;
        }

        public Criteria andManagetypeIsNull() {
            addCriterion("MANAGETYPE is null");
            return (Criteria) this;
        }

        public Criteria andManagetypeIsNotNull() {
            addCriterion("MANAGETYPE is not null");
            return (Criteria) this;
        }

        public Criteria andManagetypeEqualTo(String value) {
            addCriterion("MANAGETYPE =", value, "managetype");
            return (Criteria) this;
        }

        public Criteria andManagetypeNotEqualTo(String value) {
            addCriterion("MANAGETYPE <>", value, "managetype");
            return (Criteria) this;
        }

        public Criteria andManagetypeGreaterThan(String value) {
            addCriterion("MANAGETYPE >", value, "managetype");
            return (Criteria) this;
        }

        public Criteria andManagetypeGreaterThanOrEqualTo(String value) {
            addCriterion("MANAGETYPE >=", value, "managetype");
            return (Criteria) this;
        }

        public Criteria andManagetypeLessThan(String value) {
            addCriterion("MANAGETYPE <", value, "managetype");
            return (Criteria) this;
        }

        public Criteria andManagetypeLessThanOrEqualTo(String value) {
            addCriterion("MANAGETYPE <=", value, "managetype");
            return (Criteria) this;
        }

        public Criteria andManagetypeLike(String value) {
            addCriterion("MANAGETYPE like", value, "managetype");
            return (Criteria) this;
        }

        public Criteria andManagetypeNotLike(String value) {
            addCriterion("MANAGETYPE not like", value, "managetype");
            return (Criteria) this;
        }

        public Criteria andManagetypeIn(List<String> values) {
            addCriterion("MANAGETYPE in", values, "managetype");
            return (Criteria) this;
        }

        public Criteria andManagetypeNotIn(List<String> values) {
            addCriterion("MANAGETYPE not in", values, "managetype");
            return (Criteria) this;
        }

        public Criteria andManagetypeBetween(String value1, String value2) {
            addCriterion("MANAGETYPE between", value1, value2, "managetype");
            return (Criteria) this;
        }

        public Criteria andManagetypeNotBetween(String value1, String value2) {
            addCriterion("MANAGETYPE not between", value1, value2, "managetype");
            return (Criteria) this;
        }

        public Criteria andRisktypeIsNull() {
            addCriterion("RISKTYPE is null");
            return (Criteria) this;
        }

        public Criteria andRisktypeIsNotNull() {
            addCriterion("RISKTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andRisktypeEqualTo(String value) {
            addCriterion("RISKTYPE =", value, "risktype");
            return (Criteria) this;
        }

        public Criteria andRisktypeNotEqualTo(String value) {
            addCriterion("RISKTYPE <>", value, "risktype");
            return (Criteria) this;
        }

        public Criteria andRisktypeGreaterThan(String value) {
            addCriterion("RISKTYPE >", value, "risktype");
            return (Criteria) this;
        }

        public Criteria andRisktypeGreaterThanOrEqualTo(String value) {
            addCriterion("RISKTYPE >=", value, "risktype");
            return (Criteria) this;
        }

        public Criteria andRisktypeLessThan(String value) {
            addCriterion("RISKTYPE <", value, "risktype");
            return (Criteria) this;
        }

        public Criteria andRisktypeLessThanOrEqualTo(String value) {
            addCriterion("RISKTYPE <=", value, "risktype");
            return (Criteria) this;
        }

        public Criteria andRisktypeLike(String value) {
            addCriterion("RISKTYPE like", value, "risktype");
            return (Criteria) this;
        }

        public Criteria andRisktypeNotLike(String value) {
            addCriterion("RISKTYPE not like", value, "risktype");
            return (Criteria) this;
        }

        public Criteria andRisktypeIn(List<String> values) {
            addCriterion("RISKTYPE in", values, "risktype");
            return (Criteria) this;
        }

        public Criteria andRisktypeNotIn(List<String> values) {
            addCriterion("RISKTYPE not in", values, "risktype");
            return (Criteria) this;
        }

        public Criteria andRisktypeBetween(String value1, String value2) {
            addCriterion("RISKTYPE between", value1, value2, "risktype");
            return (Criteria) this;
        }

        public Criteria andRisktypeNotBetween(String value1, String value2) {
            addCriterion("RISKTYPE not between", value1, value2, "risktype");
            return (Criteria) this;
        }

        public Criteria andKindtypeIsNull() {
            addCriterion("KINDTYPE is null");
            return (Criteria) this;
        }

        public Criteria andKindtypeIsNotNull() {
            addCriterion("KINDTYPE is not null");
            return (Criteria) this;
        }

        public Criteria andKindtypeEqualTo(String value) {
            addCriterion("KINDTYPE =", value, "kindtype");
            return (Criteria) this;
        }

        public Criteria andKindtypeNotEqualTo(String value) {
            addCriterion("KINDTYPE <>", value, "kindtype");
            return (Criteria) this;
        }

        public Criteria andKindtypeGreaterThan(String value) {
            addCriterion("KINDTYPE >", value, "kindtype");
            return (Criteria) this;
        }

        public Criteria andKindtypeGreaterThanOrEqualTo(String value) {
            addCriterion("KINDTYPE >=", value, "kindtype");
            return (Criteria) this;
        }

        public Criteria andKindtypeLessThan(String value) {
            addCriterion("KINDTYPE <", value, "kindtype");
            return (Criteria) this;
        }

        public Criteria andKindtypeLessThanOrEqualTo(String value) {
            addCriterion("KINDTYPE <=", value, "kindtype");
            return (Criteria) this;
        }

        public Criteria andKindtypeLike(String value) {
            addCriterion("KINDTYPE like", value, "kindtype");
            return (Criteria) this;
        }

        public Criteria andKindtypeNotLike(String value) {
            addCriterion("KINDTYPE not like", value, "kindtype");
            return (Criteria) this;
        }

        public Criteria andKindtypeIn(List<String> values) {
            addCriterion("KINDTYPE in", values, "kindtype");
            return (Criteria) this;
        }

        public Criteria andKindtypeNotIn(List<String> values) {
            addCriterion("KINDTYPE not in", values, "kindtype");
            return (Criteria) this;
        }

        public Criteria andKindtypeBetween(String value1, String value2) {
            addCriterion("KINDTYPE between", value1, value2, "kindtype");
            return (Criteria) this;
        }

        public Criteria andKindtypeNotBetween(String value1, String value2) {
            addCriterion("KINDTYPE not between", value1, value2, "kindtype");
            return (Criteria) this;
        }

        public Criteria andOpendateIsNull() {
            addCriterion("OPENDATE is null");
            return (Criteria) this;
        }

        public Criteria andOpendateIsNotNull() {
            addCriterion("OPENDATE is not null");
            return (Criteria) this;
        }

        public Criteria andOpendateEqualTo(Date value) {
            addCriterion("OPENDATE =", value, "opendate");
            return (Criteria) this;
        }

        public Criteria andOpendateNotEqualTo(Date value) {
            addCriterion("OPENDATE <>", value, "opendate");
            return (Criteria) this;
        }

        public Criteria andOpendateGreaterThan(Date value) {
            addCriterion("OPENDATE >", value, "opendate");
            return (Criteria) this;
        }

        public Criteria andOpendateGreaterThanOrEqualTo(Date value) {
            addCriterion("OPENDATE >=", value, "opendate");
            return (Criteria) this;
        }

        public Criteria andOpendateLessThan(Date value) {
            addCriterion("OPENDATE <", value, "opendate");
            return (Criteria) this;
        }

        public Criteria andOpendateLessThanOrEqualTo(Date value) {
            addCriterion("OPENDATE <=", value, "opendate");
            return (Criteria) this;
        }

        public Criteria andOpendateIn(List<Date> values) {
            addCriterion("OPENDATE in", values, "opendate");
            return (Criteria) this;
        }

        public Criteria andOpendateNotIn(List<Date> values) {
            addCriterion("OPENDATE not in", values, "opendate");
            return (Criteria) this;
        }

        public Criteria andOpendateBetween(Date value1, Date value2) {
            addCriterion("OPENDATE between", value1, value2, "opendate");
            return (Criteria) this;
        }

        public Criteria andOpendateNotBetween(Date value1, Date value2) {
            addCriterion("OPENDATE not between", value1, value2, "opendate");
            return (Criteria) this;
        }

        public Criteria andApprovaldateIsNull() {
            addCriterion("APPROVALDATE is null");
            return (Criteria) this;
        }

        public Criteria andApprovaldateIsNotNull() {
            addCriterion("APPROVALDATE is not null");
            return (Criteria) this;
        }

        public Criteria andApprovaldateEqualTo(Date value) {
            addCriterion("APPROVALDATE =", value, "approvaldate");
            return (Criteria) this;
        }

        public Criteria andApprovaldateNotEqualTo(Date value) {
            addCriterion("APPROVALDATE <>", value, "approvaldate");
            return (Criteria) this;
        }

        public Criteria andApprovaldateGreaterThan(Date value) {
            addCriterion("APPROVALDATE >", value, "approvaldate");
            return (Criteria) this;
        }

        public Criteria andApprovaldateGreaterThanOrEqualTo(Date value) {
            addCriterion("APPROVALDATE >=", value, "approvaldate");
            return (Criteria) this;
        }

        public Criteria andApprovaldateLessThan(Date value) {
            addCriterion("APPROVALDATE <", value, "approvaldate");
            return (Criteria) this;
        }

        public Criteria andApprovaldateLessThanOrEqualTo(Date value) {
            addCriterion("APPROVALDATE <=", value, "approvaldate");
            return (Criteria) this;
        }

        public Criteria andApprovaldateIn(List<Date> values) {
            addCriterion("APPROVALDATE in", values, "approvaldate");
            return (Criteria) this;
        }

        public Criteria andApprovaldateNotIn(List<Date> values) {
            addCriterion("APPROVALDATE not in", values, "approvaldate");
            return (Criteria) this;
        }

        public Criteria andApprovaldateBetween(Date value1, Date value2) {
            addCriterion("APPROVALDATE between", value1, value2, "approvaldate");
            return (Criteria) this;
        }

        public Criteria andApprovaldateNotBetween(Date value1, Date value2) {
            addCriterion("APPROVALDATE not between", value1, value2, "approvaldate");
            return (Criteria) this;
        }

        public Criteria andBusscopeIsNull() {
            addCriterion("BUSSCOPE is null");
            return (Criteria) this;
        }

        public Criteria andBusscopeIsNotNull() {
            addCriterion("BUSSCOPE is not null");
            return (Criteria) this;
        }

        public Criteria andBusscopeEqualTo(String value) {
            addCriterion("BUSSCOPE =", value, "busscope");
            return (Criteria) this;
        }

        public Criteria andBusscopeNotEqualTo(String value) {
            addCriterion("BUSSCOPE <>", value, "busscope");
            return (Criteria) this;
        }

        public Criteria andBusscopeGreaterThan(String value) {
            addCriterion("BUSSCOPE >", value, "busscope");
            return (Criteria) this;
        }

        public Criteria andBusscopeGreaterThanOrEqualTo(String value) {
            addCriterion("BUSSCOPE >=", value, "busscope");
            return (Criteria) this;
        }

        public Criteria andBusscopeLessThan(String value) {
            addCriterion("BUSSCOPE <", value, "busscope");
            return (Criteria) this;
        }

        public Criteria andBusscopeLessThanOrEqualTo(String value) {
            addCriterion("BUSSCOPE <=", value, "busscope");
            return (Criteria) this;
        }

        public Criteria andBusscopeLike(String value) {
            addCriterion("BUSSCOPE like", value, "busscope");
            return (Criteria) this;
        }

        public Criteria andBusscopeNotLike(String value) {
            addCriterion("BUSSCOPE not like", value, "busscope");
            return (Criteria) this;
        }

        public Criteria andBusscopeIn(List<String> values) {
            addCriterion("BUSSCOPE in", values, "busscope");
            return (Criteria) this;
        }

        public Criteria andBusscopeNotIn(List<String> values) {
            addCriterion("BUSSCOPE not in", values, "busscope");
            return (Criteria) this;
        }

        public Criteria andBusscopeBetween(String value1, String value2) {
            addCriterion("BUSSCOPE between", value1, value2, "busscope");
            return (Criteria) this;
        }

        public Criteria andBusscopeNotBetween(String value1, String value2) {
            addCriterion("BUSSCOPE not between", value1, value2, "busscope");
            return (Criteria) this;
        }

        public Criteria andBusareaIsNull() {
            addCriterion("BUSAREA is null");
            return (Criteria) this;
        }

        public Criteria andBusareaIsNotNull() {
            addCriterion("BUSAREA is not null");
            return (Criteria) this;
        }

        public Criteria andBusareaEqualTo(String value) {
            addCriterion("BUSAREA =", value, "busarea");
            return (Criteria) this;
        }

        public Criteria andBusareaNotEqualTo(String value) {
            addCriterion("BUSAREA <>", value, "busarea");
            return (Criteria) this;
        }

        public Criteria andBusareaGreaterThan(String value) {
            addCriterion("BUSAREA >", value, "busarea");
            return (Criteria) this;
        }

        public Criteria andBusareaGreaterThanOrEqualTo(String value) {
            addCriterion("BUSAREA >=", value, "busarea");
            return (Criteria) this;
        }

        public Criteria andBusareaLessThan(String value) {
            addCriterion("BUSAREA <", value, "busarea");
            return (Criteria) this;
        }

        public Criteria andBusareaLessThanOrEqualTo(String value) {
            addCriterion("BUSAREA <=", value, "busarea");
            return (Criteria) this;
        }

        public Criteria andBusareaLike(String value) {
            addCriterion("BUSAREA like", value, "busarea");
            return (Criteria) this;
        }

        public Criteria andBusareaNotLike(String value) {
            addCriterion("BUSAREA not like", value, "busarea");
            return (Criteria) this;
        }

        public Criteria andBusareaIn(List<String> values) {
            addCriterion("BUSAREA in", values, "busarea");
            return (Criteria) this;
        }

        public Criteria andBusareaNotIn(List<String> values) {
            addCriterion("BUSAREA not in", values, "busarea");
            return (Criteria) this;
        }

        public Criteria andBusareaBetween(String value1, String value2) {
            addCriterion("BUSAREA between", value1, value2, "busarea");
            return (Criteria) this;
        }

        public Criteria andBusareaNotBetween(String value1, String value2) {
            addCriterion("BUSAREA not between", value1, value2, "busarea");
            return (Criteria) this;
        }

        public Criteria andDocnumIsNull() {
            addCriterion("DOCNUM is null");
            return (Criteria) this;
        }

        public Criteria andDocnumIsNotNull() {
            addCriterion("DOCNUM is not null");
            return (Criteria) this;
        }

        public Criteria andDocnumEqualTo(String value) {
            addCriterion("DOCNUM =", value, "docnum");
            return (Criteria) this;
        }

        public Criteria andDocnumNotEqualTo(String value) {
            addCriterion("DOCNUM <>", value, "docnum");
            return (Criteria) this;
        }

        public Criteria andDocnumGreaterThan(String value) {
            addCriterion("DOCNUM >", value, "docnum");
            return (Criteria) this;
        }

        public Criteria andDocnumGreaterThanOrEqualTo(String value) {
            addCriterion("DOCNUM >=", value, "docnum");
            return (Criteria) this;
        }

        public Criteria andDocnumLessThan(String value) {
            addCriterion("DOCNUM <", value, "docnum");
            return (Criteria) this;
        }

        public Criteria andDocnumLessThanOrEqualTo(String value) {
            addCriterion("DOCNUM <=", value, "docnum");
            return (Criteria) this;
        }

        public Criteria andDocnumLike(String value) {
            addCriterion("DOCNUM like", value, "docnum");
            return (Criteria) this;
        }

        public Criteria andDocnumNotLike(String value) {
            addCriterion("DOCNUM not like", value, "docnum");
            return (Criteria) this;
        }

        public Criteria andDocnumIn(List<String> values) {
            addCriterion("DOCNUM in", values, "docnum");
            return (Criteria) this;
        }

        public Criteria andDocnumNotIn(List<String> values) {
            addCriterion("DOCNUM not in", values, "docnum");
            return (Criteria) this;
        }

        public Criteria andDocnumBetween(String value1, String value2) {
            addCriterion("DOCNUM between", value1, value2, "docnum");
            return (Criteria) this;
        }

        public Criteria andDocnumNotBetween(String value1, String value2) {
            addCriterion("DOCNUM not between", value1, value2, "docnum");
            return (Criteria) this;
        }

        public Criteria andYearIsNull() {
            addCriterion("YEAR is null");
            return (Criteria) this;
        }

        public Criteria andYearIsNotNull() {
            addCriterion("YEAR is not null");
            return (Criteria) this;
        }

        public Criteria andYearEqualTo(String value) {
            addCriterion("YEAR =", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearNotEqualTo(String value) {
            addCriterion("YEAR <>", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearGreaterThan(String value) {
            addCriterion("YEAR >", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearGreaterThanOrEqualTo(String value) {
            addCriterion("YEAR >=", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearLessThan(String value) {
            addCriterion("YEAR <", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearLessThanOrEqualTo(String value) {
            addCriterion("YEAR <=", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearLike(String value) {
            addCriterion("YEAR like", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearNotLike(String value) {
            addCriterion("YEAR not like", value, "year");
            return (Criteria) this;
        }

        public Criteria andYearIn(List<String> values) {
            addCriterion("YEAR in", values, "year");
            return (Criteria) this;
        }

        public Criteria andYearNotIn(List<String> values) {
            addCriterion("YEAR not in", values, "year");
            return (Criteria) this;
        }

        public Criteria andYearBetween(String value1, String value2) {
            addCriterion("YEAR between", value1, value2, "year");
            return (Criteria) this;
        }

        public Criteria andYearNotBetween(String value1, String value2) {
            addCriterion("YEAR not between", value1, value2, "year");
            return (Criteria) this;
        }

        public Criteria andProductnatureIsNull() {
            addCriterion("PRODUCTNATURE is null");
            return (Criteria) this;
        }

        public Criteria andProductnatureIsNotNull() {
            addCriterion("PRODUCTNATURE is not null");
            return (Criteria) this;
        }

        public Criteria andProductnatureEqualTo(String value) {
            addCriterion("PRODUCTNATURE =", value, "productnature");
            return (Criteria) this;
        }

        public Criteria andProductnatureNotEqualTo(String value) {
            addCriterion("PRODUCTNATURE <>", value, "productnature");
            return (Criteria) this;
        }

        public Criteria andProductnatureGreaterThan(String value) {
            addCriterion("PRODUCTNATURE >", value, "productnature");
            return (Criteria) this;
        }

        public Criteria andProductnatureGreaterThanOrEqualTo(String value) {
            addCriterion("PRODUCTNATURE >=", value, "productnature");
            return (Criteria) this;
        }

        public Criteria andProductnatureLessThan(String value) {
            addCriterion("PRODUCTNATURE <", value, "productnature");
            return (Criteria) this;
        }

        public Criteria andProductnatureLessThanOrEqualTo(String value) {
            addCriterion("PRODUCTNATURE <=", value, "productnature");
            return (Criteria) this;
        }

        public Criteria andProductnatureLike(String value) {
            addCriterion("PRODUCTNATURE like", value, "productnature");
            return (Criteria) this;
        }

        public Criteria andProductnatureNotLike(String value) {
            addCriterion("PRODUCTNATURE not like", value, "productnature");
            return (Criteria) this;
        }

        public Criteria andProductnatureIn(List<String> values) {
            addCriterion("PRODUCTNATURE in", values, "productnature");
            return (Criteria) this;
        }

        public Criteria andProductnatureNotIn(List<String> values) {
            addCriterion("PRODUCTNATURE not in", values, "productnature");
            return (Criteria) this;
        }

        public Criteria andProductnatureBetween(String value1, String value2) {
            addCriterion("PRODUCTNATURE between", value1, value2, "productnature");
            return (Criteria) this;
        }

        public Criteria andProductnatureNotBetween(String value1, String value2) {
            addCriterion("PRODUCTNATURE not between", value1, value2, "productnature");
            return (Criteria) this;
        }

        public Criteria andRateIsNull() {
            addCriterion("RATE is null");
            return (Criteria) this;
        }

        public Criteria andRateIsNotNull() {
            addCriterion("RATE is not null");
            return (Criteria) this;
        }

        public Criteria andRateEqualTo(String value) {
            addCriterion("RATE =", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateNotEqualTo(String value) {
            addCriterion("RATE <>", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateGreaterThan(String value) {
            addCriterion("RATE >", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateGreaterThanOrEqualTo(String value) {
            addCriterion("RATE >=", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateLessThan(String value) {
            addCriterion("RATE <", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateLessThanOrEqualTo(String value) {
            addCriterion("RATE <=", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateLike(String value) {
            addCriterion("RATE like", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateNotLike(String value) {
            addCriterion("RATE not like", value, "rate");
            return (Criteria) this;
        }

        public Criteria andRateIn(List<String> values) {
            addCriterion("RATE in", values, "rate");
            return (Criteria) this;
        }

        public Criteria andRateNotIn(List<String> values) {
            addCriterion("RATE not in", values, "rate");
            return (Criteria) this;
        }

        public Criteria andRateBetween(String value1, String value2) {
            addCriterion("RATE between", value1, value2, "rate");
            return (Criteria) this;
        }

        public Criteria andRateNotBetween(String value1, String value2) {
            addCriterion("RATE not between", value1, value2, "rate");
            return (Criteria) this;
        }

        public Criteria andRaterangeIsNull() {
            addCriterion("RATERANGE is null");
            return (Criteria) this;
        }

        public Criteria andRaterangeIsNotNull() {
            addCriterion("RATERANGE is not null");
            return (Criteria) this;
        }

        public Criteria andRaterangeEqualTo(String value) {
            addCriterion("RATERANGE =", value, "raterange");
            return (Criteria) this;
        }

        public Criteria andRaterangeNotEqualTo(String value) {
            addCriterion("RATERANGE <>", value, "raterange");
            return (Criteria) this;
        }

        public Criteria andRaterangeGreaterThan(String value) {
            addCriterion("RATERANGE >", value, "raterange");
            return (Criteria) this;
        }

        public Criteria andRaterangeGreaterThanOrEqualTo(String value) {
            addCriterion("RATERANGE >=", value, "raterange");
            return (Criteria) this;
        }

        public Criteria andRaterangeLessThan(String value) {
            addCriterion("RATERANGE <", value, "raterange");
            return (Criteria) this;
        }

        public Criteria andRaterangeLessThanOrEqualTo(String value) {
            addCriterion("RATERANGE <=", value, "raterange");
            return (Criteria) this;
        }

        public Criteria andRaterangeLike(String value) {
            addCriterion("RATERANGE like", value, "raterange");
            return (Criteria) this;
        }

        public Criteria andRaterangeNotLike(String value) {
            addCriterion("RATERANGE not like", value, "raterange");
            return (Criteria) this;
        }

        public Criteria andRaterangeIn(List<String> values) {
            addCriterion("RATERANGE in", values, "raterange");
            return (Criteria) this;
        }

        public Criteria andRaterangeNotIn(List<String> values) {
            addCriterion("RATERANGE not in", values, "raterange");
            return (Criteria) this;
        }

        public Criteria andRaterangeBetween(String value1, String value2) {
            addCriterion("RATERANGE between", value1, value2, "raterange");
            return (Criteria) this;
        }

        public Criteria andRaterangeNotBetween(String value1, String value2) {
            addCriterion("RATERANGE not between", value1, value2, "raterange");
            return (Criteria) this;
        }

        public Criteria andRatecoefficientIsNull() {
            addCriterion("RATECOEFFICIENT is null");
            return (Criteria) this;
        }

        public Criteria andRatecoefficientIsNotNull() {
            addCriterion("RATECOEFFICIENT is not null");
            return (Criteria) this;
        }

        public Criteria andRatecoefficientEqualTo(String value) {
            addCriterion("RATECOEFFICIENT =", value, "ratecoefficient");
            return (Criteria) this;
        }

        public Criteria andRatecoefficientNotEqualTo(String value) {
            addCriterion("RATECOEFFICIENT <>", value, "ratecoefficient");
            return (Criteria) this;
        }

        public Criteria andRatecoefficientGreaterThan(String value) {
            addCriterion("RATECOEFFICIENT >", value, "ratecoefficient");
            return (Criteria) this;
        }

        public Criteria andRatecoefficientGreaterThanOrEqualTo(String value) {
            addCriterion("RATECOEFFICIENT >=", value, "ratecoefficient");
            return (Criteria) this;
        }

        public Criteria andRatecoefficientLessThan(String value) {
            addCriterion("RATECOEFFICIENT <", value, "ratecoefficient");
            return (Criteria) this;
        }

        public Criteria andRatecoefficientLessThanOrEqualTo(String value) {
            addCriterion("RATECOEFFICIENT <=", value, "ratecoefficient");
            return (Criteria) this;
        }

        public Criteria andRatecoefficientLike(String value) {
            addCriterion("RATECOEFFICIENT like", value, "ratecoefficient");
            return (Criteria) this;
        }

        public Criteria andRatecoefficientNotLike(String value) {
            addCriterion("RATECOEFFICIENT not like", value, "ratecoefficient");
            return (Criteria) this;
        }

        public Criteria andRatecoefficientIn(List<String> values) {
            addCriterion("RATECOEFFICIENT in", values, "ratecoefficient");
            return (Criteria) this;
        }

        public Criteria andRatecoefficientNotIn(List<String> values) {
            addCriterion("RATECOEFFICIENT not in", values, "ratecoefficient");
            return (Criteria) this;
        }

        public Criteria andRatecoefficientBetween(String value1, String value2) {
            addCriterion("RATECOEFFICIENT between", value1, value2, "ratecoefficient");
            return (Criteria) this;
        }

        public Criteria andRatecoefficientNotBetween(String value1, String value2) {
            addCriterion("RATECOEFFICIENT not between", value1, value2, "ratecoefficient");
            return (Criteria) this;
        }

        public Criteria andHasepolicyIsNull() {
            addCriterion("HASEPOLICY is null");
            return (Criteria) this;
        }

        public Criteria andHasepolicyIsNotNull() {
            addCriterion("HASEPOLICY is not null");
            return (Criteria) this;
        }

        public Criteria andHasepolicyEqualTo(String value) {
            addCriterion("HASEPOLICY =", value, "hasepolicy");
            return (Criteria) this;
        }

        public Criteria andHasepolicyNotEqualTo(String value) {
            addCriterion("HASEPOLICY <>", value, "hasepolicy");
            return (Criteria) this;
        }

        public Criteria andHasepolicyGreaterThan(String value) {
            addCriterion("HASEPOLICY >", value, "hasepolicy");
            return (Criteria) this;
        }

        public Criteria andHasepolicyGreaterThanOrEqualTo(String value) {
            addCriterion("HASEPOLICY >=", value, "hasepolicy");
            return (Criteria) this;
        }

        public Criteria andHasepolicyLessThan(String value) {
            addCriterion("HASEPOLICY <", value, "hasepolicy");
            return (Criteria) this;
        }

        public Criteria andHasepolicyLessThanOrEqualTo(String value) {
            addCriterion("HASEPOLICY <=", value, "hasepolicy");
            return (Criteria) this;
        }

        public Criteria andHasepolicyLike(String value) {
            addCriterion("HASEPOLICY like", value, "hasepolicy");
            return (Criteria) this;
        }

        public Criteria andHasepolicyNotLike(String value) {
            addCriterion("HASEPOLICY not like", value, "hasepolicy");
            return (Criteria) this;
        }

        public Criteria andHasepolicyIn(List<String> values) {
            addCriterion("HASEPOLICY in", values, "hasepolicy");
            return (Criteria) this;
        }

        public Criteria andHasepolicyNotIn(List<String> values) {
            addCriterion("HASEPOLICY not in", values, "hasepolicy");
            return (Criteria) this;
        }

        public Criteria andHasepolicyBetween(String value1, String value2) {
            addCriterion("HASEPOLICY between", value1, value2, "hasepolicy");
            return (Criteria) this;
        }

        public Criteria andHasepolicyNotBetween(String value1, String value2) {
            addCriterion("HASEPOLICY not between", value1, value2, "hasepolicy");
            return (Criteria) this;
        }

        public Criteria andPeriodIsNull() {
            addCriterion("PERIOD is null");
            return (Criteria) this;
        }

        public Criteria andPeriodIsNotNull() {
            addCriterion("PERIOD is not null");
            return (Criteria) this;
        }

        public Criteria andPeriodEqualTo(String value) {
            addCriterion("PERIOD =", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodNotEqualTo(String value) {
            addCriterion("PERIOD <>", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodGreaterThan(String value) {
            addCriterion("PERIOD >", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodGreaterThanOrEqualTo(String value) {
            addCriterion("PERIOD >=", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodLessThan(String value) {
            addCriterion("PERIOD <", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodLessThanOrEqualTo(String value) {
            addCriterion("PERIOD <=", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodLike(String value) {
            addCriterion("PERIOD like", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodNotLike(String value) {
            addCriterion("PERIOD not like", value, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodIn(List<String> values) {
            addCriterion("PERIOD in", values, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodNotIn(List<String> values) {
            addCriterion("PERIOD not in", values, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodBetween(String value1, String value2) {
            addCriterion("PERIOD between", value1, value2, "period");
            return (Criteria) this;
        }

        public Criteria andPeriodNotBetween(String value1, String value2) {
            addCriterion("PERIOD not between", value1, value2, "period");
            return (Criteria) this;
        }

        public Criteria andDeductibleIsNull() {
            addCriterion("DEDUCTIBLE is null");
            return (Criteria) this;
        }

        public Criteria andDeductibleIsNotNull() {
            addCriterion("DEDUCTIBLE is not null");
            return (Criteria) this;
        }

        public Criteria andDeductibleEqualTo(String value) {
            addCriterion("DEDUCTIBLE =", value, "deductible");
            return (Criteria) this;
        }

        public Criteria andDeductibleNotEqualTo(String value) {
            addCriterion("DEDUCTIBLE <>", value, "deductible");
            return (Criteria) this;
        }

        public Criteria andDeductibleGreaterThan(String value) {
            addCriterion("DEDUCTIBLE >", value, "deductible");
            return (Criteria) this;
        }

        public Criteria andDeductibleGreaterThanOrEqualTo(String value) {
            addCriterion("DEDUCTIBLE >=", value, "deductible");
            return (Criteria) this;
        }

        public Criteria andDeductibleLessThan(String value) {
            addCriterion("DEDUCTIBLE <", value, "deductible");
            return (Criteria) this;
        }

        public Criteria andDeductibleLessThanOrEqualTo(String value) {
            addCriterion("DEDUCTIBLE <=", value, "deductible");
            return (Criteria) this;
        }

        public Criteria andDeductibleLike(String value) {
            addCriterion("DEDUCTIBLE like", value, "deductible");
            return (Criteria) this;
        }

        public Criteria andDeductibleNotLike(String value) {
            addCriterion("DEDUCTIBLE not like", value, "deductible");
            return (Criteria) this;
        }

        public Criteria andDeductibleIn(List<String> values) {
            addCriterion("DEDUCTIBLE in", values, "deductible");
            return (Criteria) this;
        }

        public Criteria andDeductibleNotIn(List<String> values) {
            addCriterion("DEDUCTIBLE not in", values, "deductible");
            return (Criteria) this;
        }

        public Criteria andDeductibleBetween(String value1, String value2) {
            addCriterion("DEDUCTIBLE between", value1, value2, "deductible");
            return (Criteria) this;
        }

        public Criteria andDeductibleNotBetween(String value1, String value2) {
            addCriterion("DEDUCTIBLE not between", value1, value2, "deductible");
            return (Criteria) this;
        }

        public Criteria andReldeductibeIsNull() {
            addCriterion("RELDEDUCTIBE is null");
            return (Criteria) this;
        }

        public Criteria andReldeductibeIsNotNull() {
            addCriterion("RELDEDUCTIBE is not null");
            return (Criteria) this;
        }

        public Criteria andReldeductibeEqualTo(String value) {
            addCriterion("RELDEDUCTIBE =", value, "reldeductibe");
            return (Criteria) this;
        }

        public Criteria andReldeductibeNotEqualTo(String value) {
            addCriterion("RELDEDUCTIBE <>", value, "reldeductibe");
            return (Criteria) this;
        }

        public Criteria andReldeductibeGreaterThan(String value) {
            addCriterion("RELDEDUCTIBE >", value, "reldeductibe");
            return (Criteria) this;
        }

        public Criteria andReldeductibeGreaterThanOrEqualTo(String value) {
            addCriterion("RELDEDUCTIBE >=", value, "reldeductibe");
            return (Criteria) this;
        }

        public Criteria andReldeductibeLessThan(String value) {
            addCriterion("RELDEDUCTIBE <", value, "reldeductibe");
            return (Criteria) this;
        }

        public Criteria andReldeductibeLessThanOrEqualTo(String value) {
            addCriterion("RELDEDUCTIBE <=", value, "reldeductibe");
            return (Criteria) this;
        }

        public Criteria andReldeductibeLike(String value) {
            addCriterion("RELDEDUCTIBE like", value, "reldeductibe");
            return (Criteria) this;
        }

        public Criteria andReldeductibeNotLike(String value) {
            addCriterion("RELDEDUCTIBE not like", value, "reldeductibe");
            return (Criteria) this;
        }

        public Criteria andReldeductibeIn(List<String> values) {
            addCriterion("RELDEDUCTIBE in", values, "reldeductibe");
            return (Criteria) this;
        }

        public Criteria andReldeductibeNotIn(List<String> values) {
            addCriterion("RELDEDUCTIBE not in", values, "reldeductibe");
            return (Criteria) this;
        }

        public Criteria andReldeductibeBetween(String value1, String value2) {
            addCriterion("RELDEDUCTIBE between", value1, value2, "reldeductibe");
            return (Criteria) this;
        }

        public Criteria andReldeductibeNotBetween(String value1, String value2) {
            addCriterion("RELDEDUCTIBE not between", value1, value2, "reldeductibe");
            return (Criteria) this;
        }

        public Criteria andInsliabIsNull() {
            addCriterion("INSLIAB is null");
            return (Criteria) this;
        }

        public Criteria andInsliabIsNotNull() {
            addCriterion("INSLIAB is not null");
            return (Criteria) this;
        }

        public Criteria andInsliabEqualTo(String value) {
            addCriterion("INSLIAB =", value, "insliab");
            return (Criteria) this;
        }

        public Criteria andInsliabNotEqualTo(String value) {
            addCriterion("INSLIAB <>", value, "insliab");
            return (Criteria) this;
        }

        public Criteria andInsliabGreaterThan(String value) {
            addCriterion("INSLIAB >", value, "insliab");
            return (Criteria) this;
        }

        public Criteria andInsliabGreaterThanOrEqualTo(String value) {
            addCriterion("INSLIAB >=", value, "insliab");
            return (Criteria) this;
        }

        public Criteria andInsliabLessThan(String value) {
            addCriterion("INSLIAB <", value, "insliab");
            return (Criteria) this;
        }

        public Criteria andInsliabLessThanOrEqualTo(String value) {
            addCriterion("INSLIAB <=", value, "insliab");
            return (Criteria) this;
        }

        public Criteria andInsliabLike(String value) {
            addCriterion("INSLIAB like", value, "insliab");
            return (Criteria) this;
        }

        public Criteria andInsliabNotLike(String value) {
            addCriterion("INSLIAB not like", value, "insliab");
            return (Criteria) this;
        }

        public Criteria andInsliabIn(List<String> values) {
            addCriterion("INSLIAB in", values, "insliab");
            return (Criteria) this;
        }

        public Criteria andInsliabNotIn(List<String> values) {
            addCriterion("INSLIAB not in", values, "insliab");
            return (Criteria) this;
        }

        public Criteria andInsliabBetween(String value1, String value2) {
            addCriterion("INSLIAB between", value1, value2, "insliab");
            return (Criteria) this;
        }

        public Criteria andInsliabNotBetween(String value1, String value2) {
            addCriterion("INSLIAB not between", value1, value2, "insliab");
            return (Criteria) this;
        }

        public Criteria andLinknameIsNull() {
            addCriterion("LINKNAME is null");
            return (Criteria) this;
        }

        public Criteria andLinknameIsNotNull() {
            addCriterion("LINKNAME is not null");
            return (Criteria) this;
        }

        public Criteria andLinknameEqualTo(String value) {
            addCriterion("LINKNAME =", value, "linkname");
            return (Criteria) this;
        }

        public Criteria andLinknameNotEqualTo(String value) {
            addCriterion("LINKNAME <>", value, "linkname");
            return (Criteria) this;
        }

        public Criteria andLinknameGreaterThan(String value) {
            addCriterion("LINKNAME >", value, "linkname");
            return (Criteria) this;
        }

        public Criteria andLinknameGreaterThanOrEqualTo(String value) {
            addCriterion("LINKNAME >=", value, "linkname");
            return (Criteria) this;
        }

        public Criteria andLinknameLessThan(String value) {
            addCriterion("LINKNAME <", value, "linkname");
            return (Criteria) this;
        }

        public Criteria andLinknameLessThanOrEqualTo(String value) {
            addCriterion("LINKNAME <=", value, "linkname");
            return (Criteria) this;
        }

        public Criteria andLinknameLike(String value) {
            addCriterion("LINKNAME like", value, "linkname");
            return (Criteria) this;
        }

        public Criteria andLinknameNotLike(String value) {
            addCriterion("LINKNAME not like", value, "linkname");
            return (Criteria) this;
        }

        public Criteria andLinknameIn(List<String> values) {
            addCriterion("LINKNAME in", values, "linkname");
            return (Criteria) this;
        }

        public Criteria andLinknameNotIn(List<String> values) {
            addCriterion("LINKNAME not in", values, "linkname");
            return (Criteria) this;
        }

        public Criteria andLinknameBetween(String value1, String value2) {
            addCriterion("LINKNAME between", value1, value2, "linkname");
            return (Criteria) this;
        }

        public Criteria andLinknameNotBetween(String value1, String value2) {
            addCriterion("LINKNAME not between", value1, value2, "linkname");
            return (Criteria) this;
        }

        public Criteria andLinknumIsNull() {
            addCriterion("LINKNUM is null");
            return (Criteria) this;
        }

        public Criteria andLinknumIsNotNull() {
            addCriterion("LINKNUM is not null");
            return (Criteria) this;
        }

        public Criteria andLinknumEqualTo(String value) {
            addCriterion("LINKNUM =", value, "linknum");
            return (Criteria) this;
        }

        public Criteria andLinknumNotEqualTo(String value) {
            addCriterion("LINKNUM <>", value, "linknum");
            return (Criteria) this;
        }

        public Criteria andLinknumGreaterThan(String value) {
            addCriterion("LINKNUM >", value, "linknum");
            return (Criteria) this;
        }

        public Criteria andLinknumGreaterThanOrEqualTo(String value) {
            addCriterion("LINKNUM >=", value, "linknum");
            return (Criteria) this;
        }

        public Criteria andLinknumLessThan(String value) {
            addCriterion("LINKNUM <", value, "linknum");
            return (Criteria) this;
        }

        public Criteria andLinknumLessThanOrEqualTo(String value) {
            addCriterion("LINKNUM <=", value, "linknum");
            return (Criteria) this;
        }

        public Criteria andLinknumLike(String value) {
            addCriterion("LINKNUM like", value, "linknum");
            return (Criteria) this;
        }

        public Criteria andLinknumNotLike(String value) {
            addCriterion("LINKNUM not like", value, "linknum");
            return (Criteria) this;
        }

        public Criteria andLinknumIn(List<String> values) {
            addCriterion("LINKNUM in", values, "linknum");
            return (Criteria) this;
        }

        public Criteria andLinknumNotIn(List<String> values) {
            addCriterion("LINKNUM not in", values, "linknum");
            return (Criteria) this;
        }

        public Criteria andLinknumBetween(String value1, String value2) {
            addCriterion("LINKNUM between", value1, value2, "linknum");
            return (Criteria) this;
        }

        public Criteria andLinknumNotBetween(String value1, String value2) {
            addCriterion("LINKNUM not between", value1, value2, "linknum");
            return (Criteria) this;
        }

        public Criteria andProductattrIsNull() {
            addCriterion("PRODUCTATTR is null");
            return (Criteria) this;
        }

        public Criteria andProductattrIsNotNull() {
            addCriterion("PRODUCTATTR is not null");
            return (Criteria) this;
        }

        public Criteria andProductattrEqualTo(String value) {
            addCriterion("PRODUCTATTR =", value, "productattr");
            return (Criteria) this;
        }

        public Criteria andProductattrNotEqualTo(String value) {
            addCriterion("PRODUCTATTR <>", value, "productattr");
            return (Criteria) this;
        }

        public Criteria andProductattrGreaterThan(String value) {
            addCriterion("PRODUCTATTR >", value, "productattr");
            return (Criteria) this;
        }

        public Criteria andProductattrGreaterThanOrEqualTo(String value) {
            addCriterion("PRODUCTATTR >=", value, "productattr");
            return (Criteria) this;
        }

        public Criteria andProductattrLessThan(String value) {
            addCriterion("PRODUCTATTR <", value, "productattr");
            return (Criteria) this;
        }

        public Criteria andProductattrLessThanOrEqualTo(String value) {
            addCriterion("PRODUCTATTR <=", value, "productattr");
            return (Criteria) this;
        }

        public Criteria andProductattrLike(String value) {
            addCriterion("PRODUCTATTR like", value, "productattr");
            return (Criteria) this;
        }

        public Criteria andProductattrNotLike(String value) {
            addCriterion("PRODUCTATTR not like", value, "productattr");
            return (Criteria) this;
        }

        public Criteria andProductattrIn(List<String> values) {
            addCriterion("PRODUCTATTR in", values, "productattr");
            return (Criteria) this;
        }

        public Criteria andProductattrNotIn(List<String> values) {
            addCriterion("PRODUCTATTR not in", values, "productattr");
            return (Criteria) this;
        }

        public Criteria andProductattrBetween(String value1, String value2) {
            addCriterion("PRODUCTATTR between", value1, value2, "productattr");
            return (Criteria) this;
        }

        public Criteria andProductattrNotBetween(String value1, String value2) {
            addCriterion("PRODUCTATTR not between", value1, value2, "productattr");
            return (Criteria) this;
        }

        public Criteria andSignnameIsNull() {
            addCriterion("SIGNNAME is null");
            return (Criteria) this;
        }

        public Criteria andSignnameIsNotNull() {
            addCriterion("SIGNNAME is not null");
            return (Criteria) this;
        }

        public Criteria andSignnameEqualTo(String value) {
            addCriterion("SIGNNAME =", value, "signname");
            return (Criteria) this;
        }

        public Criteria andSignnameNotEqualTo(String value) {
            addCriterion("SIGNNAME <>", value, "signname");
            return (Criteria) this;
        }

        public Criteria andSignnameGreaterThan(String value) {
            addCriterion("SIGNNAME >", value, "signname");
            return (Criteria) this;
        }

        public Criteria andSignnameGreaterThanOrEqualTo(String value) {
            addCriterion("SIGNNAME >=", value, "signname");
            return (Criteria) this;
        }

        public Criteria andSignnameLessThan(String value) {
            addCriterion("SIGNNAME <", value, "signname");
            return (Criteria) this;
        }

        public Criteria andSignnameLessThanOrEqualTo(String value) {
            addCriterion("SIGNNAME <=", value, "signname");
            return (Criteria) this;
        }

        public Criteria andSignnameLike(String value) {
            addCriterion("SIGNNAME like", value, "signname");
            return (Criteria) this;
        }

        public Criteria andSignnameNotLike(String value) {
            addCriterion("SIGNNAME not like", value, "signname");
            return (Criteria) this;
        }

        public Criteria andSignnameIn(List<String> values) {
            addCriterion("SIGNNAME in", values, "signname");
            return (Criteria) this;
        }

        public Criteria andSignnameNotIn(List<String> values) {
            addCriterion("SIGNNAME not in", values, "signname");
            return (Criteria) this;
        }

        public Criteria andSignnameBetween(String value1, String value2) {
            addCriterion("SIGNNAME between", value1, value2, "signname");
            return (Criteria) this;
        }

        public Criteria andSignnameNotBetween(String value1, String value2) {
            addCriterion("SIGNNAME not between", value1, value2, "signname");
            return (Criteria) this;
        }

        public Criteria andLegalnameIsNull() {
            addCriterion("LEGALNAME is null");
            return (Criteria) this;
        }

        public Criteria andLegalnameIsNotNull() {
            addCriterion("LEGALNAME is not null");
            return (Criteria) this;
        }

        public Criteria andLegalnameEqualTo(String value) {
            addCriterion("LEGALNAME =", value, "legalname");
            return (Criteria) this;
        }

        public Criteria andLegalnameNotEqualTo(String value) {
            addCriterion("LEGALNAME <>", value, "legalname");
            return (Criteria) this;
        }

        public Criteria andLegalnameGreaterThan(String value) {
            addCriterion("LEGALNAME >", value, "legalname");
            return (Criteria) this;
        }

        public Criteria andLegalnameGreaterThanOrEqualTo(String value) {
            addCriterion("LEGALNAME >=", value, "legalname");
            return (Criteria) this;
        }

        public Criteria andLegalnameLessThan(String value) {
            addCriterion("LEGALNAME <", value, "legalname");
            return (Criteria) this;
        }

        public Criteria andLegalnameLessThanOrEqualTo(String value) {
            addCriterion("LEGALNAME <=", value, "legalname");
            return (Criteria) this;
        }

        public Criteria andLegalnameLike(String value) {
            addCriterion("LEGALNAME like", value, "legalname");
            return (Criteria) this;
        }

        public Criteria andLegalnameNotLike(String value) {
            addCriterion("LEGALNAME not like", value, "legalname");
            return (Criteria) this;
        }

        public Criteria andLegalnameIn(List<String> values) {
            addCriterion("LEGALNAME in", values, "legalname");
            return (Criteria) this;
        }

        public Criteria andLegalnameNotIn(List<String> values) {
            addCriterion("LEGALNAME not in", values, "legalname");
            return (Criteria) this;
        }

        public Criteria andLegalnameBetween(String value1, String value2) {
            addCriterion("LEGALNAME between", value1, value2, "legalname");
            return (Criteria) this;
        }

        public Criteria andLegalnameNotBetween(String value1, String value2) {
            addCriterion("LEGALNAME not between", value1, value2, "legalname");
            return (Criteria) this;
        }

        public Criteria andActuarialnameIsNull() {
            addCriterion("ACTUARIALNAME is null");
            return (Criteria) this;
        }

        public Criteria andActuarialnameIsNotNull() {
            addCriterion("ACTUARIALNAME is not null");
            return (Criteria) this;
        }

        public Criteria andActuarialnameEqualTo(String value) {
            addCriterion("ACTUARIALNAME =", value, "actuarialname");
            return (Criteria) this;
        }

        public Criteria andActuarialnameNotEqualTo(String value) {
            addCriterion("ACTUARIALNAME <>", value, "actuarialname");
            return (Criteria) this;
        }

        public Criteria andActuarialnameGreaterThan(String value) {
            addCriterion("ACTUARIALNAME >", value, "actuarialname");
            return (Criteria) this;
        }

        public Criteria andActuarialnameGreaterThanOrEqualTo(String value) {
            addCriterion("ACTUARIALNAME >=", value, "actuarialname");
            return (Criteria) this;
        }

        public Criteria andActuarialnameLessThan(String value) {
            addCriterion("ACTUARIALNAME <", value, "actuarialname");
            return (Criteria) this;
        }

        public Criteria andActuarialnameLessThanOrEqualTo(String value) {
            addCriterion("ACTUARIALNAME <=", value, "actuarialname");
            return (Criteria) this;
        }

        public Criteria andActuarialnameLike(String value) {
            addCriterion("ACTUARIALNAME like", value, "actuarialname");
            return (Criteria) this;
        }

        public Criteria andActuarialnameNotLike(String value) {
            addCriterion("ACTUARIALNAME not like", value, "actuarialname");
            return (Criteria) this;
        }

        public Criteria andActuarialnameIn(List<String> values) {
            addCriterion("ACTUARIALNAME in", values, "actuarialname");
            return (Criteria) this;
        }

        public Criteria andActuarialnameNotIn(List<String> values) {
            addCriterion("ACTUARIALNAME not in", values, "actuarialname");
            return (Criteria) this;
        }

        public Criteria andActuarialnameBetween(String value1, String value2) {
            addCriterion("ACTUARIALNAME between", value1, value2, "actuarialname");
            return (Criteria) this;
        }

        public Criteria andActuarialnameNotBetween(String value1, String value2) {
            addCriterion("ACTUARIALNAME not between", value1, value2, "actuarialname");
            return (Criteria) this;
        }

        public Criteria andSalepromnameIsNull() {
            addCriterion("SALEPROMNAME is null");
            return (Criteria) this;
        }

        public Criteria andSalepromnameIsNotNull() {
            addCriterion("SALEPROMNAME is not null");
            return (Criteria) this;
        }

        public Criteria andSalepromnameEqualTo(String value) {
            addCriterion("SALEPROMNAME =", value, "salepromname");
            return (Criteria) this;
        }

        public Criteria andSalepromnameNotEqualTo(String value) {
            addCriterion("SALEPROMNAME <>", value, "salepromname");
            return (Criteria) this;
        }

        public Criteria andSalepromnameGreaterThan(String value) {
            addCriterion("SALEPROMNAME >", value, "salepromname");
            return (Criteria) this;
        }

        public Criteria andSalepromnameGreaterThanOrEqualTo(String value) {
            addCriterion("SALEPROMNAME >=", value, "salepromname");
            return (Criteria) this;
        }

        public Criteria andSalepromnameLessThan(String value) {
            addCriterion("SALEPROMNAME <", value, "salepromname");
            return (Criteria) this;
        }

        public Criteria andSalepromnameLessThanOrEqualTo(String value) {
            addCriterion("SALEPROMNAME <=", value, "salepromname");
            return (Criteria) this;
        }

        public Criteria andSalepromnameLike(String value) {
            addCriterion("SALEPROMNAME like", value, "salepromname");
            return (Criteria) this;
        }

        public Criteria andSalepromnameNotLike(String value) {
            addCriterion("SALEPROMNAME not like", value, "salepromname");
            return (Criteria) this;
        }

        public Criteria andSalepromnameIn(List<String> values) {
            addCriterion("SALEPROMNAME in", values, "salepromname");
            return (Criteria) this;
        }

        public Criteria andSalepromnameNotIn(List<String> values) {
            addCriterion("SALEPROMNAME not in", values, "salepromname");
            return (Criteria) this;
        }

        public Criteria andSalepromnameBetween(String value1, String value2) {
            addCriterion("SALEPROMNAME between", value1, value2, "salepromname");
            return (Criteria) this;
        }

        public Criteria andSalepromnameNotBetween(String value1, String value2) {
            addCriterion("SALEPROMNAME not between", value1, value2, "salepromname");
            return (Criteria) this;
        }

        public Criteria andSaleareaIsNull() {
            addCriterion("SALEAREA is null");
            return (Criteria) this;
        }

        public Criteria andSaleareaIsNotNull() {
            addCriterion("SALEAREA is not null");
            return (Criteria) this;
        }

        public Criteria andSaleareaEqualTo(String value) {
            addCriterion("SALEAREA =", value, "salearea");
            return (Criteria) this;
        }

        public Criteria andSaleareaNotEqualTo(String value) {
            addCriterion("SALEAREA <>", value, "salearea");
            return (Criteria) this;
        }

        public Criteria andSaleareaGreaterThan(String value) {
            addCriterion("SALEAREA >", value, "salearea");
            return (Criteria) this;
        }

        public Criteria andSaleareaGreaterThanOrEqualTo(String value) {
            addCriterion("SALEAREA >=", value, "salearea");
            return (Criteria) this;
        }

        public Criteria andSaleareaLessThan(String value) {
            addCriterion("SALEAREA <", value, "salearea");
            return (Criteria) this;
        }

        public Criteria andSaleareaLessThanOrEqualTo(String value) {
            addCriterion("SALEAREA <=", value, "salearea");
            return (Criteria) this;
        }

        public Criteria andSaleareaLike(String value) {
            addCriterion("SALEAREA like", value, "salearea");
            return (Criteria) this;
        }

        public Criteria andSaleareaNotLike(String value) {
            addCriterion("SALEAREA not like", value, "salearea");
            return (Criteria) this;
        }

        public Criteria andSaleareaIn(List<String> values) {
            addCriterion("SALEAREA in", values, "salearea");
            return (Criteria) this;
        }

        public Criteria andSaleareaNotIn(List<String> values) {
            addCriterion("SALEAREA not in", values, "salearea");
            return (Criteria) this;
        }

        public Criteria andSaleareaBetween(String value1, String value2) {
            addCriterion("SALEAREA between", value1, value2, "salearea");
            return (Criteria) this;
        }

        public Criteria andSaleareaNotBetween(String value1, String value2) {
            addCriterion("SALEAREA not between", value1, value2, "salearea");
            return (Criteria) this;
        }

        public Criteria andSalechannelIsNull() {
            addCriterion("SALECHANNEL is null");
            return (Criteria) this;
        }

        public Criteria andSalechannelIsNotNull() {
            addCriterion("SALECHANNEL is not null");
            return (Criteria) this;
        }

        public Criteria andSalechannelEqualTo(String value) {
            addCriterion("SALECHANNEL =", value, "salechannel");
            return (Criteria) this;
        }

        public Criteria andSalechannelNotEqualTo(String value) {
            addCriterion("SALECHANNEL <>", value, "salechannel");
            return (Criteria) this;
        }

        public Criteria andSalechannelGreaterThan(String value) {
            addCriterion("SALECHANNEL >", value, "salechannel");
            return (Criteria) this;
        }

        public Criteria andSalechannelGreaterThanOrEqualTo(String value) {
            addCriterion("SALECHANNEL >=", value, "salechannel");
            return (Criteria) this;
        }

        public Criteria andSalechannelLessThan(String value) {
            addCriterion("SALECHANNEL <", value, "salechannel");
            return (Criteria) this;
        }

        public Criteria andSalechannelLessThanOrEqualTo(String value) {
            addCriterion("SALECHANNEL <=", value, "salechannel");
            return (Criteria) this;
        }

        public Criteria andSalechannelLike(String value) {
            addCriterion("SALECHANNEL like", value, "salechannel");
            return (Criteria) this;
        }

        public Criteria andSalechannelNotLike(String value) {
            addCriterion("SALECHANNEL not like", value, "salechannel");
            return (Criteria) this;
        }

        public Criteria andSalechannelIn(List<String> values) {
            addCriterion("SALECHANNEL in", values, "salechannel");
            return (Criteria) this;
        }

        public Criteria andSalechannelNotIn(List<String> values) {
            addCriterion("SALECHANNEL not in", values, "salechannel");
            return (Criteria) this;
        }

        public Criteria andSalechannelBetween(String value1, String value2) {
            addCriterion("SALECHANNEL between", value1, value2, "salechannel");
            return (Criteria) this;
        }

        public Criteria andSalechannelNotBetween(String value1, String value2) {
            addCriterion("SALECHANNEL not between", value1, value2, "salechannel");
            return (Criteria) this;
        }

        public Criteria andOtherinfoIsNull() {
            addCriterion("OTHERINFO is null");
            return (Criteria) this;
        }

        public Criteria andOtherinfoIsNotNull() {
            addCriterion("OTHERINFO is not null");
            return (Criteria) this;
        }

        public Criteria andOtherinfoEqualTo(String value) {
            addCriterion("OTHERINFO =", value, "otherinfo");
            return (Criteria) this;
        }

        public Criteria andOtherinfoNotEqualTo(String value) {
            addCriterion("OTHERINFO <>", value, "otherinfo");
            return (Criteria) this;
        }

        public Criteria andOtherinfoGreaterThan(String value) {
            addCriterion("OTHERINFO >", value, "otherinfo");
            return (Criteria) this;
        }

        public Criteria andOtherinfoGreaterThanOrEqualTo(String value) {
            addCriterion("OTHERINFO >=", value, "otherinfo");
            return (Criteria) this;
        }

        public Criteria andOtherinfoLessThan(String value) {
            addCriterion("OTHERINFO <", value, "otherinfo");
            return (Criteria) this;
        }

        public Criteria andOtherinfoLessThanOrEqualTo(String value) {
            addCriterion("OTHERINFO <=", value, "otherinfo");
            return (Criteria) this;
        }

        public Criteria andOtherinfoLike(String value) {
            addCriterion("OTHERINFO like", value, "otherinfo");
            return (Criteria) this;
        }

        public Criteria andOtherinfoNotLike(String value) {
            addCriterion("OTHERINFO not like", value, "otherinfo");
            return (Criteria) this;
        }

        public Criteria andOtherinfoIn(List<String> values) {
            addCriterion("OTHERINFO in", values, "otherinfo");
            return (Criteria) this;
        }

        public Criteria andOtherinfoNotIn(List<String> values) {
            addCriterion("OTHERINFO not in", values, "otherinfo");
            return (Criteria) this;
        }

        public Criteria andOtherinfoBetween(String value1, String value2) {
            addCriterion("OTHERINFO between", value1, value2, "otherinfo");
            return (Criteria) this;
        }

        public Criteria andOtherinfoNotBetween(String value1, String value2) {
            addCriterion("OTHERINFO not between", value1, value2, "otherinfo");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}