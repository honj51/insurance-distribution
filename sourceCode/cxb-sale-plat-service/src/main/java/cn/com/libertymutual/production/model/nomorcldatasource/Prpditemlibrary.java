package cn.com.libertymutual.production.model.nomorcldatasource;

import java.util.Date;

public class Prpditemlibrary {
    private String itemcode;

    private String itemcname;

    private String itemename;

    private String itemflag;

    private String newitemcode;

    private String validstatus;

    private String flag;

    private Date startdate;

    private Date enddate;

    private Date createdate;

    private Date updatedate;

    private String id;

    public String getItemcode() {
        return itemcode;
    }

    public void setItemcode(String itemcode) {
        this.itemcode = itemcode == null ? null : itemcode.trim();
    }

    public String getItemcname() {
        return itemcname;
    }

    public void setItemcname(String itemcname) {
        this.itemcname = itemcname == null ? null : itemcname.trim();
    }

    public String getItemename() {
        return itemename;
    }

    public void setItemename(String itemename) {
        this.itemename = itemename == null ? null : itemename.trim();
    }

    public String getItemflag() {
        return itemflag;
    }

    public void setItemflag(String itemflag) {
        this.itemflag = itemflag == null ? null : itemflag.trim();
    }

    public String getNewitemcode() {
        return newitemcode;
    }

    public void setNewitemcode(String newitemcode) {
        this.newitemcode = newitemcode == null ? null : newitemcode.trim();
    }

    public String getValidstatus() {
        return validstatus;
    }

    public void setValidstatus(String validstatus) {
        this.validstatus = validstatus == null ? null : validstatus.trim();
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag == null ? null : flag.trim();
    }

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public Date getUpdatedate() {
        return updatedate;
    }

    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }
}