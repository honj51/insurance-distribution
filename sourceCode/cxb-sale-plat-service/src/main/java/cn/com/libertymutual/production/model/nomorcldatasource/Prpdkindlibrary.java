package cn.com.libertymutual.production.model.nomorcldatasource;

import java.math.BigDecimal;
import java.util.Date;

public class Prpdkindlibrary extends PrpdkindlibraryKey {
    private Date startdate;

    private Date enddate;

    private String kindcname;

    private String kindename;

    private String clausecode;

    private String relyonkindcode;

    private Date relyonstartdate;

    private Date relyonenddate;

    private String calculateflag;

    private String newkindcode;

    private String validstatus;

    private String flag;

    private String ownerriskcode;

    private Date createdate;

    private Date updatedate;

    private String ext1;

    private String ext2;

    private String validflag;

    private String id;

    private String efilestatus;

    private String kindheight;

    private String kindwidth;

    private String waysofcalc;

    private String ilogflag;

    private BigDecimal agreementrate;

    private String pluskindcode;

    private BigDecimal plusrate;

    private String shortratetype;

    public Date getStartdate() {
        return startdate;
    }

    public void setStartdate(Date startdate) {
        this.startdate = startdate;
    }

    public Date getEnddate() {
        return enddate;
    }

    public void setEnddate(Date enddate) {
        this.enddate = enddate;
    }

    public String getKindcname() {
        return kindcname;
    }

    public void setKindcname(String kindcname) {
        this.kindcname = kindcname == null ? null : kindcname.trim();
    }

    public String getKindename() {
        return kindename;
    }

    public void setKindename(String kindename) {
        this.kindename = kindename == null ? null : kindename.trim();
    }

    public String getClausecode() {
        return clausecode;
    }

    public void setClausecode(String clausecode) {
        this.clausecode = clausecode == null ? null : clausecode.trim();
    }

    public String getRelyonkindcode() {
        return relyonkindcode;
    }

    public void setRelyonkindcode(String relyonkindcode) {
        this.relyonkindcode = relyonkindcode == null ? null : relyonkindcode.trim();
    }

    public Date getRelyonstartdate() {
        return relyonstartdate;
    }

    public void setRelyonstartdate(Date relyonstartdate) {
        this.relyonstartdate = relyonstartdate;
    }

    public Date getRelyonenddate() {
        return relyonenddate;
    }

    public void setRelyonenddate(Date relyonenddate) {
        this.relyonenddate = relyonenddate;
    }

    public String getCalculateflag() {
        return calculateflag;
    }

    public void setCalculateflag(String calculateflag) {
        this.calculateflag = calculateflag == null ? null : calculateflag.trim();
    }

    public String getNewkindcode() {
        return newkindcode;
    }

    public void setNewkindcode(String newkindcode) {
        this.newkindcode = newkindcode == null ? null : newkindcode.trim();
    }

    public String getValidstatus() {
        return validstatus;
    }

    public void setValidstatus(String validstatus) {
        this.validstatus = validstatus == null ? null : validstatus.trim();
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag == null ? null : flag.trim();
    }

    public String getOwnerriskcode() {
        return ownerriskcode;
    }

    public void setOwnerriskcode(String ownerriskcode) {
        this.ownerriskcode = ownerriskcode == null ? null : ownerriskcode.trim();
    }

    public Date getCreatedate() {
        return createdate;
    }

    public void setCreatedate(Date createdate) {
        this.createdate = createdate;
    }

    public Date getUpdatedate() {
        return updatedate;
    }

    public void setUpdatedate(Date updatedate) {
        this.updatedate = updatedate;
    }

    public String getExt1() {
        return ext1;
    }

    public void setExt1(String ext1) {
        this.ext1 = ext1 == null ? null : ext1.trim();
    }

    public String getExt2() {
        return ext2;
    }

    public void setExt2(String ext2) {
        this.ext2 = ext2 == null ? null : ext2.trim();
    }

    public String getValidflag() {
        return validflag;
    }

    public void setValidflag(String validflag) {
        this.validflag = validflag == null ? null : validflag.trim();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id == null ? null : id.trim();
    }

    public String getEfilestatus() {
        return efilestatus;
    }

    public void setEfilestatus(String efilestatus) {
        this.efilestatus = efilestatus == null ? null : efilestatus.trim();
    }

    public String getKindheight() {
        return kindheight;
    }

    public void setKindheight(String kindheight) {
        this.kindheight = kindheight == null ? null : kindheight.trim();
    }

    public String getKindwidth() {
        return kindwidth;
    }

    public void setKindwidth(String kindwidth) {
        this.kindwidth = kindwidth == null ? null : kindwidth.trim();
    }

    public String getWaysofcalc() {
        return waysofcalc;
    }

    public void setWaysofcalc(String waysofcalc) {
        this.waysofcalc = waysofcalc == null ? null : waysofcalc.trim();
    }

    public String getIlogflag() {
        return ilogflag;
    }

    public void setIlogflag(String ilogflag) {
        this.ilogflag = ilogflag == null ? null : ilogflag.trim();
    }

    public BigDecimal getAgreementrate() {
        return agreementrate;
    }

    public void setAgreementrate(BigDecimal agreementrate) {
        this.agreementrate = agreementrate;
    }

    public String getPluskindcode() {
        return pluskindcode;
    }

    public void setPluskindcode(String pluskindcode) {
        this.pluskindcode = pluskindcode == null ? null : pluskindcode.trim();
    }

    public BigDecimal getPlusrate() {
        return plusrate;
    }

    public void setPlusrate(BigDecimal plusrate) {
        this.plusrate = plusrate;
    }

    public String getShortratetype() {
        return shortratetype;
    }

    public void setShortratetype(String shortratetype) {
        this.shortratetype = shortratetype == null ? null : shortratetype.trim();
    }
}