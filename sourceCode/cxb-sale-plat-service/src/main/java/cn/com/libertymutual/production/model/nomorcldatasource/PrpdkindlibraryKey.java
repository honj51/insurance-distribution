package cn.com.libertymutual.production.model.nomorcldatasource;

public class PrpdkindlibraryKey {
    private String kindcode;

    private String kindversion;

    public String getKindcode() {
        return kindcode;
    }

    public void setKindcode(String kindcode) {
        this.kindcode = kindcode == null ? null : kindcode.trim();
    }

    public String getKindversion() {
        return kindversion;
    }

    public void setKindversion(String kindversion) {
        this.kindversion = kindversion == null ? null : kindversion.trim();
    }
}