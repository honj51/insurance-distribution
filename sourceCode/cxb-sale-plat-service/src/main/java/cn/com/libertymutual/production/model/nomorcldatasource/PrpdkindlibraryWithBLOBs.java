package cn.com.libertymutual.production.model.nomorcldatasource;

public class PrpdkindlibraryWithBLOBs extends Prpdkindlibrary {
    private String powagentcode;

    private String remark;

    public String getPowagentcode() {
        return powagentcode;
    }

    public void setPowagentcode(String powagentcode) {
        this.powagentcode = powagentcode == null ? null : powagentcode.trim();
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }
}