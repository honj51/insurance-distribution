package cn.com.libertymutual.production.model.nomorcldatasource;

public class Prpdplanprops extends PrpdplanpropsKey {
    private String validstatus;

    private String startvalue;

    private String endvalue;

    private String ext1;

    private String ext2;

    private String propsvalue;

    public String getValidstatus() {
        return validstatus;
    }

    public void setValidstatus(String validstatus) {
        this.validstatus = validstatus == null ? null : validstatus.trim();
    }

    public String getStartvalue() {
        return startvalue;
    }

    public void setStartvalue(String startvalue) {
        this.startvalue = startvalue == null ? null : startvalue.trim();
    }

    public String getEndvalue() {
        return endvalue;
    }

    public void setEndvalue(String endvalue) {
        this.endvalue = endvalue == null ? null : endvalue.trim();
    }

    public String getExt1() {
        return ext1;
    }

    public void setExt1(String ext1) {
        this.ext1 = ext1 == null ? null : ext1.trim();
    }

    public String getExt2() {
        return ext2;
    }

    public void setExt2(String ext2) {
        this.ext2 = ext2 == null ? null : ext2.trim();
    }

    public String getPropsvalue() {
        return propsvalue;
    }

    public void setPropsvalue(String propsvalue) {
        this.propsvalue = propsvalue == null ? null : propsvalue.trim();
    }
}