package cn.com.libertymutual.production.model.nomorcldatasource;

public class Prpdprops {
    private String propscode;

    private String propscname;

    private String propsename;

    public String getPropscode() {
        return propscode;
    }

    public void setPropscode(String propscode) {
        this.propscode = propscode == null ? null : propscode.trim();
    }

    public String getPropscname() {
        return propscname;
    }

    public void setPropscname(String propscname) {
        this.propscname = propscname == null ? null : propscname.trim();
    }

    public String getPropsename() {
        return propsename;
    }

    public void setPropsename(String propsename) {
        this.propsename = propsename == null ? null : propsename.trim();
    }
}