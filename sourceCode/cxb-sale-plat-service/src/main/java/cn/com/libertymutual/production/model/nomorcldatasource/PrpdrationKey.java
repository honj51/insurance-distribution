package cn.com.libertymutual.production.model.nomorcldatasource;

public class PrpdrationKey {
    private String riskcode;

    private String riskversion;

    private String plancode;

    private Long serialno;

    public String getRiskcode() {
        return riskcode;
    }

    public void setRiskcode(String riskcode) {
        this.riskcode = riskcode == null ? null : riskcode.trim();
    }

    public String getRiskversion() {
        return riskversion;
    }

    public void setRiskversion(String riskversion) {
        this.riskversion = riskversion == null ? null : riskversion.trim();
    }

    public String getPlancode() {
        return plancode;
    }

    public void setPlancode(String plancode) {
        this.plancode = plancode == null ? null : plancode.trim();
    }

    public Long getSerialno() {
        return serialno;
    }

    public void setSerialno(Long serialno) {
        this.serialno = serialno;
    }
}