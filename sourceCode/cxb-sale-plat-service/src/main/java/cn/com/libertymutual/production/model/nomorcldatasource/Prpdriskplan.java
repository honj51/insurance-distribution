package cn.com.libertymutual.production.model.nomorcldatasource;

import java.util.Date;

import cn.com.libertymutual.production.utils.LogFiled;

public class Prpdriskplan extends PrpdriskplanKey {

	@LogFiled(chineseName = "方案中文名称")
	private String plancname;

	@LogFiled(chineseName = "方案英文名称")
	private String planename;

	private Long serialno;

	@LogFiled(chineseName = "方案有效性")
	private String validstatus;

	private String flag;

	private String plandesc;

	@LogFiled(chineseName = "方案类型")
	private String plantype;

	@LogFiled(chineseName = "方案权限适用机构")
	private String applycomcode;

	@LogFiled(chineseName = "方案权限适用渠道")
	private String applychannel;

	@LogFiled(chineseName = "是否自动续保")
	private String autoundwrt;

	@LogFiled(chineseName = "预设保险期限方式")
	private String periodtype;

	@LogFiled(chineseName = "预设保险期限值")
	private Integer period;

	@LogFiled(chineseName = "职业分类代码")
	private String occupationcode;

	@LogFiled(chineseName = "行程目的地")
	private String traveldestination;

	private Date createdate;

	private Date updatedate;

	@LogFiled(chineseName = "是否自动单证审核通过")
	private String autodoc;

	public String getAutodoc() {
		return autodoc;
	}

	public void setAutodoc(String autodoc) {
		this.autodoc = autodoc;
	}

	public String getPlancname() {
		return plancname;
	}

	public void setPlancname(String plancname) {
		this.plancname = plancname == null ? null : plancname.trim();
	}

	public String getPlanename() {
		return planename;
	}

	public void setPlanename(String planename) {
		this.planename = planename == null ? null : planename.trim();
	}

	public Long getSerialno() {
		return serialno;
	}

	public void setSerialno(Long serialno) {
		this.serialno = serialno;
	}

	public String getValidstatus() {
		return validstatus;
	}

	public void setValidstatus(String validstatus) {
		this.validstatus = validstatus == null ? null : validstatus.trim();
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag == null ? null : flag.trim();
	}

	public String getPlandesc() {
		return plandesc;
	}

	public void setPlandesc(String plandesc) {
		this.plandesc = plandesc == null ? null : plandesc.trim();
	}

	public String getPlantype() {
		return plantype;
	}

	public void setPlantype(String plantype) {
		this.plantype = plantype == null ? null : plantype.trim();
	}

	public String getApplycomcode() {
		return applycomcode;
	}

	public void setApplycomcode(String applycomcode) {
		this.applycomcode = applycomcode == null ? null : applycomcode.trim();
	}

	public String getApplychannel() {
		return applychannel;
	}

	public void setApplychannel(String applychannel) {
		this.applychannel = applychannel == null ? null : applychannel.trim();
	}

	public String getAutoundwrt() {
		return autoundwrt;
	}

	public void setAutoundwrt(String autoundwrt) {
		this.autoundwrt = autoundwrt == null ? null : autoundwrt.trim();
	}

	public String getPeriodtype() {
		return periodtype;
	}

	public void setPeriodtype(String periodtype) {
		this.periodtype = periodtype == null ? null : periodtype.trim();
	}

	public Integer getPeriod() {
		return period;
	}

	public void setPeriod(Integer period) {
		this.period = period;
	}

	public String getOccupationcode() {
		return occupationcode;
	}

	public void setOccupationcode(String occupationcode) {
		this.occupationcode = occupationcode == null ? null : occupationcode
				.trim();
	}

	public String getTraveldestination() {
		return traveldestination;
	}

	public void setTraveldestination(String traveldestination) {
		this.traveldestination = traveldestination == null ? null
				: traveldestination.trim();
	}

	public Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

	public Date getUpdatedate() {
		return updatedate;
	}

	public void setUpdatedate(Date updatedate) {
		this.updatedate = updatedate;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		if (!super.equals(o)) return false;

		Prpdriskplan that = (Prpdriskplan) o;

		if (plancname != null ? !plancname.equals(that.plancname) : that.plancname != null) return false;
		if (planename != null ? !planename.equals(that.planename) : that.planename != null) return false;
		if (serialno != null ? !serialno.equals(that.serialno) : that.serialno != null) return false;
		if (validstatus != null ? !validstatus.equals(that.validstatus) : that.validstatus != null) return false;
		if (flag != null ? !flag.equals(that.flag) : that.flag != null) return false;
		if (plandesc != null ? !plandesc.equals(that.plandesc) : that.plandesc != null) return false;
		if (plantype != null ? !plantype.equals(that.plantype) : that.plantype != null) return false;
		if (applycomcode != null ? !applycomcode.equals(that.applycomcode) : that.applycomcode != null) return false;
		if (applychannel != null ? !applychannel.equals(that.applychannel) : that.applychannel != null) return false;
		if (autoundwrt != null ? !autoundwrt.equals(that.autoundwrt) : that.autoundwrt != null) return false;
		if (periodtype != null ? !periodtype.equals(that.periodtype) : that.periodtype != null) return false;
		if (period != null ? !period.equals(that.period) : that.period != null) return false;
		if (occupationcode != null ? !occupationcode.equals(that.occupationcode) : that.occupationcode != null)
			return false;
		if (traveldestination != null ? !traveldestination.equals(that.traveldestination) : that.traveldestination != null)
			return false;
		if (createdate != null ? !createdate.equals(that.createdate) : that.createdate != null) return false;
		if (updatedate != null ? !updatedate.equals(that.updatedate) : that.updatedate != null) return false;
		return autodoc != null ? autodoc.equals(that.autodoc) : that.autodoc == null;

	}

	@Override
	public int hashCode() {
		int result = super.hashCode();
		result = 31 * result + (plancname != null ? plancname.hashCode() : 0);
		result = 31 * result + (planename != null ? planename.hashCode() : 0);
		result = 31 * result + (serialno != null ? serialno.hashCode() : 0);
		result = 31 * result + (validstatus != null ? validstatus.hashCode() : 0);
		result = 31 * result + (flag != null ? flag.hashCode() : 0);
		result = 31 * result + (plandesc != null ? plandesc.hashCode() : 0);
		result = 31 * result + (plantype != null ? plantype.hashCode() : 0);
		result = 31 * result + (applycomcode != null ? applycomcode.hashCode() : 0);
		result = 31 * result + (applychannel != null ? applychannel.hashCode() : 0);
		result = 31 * result + (autoundwrt != null ? autoundwrt.hashCode() : 0);
		result = 31 * result + (periodtype != null ? periodtype.hashCode() : 0);
		result = 31 * result + (period != null ? period.hashCode() : 0);
		result = 31 * result + (occupationcode != null ? occupationcode.hashCode() : 0);
		result = 31 * result + (traveldestination != null ? traveldestination.hashCode() : 0);
		result = 31 * result + (createdate != null ? createdate.hashCode() : 0);
		result = 31 * result + (updatedate != null ? updatedate.hashCode() : 0);
		result = 31 * result + (autodoc != null ? autodoc.hashCode() : 0);
		return result;
	}
}