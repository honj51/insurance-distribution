package cn.com.libertymutual.production.model.nomorcldatasource;

public class PrpdriskplanWithBLOBs extends Prpdriskplan {
    private String applycomcode;

    private String applychannel;

    private String traveldestination;

    public String getApplycomcode() {
        return applycomcode;
    }

    public void setApplycomcode(String applycomcode) {
        this.applycomcode = applycomcode == null ? null : applycomcode.trim();
    }

    public String getApplychannel() {
        return applychannel;
    }

    public void setApplychannel(String applychannel) {
        this.applychannel = applychannel == null ? null : applychannel.trim();
    }

    public String getTraveldestination() {
        return traveldestination;
    }

    public void setTraveldestination(String traveldestination) {
        this.traveldestination = traveldestination == null ? null : traveldestination.trim();
    }
}