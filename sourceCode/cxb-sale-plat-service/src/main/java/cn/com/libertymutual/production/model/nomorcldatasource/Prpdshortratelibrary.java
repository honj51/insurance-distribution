package cn.com.libertymutual.production.model.nomorcldatasource;

import java.math.BigDecimal;

public class Prpdshortratelibrary {
    private String shortratetype;

    private BigDecimal rate1;

    private BigDecimal rate2;

    private BigDecimal rate3;

    private BigDecimal rate4;

    private BigDecimal rate5;

    private BigDecimal rate6;

    private BigDecimal rate7;

    private BigDecimal rate8;

    private BigDecimal rate9;

    private BigDecimal rate10;

    private BigDecimal rate11;

    private BigDecimal rate12;

    private String validstatus;

    public String getShortratetype() {
        return shortratetype;
    }

    public void setShortratetype(String shortratetype) {
        this.shortratetype = shortratetype == null ? null : shortratetype.trim();
    }

    public BigDecimal getRate1() {
        return rate1;
    }

    public void setRate1(BigDecimal rate1) {
        this.rate1 = rate1;
    }

    public BigDecimal getRate2() {
        return rate2;
    }

    public void setRate2(BigDecimal rate2) {
        this.rate2 = rate2;
    }

    public BigDecimal getRate3() {
        return rate3;
    }

    public void setRate3(BigDecimal rate3) {
        this.rate3 = rate3;
    }

    public BigDecimal getRate4() {
        return rate4;
    }

    public void setRate4(BigDecimal rate4) {
        this.rate4 = rate4;
    }

    public BigDecimal getRate5() {
        return rate5;
    }

    public void setRate5(BigDecimal rate5) {
        this.rate5 = rate5;
    }

    public BigDecimal getRate6() {
        return rate6;
    }

    public void setRate6(BigDecimal rate6) {
        this.rate6 = rate6;
    }

    public BigDecimal getRate7() {
        return rate7;
    }

    public void setRate7(BigDecimal rate7) {
        this.rate7 = rate7;
    }

    public BigDecimal getRate8() {
        return rate8;
    }

    public void setRate8(BigDecimal rate8) {
        this.rate8 = rate8;
    }

    public BigDecimal getRate9() {
        return rate9;
    }

    public void setRate9(BigDecimal rate9) {
        this.rate9 = rate9;
    }

    public BigDecimal getRate10() {
        return rate10;
    }

    public void setRate10(BigDecimal rate10) {
        this.rate10 = rate10;
    }

    public BigDecimal getRate11() {
        return rate11;
    }

    public void setRate11(BigDecimal rate11) {
        this.rate11 = rate11;
    }

    public BigDecimal getRate12() {
        return rate12;
    }

    public void setRate12(BigDecimal rate12) {
        this.rate12 = rate12;
    }

    public String getValidstatus() {
        return validstatus;
    }

    public void setValidstatus(String validstatus) {
        this.validstatus = validstatus == null ? null : validstatus.trim();
    }
}