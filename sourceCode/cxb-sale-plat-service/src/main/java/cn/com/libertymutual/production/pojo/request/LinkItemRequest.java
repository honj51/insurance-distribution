package cn.com.libertymutual.production.pojo.request;

import java.util.List;

public class LinkItemRequest extends Request {
	
	private PrpdKindLibraryRequest kind;
	private List<PrpdItemLibraryRequest> selectedRows;
	
	public PrpdKindLibraryRequest getKind() {
		return kind;
	}
	public void setKind(PrpdKindLibraryRequest kind) {
		this.kind = kind;
	}
	public List<PrpdItemLibraryRequest> getSelectedRows() {
		return selectedRows;
	}
	public void setSelectedRows(List<PrpdItemLibraryRequest> selectedRows) {
		this.selectedRows = selectedRows;
	}
	
	
}
