package cn.com.libertymutual.production.pojo.request;

import java.util.Date;
import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdrisk;

public class PrpdItemLibraryRequest extends Request {
	private String itemcode;

	private String itemcname;

	private String itemename;

	private String itemflag;

	private String newitemcode;

	private String validstatus;

	private String flag;

	private Date startdate;

	private Date enddate;

	private Date createdate;

	private String id;

	private PrpdKindLibraryRequest kind;

	private String riskcode;

	private String riskversion;

	private List<Prpdrisk> risks;

	private String owneritemcode;

	private String ownerflag;

	public String getOwnerflag() {
		return ownerflag;
	}

	public void setOwnerflag(String ownerflag) {
		this.ownerflag = ownerflag;
	}

	public String getItemcode() {
		return itemcode;
	}

	public void setItemcode(String itemcode) {
		this.itemcode = itemcode;
	}

	public String getItemcname() {
		return itemcname;
	}

	public void setItemcname(String itemcname) {
		this.itemcname = itemcname;
	}

	public String getItemename() {
		return itemename;
	}

	public void setItemename(String itemename) {
		this.itemename = itemename;
	}

	public String getItemflag() {
		return itemflag;
	}

	public void setItemflag(String itemflag) {
		this.itemflag = itemflag;
	}

	public String getNewitemcode() {
		return newitemcode;
	}

	public void setNewitemcode(String newitemcode) {
		this.newitemcode = newitemcode;
	}

	public String getValidstatus() {
		return validstatus;
	}

	public void setValidstatus(String validstatus) {
		this.validstatus = validstatus;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

	public Date getStartdate() {
		return startdate;
	}

	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}

	public Date getEnddate() {
		return enddate;
	}

	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public PrpdKindLibraryRequest getKind() {
		return kind;
	}

	public void setKind(PrpdKindLibraryRequest kind) {
		this.kind = kind;
	}

	public String getRiskcode() {
		return riskcode;
	}

	public void setRiskcode(String riskcode) {
		this.riskcode = riskcode;
	}

	public String getRiskversion() {
		return riskversion;
	}

	public void setRiskversion(String riskversion) {
		this.riskversion = riskversion;
	}

	public List<Prpdrisk> getRisks() {
		return risks;
	}

	public void setRisks(List<Prpdrisk> risks) {
		this.risks = risks;
	}

	public String getOwneritemcode() {
		return owneritemcode;
	}

	public void setOwneritemcode(String owneritemcode) {
		this.owneritemcode = owneritemcode;
	}

	public Date getCreatedate() {
		return createdate;
	}

	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}

}
