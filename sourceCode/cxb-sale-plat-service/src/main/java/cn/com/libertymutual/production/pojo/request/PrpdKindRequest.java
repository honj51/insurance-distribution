package cn.com.libertymutual.production.pojo.request;

import java.util.List;

public class PrpdKindRequest extends Request {

	private PrpdKindLibraryRequest kind;
	
	private List<PrpdKindLibraryRequest> selectedRows;

	public PrpdKindLibraryRequest getKind() {
		return kind;
	}

	public void setKind(PrpdKindLibraryRequest kind) {
		this.kind = kind;
	}

	public List<PrpdKindLibraryRequest> getSelectedRows() {
		return selectedRows;
	}

	public void setSelectedRows(List<PrpdKindLibraryRequest> selectedRows) {
		this.selectedRows = selectedRows;
	}
	
}
