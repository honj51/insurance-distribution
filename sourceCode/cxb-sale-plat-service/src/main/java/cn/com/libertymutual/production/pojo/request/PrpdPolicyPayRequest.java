package cn.com.libertymutual.production.pojo.request;

import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdpolicypay;

public class PrpdPolicyPayRequest extends Request {

	private String comcode;

	private String riskcode;

	private String agentcode;

	private String ispolicypay;

	private List<Prpdpolicypay> selectedRows;

	public List<Prpdpolicypay> getSelectedRows() {
		return selectedRows;
	}

	public void setSelectedRows(List<Prpdpolicypay> selectedRows) {
		this.selectedRows = selectedRows;
	}

	public String getIspolicypay() {
		return ispolicypay;
	}

	public void setIspolicypay(String ispolicypay) {
		this.ispolicypay = ispolicypay;
	}

	public String getAgentcode() {
		return agentcode;
	}

	public void setAgentcode(String agentcode) {
		this.agentcode = agentcode;
	}

	public String getComcode() {
		return comcode;
	}

	public void setComcode(String comcode) {
		this.comcode = comcode;
	}

	public String getRiskcode() {
		return riskcode;
	}

	public void setRiskcode(String riskcode) {
		this.riskcode = riskcode;
	}
}
