package cn.com.libertymutual.production.pojo.response;

import cn.com.libertymutual.core.exception.CustomLangException;
import cn.com.libertymutual.core.util.BeanUtilExt;

public class MessageProcessor {

	/**
	 * 成功
	 */
	public static final int STATE_SUCCESS = 0;
	/**
	 * 应用异常
	 */
	public static final int STATE_APP_EXCEPTION = 1;
	/**
	 * 其他异常
	 */
	public static final int STATE_EXCEPTION = 2;
	/**
	 * 没有登录
	 */
	public static final int STATE_NO_SESSION = 3;

	protected Object result;

	protected int state;
	
	protected String resCode;
	
	protected String token;

	public Object getResult() {
		return result == null ? "" : result;
	}

	public void setResult(Object result) {
		this.result = result;
	}
	

	public <L> L getEntity( Class<L> responseType )
	{
		return BeanUtilExt.copy(responseType, result);
	}
	
	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public String getResCode() {
		return resCode;
	}

	public void setResCode(String resCode) {
		this.resCode = resCode;
	}
	
	public void setSuccess() {
		this.state = STATE_SUCCESS;
	}
	
	public void setFail() {
		this.state = STATE_EXCEPTION;
	}
	
	public void setAppFail() {
		this.state = STATE_APP_EXCEPTION;
	}
	
	public void setAppFail( CustomLangException ee ) {
		this.state = STATE_APP_EXCEPTION;
		this.resCode=String.valueOf( ee.getErrorCode());
		this.result =this.result==null? ee.getErrorMessage():this.result;
	}
	
	public String getToken() {
		return this.token;
	}
	
	public void setToken( String token ) {
		this.token = token;
	}

	public boolean isSuccess() {
		return this.state == STATE_SUCCESS;
	}

}
