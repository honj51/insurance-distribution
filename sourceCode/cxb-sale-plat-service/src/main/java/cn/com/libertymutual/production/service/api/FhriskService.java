package cn.com.libertymutual.production.service.api;

import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Fhrisk;

/**
 * Created by steven.li on 2017/12/18.
 */
public interface FhriskService {

    /**
     * 新增Fhrisk
     * @param record
     */
    void insert(Fhrisk record);

    List<Fhrisk> findAll();

    List<Fhrisk> findByRisk(String riskcode);
}
