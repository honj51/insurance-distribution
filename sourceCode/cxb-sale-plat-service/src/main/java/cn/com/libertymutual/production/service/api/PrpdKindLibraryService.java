package cn.com.libertymutual.production.service.api;


import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdkindlibrary;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdkindlibraryWithBLOBs;
import cn.com.libertymutual.production.pojo.request.PrpdKindLibraryRequest;

import com.github.pagehelper.PageInfo;

public interface PrpdKindLibraryService {

	/**
	 * 查询条款信息
	 * @param PrpdKindLibraryRequest
	 * @return
	 */
	public PageInfo<PrpdkindlibraryWithBLOBs> findPrpdKindLibrary(PrpdKindLibraryRequest prpdKindLibraryRequest);
	
	public void insert(PrpdkindlibraryWithBLOBs record);
	
	public void update(PrpdkindlibraryWithBLOBs record);
	
	public List<PrpdkindlibraryWithBLOBs> fetchKinds(String kindCode,
											String kindVersion,
											String ownerRiskCode, 
											String validStatus);
	
	public List<PrpdkindlibraryWithBLOBs> fetchKinds(String kindCode,
											String kindVersion,
											String validStatus);
	
	//修改是否配置备案号
	public void updateEFileStatus(PrpdkindlibraryWithBLOBs record, String kindCode, String kindVersion);
	
	/**
	 * 根据归属险类查询对应的条款
	 * @param prpdKindLibraryRequest
	 * @return
	 */
	public PageInfo<Prpdkindlibrary> findByOwnerRisk(PrpdKindLibraryRequest prpdKindLibraryRequest);
	
}
