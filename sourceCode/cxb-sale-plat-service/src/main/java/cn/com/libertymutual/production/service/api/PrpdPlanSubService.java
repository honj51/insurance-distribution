package cn.com.libertymutual.production.service.api;

import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpdplansub;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdriskplan;

public interface PrpdPlanSubService {
	
	public void insert(Prpdplansub record);

	public List<Prpdplansub> select(Prpdriskplan plan);

	public void deleteByPlan(Prpdriskplan plan);
	
}
