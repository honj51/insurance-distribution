package cn.com.libertymutual.production.service.api;

import java.util.List;

import cn.com.libertymutual.production.model.nomorcldatasource.Prpsagent;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpsagentWithBLOBs;
import cn.com.libertymutual.production.pojo.request.Request;

public interface PrpsAgentService {
	
	public List<PrpsagentWithBLOBs> findPrpsAgentByComCode(List<String> comCodes, Request request);
}
