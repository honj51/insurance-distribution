package cn.com.libertymutual.production.service.api.business;

import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import cn.com.libertymutual.production.pojo.request.PrpdClauseRequest;
import cn.com.libertymutual.production.pojo.response.Response;
import cn.com.libertymutual.production.service.api.PrpMaxNoService;
import cn.com.libertymutual.production.service.api.PrpdClauseService;
import cn.com.libertymutual.production.service.api.PrpdCodeRiskService;
import cn.com.libertymutual.production.service.api.PrpdRiskClauseService;
import cn.com.libertymutual.production.service.impl.SystemLog;
import cn.com.libertymutual.production.utils.Constant;

/** 
 * @Description: 后台业务逻辑入口
 * @author Steven.Li
 * @date 2017年7月28日
 *  
 */
public abstract class ClauseBusinessService {

	protected static final Logger log = Constant.log;
	
	@Autowired
	protected SystemLog systemLog;
	@Autowired
	protected PrpdClauseService prpdClauseService;
	@Autowired
	protected PrpMaxNoService prpMaxNoService;
	@Autowired
	protected PrpdCodeRiskService prpdCodeRiskService;
	@Autowired
	protected PrpdRiskClauseService prpdRiskClauseService;

	/**
	 * 获取特约基本信息
	 * @param prpdClauseLibraryRequest
	 * @return
	 */
	public abstract Response findPrpdClause(PrpdClauseRequest prpdClauseRequest);
	
	/**
	 * 获取特约序列号
	 * @param prpdItemRequest
	 * @return
	 */
	public abstract Response findPrpdClauseCodeNo();
	
	/**
	 * 新增特约信息
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public abstract Response insertPrpdClause(PrpdClauseRequest request) throws Exception;
	
	/**
	 * 通过特约查询适用险种
	 * @param clauseCode
	 * @return
	 */
	public abstract Response findCodeRiskByClause(String clauseCode);
	
	/**
	 * 通过特约查询默认带出险种、默认带出分公司、适用计划
	 * @param clauseCode
	 * @return
	 */
	public abstract Response findRiskClauseByClause(String clauseCode);
	
	/**
	 * 修改特约
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public abstract Response updatePrpdClause(PrpdClauseRequest request) throws Exception;
}