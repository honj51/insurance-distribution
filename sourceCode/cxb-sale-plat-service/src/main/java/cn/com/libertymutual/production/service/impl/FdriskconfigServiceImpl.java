package cn.com.libertymutual.production.service.impl;

import cn.com.libertymutual.production.dao.nomorcldatasource.FdriskconfigMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Fdriskconfig;
import cn.com.libertymutual.production.model.nomorcldatasource.FdriskconfigExample;
import cn.com.libertymutual.production.service.api.FdriskconfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by steven.li on 2017/12/18.
 */
@Service
public class FdriskconfigServiceImpl implements FdriskconfigService {

    @Autowired
    private FdriskconfigMapper fdriskconfigMapper;

    @Override
    public void insert(Fdriskconfig record) {
        fdriskconfigMapper.insertSelective(record);
    }

    @Override
    public List<Fdriskconfig> findAll() {
        FdriskconfigExample criteria = new FdriskconfigExample();
        return fdriskconfigMapper.selectByExample(criteria);
    }

    @Override
    public List<Fdriskconfig> findByRisk(String riskcode) {
        FdriskconfigExample criteria = new FdriskconfigExample();
        criteria.createCriteria().andRiskcodeEqualTo(riskcode);
        return fdriskconfigMapper.selectByExample(criteria);
    }
}
