package cn.com.libertymutual.production.service.impl;

import cn.com.libertymutual.production.dao.nomorcldatasource.FhexitemkindMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Fhexitemkind;
import cn.com.libertymutual.production.model.nomorcldatasource.FhexitemkindExample;
import cn.com.libertymutual.production.service.api.FhexitemkindService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by steven.li on 2017/12/18.
 */
@Service
public class FhexitemkindServiceImpl implements FhexitemkindService {

    @Autowired
    private FhexitemkindMapper fhexitemkindMapper;


    @Override
    public void insert(Fhexitemkind record) {
        fhexitemkindMapper.insertSelective(record);
    }

    @Override
    public List<Fhexitemkind> findAll(String treatyno, String sectionno) {
        FhexitemkindExample criteria = new FhexitemkindExample();
        criteria.createCriteria().andTreatynoEqualTo(treatyno)
                                 .andSectionnoEqualTo(sectionno);
        criteria.setOrderByClause("itemkind asc");
        return fhexitemkindMapper.selectByExampleWithBLOBs(criteria);
    }

    @Override
    public List<Fhexitemkind> findByRisk(String riskcode) {
        FhexitemkindExample criteria = new FhexitemkindExample();
        criteria.createCriteria().andRiskcodeEqualTo(riskcode);
        criteria.setOrderByClause("itemkind asc");
        return fhexitemkindMapper.selectByExampleWithBLOBs(criteria);
    }
}
