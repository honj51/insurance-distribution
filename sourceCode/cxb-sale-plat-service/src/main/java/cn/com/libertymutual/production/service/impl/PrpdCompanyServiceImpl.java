package cn.com.libertymutual.production.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.libertymutual.production.dao.nomorcldatasource.PrpdcompanyMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdcompany;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdcompanyExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdcompanyExample.Criteria;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdcompanyWithBLOBs;
import cn.com.libertymutual.production.pojo.request.Request;
import cn.com.libertymutual.production.service.api.PrpdCompanyService;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

/** 
 * @author Steven.Li
 * @date 2017年5月19日
 *  
 */
@Service
public class PrpdCompanyServiceImpl implements PrpdCompanyService {

	@Autowired
	private PrpdcompanyMapper prpdcompanyMapper;

	@Override
	public PageInfo<PrpdcompanyWithBLOBs> findPrpdCompanys(Request request) {
		PrpdcompanyExample example = new PrpdcompanyExample();
		Criteria criteria = example.createCriteria();
		criteria.andComlevelGreaterThanOrEqualTo("1");
		criteria.andComlevelLessThanOrEqualTo("3");
		criteria.andValidstatusEqualTo("1");
		PageHelper.startPage(request.getCurrentPage(), request.getPageSize());
		PageInfo<PrpdcompanyWithBLOBs> pageInfo = new PageInfo<>(prpdcompanyMapper.selectByExampleWithBLOBs(example));
		return pageInfo;
	}

	@Override
	public List<PrpdcompanyWithBLOBs> findCompanys() {
		PrpdcompanyExample example = new PrpdcompanyExample();
		Criteria criteria = example.createCriteria();
		criteria.andCenterflagEqualTo("1");
		criteria.andComcodeNotEqualTo("00000000");
		criteria.andValidstatusEqualTo("1");
		return prpdcompanyMapper.selectByExampleWithBLOBs(example);
	}

	@Override
	public List<PrpdcompanyWithBLOBs> findCompanysBycodes(List<String> comcodes) {
		PrpdcompanyExample example = new PrpdcompanyExample();
		Criteria criteria = example.createCriteria();
		criteria.andCenterflagEqualTo("1");
		criteria.andComcodeNotEqualTo("00000000");
		criteria.andValidstatusEqualTo("1");
		criteria.andComcodeIn(comcodes);
		return prpdcompanyMapper.selectByExampleWithBLOBs(example);
	}


}
