package cn.com.libertymutual.production.service.impl;

import cn.com.libertymutual.production.dao.nomorcldatasource.PrpdplansubMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdplansub;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdplansubExample;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdriskplan;
import cn.com.libertymutual.production.service.api.PrpdPlanSubService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class PrpdPlanSubServiceImpl implements PrpdPlanSubService {

	@Autowired
	private PrpdplansubMapper prpdplansubMapper;
	
	@Override
	public void insert(Prpdplansub record) {
		prpdplansubMapper.insertSelective(record);
	}

	@Override
	public List<Prpdplansub> select(Prpdriskplan plan) {
		
		PrpdplansubExample example = new PrpdplansubExample();
		PrpdplansubExample.Criteria criteria = example.createCriteria();
        if (!StringUtils.isEmpty(plan.getPlancode())) {
		    criteria.andPlancodeEqualTo(plan.getPlancode());
        }
        if (!StringUtils.isEmpty(plan.getRiskcode())) {
		    criteria.andRiskcodeEqualTo(plan.getRiskcode());
        }
        if (!StringUtils.isEmpty(plan.getRiskversion())) {
		    criteria.andRiskversionEqualTo(plan.getRiskversion());
        }
		criteria.andValidstatusEqualTo("1");
		example.setOrderByClause("LINENO ASC");
		return prpdplansubMapper.selectByExampleWithBLOBs(example);
	}

	@Override
	public void deleteByPlan(Prpdriskplan plan) {
		PrpdplansubExample example = new PrpdplansubExample();
		example.createCriteria().andPlancodeEqualTo(plan.getPlancode())
								.andRiskcodeEqualTo(plan.getRiskcode())
								.andRiskversionEqualTo(plan.getRiskversion());
		prpdplansubMapper.deleteByExample(example);
	}
	
}
