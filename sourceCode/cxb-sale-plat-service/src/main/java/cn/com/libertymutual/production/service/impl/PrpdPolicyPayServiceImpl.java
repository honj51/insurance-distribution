package cn.com.libertymutual.production.service.impl;

import cn.com.libertymutual.production.dao.nomorcldatasource.PrpdpolicypayMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdpolicypay;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdpolicypayExample;
import cn.com.libertymutual.production.pojo.request.PrpdPolicyPayRequest;
import cn.com.libertymutual.production.service.api.PrpdPolicyPayService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

/**
 * @author steven.li
 * @create 2018/1/31
 */
@Service
public class PrpdPolicyPayServiceImpl implements PrpdPolicyPayService {

    @Autowired
    private PrpdpolicypayMapper prpdpolicypayMapper;

    @Override
    public void insert(Prpdpolicypay record) {
        prpdpolicypayMapper.insertSelective(record);
    }

    @Override
    public void update(Prpdpolicypay record) {
        PrpdpolicypayExample prpdpolicypayExample = new PrpdpolicypayExample();
        PrpdpolicypayExample.Criteria criteria = prpdpolicypayExample.createCriteria();
        if (!StringUtils.isEmpty(record.getComcode())) {
            criteria.andComcodeEqualTo(record.getComcode());
        }
        if (!StringUtils.isEmpty(record.getRiskcode())) {
            criteria.andRiskcodeEqualTo(record.getRiskcode());
        }
        if (!StringUtils.isEmpty(record.getAgentcode())) {
            criteria.andAgentcodeEqualTo(record.getAgentcode());
        }
        prpdpolicypayMapper.updateByExampleSelective(record,prpdpolicypayExample);
    }

    @Override
    public void delete(Prpdpolicypay where) {
        PrpdpolicypayExample prpdpolicypayExample = new PrpdpolicypayExample();
        PrpdpolicypayExample.Criteria criteria = prpdpolicypayExample.createCriteria();
        if (!StringUtils.isEmpty(where.getComcode())) {
            criteria.andComcodeEqualTo(where.getComcode());
        }
        if (!StringUtils.isEmpty(where.getRiskcode())) {
            criteria.andRiskcodeEqualTo(where.getRiskcode());
        }
        if (!StringUtils.isEmpty(where.getAgentcode())) {
            criteria.andAgentcodeEqualTo(where.getAgentcode());
        }
        prpdpolicypayMapper.deleteByExample(prpdpolicypayExample);
    }

    @Override
    public PageInfo<Prpdpolicypay> selectByPage(PrpdPolicyPayRequest prpdPolicyPayRequest) {
        PrpdpolicypayExample prpdpolicypayExample = new PrpdpolicypayExample();
        PrpdpolicypayExample.Criteria criteria = prpdpolicypayExample.createCriteria();

        if (!StringUtils.isEmpty(prpdPolicyPayRequest.getComcode())) {
            criteria.andComcodeEqualTo(prpdPolicyPayRequest.getComcode());
        }
        if (!StringUtils.isEmpty(prpdPolicyPayRequest.getRiskcode())) {
            criteria.andRiskcodeEqualTo(prpdPolicyPayRequest.getRiskcode());
        }
        if (!StringUtils.isEmpty(prpdPolicyPayRequest.getAgentcode())) {
            criteria.andAgentcodeEqualTo(prpdPolicyPayRequest.getAgentcode());
        }

        String orderBy = prpdPolicyPayRequest.getOrderBy();
        String order = prpdPolicyPayRequest.getOrder();
        if (!StringUtils.isEmpty(orderBy) && !"undefined".equals(orderBy)) {
            prpdpolicypayExample.setOrderByClause(orderBy + " " + order);
        }

        PageHelper.startPage(prpdPolicyPayRequest.getCurrentPage(), prpdPolicyPayRequest.getPageSize());
        PageInfo<Prpdpolicypay> pageInfo = new PageInfo<>(prpdpolicypayMapper.selectByExample(prpdpolicypayExample));
        return pageInfo;
    }
}
