package cn.com.libertymutual.production.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import cn.com.libertymutual.production.dao.nomorcldatasource.PrpdpropsMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdprops;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdpropsExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdpropsExample.Criteria;
import cn.com.libertymutual.production.pojo.request.PrpdPropsRequest;
import cn.com.libertymutual.production.service.api.PrpdPropsService;

@Service
public class PrpdPropsServiceImpl implements PrpdPropsService {
	
	@Autowired
	private PrpdpropsMapper prpdpropsMapper;

	@Override
	public List<Prpdprops> select(PrpdPropsRequest request) {
		PrpdpropsExample example = new PrpdpropsExample();
		Criteria criteria = example.createCriteria();
		if(!StringUtils.isEmpty(request.getPropscode())) {
			criteria.andPropscodeLike("%" + request.getPropscode() + "%");
		}
		if(!StringUtils.isEmpty(request.getPropscname())) {
			criteria.andPropscnameLike("%" + request.getPropscname() + "%");
		}
		return prpdpropsMapper.selectByExample(example);
	}

}
