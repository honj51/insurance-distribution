package cn.com.libertymutual.production.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.libertymutual.production.dao.nomorcldatasource.PrpdriskclauseMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdriskclause;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdriskclauseExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdriskclauseWithBLOBs;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdriskclauseExample.Criteria;
import cn.com.libertymutual.production.service.api.PrpdRiskClauseService;

/**
 * @author GuoYue
 * @date 2017年9月12日
 */
@Service
public class PrpdRiskClauseServiceImpl implements PrpdRiskClauseService {

    @Autowired
    private PrpdriskclauseMapper prpdriskclauseMapper;

    @Override
    public void insert(PrpdriskclauseWithBLOBs record) {
        prpdriskclauseMapper.insertSelective(record);
    }

    @Override
    public List<PrpdriskclauseWithBLOBs> findByCluaseCode(String clauseCode) {
        PrpdriskclauseExample example = new PrpdriskclauseExample();
        Criteria criteria = example.createCriteria();
        criteria.andClausecodeEqualTo(clauseCode);
        return prpdriskclauseMapper.selectByExampleWithBLOBs(example);
    }

    @Override
    public void update(PrpdriskclauseWithBLOBs record) {
        PrpdriskclauseExample example = new PrpdriskclauseExample();
        Criteria criteria = example.createCriteria();
        criteria.andClausecodeEqualTo(record.getClausecode());
        prpdriskclauseMapper.updateByExampleSelective(record, example);
    }

    @Override
    public void delete(Prpdriskclause record) {
        PrpdriskclauseExample example = new PrpdriskclauseExample();
        Criteria criteria = example.createCriteria();
        criteria.andClausecodeEqualTo(record.getClausecode());
        prpdriskclauseMapper.deleteByExample(example);
    }

    @Override
    public List<PrpdriskclauseWithBLOBs> findRiskClauseByCondition(String clausecode) {
        PrpdriskclauseExample example = new PrpdriskclauseExample();
        Criteria criteria = example.createCriteria();
        criteria.andClausecodeEqualTo(clausecode);
        return prpdriskclauseMapper.selectByExampleWithBLOBs(example);
    }
}
