package cn.com.libertymutual.production.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import cn.com.libertymutual.production.dao.nomorcldatasource.PrpdriskplanMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdriskplan;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdriskplanExample;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdriskplanWithBLOBs;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdriskplanExample.Criteria;
import cn.com.libertymutual.production.pojo.request.PrpdRiskPlanRequest;
import cn.com.libertymutual.production.service.api.PrpdRiskPlanService;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

/** 
 * @author Steven.Li
 * @date 2017年7月28日
 *  
 */
@Service
public class PrpdRiskPlanServiceImpl implements PrpdRiskPlanService {

	@Autowired
	private PrpdriskplanMapper prpdriskplanMapper;

	@Override
	public PageInfo<PrpdriskplanWithBLOBs> findPrpdRiskPlan(PrpdRiskPlanRequest prpdRiskPlanRequest) {
		PrpdriskplanExample example = new PrpdriskplanExample();
		Criteria criteria = example.createCriteria();
		String planCode = prpdRiskPlanRequest.getPlancode();
		String riskCode = prpdRiskPlanRequest.getRiskcode();
		String riskVersion = prpdRiskPlanRequest.getRiskversion();
		String planCName = prpdRiskPlanRequest.getPlancname();
		Long serialno = prpdRiskPlanRequest.getSerialno();
		Date createDate = prpdRiskPlanRequest.getCreatedate();
		String validStatus = prpdRiskPlanRequest.getValidstatus();
		String orderBy = prpdRiskPlanRequest.getOrderBy();
		String order = prpdRiskPlanRequest.getOrder();
		
		example.setOrderByClause("createdate desc, serialno");
		if(!StringUtils.isEmpty(planCode)) {
			criteria.andPlancodeLike("%" + planCode + "%");
		}
		if (!StringUtils.isEmpty(riskCode)) {
			criteria.andRiskcodeEqualTo(riskCode);
		}
		if (!StringUtils.isEmpty(riskVersion)) {
			criteria.andRiskversionEqualTo(riskVersion);
		}
		if (!StringUtils.isEmpty(planCName)) {
			criteria.andPlancnameLike("%" + planCName + "%");
		}
		if (!StringUtils.isEmpty(serialno)) {
			criteria.andSerialnoEqualTo(serialno);
		}
		if (createDate != null) {
			criteria.andCreatedateGreaterThanOrEqualTo(createDate);
			Calendar c = Calendar.getInstance();  
	        c.setTime(createDate);  
	        c.add(Calendar.DAY_OF_MONTH, 1);// 今天+1天  
	        Date tomorrow = c.getTime();
	        criteria.andCreatedateLessThanOrEqualTo(tomorrow);
		}
		if (!StringUtils.isEmpty(validStatus)) {
			criteria.andValidstatusEqualTo(validStatus);
		}
		if (!StringUtils.isEmpty(orderBy) && !"undefined".equals(orderBy)) {
			example.setOrderByClause(orderBy + " " + order + ", serialno");
		}
		
		PageHelper.startPage(prpdRiskPlanRequest.getCurrentPage(), prpdRiskPlanRequest.getPageSize());
		PageInfo<PrpdriskplanWithBLOBs> pageInfo = new PageInfo<>(prpdriskplanMapper.selectByExampleWithBLOBs(example));
		return pageInfo;
	}

	@Override
	public void insert(PrpdriskplanWithBLOBs record) {
		prpdriskplanMapper.insertSelective(record);
	}

	@Override
	public void update(PrpdriskplanWithBLOBs riskPlan) {
		prpdriskplanMapper.updateByPrimaryKeySelective(riskPlan);
	}

	@Override
	public void inActiveRiskPlan(String kindCode, String kindVersion) {
		prpdriskplanMapper.inActiveRiskPlan(kindCode, kindVersion);
	}

	@Override
	public List<PrpdriskplanWithBLOBs> findPlansByRisks(List<String> riskCodes) {
		List<PrpdriskplanWithBLOBs> plans = new ArrayList<PrpdriskplanWithBLOBs>();
		PrpdriskplanExample example = new PrpdriskplanExample();
		Criteria criteria = example.createCriteria();
		if(riskCodes.isEmpty()) {
			return plans;
		}
		criteria.andRiskcodeIn(riskCodes);
		criteria.andValidstatusEqualTo("1");
		plans = prpdriskplanMapper.selectByExampleWithBLOBs(example);
		return plans;
	}
	
	@Override
	public List<Prpdriskplan> checkRiskLinkedDeletable(String kindCode,
			String riskcode, String riskversion) {
		return prpdriskplanMapper.checkRiskLinkedDeletable(kindCode, riskcode, riskversion);
	}

	@Override
	public List<Prpdriskplan> checkItemLinkedDeletable(String kindCode,
			String kindVersion,
			String itemcode) {
		return prpdriskplanMapper.checkItemLinkedDeletable(kindCode, kindVersion, itemcode);
	}

	@Override
	public List<String> fetchPlancodesKindUsed(String key, String value) {
		return prpdriskplanMapper.fetchPlancodesKindUsed(key, value);
	}
	
}
