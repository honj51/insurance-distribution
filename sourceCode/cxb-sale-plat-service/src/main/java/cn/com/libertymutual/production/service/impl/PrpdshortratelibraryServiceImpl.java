package cn.com.libertymutual.production.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import cn.com.libertymutual.production.dao.nomorcldatasource.PrpdshortratelibraryMapper;
import cn.com.libertymutual.production.model.nomorcldatasource.Prpdshortratelibrary;
import cn.com.libertymutual.production.model.nomorcldatasource.PrpdshortratelibraryExample;
import cn.com.libertymutual.production.service.api.PrpdshortratelibraryService;

@Service
public class PrpdshortratelibraryServiceImpl implements PrpdshortratelibraryService{
	
	@Autowired
	private PrpdshortratelibraryMapper prpdshortratelibraryMapper;

	@Override
	public List<Prpdshortratelibrary> findAll() {
		PrpdshortratelibraryExample criteria = new PrpdshortratelibraryExample();
		criteria.createCriteria().andValidstatusEqualTo("1");
		criteria.setOrderByClause("shortratetype");
		return prpdshortratelibraryMapper.selectByExample(criteria);
	}

}
