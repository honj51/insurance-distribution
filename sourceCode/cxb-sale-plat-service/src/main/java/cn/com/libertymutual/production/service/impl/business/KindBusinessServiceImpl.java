package cn.com.libertymutual.production.service.impl.business;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import cn.com.libertymutual.production.exception.UserException;
import cn.com.libertymutual.production.model.nomorcldatasource.*;
import cn.com.libertymutual.production.pojo.request.LinkItemRequest;
import cn.com.libertymutual.production.pojo.request.LinkRiskRequest;
import cn.com.libertymutual.production.pojo.request.PrpdItemLibraryRequest;
import cn.com.libertymutual.production.pojo.request.PrpdItemRequest;
import cn.com.libertymutual.production.pojo.request.PrpdKindLibraryRequest;
import cn.com.libertymutual.production.pojo.request.PrpdRiskRequest;
import cn.com.libertymutual.production.pojo.response.ItemKind;
import cn.com.libertymutual.production.pojo.response.PrpdkindV1;
import cn.com.libertymutual.production.pojo.response.Response;
import cn.com.libertymutual.production.service.api.business.KindBusinessService;
import cn.com.libertymutual.production.utils.Constant.BUSINESSLOGTYPE;
import cn.com.libertymutual.production.utils.Constant.BUSINESSTYPE;
import cn.com.libertymutual.production.utils.Constant.OPERTYPE;
import cn.com.libertymutual.production.utils.ObjectUtil;

import com.github.pagehelper.PageInfo;
import com.xiaoleilu.hutool.util.StrUtil;

/**
 * 事务管理注意事项：
 * 1. 新增/修改操作必须启用事务
 * 2. 方法体内部抛出异常即可启用事务
 * @author Steven.Li
 * @date 2017年7月28日
 *
 */
@Service
@Transactional(rollbackFor = Exception.class, transactionManager = "transactionManagerCoreData")
public class KindBusinessServiceImpl extends KindBusinessService {

	@Override
	public Response findPrpdKindLibrary(
			PrpdKindLibraryRequest prpdKindLibraryRequest) {
		Response result = new Response();
		try {
			PageInfo<PrpdkindlibraryWithBLOBs> pageInfo = prpdKindLibraryService
					.findPrpdKindLibrary(prpdKindLibraryRequest);
			result.setTotal(pageInfo.getTotal());
			result.setResult(pageInfo.getList());
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}


	@Override
	public Response findPrpdItemLibrary(
			PrpdItemLibraryRequest prpdItemLibraryRequest) {
		Response result = new Response();
		try {
			PageInfo<PrpdItemRiskLibrary> pageInfo = prpdItemLibraryService
					.findPrpdItemWithLinked(prpdItemLibraryRequest);
			result.setTotal(pageInfo.getTotal());
			result.setResult(pageInfo.getList());
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	@Override
	public Response insertPrpdKindLibrary(
			PrpdKindLibraryRequest prpdKindLibraryRequest) throws Exception {

		Response result = new Response();

		PrpdkindlibraryWithBLOBs record = new PrpdkindlibraryWithBLOBs();
		record.setKindcode(prpdKindLibraryRequest.getKindcode());
		record.setKindversion(prpdKindLibraryRequest.getKindversion());
		record.setStartdate(prpdKindLibraryRequest.getStartdate());
		record.setEnddate(prpdKindLibraryRequest.getEnddate());
		record.setKindcname(prpdKindLibraryRequest.getKindcname());
		record.setKindename(prpdKindLibraryRequest.getKindename());
		record.setClausecode(prpdKindLibraryRequest.getClausecode());
		record.setCalculateflag(prpdKindLibraryRequest.getCalculateflag());
		record.setNewkindcode(prpdKindLibraryRequest.getKindcode());
		record.setValidstatus(prpdKindLibraryRequest.getValidstatus());
		record.setFlag(prpdKindLibraryRequest.getFlag());
		record.setOwnerriskcode(prpdKindLibraryRequest.getOwnerriskcode());
		record.setId(prpdKindLibraryRequest.getId());
		record.setEfilestatus(prpdKindLibraryRequest.getEfilestatus());
		record.setKindheight(prpdKindLibraryRequest.getKindheight());
		record.setKindwidth(prpdKindLibraryRequest.getKindwidth());
		record.setWaysofcalc(prpdKindLibraryRequest.getWaysofcalc());
		record.setIlogflag(prpdKindLibraryRequest.getIlogflag());
		record.setAgreementrate(prpdKindLibraryRequest.getAgreementrate());
		record.setPluskindcode(prpdKindLibraryRequest.getPluskindcode());
		record.setPlusrate(prpdKindLibraryRequest.getPlusrate());
		String powagentCodes = prpdKindLibraryRequest.getPowagentcode();
		if(!StringUtils.isEmpty(powagentCodes)) {
			String[] powagentCodeList = powagentCodes.split(",");
			powagentCodes = "";
			for (String powagentCode : powagentCodeList) {
				if (!"ALL".equals(powagentCode)) {
					powagentCodes += powagentCode + ",";
				}
			}
			if (StringUtils.isEmpty(powagentCodes)) {
				powagentCodes = null;
			} else {
				powagentCodes = powagentCodes.substring(0, powagentCodes.length() - 1);
			}
		}
		record.setPowagentcode(powagentCodes);
		record.setRemark(prpdKindLibraryRequest.getRemark());
		record.setShortratetype(prpdKindLibraryRequest.getShortratetype());
		//如果新增条款为有效，则将条款曾经有效标识设置为1
		if("1".equals(prpdKindLibraryRequest.getValidstatus())) {
			record.setValidflag("1");
		}
		try {
			prpdKindLibraryService.insert(record);

			String classCode = prpdKindLibraryRequest.getOwnerriskcode()
					.substring(0, 2);
			String serialType = prpdKindLibraryRequest.getKindcode().substring(
					2, 3);
			int serialNo = Integer.parseInt(prpdKindLibraryRequest
					.getKindcode().substring(3));
			updateNewKindSerialno(BUSINESSTYPE.kind, classCode, serialType,
					serialNo);

			systemLog.save(BUSINESSLOGTYPE.KIND, OPERTYPE.INSERT,
					record.getKindcode(), "新增条款:" + ObjectUtil.info(record));

		} catch (Exception e) {
			log.error("系统异常错误！", e);
			throw e;
		}
		return result;
	}

	@Override
	public Response getNewKindSerialno(String ownerriskcode, int kindType)
			throws Exception {

		Response result = new Response();

		String newSerialNo = "";
		String classCode = ownerriskcode.substring(0, 2);
		String serialType = kindType == 1 ? "M" : "S";
		List<Prpdserialno> list = prpdSerialNoService.findPrpdSerialno(
				BUSINESSTYPE.kind.toString(), classCode, serialType);
		try {
			if (list.isEmpty()) {
				newSerialNo = "00001";
				prpdSerialNoService.insertPrpdSerialno(
						BUSINESSTYPE.kind.toString(), classCode, serialType, 1);
			} else {
				Integer dbSerialNo = list.get(0).getSerialno();
				newSerialNo = StrUtil.padPre(dbSerialNo.toString(), 5, '0');
			}
			result.setResult(classCode + serialType + newSerialNo);
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			throw e;
		}
		return result;
	}

	private void updateNewKindSerialno(BUSINESSTYPE businessType,
									   String classCode, String serialType, int serialNo) {
		prpdSerialNoService.updatePrpdSerialno(businessType.toString(),
				classCode, serialType, serialNo + 1);
	}

	@Override
	public Response updatePrpdKindLibrary(
			PrpdKindLibraryRequest prpdKindLibraryRequest) throws Exception {

		Response result = new Response();

		List<PrpdkindlibraryWithBLOBs> fetchKinds = prpdKindLibraryService.fetchKinds(
				prpdKindLibraryRequest.getKindcode(),
				prpdKindLibraryRequest.getKindversion(), null);
		PrpdkindlibraryWithBLOBs record = new PrpdkindlibraryWithBLOBs();
		try {
			//判断是否为升级版本
			if (fetchKinds != null && !fetchKinds.isEmpty()) {
				record.setKindcode(prpdKindLibraryRequest.getKindcode());
				record.setKindversion(prpdKindLibraryRequest.getKindversion());
				record.setStartdate(prpdKindLibraryRequest.getStartdate());
				record.setEnddate(prpdKindLibraryRequest.getEnddate());
				record.setKindcname(prpdKindLibraryRequest.getKindcname());
				record.setKindename(prpdKindLibraryRequest.getKindename());
				record.setClausecode(prpdKindLibraryRequest.getClausecode());
				record.setCalculateflag(prpdKindLibraryRequest
						.getCalculateflag());
				record.setNewkindcode(prpdKindLibraryRequest.getKindcode());
				record.setValidstatus(prpdKindLibraryRequest.getValidstatus());
				record.setFlag(prpdKindLibraryRequest.getFlag());
				record.setOwnerriskcode(prpdKindLibraryRequest
						.getOwnerriskcode());
				record.setEfilestatus(prpdKindLibraryRequest.getEfilestatus());
				record.setWaysofcalc(prpdKindLibraryRequest.getWaysofcalc());
				record.setIlogflag(prpdKindLibraryRequest.getIlogflag());
				record.setAgreementrate(prpdKindLibraryRequest
						.getAgreementrate());
				record.setPluskindcode(prpdKindLibraryRequest.getPluskindcode());
				record.setPlusrate(prpdKindLibraryRequest.getPlusrate());
				String powagentCodes = prpdKindLibraryRequest.getPowagentcode();
				if(!StringUtils.isEmpty(powagentCodes)) {
					String[] powagentCodeList = powagentCodes.split(",");
					powagentCodes = "";
					for (String powagentCode : powagentCodeList) {
						if (!"ALL".equals(powagentCode)) {
							powagentCodes += powagentCode + ",";
						}
					}
					if (StringUtils.isEmpty(powagentCodes)) {
						powagentCodes = null;
					} else {
						powagentCodes = powagentCodes.substring(0, powagentCodes.length() - 1);
					}
				}
				record.setPowagentcode(powagentCodes);
				record.setRemark(prpdKindLibraryRequest.getRemark());
				record.setShortratetype(prpdKindLibraryRequest.getShortratetype());
				record.setUpdatedate(new Date());
				//如果修改条款为有效，则将条款曾经有效标识设置为1
				if("1".equals(prpdKindLibraryRequest.getValidstatus())) {
					record.setValidflag("1");
				}
				prpdKindLibraryService.update(record);

				//修改对应关联表的相关信息
//				if("1".equals(record.getValidflag())) {
					//修改prpdkind表
					Prpdkind prpdKind = new Prpdkind();
					prpdKind.setKindcode(record.getKindcode());
					prpdKind.setKindversion(record.getKindversion());
					prpdKind.setKindcname(record.getKindcname());
					prpdKind.setKindename(record.getKindename());
					prpdKind.setCalculateflag(record.getCalculateflag());
					prpdKind.setStartdate(record.getStartdate());
					prpdKind.setEnddate(record.getEnddate());
					prpdKindService.update(prpdKind);

					//修改prpdration表
					Prpdration prpdRation = new Prpdration();
					prpdRation.setKindcode(record.getKindcode());
					prpdRation.setKindversion(record.getKindversion());
					prpdRation.setKindcname(record.getKindcname());
					prpdRation.setKindename(record.getKindename());
					prpdRation.setStartdate(record.getStartdate());
					prpdRation.setEnddate(record.getEnddate());
					prpdrationService.update(prpdRation);
//				}

				// 注销关联的方案
				if ("0".equals(prpdKindLibraryRequest.getValidstatus())) {
					inActivePlan(record.getKindcode(), record.getKindversion());
				}

				// ********************************************************************************
				// 条款基础数据修改日志
				String beforeContent = ObjectUtil.infoAfterCompared(fetchKinds.get(0), record);
				String afterContent = ObjectUtil.infoAfterCompared(record,fetchKinds.get(0));
				if(!"".equals(afterContent) || !"".equals(beforeContent)) {
					systemLog.save(
							BUSINESSLOGTYPE.KIND,
							OPERTYPE.UPDATE,
							record.getKindcode(),
							"条款-修改前:"
									+ beforeContent
									+ ";"
									+ "条款-修改后:"
									+ afterContent);
				}
				// ********************************************************************************

			} else {
				//升级条款版本
				record.setKindcode(prpdKindLibraryRequest.getKindcode());
				String strKindVersion = prpdKindLibraryRequest.getKindversion();
				String beforeKindVersion = (Float.parseFloat(strKindVersion) - 1.0)
						+ "";
				record.setKindversion(beforeKindVersion);
				record.setValidstatus("0");
				prpdKindLibraryService.update(record);

				// 注销关联的方案
//				if ("0".equals(prpdKindLibraryRequest.getValidstatus())) {
				inActivePlan(record.getKindcode(), record.getKindversion());
//				}

				record.setKindcode(prpdKindLibraryRequest.getKindcode());
				record.setKindversion(prpdKindLibraryRequest.getKindversion());
				record.setStartdate(prpdKindLibraryRequest.getStartdate());
				record.setEnddate(prpdKindLibraryRequest.getEnddate());
				record.setKindcname(prpdKindLibraryRequest.getKindcname());
				record.setKindename(prpdKindLibraryRequest.getKindename());
				record.setClausecode(prpdKindLibraryRequest.getClausecode());
				record.setCalculateflag(prpdKindLibraryRequest
						.getCalculateflag());
				record.setNewkindcode(prpdKindLibraryRequest.getKindcode());
				record.setValidstatus(prpdKindLibraryRequest.getValidstatus());
				record.setFlag(prpdKindLibraryRequest.getFlag());
				record.setOwnerriskcode(prpdKindLibraryRequest
						.getOwnerriskcode());
				record.setId(prpdKindLibraryRequest.getId());
				//升级条款版本需升级配置的备案号，故这里将是否配置备案号设置为N
				record.setEfilestatus("N");
				record.setKindheight(prpdKindLibraryRequest.getKindheight());
				record.setKindwidth(prpdKindLibraryRequest.getKindwidth());
				record.setWaysofcalc(prpdKindLibraryRequest.getWaysofcalc());
				record.setIlogflag(prpdKindLibraryRequest.getIlogflag());
				record.setAgreementrate(prpdKindLibraryRequest
						.getAgreementrate());
				record.setPluskindcode(prpdKindLibraryRequest.getPluskindcode());
				record.setPlusrate(prpdKindLibraryRequest.getPlusrate());
				record.setPowagentcode(prpdKindLibraryRequest.getPowagentcode());
				record.setRemark(prpdKindLibraryRequest.getRemark());
				record.setShortratetype(prpdKindLibraryRequest.getShortratetype());
				record.setUpdatedate(new Date());

				//如果修改条款为有效，则将条款曾经有效标识设置为1
				if("1".equals(prpdKindLibraryRequest.getValidstatus())) {
					record.setValidflag("1");
				}
				prpdKindLibraryService.insert(record);

				/**************** 关联原条款险种 **********************/
				Prpdkind critera1 = new Prpdkind();
				critera1.setKindcode(prpdKindLibraryRequest.getKindcode());
				critera1.setKindversion(beforeKindVersion);
				List<Prpdkind> risksLinkedBefore = prpdKindService
						.select(critera1);
				if (!risksLinkedBefore.isEmpty()) {
					for (Prpdkind prpdkind : risksLinkedBefore) {
						prpdkind.setKindversion(strKindVersion);
						prpdkind.setEnddate(prpdKindLibraryRequest.getEnddate());
						prpdKindService.insert(prpdkind);
					}
				}
				/**************** 关联原条款险种 End **********************/

				/**************** 关联原条款标的责任 **********************/
				Prpdkinditem critera2 = new Prpdkinditem();
				critera2.setKindcode(prpdKindLibraryRequest.getKindcode());
				critera2.setKindversion(beforeKindVersion);
				List<Prpdkinditem> itemsLinkedBefore = prpdKindItemService
						.select(critera2);
				if (!itemsLinkedBefore.isEmpty()) {
					for (Prpdkinditem prpdkinditem : itemsLinkedBefore) {
						prpdkinditem.setKindversion(strKindVersion);
						prpdKindItemService.insert(prpdkinditem);
					}
				}
				/**************** 关联原条款标的责任 End **********************/

				// ********************************************************************************
				// 条款版本修改日志
				systemLog.save(BUSINESSLOGTYPE.KIND, OPERTYPE.UPDATE,
						record.getKindcode(), "注销原条款及关联方案:{条款代码=\""
								+ prpdKindLibraryRequest.getKindcode()
								+ "\"  |  条款版本=\"" + beforeKindVersion + "\"};"
								+ "添加新条款:" + ObjectUtil.info(record));
				// ********************************************************************************

			}
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			throw e;
		}

		return result;
	}

	private void inActivePlan(String kindcode, String kindversion) {
		// 注销关联方案基础数据Prpdriskplan
		prpdRiskPlanService.inActiveRiskPlan(kindcode, kindversion);

		// 注销关联方案明细数据Prpdration
		prpdrationService.inActiveRation(kindcode, kindversion);
	}

	@Override
	public Response isLastKindVersion(
			PrpdKindLibraryRequest prpdKindLibraryRequest) {

		Response result = new Response();

		String version = (Float.parseFloat(prpdKindLibraryRequest
				.getKindversion()) + 1.0) + "";
		try {
			List<PrpdkindlibraryWithBLOBs> fetchKinds = prpdKindLibraryService
					.fetchKinds(prpdKindLibraryRequest.getKindcode(), version,
							null);
			Map<String, Object> map = new HashMap<String, Object>();
			if (fetchKinds != null && !fetchKinds.isEmpty()) {
				map.put("isLastKindVersion", false);
			} else {
				map.put("isLastKindVersion", true);
			}
			result.setResult(map);
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	@Override
	public Response inActiveKindVersion(List<PrpdKindLibraryRequest> list)
			throws Exception {

		Response result = new Response();
		PrpdkindlibraryWithBLOBs record = new PrpdkindlibraryWithBLOBs();
		Map<String, String> multipleKinds = new HashMap<String, String>();
		for (PrpdKindLibraryRequest prpdKindLibraryRequest : list) {
			String kindCode = prpdKindLibraryRequest.getKindcode();
			String kindVersion = prpdKindLibraryRequest.getKindversion();
			String versionInMap = multipleKinds.get(kindCode);
			// 获取所有条款的最高版本主键
			if (versionInMap == null
					|| Double.valueOf(kindVersion) > Double
					.valueOf(versionInMap)) {
				multipleKinds.put(kindCode, kindVersion);
			}
		}

		try {

			for (Entry<String, String> kindInfo : multipleKinds.entrySet()) {
				record.setKindcode(kindInfo.getKey());
				record.setKindversion(kindInfo.getValue());
				record.setValidstatus("0"); // 注销条款，修改有效标识为 “0”
				prpdKindLibraryService.update(record);

				// 注销关联的方案
				inActivePlan(kindInfo.getKey(), kindInfo.getValue());

				// ********************************************************************************
				// 条款注销日志
				systemLog.save(BUSINESSLOGTYPE.KIND, OPERTYPE.UPDATE,
						record.getKindcode(),
						"注销条款及关联方案:{条款代码=\"" + kindInfo.getKey() + "\"  |  条款版本=\""
								+ kindInfo.getValue() + "\"}");
				// ********************************************************************************
			}
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			throw e;
		}
		return result;
	}

	@Override
	public Response linkRisk(LinkRiskRequest request) throws Exception {

		PrpdKindLibraryRequest kind = request.getKind();
		List<PrpdRiskRequest> risks = request.getSelectedRows();

		Response result = new Response();
		try {
			Prpdkind record = new Prpdkind();
			record.setKindcode(kind.getKindcode());
			record.setKindversion(kind.getKindversion());
			record.setKindcname(kind.getKindcname());
			record.setKindename(kind.getKindename());
			record.setStartdate(kind.getStartdate());
			record.setEnddate(kind.getEnddate());
			record.setNewkindcode(kind.getKindcode());
			record.setFlag(kind.getFlag());
			record.setCalculateflag(kind.getCalculateflag());
			record.setShortratetype(kind.getShortratetype());
			for (PrpdRiskRequest risk : risks) {
				if(!StringUtils.isEmpty(risk.getOwnerriskcode())) {

					////////////////////////////////////////////////////////////////////////////////
					//校验当前险种是否已关联
					Prpdkind criteria = new Prpdkind();
					criteria.setKindcode(kind.getKindcode());
					criteria.setKindversion(kind.getKindversion());
					criteria.setRiskcode(risk.getRiskcode());
					criteria.setRiskversion(risk.getRiskversion());
					criteria.setOwnerriskcode(risk.getOwnerriskcode());
					List<Prpdkind> rs = prpdKindService.select(criteria);
					if (!rs.isEmpty()) {
						throw new UserException("当前条款关联险种【" + risk.getRiskcode() + "】已配置归属险种【" + risk.getOwnerriskcode() + "】,请重新选择归属险种！");
					}
					////////////////////////////////////////////////////////////////////////////////

					record.setOwnerriskcode(risk.getOwnerriskcode());
					record.setPlancode("plan" + risk.getOwnerriskcode());
				} else {
					record.setOwnerriskcode(risk.getRiskcode());
					record.setPlancode("defaultPlan");
				}
				record.setRiskcode(risk.getRiskcode());
				record.setRiskversion(risk.getRiskversion());
				if (!StringUtils.isEmpty(risk.getOwnerflag())) {  //仅2724使用
					record.setExt1(risk.getOwnerflag().substring(0,1));
				} else {
					record.setExt1(null);
				}
				record.setValidstatus("1");
//				record.setShortratetype(request.getShortratetype());
				prpdKindService.insert(record);
			}

			///////////////////////////////////////////////////////////////////////////
			//6666/3098险种特殊处理
			record.setExt1(null);
			PrpdRiskRequest risk6666 = null;
			PrpdRiskRequest risk0602 = null;
			PrpdRiskRequest risk3098 = null;
			for (PrpdRiskRequest risk : risks) {
				if ("6666".equals(risk.getRiskcode())) {
					risk6666 = risk;
				} else if("3098".equals(risk.getRiskcode())){
					risk3098 = risk;
				} else if ("0602".equals(risk.getRiskcode())) {
					risk0602 = risk;
				} else if (risk6666 != null && risk0602 != null && risk3098 != null) {
					break;
				}
			}
			if (risk6666 != null && risk0602 == null) {
				PrpdRiskRequest criteria = new PrpdRiskRequest();
				criteria.setRiskcode("0602");
				criteria.setValidstatus("1");
				Prpdrisk risk_0602 = prpdRiskService.fetchRisks(criteria).get(0);
				Prpdkind prpdkindCriteria = new Prpdkind();
				prpdkindCriteria.setKindcode(kind.getKindcode());
				prpdkindCriteria.setKindversion(kind.getKindversion());
				prpdkindCriteria.setRiskcode(risk_0602.getRiskcode());
				prpdkindCriteria.setRiskversion(risk_0602.getRiskversion());
				List<Prpdkind> rs = prpdKindService.select(prpdkindCriteria);
				if (rs.isEmpty()) {
					record.setOwnerriskcode(risk_0602.getRiskcode());
					record.setRiskcode(risk_0602.getRiskcode());
					record.setRiskversion(risk_0602.getRiskversion());
					record.setPlancode("defaultPlan");
					record.setValidstatus("1");
					prpdKindService.insert(record);
				}
			}
			if (risk3098 == null) {
				PrpdRiskRequest criteria = new PrpdRiskRequest();
				criteria.setRiskcode("3098");
				criteria.setValidstatus("1");
				Prpdrisk risk_3098 = prpdRiskService.fetchRisks(criteria).get(0);
				for(PrpdRiskRequest risk : risks) {
					if ("0".equals(risk.getCompositeflag())) {
						Prpdkind prpdkindCriteria = new Prpdkind();
						prpdkindCriteria.setKindcode(kind.getKindcode());
						prpdkindCriteria.setKindversion(kind.getKindversion());
						prpdkindCriteria.setRiskcode(risk_3098.getRiskcode());
						prpdkindCriteria.setRiskversion(risk_3098.getRiskversion());
						prpdkindCriteria.setOwnerriskcode(risk.getRiskcode());
						List<Prpdkind> rs = prpdKindService.select(prpdkindCriteria);
						if (rs.isEmpty()) {
							record.setOwnerriskcode(risk.getRiskcode());
							record.setRiskcode(risk_3098.getRiskcode());
							record.setRiskversion(risk_3098.getRiskversion());
							record.setPlancode("plan" + risk.getRiskcode());
							record.setValidstatus("1");
							prpdKindService.insert(record);
						}
					}
				}
				if (risk6666 != null && risk0602 == null) {
					criteria = new PrpdRiskRequest();
					criteria.setRiskcode("0602");
					criteria.setValidstatus("1");
					Prpdrisk risk_0602 = prpdRiskService.fetchRisks(criteria).get(0);
					Prpdkind prpdkindCriteria = new Prpdkind();
					prpdkindCriteria.setKindcode(kind.getKindcode());
					prpdkindCriteria.setKindversion(kind.getKindversion());
					prpdkindCriteria.setRiskcode(risk_3098.getRiskcode());
					prpdkindCriteria.setRiskversion(risk_3098.getRiskversion());
					prpdkindCriteria.setOwnerriskcode(risk_0602.getRiskcode());
					List<Prpdkind> rs = prpdKindService.select(prpdkindCriteria);
					if (rs.isEmpty()) {
						record.setOwnerriskcode(risk_0602.getRiskcode());
						record.setRiskcode(risk_3098.getRiskcode());
						record.setRiskversion(risk_3098.getRiskversion());
						record.setPlancode("plan" + risk_0602.getRiskcode());
						record.setValidstatus("1");
						prpdKindService.insert(record);
					}
				}
			}
			///////////////////////////////////////////////////////////////////////////

		} catch (Exception e) {
			log.error("系统异常错误！", e);
			throw e;
		}
		return result;
	}

	@Override
	public Response linkItem(LinkItemRequest linkItemRequest) throws Exception {
		Response result = new Response();
		Prpdkinditem prpdkinditem = new Prpdkinditem();
		PrpdKindLibraryRequest prpdKindLibraryRequest = linkItemRequest
				.getKind();
		List<PrpdItemLibraryRequest> list = linkItemRequest.getSelectedRows();
		try {
			prpdkinditem.setKindcode(prpdKindLibraryRequest.getKindcode());
			prpdkinditem
					.setKindversion(prpdKindLibraryRequest.getKindversion());
			prpdkinditem.setPlancode("defaultPlan");
			prpdkinditem.setValidstatus("1");
			for (PrpdItemLibraryRequest prpdItemLibraryRequest : list) {
				prpdkinditem.setRiskcode(prpdItemLibraryRequest.getRiskcode());
				prpdkinditem.setRiskversion(prpdItemLibraryRequest
						.getRiskversion());
				prpdkinditem.setItemcode(prpdItemLibraryRequest.getItemcode());
				if (StringUtils.isEmpty(prpdItemLibraryRequest.getOwneritemcode())) {
					prpdkinditem.setFlag(prpdItemLibraryRequest.getItemcode());
				} else {
					prpdkinditem.setFlag(prpdItemLibraryRequest.getOwneritemcode()); //根据业务需求，使用扩展字段FLAG存储父级责任代码
				}
				if (!StringUtils.isEmpty(prpdItemLibraryRequest.getOwnerflag())) {
					prpdkinditem.setExt1(prpdItemLibraryRequest.getOwnerflag().substring(0,1));
				} else {
					prpdkinditem.setExt1(null);
				}
				prpdKindItemService.insert(prpdkinditem);

				///////////////////////////////////////////////////////////////////////////
				//6666险种特殊处理
				prpdkinditem.setExt1(null);
				if ("6666".equals(prpdItemLibraryRequest.getRiskcode())) {
					PrpdRiskRequest where = new PrpdRiskRequest();
					where.setRiskcode("0602");
					where.setValidstatus("1");
					Prpdrisk risk0602 = prpdRiskService.fetchRisks(where).get(0);
					Prpdkinditem criteria = new Prpdkinditem();
					criteria.setKindcode(prpdKindLibraryRequest.getKindcode());
					criteria.setKindversion(prpdKindLibraryRequest.getKindversion());
					criteria.setRiskcode(risk0602.getRiskcode());
					criteria.setRiskversion(risk0602.getRiskversion());
					criteria.setItemcode(prpdItemLibraryRequest.getItemcode());
					List<Prpdkinditem> rs = prpdKindItemService.select(criteria);
					if (rs.isEmpty()) {
						prpdkinditem.setRiskcode(risk0602.getRiskcode());
						prpdkinditem.setRiskversion(risk0602.getRiskversion());
						prpdkinditem.setItemcode(prpdItemLibraryRequest.getItemcode());
						if (StringUtils.isEmpty(prpdItemLibraryRequest.getOwneritemcode())) {
							prpdkinditem.setFlag(prpdItemLibraryRequest.getItemcode());
						} else {
							prpdkinditem.setFlag(prpdItemLibraryRequest.getOwneritemcode());
						}
						prpdKindItemService.insert(prpdkinditem);
					}
				}
				///////////////////////////////////////////////////////////////////////////

			}


		} catch (Exception e) {
			log.error("系统异常错误！", e);
			throw e;
		}
		return result;
	}

	@Override
	public Response insertPrpdItemLibrary(PrpdItemLibraryRequest prpdItemLibraryRequest) throws Exception {
		Response result = new Response();
		PrpditemlibraryWithBLOBs prpditemlibrary = new PrpditemlibraryWithBLOBs();
		try {
			prpditemlibrary.setItemcode(prpdItemLibraryRequest.getItemcode());
			prpditemlibrary.setItemcname(prpdItemLibraryRequest.getItemcname());
			prpditemlibrary.setItemename(prpdItemLibraryRequest.getItemename());
			prpditemlibrary.setItemflag("N");
			prpditemlibrary
					.setNewitemcode(prpdItemLibraryRequest.getItemcode());
			prpditemlibrary.setValidstatus(prpdItemLibraryRequest
					.getValidstatus());
			prpditemlibrary.setFlag(prpdItemLibraryRequest.getFlag());
			prpditemlibrary.setStartdate(prpdItemLibraryRequest.getStartdate());
			prpditemlibrary.setEnddate(prpdItemLibraryRequest.getEnddate());
			prpditemlibrary.setId(prpdItemLibraryRequest.getId());

			//当新增标的/责任时选择了关联险种，则执行关联操作
			List<Prpdrisk> risks = prpdItemLibraryRequest.getRisks();
			if(!risks.isEmpty()) {
				List<PrpditemlibraryWithBLOBs> item = new ArrayList<PrpditemlibraryWithBLOBs>();
				item.add(prpditemlibrary);
				for(Prpdrisk risk : risks) {
					PrpdItemRequest prpdItemRequest = new PrpdItemRequest();
					BeanUtils.copyProperties(prpdItemLibraryRequest, prpdItemRequest);
					prpdItemRequest.setRisk(risk);
					prpdItemRequest.setSelectedRows(item);
					linkItem2Risk(prpdItemRequest);
				}

				///////////////////////////////////////////////////////////////////////////
				//6666险种特殊处理
				Prpdrisk risk6666 = null;
				Prpdrisk risk0602 = null;
				for (Prpdrisk risk : risks) {
					if ("6666".equals(risk.getRiskcode())) {
						risk6666 = risk;
					} else if ("0602".equals(risk.getRiskcode())) {
						risk0602 = risk;
					} else if (risk6666 != null && risk0602 != null) {
						break;
					}
				}
				if (risk6666 != null && risk0602 == null) {
					PrpdRiskRequest criteria = new PrpdRiskRequest();
					criteria.setRiskcode("0602");
					criteria.setValidstatus("1");
					risk0602 = prpdRiskService.fetchRisks(criteria).get(0);
					Prpditem where = new Prpditem();
					where.setRiskcode(risk0602.getRiskcode());
					where.setRiskversion(risk0602.getRiskversion());
					where.setItemcode(prpditemlibrary.getItemcode());
					List<PrpditemWithBLOBs> rs = prpdItemService.select(where);
					if (rs.isEmpty()) {
						PrpditemWithBLOBs record = new PrpditemWithBLOBs();
						record.setRiskcode(risk0602.getRiskcode());
						record.setRiskversion(risk0602.getRiskversion());
						record.setPlancode("defaultPlan");
						record.setItemcode(prpditemlibrary.getItemcode());
						record.setItemcname(prpditemlibrary.getItemcname());
						record.setItemename(StringUtils.isEmpty(prpditemlibrary.getItemename()) ? "" : prpditemlibrary.getItemename());
						record.setItemflag(StringUtils.isEmpty(prpditemlibrary.getItemflag()) ? "" : prpditemlibrary.getItemflag());
						record.setNewitemcode(prpditemlibrary.getItemcode());
						record.setValidstatus("1");
						prpdItemService.insert(record);
					}
				}
				///////////////////////////////////////////////////////////////////////////

			}

			prpdItemLibraryService.insert(prpditemlibrary);

			updateNewKindSerialno(BUSINESSTYPE.item, null, null,
					Integer.parseInt(prpdItemLibraryRequest.getItemcode()));

		} catch (Exception e) {
			log.error("系统异常错误！", e);
			throw e;
		}
		return result;
	}

	@Override
	public Response getNewItemCode() {
		Response result = new Response();

		String newSerialNo = "";
		List<Prpdserialno> list = prpdSerialNoService.findPrpdSerialno(
				BUSINESSTYPE.item.toString(), null, null);
		try {
			if (list.isEmpty()) {
				newSerialNo = "00001";
				prpdSerialNoService.insertPrpdSerialno(
						BUSINESSTYPE.item.toString(), null, null, 1);
			} else {
				Integer dbSerialNo = list.get(0).getSerialno();
				newSerialNo = StrUtil.padPre(dbSerialNo.toString(), 5, '0');
			}
			result.setResult(newSerialNo);
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			throw e;
		}
		return result;
	}

	@Override
	public Response findItemLinked(LinkItemRequest linkItemRequest) {
		Response result = new Response();
		try {
			PageInfo<ItemKind> pageInfo = prpdItemLibraryService.selectWithItemKind(linkItemRequest);
			List<ItemKind> items = pageInfo.getList();
			if (items != null && items.size() > 0) {
				result.setTotal(pageInfo.getTotal());
				result.setResult(items);
			} else {
				result.setTotal(0);
				result.setResult(new ArrayList<Prpditemlibrary>());
			}
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	@Override
	public Response findRiskLinked(LinkRiskRequest linkRiskRequest) {
		Response result = new Response();
		List<String> riskCodes = new ArrayList<String>();
		PageInfo<Prpdrisk> pageInfo = new PageInfo<Prpdrisk>();
		Prpdkind prpdkind = new Prpdkind();
		try {
			prpdkind.setKindcode(linkRiskRequest.getKind().getKindcode());
			prpdkind.setKindversion(linkRiskRequest.getKind().getKindversion());
			List<Prpdkind> kinds = prpdKindService.select(prpdkind);
			for (Prpdkind kind : kinds) {
				riskCodes.add(kind.getRiskcode());
			}
			if (!riskCodes.isEmpty()) {
				pageInfo = prpdRiskService.selectByPage(riskCodes,
						linkRiskRequest);

				List<PrpdkindV1> rs = new ArrayList<PrpdkindV1>();
				PrpdkindV1 kindV1 = null;
				for (Prpdkind kind : kinds) {
					for (Prpdrisk risk: pageInfo.getList()) {
						if (kind.getRiskcode().equals(risk.getRiskcode()) &&
								kind.getRiskversion().equals(risk.getRiskversion())) {
							kindV1 = new PrpdkindV1();
							BeanUtils.copyProperties(kind, kindV1);
							kindV1.setRiskcname(risk.getRiskcname());
							kindV1.setRiskename(risk.getRiskename());
							kindV1.setRiskStartDate(risk.getSalestartdate());
							kindV1.setRiskEndDate(risk.getSaleenddate());
							rs.add(kindV1);
							break;
						}
					}
				}

				result.setTotal(pageInfo.getTotal());
				result.setResult(rs);
			} else {
				result.setTotal(0);
				result.setResult(new ArrayList<Prpdrisk>());
			}
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	@Override
	public Response delItemLinked(LinkItemRequest linkItemRequest)
			throws Exception {
		Response result = new Response();
		Prpdkinditem prpdkinditem = new Prpdkinditem();
		try {
			prpdkinditem.setPlancode("defaultPlan");
			prpdkinditem.setKindcode(linkItemRequest.getKind().getKindcode());
			prpdkinditem.setKindversion(linkItemRequest.getKind()
					.getKindversion());
			List<PrpdItemLibraryRequest> list = linkItemRequest
					.getSelectedRows();
			for (PrpdItemLibraryRequest prpdItemLibraryRequest : list) {
				prpdkinditem.setItemcode(prpdItemLibraryRequest.getItemcode());
				prpdkinditem.setRiskcode(prpdItemLibraryRequest.getRiskcode());
				prpdkinditem.setRiskversion(prpdItemLibraryRequest
						.getRiskversion());
				prpdKindItemService.delete(prpdkinditem);
			}

			// ************************************************************************
			// 如果有方案正在使用该条款及责任，则级联注销方案
			Set<Prpdriskplan> usedPlans = new HashSet<>();
			String kindCode = linkItemRequest.getKind().getKindcode();
			String kindVersion = linkItemRequest.getKind().getKindversion();
			for (PrpdItemLibraryRequest riskItem : linkItemRequest
					.getSelectedRows()) {
				List<Prpdriskplan> rs = prpdRiskPlanService
						.checkItemLinkedDeletable(kindCode, kindVersion,
								riskItem.getItemcode());
				if (!rs.isEmpty()) {
					usedPlans.addAll(rs);
				}
			}
			if (!usedPlans.isEmpty()) { // 注销方案
				for (Prpdriskplan plan : usedPlans) {
					PrpdriskplanWithBLOBs riskPlan = new PrpdriskplanWithBLOBs();
					riskPlan.setPlancode(plan.getPlancode());
					riskPlan.setRiskcode(plan.getRiskcode());
					riskPlan.setRiskversion(plan.getRiskversion());
					riskPlan.setValidstatus("0");
					prpdRiskPlanService.update(riskPlan);

					Prpdration ration = new Prpdration();
					ration.setPlancode(plan.getPlancode());
					ration.setRiskcode(plan.getRiskcode());
					ration.setRiskversion(plan.getRiskversion());
					ration.setKindcode(linkItemRequest.getKind().getKindcode());
					ration.setKindversion(linkItemRequest.getKind()
							.getKindversion());
					ration.setValidstatus("0");
					for (PrpdItemLibraryRequest riskItem : linkItemRequest
							.getSelectedRows()) {
						ration.setItemcode(riskItem.getItemcode());
						prpdrationService.update(ration);
					}
				}
			}
			// ************************************************************************

		} catch (Exception e) {
			log.error("系统异常错误！", e);
			throw e;
		}
		return result;
	}

	@Override
	public Response delRiskLinked(LinkRiskRequest linkRiskRequest)
			throws Exception {
		Response result = new Response();
		Prpdkind prpdkind = new Prpdkind();
		try {
			// 删除条款-险种关联表数据
			prpdkind.setKindcode(linkRiskRequest.getKind().getKindcode());
			prpdkind.setKindversion(linkRiskRequest.getKind().getKindversion());
			List<PrpdRiskRequest> list = linkRiskRequest.getSelectedRows();
			for (PrpdRiskRequest prpdRiskRequest : list) {
				prpdkind.setPlancode(prpdRiskRequest.getPlancode());
				prpdkind.setRiskcode(prpdRiskRequest.getRiskcode());
				prpdkind.setRiskversion(prpdRiskRequest.getRiskversion());
				prpdKindService.delete(prpdkind);
			}
			// 删除条款-险种-标的/责任关联表数据
			Prpdkinditem prpdkinditem = new Prpdkinditem();
			prpdkinditem.setKindcode(linkRiskRequest.getKind().getKindcode());
			prpdkinditem.setKindversion(linkRiskRequest.getKind()
					.getKindversion());
			prpdkinditem.setPlancode("defaultPlan");
			for (PrpdRiskRequest prpdRiskRequest : list) {
				prpdkinditem.setRiskcode(prpdRiskRequest.getRiskcode());
				prpdkinditem.setRiskversion(prpdRiskRequest.getRiskversion());
				prpdKindItemService.delete(prpdkinditem);
			}

			// ************************************************************************
			// 如果有方案正在使用该条款及险种，则级联注销方案
			Set<Prpdriskplan> usedPlans = new HashSet<>();
			String kindCode = linkRiskRequest.getKind().getKindcode();
			for (PrpdRiskRequest risk : linkRiskRequest.getSelectedRows()) {
				List<Prpdriskplan> rs = prpdRiskPlanService
						.checkRiskLinkedDeletable(kindCode, risk.getRiskcode(),
								risk.getRiskversion());
				if (!rs.isEmpty()) {
					usedPlans.addAll(rs);
				}
			}
			if (!usedPlans.isEmpty()) { // 注销方案
				for (Prpdriskplan plan : usedPlans) {
					PrpdriskplanWithBLOBs riskPlan = new PrpdriskplanWithBLOBs();
					riskPlan.setPlancode(plan.getPlancode());
					riskPlan.setRiskcode(plan.getRiskcode());
					riskPlan.setRiskversion(plan.getRiskversion());
					riskPlan.setValidstatus("0");
					prpdRiskPlanService.update(riskPlan);

					Prpdration criteria = new Prpdration();
					criteria.setPlancode(plan.getPlancode());
					criteria.setRiskcode(plan.getRiskcode());
					criteria.setRiskversion(plan.getRiskversion());
					criteria.setKindcode(linkRiskRequest.getKind()
							.getKindcode());
					criteria.setKindversion(linkRiskRequest.getKind()
							.getKindversion());
					Prpdration record = new Prpdration();
					record.setValidstatus("0");
					prpdrationService.updateByCriteria(record, criteria);
				}
			}
			// ************************************************************************
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			throw e;
		}
		return result;
	}

	@Override
	public Response checkRiskLinked(
			PrpdKindLibraryRequest prpdKindLibraryRequest) {
		Response result = new Response();
		Prpdkind prpdkind = new Prpdkind();
		try {
			prpdkind.setKindcode(prpdKindLibraryRequest.getKindcode());
			prpdkind.setKindversion(prpdKindLibraryRequest.getKindversion());
//			prpdkind.setPlancode("defaultPlan");
			List<Prpdkind> list = prpdKindService.select(prpdkind);
			if (!list.isEmpty()) {
				result.setResult(true);
			} else {
				result.setResult(false);
			}
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	@Override
	public Response checkItemLinked(
			PrpdKindLibraryRequest prpdKindLibraryRequest) {
		Response result = new Response();
		try {
			result.setResult(prpdKindService
					.checkItemLinked(prpdKindLibraryRequest.getKindcode()));
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	@Override
	public Response findItemNotRiskLinked(PrpdItemRequest prpdItemRequest) {
		Response result = new Response();
		try {
			PageInfo<PrpditemlibraryWithBLOBs> pageInfo = prpdItemLibraryService
					.select(prpdItemRequest);
			result.setTotal(pageInfo.getTotal());
			result.setResult(pageInfo.getList());
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	@Override
	public Response linkItem2Risk(PrpdItemRequest prpdItemRequest)
			throws Exception {
		Response result = new Response();
		PrpditemWithBLOBs record = new PrpditemWithBLOBs();
		try {
			record.setRiskcode(prpdItemRequest.getRisk().getRiskcode());
			record.setRiskversion(prpdItemRequest.getRisk().getRiskversion());
			record.setPlancode("defaultPlan");
			List<PrpditemlibraryWithBLOBs> items = prpdItemRequest.getSelectedRows();
			for (PrpditemlibraryWithBLOBs item : items) {
				record.setItemcode(item.getItemcode());
				record.setItemcname(item.getItemcname());
				record.setItemename(StringUtils.isEmpty(item.getItemename()) ? ""
						: item.getItemename());
				record.setItemflag(StringUtils.isEmpty(item.getItemflag()) ? ""
						: item.getItemflag());
				record.setNewitemcode(item.getItemcode());
				record.setValidstatus("1");
				prpdItemService.insert(record);
			}

			///////////////////////////////////////////////////////////////////////////
			//6666险种特殊处理
			if ("6666".equals(prpdItemRequest.getRisk().getRiskcode())) {
				PrpdRiskRequest criteria = new PrpdRiskRequest();
				criteria.setRiskcode("0602");
				criteria.setValidstatus("1");
				Prpdrisk risk0602 = prpdRiskService.fetchRisks(criteria).get(0);
				record.setRiskcode(risk0602.getRiskcode());
				record.setRiskversion(risk0602.getRiskversion());
				for (PrpditemlibraryWithBLOBs item : items) {
					Prpditem where = new Prpditem();
					where.setRiskcode(prpdItemRequest.getRisk().getRiskcode());
					where.setRiskversion(prpdItemRequest.getRisk().getRiskversion());
					where.setItemcode(item.getItemcode());
					List<PrpditemWithBLOBs> rs = prpdItemService.select(where);
					if (rs.isEmpty()) {
						record.setItemcode(item.getItemcode());
						record.setItemcname(item.getItemcname());
						record.setItemename(StringUtils.isEmpty(item.getItemename()) ? ""
								: item.getItemename());
						record.setItemflag(StringUtils.isEmpty(item.getItemflag()) ? ""
								: item.getItemflag());
						record.setNewitemcode(item.getItemcode());
						record.setValidstatus("1");
						prpdItemService.insert(record);
					}
				}
			}
			///////////////////////////////////////////////////////////////////////////

		} catch (Exception e) {
			log.error("系统异常错误！", e);
			throw e;
		}
		return result;
	}

	@Override
	public Response checkItemLinkedDeletable(LinkItemRequest request) {
		Response result = new Response();

		try {
			// *******************************************************************
			// 校验是否有方案正在使用该条款及责任
			Set<String> usedPlans = new HashSet<String>();
			StringBuffer errorMsg = new StringBuffer();
			String kindCode = request.getKind().getKindcode();
			String kindVersion = request.getKind().getKindversion();
			for (PrpdItemLibraryRequest riskItem : request.getSelectedRows()) {
				List<Prpdriskplan> rs = prpdRiskPlanService
						.checkItemLinkedDeletable(kindCode, kindVersion,
								riskItem.getItemcode());
				if (!rs.isEmpty()) {
					for (Prpdriskplan prpdriskplan : rs) {
						usedPlans.add(prpdriskplan.getPlancode());
						errorMsg.append(prpdriskplan.getPlancode() + ",");
					}
				}
			}
			if (!usedPlans.isEmpty()) {
				result.setAppFail();
				result.setResult("方案【"
						+ errorMsg.substring(0, errorMsg.length() - 1)
						+ "】正在使用该条款及责任！若继续，方案将被注销！");
				return result;
			}
			// *******************************************************************

			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	@Override
	public Response checkRiskLinkedDeletable(LinkRiskRequest linkRiskRequest) {
		Response result = new Response();

		try {
			// *******************************************************************
			// 校验是否有方案正在使用该条款及险种
			Set<String> usedPlans = new HashSet<String>();
			StringBuffer errorMsg = new StringBuffer();
			String kindCode = linkRiskRequest.getKind().getKindcode();
			for (PrpdRiskRequest risk : linkRiskRequest.getSelectedRows()) {
				List<Prpdriskplan> rs = prpdRiskPlanService
						.checkRiskLinkedDeletable(kindCode, risk.getRiskcode(),
								risk.getRiskversion());
				if (!rs.isEmpty()) {
					for (Prpdriskplan prpdriskplan : rs) {
						usedPlans.add(prpdriskplan.getPlancode());
						errorMsg.append(prpdriskplan.getPlancode() + ",");
					}
				}
			}
			if (!usedPlans.isEmpty()) {
				result.setAppFail();
				result.setResult("方案【"
						+ errorMsg.substring(0, errorMsg.length() - 1)
						+ "】正在使用该条款及险种！若继续，方案将被注销！");
				return result;
			}
			// *******************************************************************

			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	@Override
	public Response checkKindDeletable(List<PrpdKindLibraryRequest> list) {
		Response result = new Response();

		Map<String, String> multipleKinds = new HashMap<String, String>();
		for (PrpdKindLibraryRequest prpdKindLibraryRequest : list) {
			String kindCode = prpdKindLibraryRequest.getKindcode();
			String kindVersion = prpdKindLibraryRequest.getKindversion();
			String versionInMap = multipleKinds.get(kindCode);
			// 获取所有条款的最高版本主键
			if (versionInMap == null
					|| Double.valueOf(kindVersion) > Double
					.valueOf(versionInMap)) {
				multipleKinds.put(kindCode, kindVersion);
			}
		}

		try {
			// *******************************************************************
			// 校验是否有方案正在使用该条款
			StringBuffer errorMsg = new StringBuffer();
			Set<String> uniquePlanCodes = new HashSet<String>();
			for (Entry<String, String> kindInfo : multipleKinds.entrySet()) {
				List<String> planCodes = prpdRiskPlanService.fetchPlancodesKindUsed(kindInfo.getKey(), kindInfo.getValue());
				for (String planCode : planCodes) {
					uniquePlanCodes.add(planCode);
				}
			}

			if (!uniquePlanCodes.isEmpty()) {
				for (String planCode : uniquePlanCodes) {
					errorMsg.append(planCode + ",");
				}
				result.setAppFail();
				result.setResult("方案【" + errorMsg.substring(0, errorMsg.length() - 1) + "】正在使用该条款！若继续，则方案将被注销！");
				return result;
			}
			// *******************************************************************

			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	@Override
	public Response findAllShortRate() {
		Response result = new Response();
		try {
			List<Prpdshortratelibrary> shortRate = prpdshortratelibraryService
					.findAll();
			result.setResult(shortRate);
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

	@Override
	public Response findByOwnerRisk(PrpdKindLibraryRequest request) {
		Response result = new Response();
		try {
			PageInfo<Prpdkindlibrary> pageInfo = prpdKindLibraryService.findByOwnerRisk(request);
			result.setTotal(pageInfo.getTotal());
			result.setResult(pageInfo.getList());
			result.setSuccess();
		} catch (Exception e) {
			log.error("系统异常错误！", e);
			result.setAppFail();
			result.setResult(e.getMessage());
		}
		return result;
	}

}
