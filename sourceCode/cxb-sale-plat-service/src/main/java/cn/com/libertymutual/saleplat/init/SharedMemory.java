package cn.com.libertymutual.saleplat.init;

import java.io.IOException;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import cn.com.libertymutual.sp.service.api.InitService;
import cn.com.libertymutual.sys.nio.ThreadPoolFileServer;
import cn.com.libertymutual.sys.service.api.IInitSharedMemory;


@Component
public class SharedMemory implements ApplicationListener<ContextRefreshedEvent>{

	@Resource IInitSharedMemory initSharedMemory;
	@Resource InitService initService;

	//@Resource private RedisUtils redisUtils;
	
	@Value("${file.server.port:19999}")
	private int portNo;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		
		//初始化Task执行标志，bob.kuang 20180207
		///redisUtils.set(Constants.TIMER_FLAG, 0l);使用Quartz，屏蔽此种方式
		
		initSharedMemory.initData();
		initService.initData();
		
		/*initSharedMemory.initServiceInfo();
		initSharedMemory.initRiskInfo() ;
		initSharedMemory.initCodeInto();
		initSharedMemory.areaSortIntoRedis();
		initSharedMemory.kindIntoReids();
		initSharedMemory.basePremiumIntoReids();
		initSharedMemory.baseTaxIntoReids();
		initSharedMemory.initDepreciationConfig();
		initSharedMemory.initPredefineReids();
		initSharedMemory.initAccountReids();
		
		initSharedMemory.initAgreementConfReids();
		initSharedMemory.initSysPayTypeReids();
		initSharedMemory.initSysMenu();*/
		
		
		/*System.out.println("启动NettyServer start... ...");
        Thread thread = new Thread(new FileNettyServer( portNo ), "文件传输服务" );
        thread.start();
        System.out.println("启动NettyServer end... ...");*/
        
        /**  20180129 采用NIO的方式传输 **/
		new Thread( new Runnable() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				ThreadPoolFileServer server = new ThreadPoolFileServer();
				try {
					server.startServer( portNo );
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}, "文件传输服务" ).start();
		
		
	}
}
