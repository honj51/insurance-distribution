package cn.com.libertymutual.sp.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.annotation.SystemValidate;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.service.api.ApplicantService;

@RestController
@RequestMapping(value = "/nol/application")
public class ApplicantController {
	private Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	private ApplicantService applicantService;

	@PostMapping(value = "/findIdCar")
	@SystemValidate(validate = false, description = "无需校验接口")
	public ServiceResult insure(String idCar) {
		return applicantService.findAppliIdCar(idCar);
	}

}
