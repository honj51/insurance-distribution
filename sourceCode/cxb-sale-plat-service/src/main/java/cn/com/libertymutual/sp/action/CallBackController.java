package cn.com.libertymutual.sp.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.annotation.SystemValidate;
import cn.com.libertymutual.sp.dao.OrderDao;
import cn.com.libertymutual.sp.dto.CallBackRequestDto;
import cn.com.libertymutual.sp.service.api.PayService;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/admin-cxb")
public class CallBackController {
	private Logger log = LoggerFactory.getLogger(getClass());
	private final String SUCCESS = "success";

	@Autowired
	private OrderDao orderDao;
	@Autowired
	private PayService payService;

	@ApiOperation(value = "回调", notes = "回调")
	@PostMapping(value = "/callback")
	@SystemValidate(validate = false, description = "无需校验接口")
	public String callback(@RequestBody CallBackRequestDto cbrd) {

		payService.callbackUpdateOrder(cbrd);
		return SUCCESS;
	}

}
