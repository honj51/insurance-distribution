package cn.com.libertymutual.sp.action;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import cn.com.libertymutual.core.util.Current;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.service.api.ScoreService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/nol/integral")
public class ScoreController {
	@Autowired
	private ScoreService scoreService;

	/**
	 * 我的积分
	 */
	@ApiOperation(value = "我的积分", notes = "我的积分")
	@PostMapping(value = "/myIntegral")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "userCode", value = "用户编码", required = true, paramType = "query", dataType = "String"), })
	public ServiceResult myScore(String userCode) {
		ServiceResult sr = new ServiceResult();

		sr = scoreService.myScore(Current.userCode.get());
		return sr;
	}

	/**
	 * 积分收支记录
	 */

	@ApiOperation(value = "积分收支明细", notes = "积分收支明细")
	@PostMapping(value = "/integralInOrOutList")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "userCode", value = "用户编码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "type", value = "类型", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "条数", required = true, paramType = "query", dataType = "Long"), })
	public ServiceResult scoreDetail(String userCode, String type, int pageNumber, int pageSize) {
		ServiceResult sr = new ServiceResult();

		sr = scoreService.scoreDetail(Current.userCode.get(), type, pageNumber, pageSize);
		return sr;
	}

	/**
	 * 积分详情
	 * 
	 * @param userCode
	 * @param score
	 * @return
	 */
	@ApiOperation(value = "积分详情", notes = "积分详情")
	@PostMapping(value = "/findIntegralOne")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "type", value = "收支类型", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "id", value = "id", required = true, paramType = "query", dataType = "Long") })
	public ServiceResult findIntegralDetail(String type, Integer id) {
		ServiceResult sr = new ServiceResult();

		sr = scoreService.findIntegralDetail(type, id);
		return sr;
	}

	/**
	 * 积分兑换提现
	 * 
	 * @param userCode
	 * @param score
	 * @return
	 */
	@ApiOperation(value = "积分兑换提现", notes = "积分兑换提现")
	@PostMapping(value = "/integralCash")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "userCode", value = "用户编码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "score", value = "积分", required = true, paramType = "query", dataType = "Double"),
			@ApiImplicitParam(name = "branchCode", value = "机构代码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "bankNumber", value = "银行卡号", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "bankId", value = "银行卡Id", required = true, paramType = "query", dataType = "Integer") })
	public ServiceResult cash(String userCode, double score, String branchCode, String bankNumber, Integer bankId) {
		ServiceResult sr = new ServiceResult();

		sr = scoreService.cash(Current.userCode.get(), score, branchCode, bankNumber, bankId);
		return sr;
	}
	/**
	 * 积分兑换提现——补录
	 * 
	 * @param userCode
	 * @param score
	 * @return
	 */
	/*
	 * @ApiOperation(value = "积分兑换提现——补录", notes = "积分兑换提现——补录")
	 * 
	 * @PostMapping(value = "/integralCashPatch")
	 * 
	 * @ApiImplicitParams(value = {
	 * 
	 * @ApiImplicitParam(name = "businessNo", value = "补录的业务单号", required = true,
	 * paramType = "query", dataType = "String"),
	 * 
	 * @ApiImplicitParam(name = "branchCode", value = "机构代码", required = false,
	 * paramType = "query", dataType = "String"),
	 * 
	 * @ApiImplicitParam(name = "bankId", value = "银行卡Id", required = false,
	 * paramType = "query", dataType = "Integer") }) public ServiceResult
	 * cashPatch(String businessNo,String branchCode, Integer bankId) {
	 * ServiceResult sr = new ServiceResult();
	 * 
	 * sr = scoreService.cashPatch( businessNo, branchCode, bankId); return sr; }
	 */

	/**
	 * 积分兑换记录
	 */

	@ApiOperation(value = "积分兑换记录", notes = "积分兑换记录")
	@PostMapping(value = "/findCashAll")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "userCode", value = "用户编码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "每页条数", required = true, paramType = "query", dataType = "Long"), })
	public ServiceResult cashLog(String userCode, int pageNumber, int pageSize) {
		ServiceResult sr = new ServiceResult();

		sr = scoreService.cashLog("",null, null, null, Current.userCode.get(), null, null, pageNumber, pageSize);
		return sr;

	}

	/*
	 * epayment快钱查询
	 */

	@ApiOperation(value = "快钱交易查询", notes = "快钱交易查询")
	@PostMapping(value = "/queryCnp")
	@ApiImplicitParams(value = { @ApiImplicitParam(name = "branchCode", value = "分公司代码", required = true, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "businessNo", value = "业务单号", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "pageNumber", value = "页码:从0开始", required = false, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "pageSize", value = "每页条数", required = false, paramType = "query", dataType = "Long"),
			@ApiImplicitParam(name = "startDate", value = "交易开始时间 (格式:yyyyMMdd)", required = false, paramType = "query", dataType = "String"),
			@ApiImplicitParam(name = "endDate", value = "交易结束时间 (格式:yyyyMMdd)", required = false, paramType = "query", dataType = "String") })
	public ServiceResult queryCnp(String businessNo, String branchCode, int pageNumber, int pageSize, String startDate, String endDate) {
		ServiceResult sr = new ServiceResult();

		if (StringUtils.isNotBlank(startDate)) {
			startDate = startDate.replace("-", "");
		}
		if (StringUtils.isNotBlank(endDate)) {
			endDate = endDate.replace("-", "");
		}
		sr = scoreService.queryCnp(businessNo, branchCode, startDate, endDate, pageNumber, pageSize);
		return sr;

	}

	@ApiOperation(value = "积分配置", notes = "测试")
	@PostMapping(value = "/findScoreConfig")
	public ServiceResult findScoreConfig(String userCode) {
		return scoreService.findScoreConfig();
	}
}
