package cn.com.libertymutual.sp.action;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.bean.TbSpShare;
import cn.com.libertymutual.sp.service.api.ShareService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(value = "/admin/share")
public class ShareControllerWeb {

	
	private Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	private ShareService shareService;
	
	@ApiOperation(value = "分享", notes = "分享")
	@ApiImplicitParams(value = {
		  @ApiImplicitParam(name = "shareId", value = "分享标识", required = false, paramType = "query", dataType = "String"),
		  @ApiImplicitParam(name = "userCode", value = "用户", required = false, paramType = "query", dataType = "String"),
		  @ApiImplicitParam(name = "agrementNo", value = "业务关系代码", required = false, paramType = "query", dataType = "String"),
		  @ApiImplicitParam(name = "startTime", value = "开始时间", required = false, paramType = "query", dataType = "String"),
		  @ApiImplicitParam(name = "endTime", value = "结束时间", required = false, paramType = "query", dataType = "String"),
          @ApiImplicitParam(name = "pageNumber", value = "页码", required = true, paramType = "query", dataType = "Integer"),
          @ApiImplicitParam(name = "pageSize", value = "条数", required = true, paramType = "query", dataType = "Integer")
	})
	@PostMapping(value = "/shareList")
	public ServiceResult shareList(String shareId,String userCode,String agrementNo,String startTime,String endTime,Integer pageNumber,Integer pageSize){
		return shareService.shareList(shareId,userCode,agrementNo,startTime,endTime,pageNumber,pageSize);
	}
	
	
	@ApiOperation(value = "新增分享", notes = "新增分享")
	@PostMapping(value = "/addShare")
	public ServiceResult addShare(@RequestBody @ApiParam(name = "tbSpShare", value = "tbSpShare", required = true)TbSpShare tbSpShare){
		return shareService.addShare(tbSpShare);
	}
	
	
	@ApiOperation(value = "删除分享", notes = "删除分享")
	@ApiImplicitParams(value = {
			  @ApiImplicitParam(name = "id", value = "分享标识", required = false, paramType = "query", dataType = "Long"),
		})
	@PostMapping(value = "/removeShare")
	public ServiceResult removeShare(Integer id){
		return shareService.removeShare(id);
	}
	
	
	
	
	
}
