package cn.com.libertymutual.sp.action.production;

import cn.com.libertymutual.production.pojo.request.PrpdPolicyPayRequest;
import cn.com.libertymutual.production.pojo.response.Response;
import cn.com.libertymutual.production.service.api.business.PolicyPayBusinessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author steven.li
 * @create 2018/1/31
 */
@RestController
@RequestMapping("policypay")
public class PolicyPayController {

    @Autowired
    private PolicyPayBusinessService policyPayBusinessService;

    /**
     * 获取见费出单基本信息
     * @param PrpdPolicyPayRequest
     * @return
     */
    @RequestMapping("queryPolicyPays")
    public Response queryPolicyPays(PrpdPolicyPayRequest prpdPolicyPayRequest) {
        Response result =  policyPayBusinessService.findPolicyPays(prpdPolicyPayRequest);
        return result;
    }

    /**
     * 新增见费出单基本信息
     * @param PrpdPolicyPayRequest
     * @return
     */
    @RequestMapping("insertPrpdPolicyPay")
    public Response insertPrpdPolicyPay(PrpdPolicyPayRequest prpdPolicyPayRequest) {
        Response result = new Response();
        try {
            result =  policyPayBusinessService.insert(prpdPolicyPayRequest);
            result.setSuccess();
        } catch (Exception e) {
            result.setAppFail();
            result.setResult(e.getMessage());
        }
        return result;
    }

    /**
     * 删除见费出单基本信息
     * @param PrpdPolicyPayRequest
     * @return
     */
    @RequestMapping("deletePrpdPolicyPay")
    public Response deletePrpdPolicyPay(@RequestBody PrpdPolicyPayRequest prpdPolicyPayRequest) {
        Response result = new Response();
        try {
            result =  policyPayBusinessService.delete(prpdPolicyPayRequest);
            result.setSuccess();
        } catch (Exception e) {
            result.setAppFail();
            result.setResult(e.getMessage());
        }
        return result;
    }

    /**
     * 修改见费出单基本信息
     * @param PrpdPolicyPayRequest
     * @return
     */
    @RequestMapping("updatePrpdPolicyPay")
    public Response updatePrpdPolicyPay(PrpdPolicyPayRequest prpdPolicyPayRequest) {
        Response result = new Response();
        try {
            result =  policyPayBusinessService.update(prpdPolicyPayRequest);
            result.setSuccess();
        } catch (Exception e) {
            result.setAppFail();
            result.setResult(e.getMessage());
        }
        return result;
    }

}
