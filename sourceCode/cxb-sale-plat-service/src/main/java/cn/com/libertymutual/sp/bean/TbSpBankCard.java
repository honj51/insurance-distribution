package cn.com.libertymutual.sp.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_sp_bank_card", catalog = "sale_plat")
public class TbSpBankCard implements Serializable {

	private static final long serialVersionUID = -3179274832054046642L;
	private Integer id;// ID
	private String bankCode;// 银行简写字母编码
	private String cardLastNo;// 银行卡末尾号码
	private String bankName;// 银行名称
	private String bankNamePinYin;// 银行名称全拼音
	private String cardTypeName;// 银行卡卡种名称
	private String cardTypeNamePinYin;// 银行卡卡种名称全拼音

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "BANK_CODE")
	public String getBankCode() {
		return bankCode;
	}

	public void setBankCode(String bankCode) {
		this.bankCode = bankCode;
	}

	@Column(name = "CARD_LAST_NO")
	public String getCardLastNo() {
		return cardLastNo;
	}

	public void setCardLastNo(String cardLastNo) {
		this.cardLastNo = cardLastNo;
	}

	@Column(name = "BANK_NAME")
	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	@Column(name = "BANK_NAME_PIN_YIN")
	public String getBankNamePinYin() {
		return bankNamePinYin;
	}

	public void setBankNamePinYin(String bankNamePinYin) {
		this.bankNamePinYin = bankNamePinYin;
	}

	@Column(name = "CARD_TYPE_NAME")
	public String getCardTypeName() {
		return cardTypeName;
	}

	public void setCardTypeName(String cardTypeName) {
		this.cardTypeName = cardTypeName;
	}

	@Column(name = "CARD_TYPE_NAME_PIN_YIN")
	public String getCardTypeNamePinYin() {
		return cardTypeNamePinYin;
	}

	public void setCardTypeNamePinYin(String cardTypeNamePinYin) {
		this.cardTypeNamePinYin = cardTypeNamePinYin;
	}

}
