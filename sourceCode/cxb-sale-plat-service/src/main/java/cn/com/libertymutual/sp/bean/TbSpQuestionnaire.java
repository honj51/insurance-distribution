package cn.com.libertymutual.sp.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_sp_questionnaire")
public class TbSpQuestionnaire implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
	private String questId;
	private String title;
	private String introduction;
	private String introductionTitle;
	private String question;
	private String statement;
	private String statementTitle;
	private String type;
	private String limitItem;
	// private String introductionRisk;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "TYPE")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Column(name = "LIMIT_ITEM")
	public String getLimitItem() {
		return limitItem;
	}

	public void setLimitItem(String limitItem) {
		this.limitItem = limitItem;
	}

	@Column(name = "INTRODUCTION_TITLE")
	public String getIntroductionTitle() {
		return introductionTitle;
	}

	public void setIntroductionTitle(String introductionTitle) {
		this.introductionTitle = introductionTitle;
	}

	@Column(name = "STATEMENT")
	public String getStatement() {
		return statement;
	}

	public void setStatement(String statement) {
		this.statement = statement;
	}

	@Column(name = "STATEMENT_TITLE")
	public String getStatementTitle() {
		return statementTitle;
	}

	public void setStatementTitle(String statementTitle) {
		this.statementTitle = statementTitle;
	}

	@Column(name = "QUEST_ID")
	public String getQuestId() {
		return questId;
	}

	public void setQuestId(String questId) {
		this.questId = questId;
	}

	@Column(name = "TITLE")
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "INTRODUCTION")
	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	@Column(name = "QUESTION")
	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	// @Column(name = "INTRODUCTION_RISE")
	// public String getIntroductionRisk() {
	// return introductionRisk;
	// }
	//
	// public void setIntroductionRisk(String introductionRisk) {
	// this.introductionRisk = introductionRisk;
	// }

}
