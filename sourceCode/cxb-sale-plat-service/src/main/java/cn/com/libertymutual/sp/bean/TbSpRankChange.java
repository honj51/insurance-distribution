package cn.com.libertymutual.sp.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "tb_sp_rank_change")
public class TbSpRankChange implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3622331691985364129L;

	private Integer id;
	private String userCode;
	private String changeType;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date changeTime;
	private String oldRank;
	private String newRank;
	private String remark;
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "USER_CODE")
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	
	
	@Column(name = "CHANGE_TYPE")
	public String getChangeType() {
		return changeType;
	}
	public void setChangeType(String changeType) {
		this.changeType = changeType;
	}
	
	@Column(name = "CHANGE_TIME")
	public Date getChangeTime() {
		return changeTime;
	}
	public void setChangeTime(Date changeTime) {
		this.changeTime = changeTime;
	}
	
	@Column(name = "OLD_RANK")
	public String getOldRank() {
		return oldRank;
	}
	public void setOldRank(String oldRank) {
		this.oldRank = oldRank;
	}
	
	@Column(name = "NEW_RANK")
	public String getNewRank() {
		return newRank;
	}
	public void setNewRank(String newRank) {
		this.newRank = newRank;
	}
	
	@Column(name = "REMARK")
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
	
	
	
}
