package cn.com.libertymutual.sp.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;

@ApiModel
@Entity
@Table(name = "tb_sp_share")
public class TbSpShare implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1500951735381382958L;
	private Integer id;
	private String productId;
	private String shareId;
	private String userCode;
	private Integer sharinChain;
	private String userCodeChain;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date createTime;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date updateTime;
	private String shareType;
	private String lastShareType;
	private String state;
	private String agrementNo;
	private String saleCode;
	private String saleName;
	private String channelName;
	private String refShareId;
	private Integer shareNum;

	public static final String SHARE_TYPE_NO_VISIT = "0";
	public static final String SHARE_TYPE_VISIT = "1";

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "share_num")
	public Integer getShareNum() {
		return shareNum;
	}

	public void setShareNum(Integer shareNum) {
		this.shareNum = shareNum;
	}

	@Column(name = "refShareId")
	public String getRefShareId() {
		return refShareId;
	}

	public void setRefShareId(String refShareId) {
		this.refShareId = refShareId;
	}

	@Column(name = "update_time")
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Column(name = "CHANNEL_NAME")
	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	@Column(name = "state")
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "lastShareType")
	public String getLastShareType() {
		return lastShareType;
	}

	public void setLastShareType(String lastShareType) {
		this.lastShareType = lastShareType;
	}

	@Column(name = "shareType")
	public String getShareType() {
		return shareType;
	}

	public void setShareType(String shareType) {
		this.shareType = shareType;
	}

	@Column(name = "createTime")
	public Date getCreateTime() {
		return createTime;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	@Column(name = "product_id")
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Column(name = "share_id")
	public String getShareId() {
		return shareId;
	}

	public void setShareId(String shareId) {
		this.shareId = shareId;
	}

	@Column(name = "user_Code")
	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	@Column(name = "sharin_Chain")
	public Integer getSharinChain() {
		return sharinChain;
	}

	public void setSharinChain(Integer sharinChain) {
		this.sharinChain = sharinChain;
	}

	@Column(name = "userCode_Chain")
	public String getUserCodeChain() {
		return userCodeChain;
	}

	public void setUserCodeChain(String userCodeChain) {
		this.userCodeChain = userCodeChain;
	}

	@Column(name = "AGREMENT_NO")
	public String getAgrementNo() {
		return this.agrementNo;
	}

	public void setAgrementNo(String agrementNo) {
		this.agrementNo = agrementNo;
	}

	@Column(name = "SALE_CODE")
	public String getSaleCode() {
		return saleCode;
	}

	public void setSaleCode(String saleCode) {
		this.saleCode = saleCode;
	}

	@Column(name = "SALE_NAME")
	public String getSaleName() {
		return saleName;
	}

	public void setSaleName(String saleName) {
		this.saleName = saleName;
	}

}
