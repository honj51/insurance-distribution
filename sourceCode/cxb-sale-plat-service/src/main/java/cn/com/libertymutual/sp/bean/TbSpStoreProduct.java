package cn.com.libertymutual.sp.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "tb_sp_storeproduct")
public class TbSpStoreProduct implements Serializable {

	// Fields

	/**
	 * 
	 */
	private static final long serialVersionUID = -5216624608991728637L;
	private Integer id;
	private String userCode;
	private Integer productId;
	@Transient // 非数据库字段
	private String productName;
	private Integer serialNo;
	private Integer riskSerialNo;
	private String isShow;

	// Constructors

	/** default constructor */
	public TbSpStoreProduct() {
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "RISK_SERIAL_NO")
	public Integer getRiskSerialNo() {
		return riskSerialNo;
	}

	public void setRiskSerialNo(Integer riskSerialNo) {
		this.riskSerialNo = riskSerialNo;
	}

	@Column(name = "USER_CODE")
	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	@Column(name = "Product_id")
	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	@Transient // 非数据库字段
	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	@Column(name = "SERIAL_NO")
	public Integer getSerialNo() {
		return this.serialNo;
	}

	public void setSerialNo(Integer serialNo) {
		this.serialNo = serialNo;
	}

	@Column(name = "IS_SHOW", length = 1)
	public String getIsShow() {
		return this.isShow;
	}

	public void setIsShow(String isShow) {
		this.isShow = isShow;
	}

}