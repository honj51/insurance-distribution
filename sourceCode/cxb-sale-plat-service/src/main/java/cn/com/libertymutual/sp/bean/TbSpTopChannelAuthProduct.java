package cn.com.libertymutual.sp.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tb_sp_topchannel_auth_product")
public class TbSpTopChannelAuthProduct implements  Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2719909151792777185L;

	private Integer id;
	private String userCode;
	private Integer productId;
	private String productCname;
	private String productEname;
	
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	@Column(name = "USER_CODE", length = 20)
	public String getUserCode() {
		return userCode;
	}
	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}
	@Column(name = "PRODUCT_ID", length = 11)
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	
	@Column(name = "PRODUCT_CNAME")
	public String getProductCname() {
		return productCname;
	}
	public void setProductCname(String productCname) {
		this.productCname = productCname;
	}
	
	@Column(name = "PRODUCT_ENAME")
	public String getProductEname() {
		return productEname;
	}
	public void setProductEname(String productEname) {
		this.productEname = productEname;
	}
	
	
}
