package cn.com.libertymutual.sp.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnore;

import cn.com.libertymutual.core.util.Constants;
import cn.com.libertymutual.sys.bean.SysRoleUser;
import cn.com.libertymutual.sys.bean.SysUser;

@Entity
@Table(name = "tb_sp_user")
public class TbSpUser implements Serializable {
	// Fields
	private static final long serialVersionUID = -4475549745824694358L;

	@Transient // 非数据库字段
	private List<TbSpBank> banks;
	@Transient // 非数据库字段
	private List<TbSpStoreProduct> storeProducts;

	private Integer id;
	// 用户编码
	// @JsonIgnore // json转换忽略
	private String userCode;
	// 用户编码(普通浏览器账户)
	// @JsonIgnore // json转换忽略
	private String userCodeBs;
	// 用户的标识，对当前公众号唯一
	private String wxOpenId;
	// 员工编码
	private String employeeCode;
	// 用户姓名
	private String userName;
	// 用户昵称
	private String nickName;
	// 用户密码
	@JsonIgnore // json转换忽略
	private String password;
	// 用户新设置的密码（仅传参）
	@Transient // 非数据库字段
	private String newPassword;
	// 手机号码
	private String mobile;
	// 身份证号码
	private String idNumber;
	// 邮箱
	private String email;
	// CA协议 accType=2&&codeType=0
	// 中介协议 accType=1&&codeType=(1,2,3,4,i,k)
	// 用户类型: 1游客：
	// 2普通客户：CA协议-->无业务关系代码
	// 3专业代理：中介协议-->个贷+机构代理
	// 4商户：CA协议-->组织机构代码或营业执照号任意不为空
	// 5销售人员：CA协议-->非商户
	private String userType = Constants.USER_TYPE_2;// 1游客,2普通客户,3专业代理,4商户,5销售人员

	private String applyAgrNoType;// 申请业务关系代码类型:1=我是代理人,2=我是非保险机构
	// 业务关系代码
	// 业务关系代码类型(codeType) 0=内部员工,1=个贷,2=专业代理,3=兼业代理,4=经纪业务,5=电销,6=网销,i=邮政代理,k=银保代理；
	// 业务关系代码佣金类型(accType) 1=代理人,2=提佣,3=绩效
	private String agrementNo;
	// 业务关系代码代理名称
	private String agrementName;
	// 分公司机构编码
	private String branchCode;
	// 销售人员代码
	private String saleCode;
	// 销售人员姓名
	private String saleName;
	// 销售渠道编码
	private String channelCode;
	// 销售渠道名称
	private String channelName;
	// 常驻地区编码
	private String areaCode;
	// 常驻地区名
	private String areaName;
	// 介绍人ID
	private String introduceId;
	// 用户头像，最后一个数值代表正方形头像大小（有0、46、64、96、132数值可选，0代表640*640正方形头像），用户没有头像时该项为空。若用户更换头像，原有头像URL将失效。
	private String headUrl;

	// 用户是否订阅该公众号标识，值为0时，代表此用户没有关注该公众号，拉取不到其余信息，只有openid和UnionID（在该公众号绑定到了微信开放平台账号时才有）。
	private String wxSubscribe;
	// 用户的性别，值为1时是男性，值为2时是女性，值为0时是未知
	private String wxSex;
	// 用户所在城市
	private String wxCity;
	// 用户所在国家
	private String wxCountry;
	// 用户所在省份
	private String wxProvince;
	// 用户的语言，简体中文为zh_CN
	private String wxLanguage;
	// 用户关注时间，为时间戳。如果用户曾多次关注，则取最后关注时间
	private Timestamp wxSubscribeTime;
	// 只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。
	private String wxUnionid;
	// 公众号运营者对粉丝的备注，公众号运营者可在微信公众平台用户管理界面对粉丝添加备注
	private String wxRemark;
	// 用户所在的分组ID（暂时兼容用户分组旧接口）
	private String wxGroupid;
	// 用户被打上的标签ID列表
	private String wxTagidList;

	private String idType = Constants.USER_ID_CARD_TYPE_1;// 证件类型：1身份证，2社会信用代码
	private Double baseRate = 0.0;// 下级佣金比例
	private String comCode;// 归属部门
	// 用户积分余额
	private Integer points = 0;
	// 注册时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Timestamp registeDate;
	// 最后登录时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Timestamp lastloginDate;
	// 登录次数
	private int loginTimes = 0;
	// 连续登陆天数
	private int loginTimesContinuity = 0;
	// 配送地址
	private String shippingAddress;
	// 用户等级
	private String level;
	// 店铺开店时间
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Timestamp storeCreateTime;
	// 是否店主
	private String storeFlag = Constants.USER_STORE_FLAG_0;
	// 登录状态，0=未登录，1=已登录，2=已锁定
	private String loginState = Constants.USER_LOGIN_STATE_0;
	// 账户状态，0=已注销，1=有效，2=挂失
	private String state = Constants.USER_STATE_1;

	private String typeState = "00";
	// 推荐人
	private String refereeId;
	// Constructors
	private String refereePro;

	private Integer volume;

	// 人气
	private Integer popularity = 0;

	private String shopName;
	private String shopIntroduct;
	// 推荐人信息
	private TbSpUser refereeUser;

	private TbSpScore score;
	private String channelType;// 客户单位类型
	private String customName;// 客户单位名称
	private TbSpApprove approve;
	private Integer configSerialNo;

	private SysRoleUser role;
	private SysUser sysUser;
	private String registerType = Constants.USER_REGISTER_TYPE_0;// 注册类型：0=wx自主/分享注册(个人用户)，1=后台注册(渠道用户)，2=分享注册(渠道用户)
	private String canModify = Constants.FALSE;// 是否允许前端修改渠道信息,0:否,1:是
	private String canScore = Constants.TRUE;// 下级能否可以获得积分,0:否,1:是
	private String upUserCode;// 关系上属

	@Column(name = "REGISTER_TYPE")
	public String getRegisterType() {
		return registerType;
	}

	public void setRegisterType(String registerType) {
		this.registerType = registerType;
	}

	@Column(name = "CAN_MODIFY")
	public String getCanModify() {
		return canModify;
	}

	public void setCanModify(String canModify) {
		this.canModify = canModify;
	}

	@Column(name = "CAN_SCORE")
	public String getCanScore() {
		return canScore;
	}

	public void setCanScore(String canScore) {
		this.canScore = canScore;
	}

	@Transient
	public SysRoleUser getRole() {
		return role;
	}

	public void setRole(SysRoleUser role) {
		this.role = role;
	}

	@Transient
	public SysUser getSysUser() {
		return sysUser;
	}

	public void setSysUser(SysUser sysUser) {
		this.sysUser = sysUser;
	}

	@Column(name = "CONFIG_SERIALNO")
	public Integer getConfigSerialNo() {
		return configSerialNo;
	}

	public void setConfigSerialNo(Integer configSerialNo) {
		this.configSerialNo = configSerialNo;
	}

	@Transient
	public TbSpApprove getApprove() {
		return approve;
	}

	public void setApprove(TbSpApprove approve) {
		this.approve = approve;
	}

	@Column(name = "CHANNEL_TYPE")
	public String getChannelType() {
		return channelType;
	}

	public void setChannelType(String channelType) {
		this.channelType = channelType;
	}

	@Column(name = "CUSTOM_NAME")
	public String getCustomName() {
		return customName;
	}

	public void setCustomName(String customName) {
		this.customName = customName;
	}

	@Column(name = "UP_USERCODE")
	public String getUpUserCode() {
		return upUserCode;
	}

	public void setUpUserCode(String upUserCode) {
		this.upUserCode = upUserCode;
	}

	@Transient
	public TbSpScore getScore() {
		return score;
	}

	public void setScore(TbSpScore score) {
		this.score = score;
	}

	@Transient
	public TbSpUser getRefereeUser() {
		return refereeUser;
	}

	public void setRefereeUser(TbSpUser refereeUser) {
		this.refereeUser = refereeUser;
	}

	/** default constructor */
	public TbSpUser() {
	}

	@Transient // 非数据库字段
	public List<TbSpBank> getBanks() {
		return banks;
	}

	public void setBanks(List<TbSpBank> banks) {
		this.banks = banks;
	}

	@Transient // 非数据库字段
	public List<TbSpStoreProduct> getStoreProducts() {
		return storeProducts;
	}

	public void setStoreProducts(List<TbSpStoreProduct> storeProducts) {
		this.storeProducts = storeProducts;
	}

	private String type;
	private String roleType;

	@Transient
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Transient
	public String getRoleType() {
		return roleType;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "USER_CODE")
	public String getUserCode() {
		return this.userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	@Column(name = "USER_CODE_BS")
	public String getUserCodeBs() {
		return userCodeBs;
	}

	public void setUserCodeBs(String userCodeBs) {
		this.userCodeBs = userCodeBs;
	}

	@Column(name = "EMPLOYEE_CODE")
	public String getEmployeeCode() {
		return employeeCode;
	}

	public void setEmployeeCode(String employeeCode) {
		this.employeeCode = employeeCode;
	}

	@Column(name = "USER_NAME")
	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Column(name = "NICK_NAME")
	public String getNickName() {
		return this.nickName;
	}

	public void setNickName(String nickName) {
		this.nickName = nickName;
	}

	@Column(name = "PASSWORD")
	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Transient // 非数据库字段
	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	@Column(name = "MOBILE")
	public String getMobile() {
		return this.mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	@Column(name = "ID_NUMBER")
	public String getIdNumber() {
		return idNumber;
	}

	public void setIdNumber(String idNumber) {
		this.idNumber = idNumber;
	}

	@Column(name = "EMAIL")
	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name = "USER_TYPE")
	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	@Column(name = "APPLY_AGRNO_TYPE")
	public String getApplyAgrNoType() {
		return applyAgrNoType;
	}

	public void setApplyAgrNoType(String applyAgrNoType) {
		this.applyAgrNoType = applyAgrNoType;
	}

	@Column(name = "AGREMENT_NO")
	public String getAgrementNo() {
		return this.agrementNo;
	}

	public void setAgrementNo(String agrementNo) {
		this.agrementNo = agrementNo;
	}

	@Column(name = "AGREMENT_NAME")
	public String getAgrementName() {
		return agrementName;
	}

	public void setAgrementName(String agrementName) {
		this.agrementName = agrementName;
	}

	@Column(name = "BRANCH_CODE")
	public String getBranchCode() {
		return this.branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	@Column(name = "ID_TYPE")
	public String getIdType() {
		return idType;
	}

	public void setIdType(String idType) {
		this.idType = idType;
	}

	@Column(name = "BASE_RATE")
	public Double getBaseRate() {
		return baseRate;
	}

	public void setBaseRate(Double baseRate) {
		this.baseRate = baseRate;
	}

	@Column(name = "COM_CODE")
	public String getComCode() {
		return comCode;
	}

	public void setComCode(String comCode) {
		this.comCode = comCode;
	}

	@Column(name = "SALE_CODE")
	public String getSaleCode() {
		return saleCode;
	}

	public void setSaleCode(String saleCode) {
		this.saleCode = saleCode;
	}

	@Column(name = "SALE_NAME")
	public String getSaleName() {
		return saleName;
	}

	public void setSaleName(String saleName) {
		this.saleName = saleName;
	}

	@Column(name = "CHANNEL_CODE")
	public String getChannelCode() {
		return channelCode;
	}

	public void setChannelCode(String channelCode) {
		this.channelCode = channelCode;
	}

	@Column(name = "CHANNEL_NAME")
	public String getChannelName() {
		return channelName;
	}

	public void setChannelName(String channelName) {
		this.channelName = channelName;
	}

	@Column(name = "LEVEL")
	public String getLevel() {
		return this.level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	@Column(name = "STORE_FLAG")
	public String getStoreFlag() {
		return storeFlag;
	}

	public void setStoreFlag(String storeFlag) {
		this.storeFlag = storeFlag;
	}

	@Column(name = "STORE_CREATE_TIME")
	public Timestamp getStoreCreateTime() {
		return storeCreateTime;
	}

	public void setStoreCreateTime(Timestamp storeCreateTime) {
		this.storeCreateTime = storeCreateTime;
	}

	@Column(name = "REGISTE_DATE")
	public Timestamp getRegisteDate() {
		return this.registeDate;
	}

	public void setRegisteDate(Timestamp registeDate) {
		this.registeDate = registeDate;
	}

	@Column(name = "LASTLOGIN_DATE")
	public Timestamp getLastloginDate() {
		return this.lastloginDate;
	}

	public void setLastloginDate(Timestamp lastloginDate) {
		this.lastloginDate = lastloginDate;
	}

	@Column(name = "LOGIN_TIMES")
	public int getLoginTimes() {
		return loginTimes;
	}

	public void setLoginTimes(int loginTimes) {
		this.loginTimes = loginTimes;
	}

	@Column(name = "LOGIN_TIMES_CONTINUITY")
	public int getLoginTimesContinuity() {
		return loginTimesContinuity;
	}

	public void setLoginTimesContinuity(int loginTimesContinuity) {
		this.loginTimesContinuity = loginTimesContinuity;
	}

	@Column(name = "SHIPPING_ADDRESS")
	public String getShippingAddress() {
		return this.shippingAddress;
	}

	public void setShippingAddress(String shippingAddress) {
		this.shippingAddress = shippingAddress;
	}

	@Column(name = "AREA_CODE")
	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	@Column(name = "AREA_NAME")
	public String getAreaName() {
		return areaName;
	}

	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}

	@Column(name = "HEAD_URL")
	public String getHeadUrl() {
		return this.headUrl;
	}

	public void setHeadUrl(String headUrl) {
		this.headUrl = headUrl;
	}

	@Column(name = "INTRODUCE_ID")
	public String getIntroduceId() {
		return this.introduceId;
	}

	public void setIntroduceId(String introduceId) {
		this.introduceId = introduceId;
	}

	@Column(name = "POINTS")
	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	@Column(name = "WX_SUBSCRIBE")
	public String getWxSubscribe() {
		return wxSubscribe;
	}

	public void setWxSubscribe(String wxSubscribe) {
		this.wxSubscribe = wxSubscribe;
	}

	@Column(name = "WX_OPEN_ID")
	public String getWxOpenId() {
		return wxOpenId;
	}

	public void setWxOpenId(String wxOpenId) {
		this.wxOpenId = wxOpenId;
	}

	@Column(name = "WX_SEX")
	public String getWxSex() {
		return wxSex;
	}

	public void setWxSex(String wxSex) {
		this.wxSex = wxSex;
	}

	@Column(name = "WX_CITY")
	public String getWxCity() {
		return wxCity;
	}

	public void setWxCity(String wxCity) {
		this.wxCity = wxCity;
	}

	@Column(name = "WX_COUNTRY")
	public String getWxCountry() {
		return wxCountry;
	}

	public void setWxCountry(String wxCountry) {
		this.wxCountry = wxCountry;
	}

	@Column(name = "WX_PROVINCE")
	public String getWxProvince() {
		return wxProvince;
	}

	public void setWxProvince(String wxProvince) {
		this.wxProvince = wxProvince;
	}

	@Column(name = "WX_LANGUAGE")
	public String getWxLanguage() {
		return wxLanguage;
	}

	public void setWxLanguage(String wxLanguage) {
		this.wxLanguage = wxLanguage;
	}

	@Column(name = "WX_SUBSCRIBE_TIME")
	public Timestamp getWxSubscribeTime() {
		return wxSubscribeTime;
	}

	public void setWxSubscribeTime(Timestamp wxSubscribeTime) {
		this.wxSubscribeTime = wxSubscribeTime;
	}

	@Column(name = "WX_UNIONID")
	public String getWxUnionid() {
		return wxUnionid;
	}

	public void setWxUnionid(String wxUnionid) {
		this.wxUnionid = wxUnionid;
	}

	@Column(name = "WX_REMARK")
	public String getWxRemark() {
		return wxRemark;
	}

	public void setWxRemark(String wxRemark) {
		this.wxRemark = wxRemark;
	}

	@Column(name = "WX_GROUPID")
	public String getWxGroupid() {
		return wxGroupid;
	}

	public void setWxGroupid(String wxGroupid) {
		this.wxGroupid = wxGroupid;
	}

	@Column(name = "WX_TAGID_LIST")
	public String getWxTagidList() {
		return wxTagidList;
	}

	public void setWxTagidList(String wxTagidList) {
		this.wxTagidList = wxTagidList;
	}

	@Transient
	public Integer getVolume() {
		return volume;
	}

	public void setVolume(Integer volume) {
		this.volume = volume;
	}

	private String approveAuth;

	@Transient
	public String getApproveAuth() {
		return approveAuth;
	}

	public void setApproveAuth(String approveAuth) {
		this.approveAuth = approveAuth;
	}

	@Column(name = "POPULARITY")
	public Integer getPopularity() {
		return popularity;
	}

	public void setPopularity(Integer popularity) {
		this.popularity = popularity;
	}

	@Column(name = "STATE")
	public String getState() {
		return state;
	}

	/**账户状态，0=已注销，1=有效，2=挂失*/
	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "REFEREE_ID")
	public String getRefereeId() {
		return refereeId;
	}

	public void setRefereeId(String refereeId) {
		this.refereeId = refereeId;
	}

	@Column(name = "REFEREE_PRO")
	public String getRefereePro() {
		return refereePro;
	}

	public void setRefereePro(String refereePro) {
		this.refereePro = refereePro;
	}

	@Column(name = "TYPE_STATE")
	public String getTypeState() {
		return typeState;
	}

	public void setTypeState(String typeState) {
		this.typeState = typeState;
	}

	@Column(name = "LOGIN_STATE")
	public String getLoginState() {
		return loginState;
	}

	/**登录状态，0=未登录，1=已登录，2=已锁定*/
	public void setLoginState(String loginState) {
		this.loginState = loginState;
	}

	@Column(name = "SHOP_NAME")
	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	@Column(name = "SHOP_INTRODUC")
	public String getShopIntroduct() {
		return shopIntroduct;
	}

	public void setShopIntroduct(String shopIntroduct) {
		this.shopIntroduct = shopIntroduct;
	}

	@Override
	public String toString() {
		return "TbSpUser [id=" + id + ", userCode=" + userCode + ", userCodeBs=" + userCodeBs + ", wxOpenId=" + wxOpenId + ", userName=" + userName
				+ ", nickName=" + nickName + ", password=" + password + ", mobile=" + mobile + ", idNumber=" + idNumber + ", email=" + email
				+ ", userType=" + userType + ", applyAgrNoType=" + applyAgrNoType + ", agrementNo=" + agrementNo + ", agrementName=" + agrementName
				+ ", branchCode=" + branchCode + ", saleCode=" + saleCode + ", saleName=" + saleName + ", channelCode=" + channelCode
				+ ", channelName=" + channelName + ", areaCode=" + areaCode + ", areaName=" + areaName + ", introduceId=" + introduceId + ", headUrl="
				+ headUrl + ", wxSubscribe=" + wxSubscribe + ", wxSex=" + wxSex + ", wxCity=" + wxCity + ", wxCountry=" + wxCountry + ", wxProvince="
				+ wxProvince + ", wxLanguage=" + wxLanguage + ", wxSubscribeTime=" + wxSubscribeTime + ", wxUnionid=" + wxUnionid + ", wxRemark="
				+ wxRemark + ", wxGroupid=" + wxGroupid + ", wxTagidList=" + wxTagidList + ", points=" + points + ", registeDate=" + registeDate
				+ ", lastloginDate=" + lastloginDate + ", loginTimes=" + loginTimes + ", loginTimesContinuity=" + loginTimesContinuity
				+ ", shippingAddress=" + shippingAddress + ", banks=" + banks + ", level=" + level + ", storeCreateTime=" + storeCreateTime
				+ ", storeFlag=" + storeFlag + ", loginState=" + loginState + ", state=" + state + ", typeState=" + typeState + ", refereeId="
				+ refereeId + ", refereePro=" + refereePro + ", volume=" + volume + ", popularity=" + popularity + ", shopName=" + shopName
				+ ", shopIntroduct=" + shopIntroduct + ", refereeUser=" + refereeUser + ", score=" + score + ", registerType=" + registerType + "]";
	}

}