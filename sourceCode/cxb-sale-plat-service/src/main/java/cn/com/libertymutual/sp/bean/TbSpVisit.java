package cn.com.libertymutual.sp.bean;

import static javax.persistence.GenerationType.IDENTITY;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;

@ApiModel
@Entity
@Table(name = "tb_sp_visit")
public class TbSpVisit implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3229128035104139871L;
	private Integer id;
	private String productId;
	private String state;
	private String accessSource;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date visitingTime;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss", timezone = "GMT+8")
	private Date updateTime;
	private String sessionId;
	private String proposalNo;

	public static final String ACCESS_SOURCE_DIRECT = "1";
	public static final String ACCESS_SOURCE_SHARE = "2";

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "ID", unique = true, nullable = false)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column(name = "product_id", length = 10)
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	@Column(name = "state", length = 10)
	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	@Column(name = "access_source", length = 10)
	public String getAccessSource() {
		return accessSource;
	}

	public void setAccessSource(String accessSource) {
		this.accessSource = accessSource;
	}

	@Column(name = "visiting_time")
	public Date getVisitingTime() {
		return visitingTime;
	}

	public void setVisitingTime(Date visitingTime) {
		this.visitingTime = visitingTime;
	}

	@Column(name = "update_time")
	public Date getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(Date updateTime) {
		this.updateTime = updateTime;
	}

	@Column(name = "session_id", length = 50)
	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	@Column(name = "Proposal_No", length = 30)
	public String getProposalNo() {
		return proposalNo;
	}

	public void setProposalNo(String proposalNo) {
		this.proposalNo = proposalNo;
	}

}
