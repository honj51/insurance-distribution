package cn.com.libertymutual.sp.bean.car;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by Ryan on 2016-09-09.
 */
@Entity
@Table(name = "tb_prp_rate", catalog = "")
public class PrpRate implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6820468663780647584L;
	private int id;
	private String kindCode;
	private String riskCode;
	private String sumInsured;
	private int serialNo;
	private String isDefault;
	private String sumInsuredName;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", nullable = false)
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Basic
	@Column(name = "KIND_CODE", nullable = false, length = 10)
	public String getKindCode() {
		return kindCode;
	}

	public void setKindCode(String kindCode) {
		this.kindCode = kindCode;
	}

	@Basic
	@Column(name = "RISK_CODE", nullable = false, length = 4)
	public String getRiskCode() {
		return riskCode;
	}

	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}

	@Basic
	@Column(name = "SUM_INSURED", nullable = false, length = 20)
	public String getSumInsured() {
		return sumInsured;
	}

	public void setSumInsured(String sumInsured) {
		this.sumInsured = sumInsured;
	}

	@Basic
	@Column(name = "SERIAL_NO", nullable = false)
	public int getSerialNo() {
		return serialNo;
	}

	public void setSerialNo(int serialNo) {
		this.serialNo = serialNo;
	}

	@Basic
	@Column(name = "IS_DEFAULT", nullable = true, length = 1)
	public String getIsDefault() {
		return isDefault;
	}

	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}

	@Basic
	@Column(name = "SUM_INSURED_NAME", nullable = true, length = 20)
	public String getSumInsuredName() {
		return sumInsuredName;
	}

	public void setSumInsuredName(String sumInsuredName) {
		this.sumInsuredName = sumInsuredName;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;
		PrpRate prpRate = (PrpRate) o;
		if (id != prpRate.id)
			return false;
		if (serialNo != prpRate.serialNo)
			return false;
		if (kindCode != null ? !kindCode.equals(prpRate.kindCode) : prpRate.kindCode != null)
			return false;
		if (riskCode != null ? !riskCode.equals(prpRate.riskCode) : prpRate.riskCode != null)
			return false;
		if (sumInsured != null ? !sumInsured.equals(prpRate.sumInsured) : prpRate.sumInsured != null)
			return false;
		if (isDefault != null ? !isDefault.equals(prpRate.isDefault) : prpRate.isDefault != null)
			return false;
		if (sumInsuredName != null ? !sumInsuredName.equals(prpRate.sumInsuredName) : prpRate.sumInsuredName != null)
			return false;
		return true;
	}

	@Override
	public int hashCode() {
		int result = id;
		result = 31 * result + (kindCode != null ? kindCode.hashCode() : 0);
		result = 31 * result + (riskCode != null ? riskCode.hashCode() : 0);
		result = 31 * result + (sumInsured != null ? sumInsured.hashCode() : 0);
		result = 31 * result + serialNo;
		result = 31 * result + (isDefault != null ? isDefault.hashCode() : 0);
		result = 31 * result + (sumInsuredName != null ? sumInsuredName.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "PrpRate [id=" + id + ", kindCode=" + kindCode + ", riskCode=" + riskCode + ", sumInsured=" + sumInsured + ", serialNo=" + serialNo
				+ ", isDefault=" + isDefault + ", sumInsuredName=" + sumInsuredName + "]";
	}
}