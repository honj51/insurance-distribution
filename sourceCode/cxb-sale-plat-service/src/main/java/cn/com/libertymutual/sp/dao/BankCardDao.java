package cn.com.libertymutual.sp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import cn.com.libertymutual.sp.bean.TbSpBankCard;

/**
 * @author AoYi
 * 注意@Modifying注解需要使用clearAutomatically=true;
 * 同一接口更新后立即查询获得更新后的数据,默认false查询还是更新前的数据
 */
@Repository
public interface BankCardDao extends PagingAndSortingRepository<TbSpBankCard, Integer>, JpaSpecificationExecutor<TbSpBankCard> {

	@Query("from TbSpBankCard")
	public List<TbSpBankCard> findAll();

	@Query("from TbSpBankCard t where cardLastNo = :cardLastNo order by id desc")
	public List<TbSpBankCard> findByCardLastNo(@Param("cardLastNo") String cardLastNo);

	@Query("from TbSpBankCard t where bankName = :bankName order by id desc")
	public List<TbSpBankCard> findByBankName(@Param("bankName") String bankName);

	@Query("from TbSpBankCard t where cardLastNo = :cardLastNo and bankName = :bankName order by id desc")
	public List<TbSpBankCard> findByCardLastNoAndBankName(@Param("cardLastNo") String cardLastNo, @Param("bankName") String bankName);

	@Query("select count(id) from TbSpBankCard t where cardLastNo =?1 and bankName =?2 and bankNamePinYin =?3 "
			+ "and cardTypeName =?4 and cardTypeNamePinYin =?5 order by id desc")
	public int countByFiveField(String cardLastNo, String bankName, String bankNamePinYin, String cardTypeName, String cardTypeNamePinYin);

}
