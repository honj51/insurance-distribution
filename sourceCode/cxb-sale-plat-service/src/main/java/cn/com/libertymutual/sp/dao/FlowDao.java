package cn.com.libertymutual.sp.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cn.com.libertymutual.sp.bean.TbSpFlow;

@Repository
public interface FlowDao extends PagingAndSortingRepository<TbSpFlow, Integer>, JpaSpecificationExecutor<TbSpFlow> {

	@Transactional
	@Modifying
	@Query(value="update tb_sp_flow set UPDATE_TIME = sysdate() where FLOW_NO = ?1 ",nativeQuery=true)
	int updateTime(String flowNo);

	
	@Query("from TbSpFlow where flowType=?1 and status=1")
	TbSpFlow findByType(String flowType);
	
	
	@Query("from TbSpFlow where flowNo=?1 and status=1")
	TbSpFlow findByFlowNo(String flowNo);
}
