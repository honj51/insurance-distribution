package cn.com.libertymutual.sp.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import cn.com.libertymutual.sp.bean.TbSpInputConfig;

@Repository
public interface InputConfigDao extends PagingAndSortingRepository<TbSpInputConfig, Integer>, JpaSpecificationExecutor<TbSpInputConfig> {

	@Query("from TbSpInputConfig q where q.inputId = ?1")
	TbSpInputConfig findByInputId(String id);
}
