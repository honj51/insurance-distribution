package cn.com.libertymutual.sp.dao;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import cn.com.libertymutual.sp.bean.SysOperationLog;
@Repository
public interface OperationLogDao extends PagingAndSortingRepository<SysOperationLog, Integer>, JpaSpecificationExecutor<SysOperationLog>{


}
