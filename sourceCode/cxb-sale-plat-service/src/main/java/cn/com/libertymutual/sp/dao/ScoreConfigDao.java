package cn.com.libertymutual.sp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.com.libertymutual.sp.bean.TbSpScoreConfig;

public interface ScoreConfigDao extends PagingAndSortingRepository<TbSpScoreConfig, Integer>, JpaSpecificationExecutor<TbSpScoreConfig> {

	@Query("from TbSpScoreConfig")
	List<TbSpScoreConfig> findAllConfig();

}
