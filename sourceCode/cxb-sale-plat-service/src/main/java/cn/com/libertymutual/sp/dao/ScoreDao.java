package cn.com.libertymutual.sp.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import cn.com.libertymutual.sp.bean.TbSpScore;

@Repository
public interface ScoreDao extends PagingAndSortingRepository<TbSpScore, Integer>, JpaSpecificationExecutor<TbSpScore> {

	@Query("from TbSpScore where userCode=?1")
	public List<TbSpScore> findByUserCode(String userCode);

	@Query("select count(id) from TbSpScore where userCode=?1")
	public int findCountByUserCode(String userCode);

	@Transactional
	@Modifying
	@Query("update TbSpScore set statisticTime=sysdate(), effctiveScore =effctiveScore- ?1 where userCode = ?2")
	public int reduceScore(double reduceScore, String userCode);

	@Transactional
	@Modifying
	@Query("update TbSpScore set statisticTime=sysdate(), effctiveScore =effctiveScore+ ?1 ,sumScore=sumScore+ ?1 where userCode = ?2")
	public int addScore(double addScore, String userCode);

	@Transactional
	@Modifying
	@Query("update TbSpScore set statisticTime=sysdate(), effctiveScore =effctiveScore+ ?1 ,noeffctiveScore=noeffctiveScore- ?1 where userCode = ?2")
	public int updateEffctiveScore(double Score, String userCode);

	// 累加积分
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query("update TbSpScore set effctiveScore = effctiveScore + :effctiveScore,noeffctiveScore = noeffctiveScore + :noeffctiveScore, "
			+ "Sumscore = Sumscore + :Sumscore where userCode = :userCode")
	public int updateScoreByUserCode(@Param("effctiveScore") Double effctiveScore, @Param("noeffctiveScore") Double noeffctiveScore,
			@Param("Sumscore") Double Sumscore, @Param("userCode") String userCode);
}
