package cn.com.libertymutual.sp.dao.car;

import java.util.List;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import cn.com.libertymutual.sp.bean.car.PrpRate;

public interface PrpRateDao extends PagingAndSortingRepository<PrpRate, Integer>, JpaSpecificationExecutor<PrpRate> {
	@Query("from PrpRate where riskCode=?1 order by serialNo asc")
	List<PrpRate> findRate(String riskCode);
}
