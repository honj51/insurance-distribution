package cn.com.libertymutual.sp.dto;

import java.io.Serializable;

public class TItemkindDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5000507605309122861L;
	
	
	protected String kindCodeMain;//险别代码
    protected String kindNameMain;//险别名称
    protected String modeCode;//
    protected String modeName;//国产/进口
    protected double premiumMain;//保费
    protected double amountMain;//保额
    
    public String getKindCodeMain() {
		return kindCodeMain;
	}
	public void setKindCodeMain(String kindCodeMain) {
		this.kindCodeMain = kindCodeMain;
	}
	public String getKindNameMain() {
		return kindNameMain;
	}
	public void setKindNameMain(String kindNameMain) {
		this.kindNameMain = kindNameMain;
	}
	public String getModeCode() {
		return modeCode;
	}
	public void setModeCode(String modeCode) {
		this.modeCode = modeCode;
	}
	public String getModeName() {
		return modeName;
	}
	public void setModeName(String modeName) {
		this.modeName = modeName;
	}
	public double getPremiumMain() {
		return premiumMain;
	}
	public void setPremiumMain(double premiumMain) {
		this.premiumMain = premiumMain;
	}
	public double getAmountMain() {
		return amountMain;
	}
	public void setAmountMain(double amountMain) {
		this.amountMain = amountMain;
	}
	
    
    
}
