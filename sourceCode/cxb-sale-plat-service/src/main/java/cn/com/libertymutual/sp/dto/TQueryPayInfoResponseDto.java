package cn.com.libertymutual.sp.dto;

import java.util.Map;

import cn.com.libertymutual.core.base.dto.ResponseBaseDto;



public class TQueryPayInfoResponseDto  extends ResponseBaseDto{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	private	Map<String,TPayInfoDto> payMap;//key:唯一识别码/支付号


	public Map<String, TPayInfoDto> getPayMap() {
		return payMap;
	}


	public void setPayMap(Map<String, TPayInfoDto> payMap) {
		this.payMap = payMap;
	}

	
	
}
