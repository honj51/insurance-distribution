package cn.com.libertymutual.sp.dto;


import java.io.Serializable;

import cn.com.libertymutual.core.base.dto.RequestBaseDto;


public class TQuerySalesRequestDto extends RequestBaseDto implements
Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7569392257181243237L;
	private String agreementCode;
	private String saleName;
	//默认查询20条
	private int pageSize=20;
	//默认第一页
	private int pageNum=0;
	public String getAgreementCode() {
		return agreementCode;
	}
	public String getSaleName() {
		return saleName== null? "": saleName;
	}
	public int getPageSize() {
		return pageSize;
	}
	public int getPageNum() {
		return  pageNum;
	}
	public void setAgreementCode(String agreementCode) {
		this.agreementCode = agreementCode;
	}
	public void setSaleName(String saleName) {
		this.saleName = saleName;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public void setPageNum(int pageNum) {
		this.pageNum = pageNum;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((agreementCode == null) ? 0 : agreementCode.hashCode());
		result = prime * result + pageNum;
		result = prime * result + pageSize;
		result = prime * result
				+ ((saleName == null) ? 0 : saleName.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TQuerySalesRequestDto other = (TQuerySalesRequestDto) obj;
		if (agreementCode == null) {
			if (other.agreementCode != null)
				return false;
		} else if (!agreementCode.equals(other.agreementCode))
			return false;
		if (pageNum != other.pageNum)
			return false;
		if (pageSize != other.pageSize)
			return false;
		if (saleName == null) {
			if (other.saleName != null)
				return false;
		} else if (!saleName.equals(other.saleName))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return "TQuerySalesRequestDto [agreementCode=" + agreementCode
				+ ", saleName=" + saleName + ", pageSize=" + pageSize
				+ ", pageNum=" + pageNum + "]";
	}
	public TQuerySalesRequestDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public TQuerySalesRequestDto(String agreementCode, String saleName) {
		super();
		this.agreementCode = agreementCode;
		this.saleName = saleName;
	}
	
	
	

}

