package cn.com.libertymutual.sp.dto.car.bean;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
/**
 * Created by Ryan on 2016-09-05.
 */

public class TpTitemApplicant  implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 5959169325879422022L;
	
	private int applicantId;
    private String flowId;
    private String name;
    private String idType;
    private String idNo;
    private String cellPhoneNo;
    private String address;
    private String email;
    private String gental;
    private Date brithday;
    private String applicantType;
    //客户类型
  	private String natureOfRole;
    public int getApplicantId() {
        return applicantId;
    }
    public void setApplicantId(int applicantId) {
        this.applicantId = applicantId;
    }
    public String getFlowId() {
        return flowId;
    }
    public void setFlowId(String flowId) {
        this.flowId = flowId;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getNatureOfRole() {
    	return natureOfRole;
    }
    
    public void setNatureOfRole(String natureOfRole) {
    	this.natureOfRole = natureOfRole;
    }
    public String getIdType() {
        return idType;
    }
    public void setIdType(String idType) {
        this.idType = idType;
    }
    public String getIdNo() {
        return idNo;
    }
    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }
    public String getCellPhoneNo() {
        return cellPhoneNo;
    }
    public void setCellPhoneNo(String cellPhoneNo) {
        this.cellPhoneNo = cellPhoneNo;
    }
    public String getAddress() {
        return address;
    }
    public void setAddress(String address) {
        this.address = address;
    }
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public String getGental() {
        return gental;
    }
    public void setGental(String gental) {
        this.gental = gental;
    }
    public Date getBrithday() {
        return brithday;
    }
    public void setBrithday(Date brithday) {
        this.brithday = brithday;
    }
    public String getApplicantType() {
        return applicantType;
    }
    public void setApplicantType(String applicantType) {
        this.applicantType = applicantType;
    }
    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TpTitemApplicant other = (TpTitemApplicant) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (applicantId != other.applicantId)
			return false;
		if (applicantType == null) {
			if (other.applicantType != null)
				return false;
		} else if (!applicantType.equals(other.applicantType))
			return false;
		if (brithday == null) {
			if (other.brithday != null)
				return false;
		} else if (!brithday.equals(other.brithday))
			return false;
		if (cellPhoneNo == null) {
			if (other.cellPhoneNo != null)
				return false;
		} else if (!cellPhoneNo.equals(other.cellPhoneNo))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (flowId == null) {
			if (other.flowId != null)
				return false;
		} else if (!flowId.equals(other.flowId))
			return false;
		if (gental == null) {
			if (other.gental != null)
				return false;
		} else if (!gental.equals(other.gental))
			return false;
		if (idNo == null) {
			if (other.idNo != null)
				return false;
		} else if (!idNo.equals(other.idNo))
			return false;
		if (idType == null) {
			if (other.idType != null)
				return false;
		} else if (!idType.equals(other.idType))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (natureOfRole == null) {
			if (other.natureOfRole != null)
				return false;
		} else if (!natureOfRole.equals(other.natureOfRole))
			return false;
		return true;
	}
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + applicantId;
		result = prime * result
				+ ((applicantType == null) ? 0 : applicantType.hashCode());
		result = prime * result
				+ ((brithday == null) ? 0 : brithday.hashCode());
		result = prime * result
				+ ((cellPhoneNo == null) ? 0 : cellPhoneNo.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + ((flowId == null) ? 0 : flowId.hashCode());
		result = prime * result + ((gental == null) ? 0 : gental.hashCode());
		result = prime * result + ((idNo == null) ? 0 : idNo.hashCode());
		result = prime * result + ((idType == null) ? 0 : idType.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((natureOfRole == null) ? 0 : natureOfRole.hashCode());
		return result;
	}
	@Override
	public String toString() {
		return "TpTitemApplicant [applicantId=" + applicantId + ", flowId="
				+ flowId + ", name=" + name + ", idType=" + idType + ", idNo="
				+ idNo + ", cellPhoneNo=" + cellPhoneNo + ", address="
				+ address + ", email=" + email + ", gental=" + gental
				+ ", brithday=" + brithday + ", applicantType=" + applicantType
				+ ", natureOfRole=" + natureOfRole + "]";
	}
}