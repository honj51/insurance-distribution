package cn.com.libertymutual.sp.dto.car.dto;

import java.io.Serializable;
import java.util.List;

import cn.com.libertymutual.sp.dto.car.bean.TpTitemApplicant;
import cn.com.libertymutual.sp.dto.car.bean.TpTitemDistribute;
import cn.com.libertymutual.sp.dto.car.bean.TpTitemEngage;

/**
 * 提交核保请求对象
 * @author Ryan
 *
 */
public class TProposalSaveRequestDto extends RequestBaseDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4865205602334092257L;
	private String veriCode; // 承保验证码
	// 变更为配送对象 ryan.lv 20170104
	private TpTitemDistribute tdistributionDto; // 配送信息
	private TpTitemApplicant tprptCarOwnerDto; // 车主信息
	private TpTitemApplicant tprptApplicantDto; // 投保人信息
	private TpTitemApplicant tprptInsuredDto; // 被保险人信息

	private List<TpTitemEngage> tprptEngageDTOList; // 特别约定列表

	public String getVeriCode() {
		return veriCode;
	}

	public void setVeriCode(String veriCode) {
		this.veriCode = veriCode;
	}

	public TpTitemDistribute getTdistributionDto() {
		return tdistributionDto;
	}

	public void setTdistributionDto(TpTitemDistribute tdistributionDto) {
		this.tdistributionDto = tdistributionDto;
	}

	public TpTitemApplicant getTprptCarOwnerDto() {
		return tprptCarOwnerDto;
	}

	public void setTprptCarOwnerDto(TpTitemApplicant tprptCarOwnerDto) {
		this.tprptCarOwnerDto = tprptCarOwnerDto;
	}

	public TpTitemApplicant getTprptApplicantDto() {
		return tprptApplicantDto;
	}

	public void setTprptApplicantDto(TpTitemApplicant tprptApplicantDto) {
		this.tprptApplicantDto = tprptApplicantDto;
	}

	public TpTitemApplicant getTprptInsuredDto() {
		return tprptInsuredDto;
	}

	public void setTprptInsuredDto(TpTitemApplicant tprptInsuredDto) {
		this.tprptInsuredDto = tprptInsuredDto;
	}

	public List<TpTitemEngage> getTprptEngageDTOList() {
		return tprptEngageDTOList;
	}

	public void setTprptEngageDTOList(List<TpTitemEngage> tprptEngageDTOList) {
		this.tprptEngageDTOList = tprptEngageDTOList;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((tdistributionDto == null) ? 0 : tdistributionDto.hashCode());
		result = prime * result + ((tprptApplicantDto == null) ? 0 : tprptApplicantDto.hashCode());
		result = prime * result + ((tprptCarOwnerDto == null) ? 0 : tprptCarOwnerDto.hashCode());
		result = prime * result + ((tprptEngageDTOList == null) ? 0 : tprptEngageDTOList.hashCode());
		result = prime * result + ((tprptInsuredDto == null) ? 0 : tprptInsuredDto.hashCode());
		result = prime * result + ((veriCode == null) ? 0 : veriCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TProposalSaveRequestDto other = (TProposalSaveRequestDto) obj;
		if (tdistributionDto == null) {
			if (other.tdistributionDto != null)
				return false;
		} else if (!tdistributionDto.equals(other.tdistributionDto))
			return false;
		if (tprptApplicantDto == null) {
			if (other.tprptApplicantDto != null)
				return false;
		} else if (!tprptApplicantDto.equals(other.tprptApplicantDto))
			return false;
		if (tprptCarOwnerDto == null) {
			if (other.tprptCarOwnerDto != null)
				return false;
		} else if (!tprptCarOwnerDto.equals(other.tprptCarOwnerDto))
			return false;
		if (tprptEngageDTOList == null) {
			if (other.tprptEngageDTOList != null)
				return false;
		} else if (!tprptEngageDTOList.equals(other.tprptEngageDTOList))
			return false;
		if (tprptInsuredDto == null) {
			if (other.tprptInsuredDto != null)
				return false;
		} else if (!tprptInsuredDto.equals(other.tprptInsuredDto))
			return false;
		if (veriCode == null) {
			if (other.veriCode != null)
				return false;
		} else if (!veriCode.equals(other.veriCode))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TProposalSaveRequestDto [veriCode=" + veriCode + ", tdistributionDto=" + tdistributionDto + ", tprptCarOwnerDto=" + tprptCarOwnerDto
				+ ", tprptApplicantDto=" + tprptApplicantDto + ", tprptInsuredDto=" + tprptInsuredDto + ", tprptEngageDTOList=" + tprptEngageDTOList
				+ "]";
	}

}