package cn.com.libertymutual.sp.dto.queryplans;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import cn.com.libertymutual.core.base.dto.RequestBaseDto;

public class QuerySPPlansRequestDTO extends RequestBaseDto implements Serializable {
	/**
	 * 自营销售平台 通用查询请求
	 */
	private static final long serialVersionUID = -4327671355137839832L;

	private String agencyAbname;// 系统来源
	private Integer seatCount;
	private Integer age;// 被保人年龄
	private Integer deadLine;// 保障期限
	private String companyCode;
	private String motorProposalNo;
	private List<CrossSalePlan> planLists;
	private String documentNo;
	/*新查询方式：以代理人代码和动态参数 查询
	 * 动态参数  key:value,比如： 00005:1
	 * 00005 车辆类型(0-货运车,1-乘用车)
	 * 00006 车辆使用性质(0-营业,1-非营业)
	 * 00007 车辆吨位数
	 * 00001 座位
	 * 00002 天
	 * 00003 年龄
	 * 00004 地区
	 * */
	private  Map<String,String> paraMap;

	private String[] planIds;// 支持多个id: 以,隔开 比如： 1,2,3


	public QuerySPPlansRequestDTO() {
		super();
		// TODO Auto-generated constructor stub
	}

	public QuerySPPlansRequestDTO(String agencyAbname, Integer age, Integer deadLine, String[] planIds) {
		super();
		this.agencyAbname = agencyAbname;
		this.age = age;
		this.deadLine = deadLine;
		this.planIds = planIds;
	}

	public String getAgencyAbname() {
		return agencyAbname;
	}

	public void setAgencyAbname(String agencyAbname) {
		this.agencyAbname = agencyAbname;
	}

	public Integer getSeatCount() {
		return seatCount;
	}

	public void setSeatCount(Integer seatCount) {
		this.seatCount = seatCount;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public Integer getDeadLine() {
		return deadLine;
	}

	public void setDeadLine(Integer deadLine) {
		this.deadLine = deadLine;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public String getMotorProposalNo() {
		return motorProposalNo;
	}

	public void setMotorProposalNo(String motorProposalNo) {
		this.motorProposalNo = motorProposalNo;
	}

	public List<CrossSalePlan> getPlanLists() {
		return planLists;
	}

	public void setPlanLists(List<CrossSalePlan> planLists) {
		this.planLists = planLists;
	}

	public String getDocumentNo() {
		return documentNo;
	}

	public void setDocumentNo(String documentNo) {
		this.documentNo = documentNo;
	}

	public Map<String, String> getParaMap() {
		return paraMap;
	}

	public void setParaMap(Map<String, String> paraMap) {
		this.paraMap = paraMap;
	}

	public String[] getPlanIds() {
		return planIds;
	}

	public void setPlanIds(String[] planIds) {
		this.planIds = planIds;
	}

	

}
