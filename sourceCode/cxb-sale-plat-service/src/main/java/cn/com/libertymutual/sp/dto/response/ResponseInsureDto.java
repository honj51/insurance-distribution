package cn.com.libertymutual.sp.dto.response;

public class ResponseInsureDto {
	private String bussFromNo;
	private String bussNewNo;
	private String uniqueCode;
	private Double premium;
	private Double amount;
	private String sequenceId;
	private String success;
	private String message;
	private String ConsumerId;
	private String OperatorCode;
	public String getBussFromNo() {
		return bussFromNo;
	}
	public void setBussFromNo(String bussFromNo) {
		this.bussFromNo = bussFromNo;
	}
	public String getBussNewNo() {
		return bussNewNo;
	}
	public void setBussNewNo(String bussNewNo) {
		this.bussNewNo = bussNewNo;
	}
	public String getUniqueCode() {
		return uniqueCode;
	}
	public void setUniqueCode(String uniqueCode) {
		this.uniqueCode = uniqueCode;
	}
	public Double getPremium() {
		return premium;
	}
	public void setPremium(Double premium) {
		this.premium = premium;
	}
	public Double getAmount() {
		return amount;
	}
	public void setAmount(Double amount) {
		this.amount = amount;
	}
	public String getSequenceId() {
		return sequenceId;
	}
	public void setSequenceId(String sequenceId) {
		this.sequenceId = sequenceId;
	}
	public String getSuccess() {
		return success;
	}
	public void setSuccess(String success) {
		this.success = success;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getConsumerId() {
		return ConsumerId;
	}
	public void setConsumerId(String consumerId) {
		ConsumerId = consumerId;
	}
	public String getOperatorCode() {
		return OperatorCode;
	}
	public void setOperatorCode(String operatorCode) {
		OperatorCode = operatorCode;
	}
	@Override
	public String toString() {
		return "ResponseInsureDto [bussFromNo=" + bussFromNo + ", bussNewNo="
				+ bussNewNo + ", uniqueCode=" + uniqueCode + ", premium="
				+ premium + ", amount=" + amount + ", sequenceId=" + sequenceId
				+ ", success=" + success + ", message=" + message
				+ ", ConsumerId=" + ConsumerId + ", OperatorCode="
				+ OperatorCode + "]";
	}
	
	
	
}
