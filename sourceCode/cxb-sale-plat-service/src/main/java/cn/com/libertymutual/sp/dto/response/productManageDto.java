package cn.com.libertymutual.sp.dto.response;

public class productManageDto {
	private String productId;
	private String riskCode;
	private String riskName;
	private String productCname;
	private Double rate;
	private String order;
	public String getProductId() {
		return productId;
	}
	
	public String getRiskName() {
		return riskName;
	}

	public void setRiskName(String riskName) {
		this.riskName = riskName;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		this.order = order;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getRiskCode() {
		return riskCode;
	}
	public void setRiskCode(String riskCode) {
		this.riskCode = riskCode;
	}
	public String getProductCname() {
		return productCname;
	}
	public void setProductCname(String productCname) {
		this.productCname = productCname;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}






	


	
	
}
