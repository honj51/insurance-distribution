package cn.com.libertymutual.sp.dto.savePlan.base;

/**
 * 销售各产品平台基础Dto
 * 后续如有通用属性可添加到基础类中
 */
public class AgentBaseDto {
	private String agreementCode;//业务关系代码
	private String agreementName;//业务关系名称 // 后加字段
	private String agentcode;//代理人代码
	private String agentName;//代理人名称
	private String salerName;//售人员名称
	private String salerNumber;//销售人员职业证号
	public String getAgreementCode() {
		return agreementCode;
	}
	public void setAgreementCode(String agreementCode) {
		this.agreementCode = agreementCode;
	}
	public String getAgentcode() {
		return agentcode;
	}
	public void setAgentcode(String agentcode) {
		this.agentcode = agentcode;
	}
	public String getAgentName() {
		return agentName;
	}
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}
	
	public String getSalerName() {
		return salerName;
	}
	public void setSalerName(String salerName) {
		this.salerName = salerName;
	}
	public String getSalerNumber() {
		return salerNumber;
	}
	public void setSalerNumber(String salerNumber) {
		this.salerNumber = salerNumber;
	}
	public String getAgreementName() {
		return agreementName;
	}
	public void setAgreementName(String agreementName) {
		this.agreementName = agreementName;
	}
	
	
	
	public AgentBaseDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public AgentBaseDto(String agreementCode, String agreementName, String agentcode, String agentName,
			String salerName, String salerNumber) {
		super();
		this.agreementCode = agreementCode;
		this.agreementName = agreementName;
		this.agentcode = agentcode;
		this.agentName = agentName;
		this.salerName = salerName;
		this.salerNumber = salerNumber;
	}
	@Override
	public String toString() {
		return "BaseAgentDto [agreementCode=" + agreementCode + ", agreementName=" + agreementName + ", agentcode="
				+ agentcode + ", agentName=" + agentName + ", salerName=" + salerName + ", salerNumber=" + salerNumber
				+ "]";
	}
	
	
}
