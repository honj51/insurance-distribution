package cn.com.libertymutual.sp.req;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
@ApiModel
@Entity
public class ScoreDetail implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -926986202507252670L;
	private String changeType;
	private Double reChangeScore;
	private Double acChangeScore;
	private String reasonNo;
	private String reason;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
	private Date changeTime;
	private String remark;
	public String getChangeType() {
		return changeType;
	}
	public void setChangeType(String changeType) {
		this.changeType = changeType;
	}
	public Double getReChangeScore() {
		return reChangeScore;
	}
	public void setReChangeScore(Double reChangeScore) {
		this.reChangeScore = reChangeScore;
	}
	public Double getAcChangeScore() {
		return acChangeScore;
	}
	public void setAcChangeScore(Double acChangeScore) {
		this.acChangeScore = acChangeScore;
	}
	public String getReasonNo() {
		return reasonNo;
	}
	public void setReasonNo(String reasonNo) {
		this.reasonNo = reasonNo;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public Date getChangeTime() {
		return changeTime;
	}
	public void setChangeTime(Date changeTime) {
		this.changeTime = changeTime;
	}
	public String getRemark() {
		return remark;
	}
	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
}
