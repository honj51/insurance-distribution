package cn.com.libertymutual.sp.service.api;

import javax.servlet.http.HttpServletRequest;

public interface BaseService {
	/**
	 * Remarks: 获取域名全路径<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年1月22日下午2:04:51<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String getServerNameFull(HttpServletRequest request);

	/**Remarks: 获取域名<br>
	 * Version：1.0<br>
	 * Author：AoYi<br>
	 * DateTime：2018年1月22日下午2:05:19<br>
	 * Project：liberty_sale_plat<br>
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public String getServerName(HttpServletRequest request);

}
