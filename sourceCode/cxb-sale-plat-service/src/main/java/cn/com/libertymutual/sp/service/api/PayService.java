package cn.com.libertymutual.sp.service.api;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.dto.CallBackRequestDto;
import cn.com.libertymutual.sp.dto.RequestPayDto;
import cn.com.libertymutual.sp.dto.TQueryPolicyRequstDto;

public interface PayService {
	public void payNsync(ServletRequest servletRequest, HttpServletRequest request, HttpServletResponse response, boolean isCalBack);

	public void paySync(ServletRequest servletRequest, HttpServletRequest request, HttpServletResponse response, boolean isCalBack);

	public ServiceResult getPayData(RequestPayDto requestPayDto, HttpServletRequest request);

	public ServiceResult payInfoFind(RequestPayDto requestPayDto, HttpServletRequest request);

	public ServiceResult queryPayList(TQueryPolicyRequstDto requestPayDto, HttpServletRequest request);

	ServiceResult callbackUrl(String documentNo, Boolean isJump, HttpServletResponse response);

	ServiceResult updateOrderState(Object data);

	ServiceResult callbackUpdateOrder(CallBackRequestDto cbrd);

	String resetState(String state);

}
