package cn.com.libertymutual.sp.service.api;

import java.util.Map;

import cn.com.libertymutual.sp.dto.TQueryPolicyResponseDto;

public interface PolicyService {

	Map<String, Object> doCxfQueryNonCarPolicy(String policyCode)
			throws Exception;

	Map<String, Object> doCxfQueryCarPolicy(String policyCode) throws Exception;

	TQueryPolicyResponseDto queryDetail(String policyNo);

}
