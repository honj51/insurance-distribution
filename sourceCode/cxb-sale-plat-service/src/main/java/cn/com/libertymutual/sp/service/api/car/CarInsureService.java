package cn.com.libertymutual.sp.service.api.car;

import cn.com.libertymutual.core.web.ServiceResult;
import cn.com.libertymutual.sp.dto.car.dto.QueryVehicleRequestDto;
import cn.com.libertymutual.sp.dto.car.dto.TRenewalQueryRequestDto;
import cn.com.libertymutual.sp.dto.car.dto.TVerifyRequstDTO;
import cn.com.libertymutual.sp.dto.queryplans.QuerySPPlansRequestDTO;
import cn.com.libertymutual.sp.dto.savePlan.PropsalSaveRequestDto;

public interface CarInsureService {
	/**
	 * 续保车辆查询
	 * @param trRequest
	 * @return
	 */
	ServiceResult renewalQuery(TRenewalQueryRequestDto trRequest);

	/**
	 * 投保车辆查询
	 * @param venDto
	 * @return
	 */
	ServiceResult queryVehicle(QueryVehicleRequestDto venDto);

	/**
	 * vin码查询
	 * @param venDto
	 * @return
	 */
	ServiceResult queryVin(QueryVehicleRequestDto venDto);

	/**
	 * 保费计算接口
	 * @param venDto
	 * @return
	 */
	ServiceResult combinecalculate(PropsalSaveRequestDto requestDto);

	/**
	 * 提交核保
	 * @param venDto
	 * @return
	 */
	ServiceResult preminumCommit(PropsalSaveRequestDto requestDto);

	/**
	 * 交叉销售
	 * @param seatCount
	 * @param age
	 * @return
	 */
	ServiceResult CrossSalePlan(Integer seatCount, Integer age, String agreementCode);

	/**
	 * 交叉销售计划保存
	 * @param seatCount
	 * @param age
	 * @return
	 */
	ServiceResult CrossSalePlanSave(QuerySPPlansRequestDTO requestDto);

	/**
	 * 交叉销售计划查询
	 * @param seatCount
	 * @param age
	 * @return
	 */
	ServiceResult CrossSalePlanFind(String documentNo, String agreementCode);

	/**
	 * 转保
	 * @param tVerifyRequstDTO
	 * @return
	 */
	ServiceResult Verify(TVerifyRequstDTO tVerifyRequstDTO);

	/**
	 * Token认证接口
	 * @return
	 */
	String getToken();
}
