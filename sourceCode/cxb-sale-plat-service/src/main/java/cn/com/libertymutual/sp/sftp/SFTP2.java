package cn.com.libertymutual.sp.sftp;
import java.util.Date;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import cn.com.libertymutual.core.sftp.SFtpClient;
import cn.com.libertymutual.core.sftp.SFtpConfig;
import cn.com.libertymutual.core.web.util.RequestUtils;
import cn.com.libertymutual.sp.bean.TbSpSaleLog;
import cn.com.libertymutual.sp.dao.SpSaleLogDao;

import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.SftpException;

//@Component
//@RefreshScope // 刷新配置无需重启服务
public class SFTP2 {

	private Logger log = LoggerFactory.getLogger(getClass());
	@Autowired
	private SpSaleLogDao saleLogDao;
	@Value("${ftp.ports}")
	private String ports;

	@Value("${ftp.hosts}")
	private String hosts;

	@Value("${ftp.usernames}")
	private String usernames;

	@Value("${ftp.passwords}")
	private String passwords;

	@Value("${ftp.dsts}")
	private String dsts;
	
	private String[] portss = null;
	private String[] hostss = null;
	private String[] usernamess = null;
	private String[] passwordss = null;
	private String[] dstss = null;
	
	private String thisIP = null;
	
	@PostConstruct
	public void init () {
		portss = ports.split(",");
		hostss = hosts.split(",");
		usernamess = usernames.split(",");
		passwordss = passwords.split(",");
		dstss = dsts.split(",");
		

		log.info("------hostss-----------{}", hostss[0]);
		log.info("------usernames-----------{}", usernamess[0]);
		

		thisIP = RequestUtils.getLocalHostLANAddress()[0];

		log.info("------thisIP-----------{}", thisIP);

	}

	public void SFTPupload(String src,String parampath) {

		/*String[] portss = ports.split(",");
		String[] hostss = hosts.split(",");
		String[] usernamess = usernames.split(",");
		String[] passwordss = passwords.split(",");
		String[] dstss = dsts.split(",");*/

		SFtpConfig sftpConfig = null;
		SFtpClient sfc = null;
		for (int i = 0; i < hostss.length; i++) {
			
			if( hostss[i].equals( thisIP ) ) {
				log.info("本机服务器{}，不用上传.", thisIP);
				continue;
			}
			TbSpSaleLog upload = new TbSpSaleLog();
			log.info("============yuansheng============");
			sftpConfig = new SFtpConfig();
			sftpConfig.setHost(hostss[i]);
			sftpConfig.setPassword(passwordss[i]);
			sftpConfig.setUsername(usernamess[i]);
			sftpConfig.setPort(Integer.parseInt(portss[i]));
			
			sfc = new SFtpClient();
			sfc.setSftpConfig(sftpConfig);
			try {
				upload.setRequestTime(new Date());
				upload.setRequestData(thisIP+"的"+src+" SFTP到: "+hostss[i]+"下"+dstss[i]+parampath+"/");
				
				log.info("发送文件{}到{}", src, dstss[i]+parampath+"/");
//				log.info("发送文件{}到{}", src, dstss[i]+parampath+"/testimage/");
//				Boolean boo = sfc.putFile(src,dstss[i]+parampath+"/testimage/");
				Boolean boo = sfc.putFile(src,dstss[i]+parampath+"/");
				upload.setResponseData(boo.toString());
				upload.setMark(hostss[i]);
				upload.setOperation("SFTP上传文件");
				saleLogDao.save(upload);
			} catch (JSchException e) {
				e.printStackTrace();
			} catch (SftpException e) {
				e.printStackTrace();
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			finally {
				sfc.destroy();
			}
		}
	}
}
