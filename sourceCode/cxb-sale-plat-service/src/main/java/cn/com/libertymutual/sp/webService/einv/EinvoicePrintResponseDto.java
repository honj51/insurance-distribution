package cn.com.libertymutual.sp.webService.einv;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlAccessorType(XmlAccessType.FIELD)	
@XmlRootElement(name="EinvPrintRsgData")
public class EinvoicePrintResponseDto {
	//字段名	注释	数据类型	最大长度	是否必填	说明
	private String errorCode;	//错误代码	字符	3	是	000为成功，其他均为失败
	private String errorMsg;	//错误描述	字符	100	否	错误详情
	private String vatCode;	//发票代码	字符	50	否	发票代码 
	private String vatSerialNo;	//发票号	字符	50	否	发票号
	private String vatPrintDate;	//发票打印时间	字符	8	否	格式 yyyyMMddHHmmss
	private String pdfUrl; //发票下载地址
	
	//附加参数，用于开具成功后查询
	private String policyNoAuto; //保单号
	private String identifyNumberAuto; //证件号码
	private String validateCode; //验证码
	
	
	
	public String getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}
	public String getVatCode() {
		return vatCode;
	}
	public void setVatCode(String vatCode) {
		this.vatCode = vatCode;
	}
	public String getVatSerialNo() {
		return vatSerialNo;
	}
	public void setVatSerialNo(String vatSerialNo) {
		this.vatSerialNo = vatSerialNo;
	}
	public String getVatPrintDate() {
		return vatPrintDate;
	}
	public void setVatPrintDate(String vatPrintDate) {
		this.vatPrintDate = vatPrintDate;
	}
	public String getPdfUrl() {
		return pdfUrl;
	}
	public void setPdfUrl(String pdfUrl) {
		this.pdfUrl = pdfUrl;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public String getPolicyNoAuto() {
		return policyNoAuto;
	}
	public void setPolicyNoAuto(String policyNoAuto) {
		this.policyNoAuto = policyNoAuto;
	}
	public String getIdentifyNumberAuto() {
		return identifyNumberAuto;
	}
	public void setIdentifyNumberAuto(String identifyNumberAuto) {
		this.identifyNumberAuto = identifyNumberAuto;
	}
	public String getValidateCode() {
		return validateCode;
	}
	public void setValidateCode(String validateCode) {
		this.validateCode = validateCode;
	}
}
