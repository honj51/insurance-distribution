
package cn.com.libertymutual.sp.webService.policy;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for policyDetailInquiryResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="policyDetailInquiryResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="policyDetailInquiryResponseDto" type="{http://prpall.liberty.com/all/cb/policyDetailInquiry/bean}policyDetailInquiryResponseDto" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "policyDetailInquiryResponse", namespace = "http://prpall.liberty.com/all/cb/policyDetailInquiry/intf/PolicyDetailInquiryResponse", propOrder = {
    "policyDetailInquiryResponseDto"
})
public class PolicyDetailInquiryResponse {

    protected PolicyDetailInquiryResponseDto policyDetailInquiryResponseDto;

    /**
     * Gets the value of the policyDetailInquiryResponseDto property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyDetailInquiryResponseDto }
     *     
     */
    public PolicyDetailInquiryResponseDto getPolicyDetailInquiryResponseDto() {
        return policyDetailInquiryResponseDto;
    }

    /**
     * Sets the value of the policyDetailInquiryResponseDto property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyDetailInquiryResponseDto }
     *     
     */
    public void setPolicyDetailInquiryResponseDto(PolicyDetailInquiryResponseDto value) {
        this.policyDetailInquiryResponseDto = value;
    }

}
