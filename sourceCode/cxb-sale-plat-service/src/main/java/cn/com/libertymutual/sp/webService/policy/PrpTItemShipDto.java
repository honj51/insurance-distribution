
package cn.com.libertymutual.sp.webService.policy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for prpTItemShipDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prpTItemShipDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="makestartdate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="shipcname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shipcode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shiptypecode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prpTItemShipDto", propOrder = {
    "makestartdate",
    "shipcname",
    "shipcode",
    "shiptypecode"
})
public class PrpTItemShipDto implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 1515020410706911372L;
	@XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar makestartdate;
    protected String shipcname;
    protected String shipcode;
    protected String shiptypecode;

    /**
     * Gets the value of the makestartdate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getMakestartdate() {
        return makestartdate;
    }

    /**
     * Sets the value of the makestartdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setMakestartdate(XMLGregorianCalendar value) {
        this.makestartdate = value;
    }

    /**
     * Gets the value of the shipcname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipcname() {
        return shipcname;
    }

    /**
     * Sets the value of the shipcname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipcname(String value) {
        this.shipcname = value;
    }

    /**
     * Gets the value of the shipcode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShipcode() {
        return shipcode;
    }

    /**
     * Sets the value of the shipcode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShipcode(String value) {
        this.shipcode = value;
    }

    /**
     * Gets the value of the shiptypecode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShiptypecode() {
        return shiptypecode;
    }

    /**
     * Sets the value of the shiptypecode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShiptypecode(String value) {
        this.shiptypecode = value;
    }

}
