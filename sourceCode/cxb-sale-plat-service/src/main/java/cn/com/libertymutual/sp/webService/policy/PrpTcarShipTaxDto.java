
package cn.com.libertymutual.sp.webService.policy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for prpTcarShipTaxDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prpTcarShipTaxDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="annualTaxAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="annualTaxDue" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="calculateMode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="declareDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="deduction" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="deductionDocumentNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="deductionDueCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="deductionRate" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="deductionType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="exceedDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="exceedDaysCount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="overDue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sumOverdue" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="sumTax" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="sumTaxDefault" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="taxConditionCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="taxDepartment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="taxDepartmentCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="taxDocumentNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="taxDue" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="taxEndDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="taxLocationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="taxPayerIdentificationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="taxRegistryNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="taxStartDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="taxTermTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="taxUnitTypeCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="taxpayerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="totalAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="unitRate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prpTcarShipTaxDto", propOrder = {
    "annualTaxAmount",
    "annualTaxDue",
    "calculateMode",
    "declareDate",
    "deduction",
    "deductionDocumentNumber",
    "deductionDueCode",
    "deductionRate",
    "deductionType",
    "exceedDate",
    "exceedDaysCount",
    "overDue",
    "sumOverdue",
    "sumTax",
    "sumTaxDefault",
    "taxConditionCode",
    "taxDepartment",
    "taxDepartmentCode",
    "taxDocumentNumber",
    "taxDue",
    "taxEndDate",
    "taxLocationCode",
    "taxPayerIdentificationCode",
    "taxRegistryNumber",
    "taxStartDate",
    "taxTermTypeCode",
    "taxUnitTypeCode",
    "taxpayerName",
    "totalAmount",
    "unitRate"
})
public class PrpTcarShipTaxDto implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    protected String annualTaxAmount;
    protected double annualTaxDue;
    protected String calculateMode;
    protected String declareDate;
    protected double deduction;
    protected String deductionDocumentNumber;
    protected String deductionDueCode;
    protected double deductionRate;
    protected String deductionType;
    protected String exceedDate;
    protected String exceedDaysCount;
    protected String overDue;
    protected double sumOverdue;
    protected double sumTax;
    protected double sumTaxDefault;
    protected String taxConditionCode;
    protected String taxDepartment;
    protected String taxDepartmentCode;
    protected String taxDocumentNumber;
    protected String taxDue;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar taxEndDate;
    protected String taxLocationCode;
    protected String taxPayerIdentificationCode;
    protected String taxRegistryNumber;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar taxStartDate;
    protected String taxTermTypeCode;
    protected String taxUnitTypeCode;
    protected String taxpayerName;
    protected String totalAmount;
    protected String unitRate;

    /**
     * Gets the value of the annualTaxAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnualTaxAmount() {
        return annualTaxAmount;
    }

    /**
     * Sets the value of the annualTaxAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnualTaxAmount(String value) {
        this.annualTaxAmount = value;
    }

    /**
     * Gets the value of the annualTaxDue property.
     * 
     */
    public double getAnnualTaxDue() {
        return annualTaxDue;
    }

    /**
     * Sets the value of the annualTaxDue property.
     * 
     */
    public void setAnnualTaxDue(double value) {
        this.annualTaxDue = value;
    }

    /**
     * Gets the value of the calculateMode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCalculateMode() {
        return calculateMode;
    }

    /**
     * Sets the value of the calculateMode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCalculateMode(String value) {
        this.calculateMode = value;
    }

    /**
     * Gets the value of the declareDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeclareDate() {
        return declareDate;
    }

    /**
     * Sets the value of the declareDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeclareDate(String value) {
        this.declareDate = value;
    }

    /**
     * Gets the value of the deduction property.
     * 
     */
    public double getDeduction() {
        return deduction;
    }

    /**
     * Sets the value of the deduction property.
     * 
     */
    public void setDeduction(double value) {
        this.deduction = value;
    }

    /**
     * Gets the value of the deductionDocumentNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeductionDocumentNumber() {
        return deductionDocumentNumber;
    }

    /**
     * Sets the value of the deductionDocumentNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeductionDocumentNumber(String value) {
        this.deductionDocumentNumber = value;
    }

    /**
     * Gets the value of the deductionDueCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeductionDueCode() {
        return deductionDueCode;
    }

    /**
     * Sets the value of the deductionDueCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeductionDueCode(String value) {
        this.deductionDueCode = value;
    }

    /**
     * Gets the value of the deductionRate property.
     * 
     */
    public double getDeductionRate() {
        return deductionRate;
    }

    /**
     * Sets the value of the deductionRate property.
     * 
     */
    public void setDeductionRate(double value) {
        this.deductionRate = value;
    }

    /**
     * Gets the value of the deductionType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeductionType() {
        return deductionType;
    }

    /**
     * Sets the value of the deductionType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeductionType(String value) {
        this.deductionType = value;
    }

    /**
     * Gets the value of the exceedDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExceedDate() {
        return exceedDate;
    }

    /**
     * Sets the value of the exceedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExceedDate(String value) {
        this.exceedDate = value;
    }

    /**
     * Gets the value of the exceedDaysCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExceedDaysCount() {
        return exceedDaysCount;
    }

    /**
     * Sets the value of the exceedDaysCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExceedDaysCount(String value) {
        this.exceedDaysCount = value;
    }

    /**
     * Gets the value of the overDue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOverDue() {
        return overDue;
    }

    /**
     * Sets the value of the overDue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOverDue(String value) {
        this.overDue = value;
    }

    /**
     * Gets the value of the sumOverdue property.
     * 
     */
    public double getSumOverdue() {
        return sumOverdue;
    }

    /**
     * Sets the value of the sumOverdue property.
     * 
     */
    public void setSumOverdue(double value) {
        this.sumOverdue = value;
    }

    /**
     * Gets the value of the sumTax property.
     * 
     */
    public double getSumTax() {
        return sumTax;
    }

    /**
     * Sets the value of the sumTax property.
     * 
     */
    public void setSumTax(double value) {
        this.sumTax = value;
    }

    /**
     * Gets the value of the sumTaxDefault property.
     * 
     */
    public double getSumTaxDefault() {
        return sumTaxDefault;
    }

    /**
     * Sets the value of the sumTaxDefault property.
     * 
     */
    public void setSumTaxDefault(double value) {
        this.sumTaxDefault = value;
    }

    /**
     * Gets the value of the taxConditionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxConditionCode() {
        return taxConditionCode;
    }

    /**
     * Sets the value of the taxConditionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxConditionCode(String value) {
        this.taxConditionCode = value;
    }

    /**
     * Gets the value of the taxDepartment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxDepartment() {
        return taxDepartment;
    }

    /**
     * Sets the value of the taxDepartment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxDepartment(String value) {
        this.taxDepartment = value;
    }

    /**
     * Gets the value of the taxDepartmentCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxDepartmentCode() {
        return taxDepartmentCode;
    }

    /**
     * Sets the value of the taxDepartmentCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxDepartmentCode(String value) {
        this.taxDepartmentCode = value;
    }

    /**
     * Gets the value of the taxDocumentNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxDocumentNumber() {
        return taxDocumentNumber;
    }

    /**
     * Sets the value of the taxDocumentNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxDocumentNumber(String value) {
        this.taxDocumentNumber = value;
    }

    /**
     * Gets the value of the taxDue property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxDue() {
        return taxDue;
    }

    /**
     * Sets the value of the taxDue property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxDue(String value) {
        this.taxDue = value;
    }

    /**
     * Gets the value of the taxEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTaxEndDate() {
        return taxEndDate;
    }

    /**
     * Sets the value of the taxEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTaxEndDate(XMLGregorianCalendar value) {
        this.taxEndDate = value;
    }

    /**
     * Gets the value of the taxLocationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxLocationCode() {
        return taxLocationCode;
    }

    /**
     * Sets the value of the taxLocationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxLocationCode(String value) {
        this.taxLocationCode = value;
    }

    /**
     * Gets the value of the taxPayerIdentificationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxPayerIdentificationCode() {
        return taxPayerIdentificationCode;
    }

    /**
     * Sets the value of the taxPayerIdentificationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxPayerIdentificationCode(String value) {
        this.taxPayerIdentificationCode = value;
    }

    /**
     * Gets the value of the taxRegistryNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxRegistryNumber() {
        return taxRegistryNumber;
    }

    /**
     * Sets the value of the taxRegistryNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxRegistryNumber(String value) {
        this.taxRegistryNumber = value;
    }

    /**
     * Gets the value of the taxStartDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getTaxStartDate() {
        return taxStartDate;
    }

    /**
     * Sets the value of the taxStartDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setTaxStartDate(XMLGregorianCalendar value) {
        this.taxStartDate = value;
    }

    /**
     * Gets the value of the taxTermTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxTermTypeCode() {
        return taxTermTypeCode;
    }

    /**
     * Sets the value of the taxTermTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxTermTypeCode(String value) {
        this.taxTermTypeCode = value;
    }

    /**
     * Gets the value of the taxUnitTypeCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxUnitTypeCode() {
        return taxUnitTypeCode;
    }

    /**
     * Sets the value of the taxUnitTypeCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxUnitTypeCode(String value) {
        this.taxUnitTypeCode = value;
    }

    /**
     * Gets the value of the taxpayerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxpayerName() {
        return taxpayerName;
    }

    /**
     * Sets the value of the taxpayerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxpayerName(String value) {
        this.taxpayerName = value;
    }

    /**
     * Gets the value of the totalAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotalAmount() {
        return totalAmount;
    }

    /**
     * Sets the value of the totalAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotalAmount(String value) {
        this.totalAmount = value;
    }

    /**
     * Gets the value of the unitRate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitRate() {
        return unitRate;
    }

    /**
     * Sets the value of the unitRate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitRate(String value) {
        this.unitRate = value;
    }

}
