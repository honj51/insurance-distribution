
package cn.com.libertymutual.sp.webService.policy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for prptApplicantDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="prptApplicantDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="account" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="accountName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="appliBlackStateName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="appliIdentifyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="appliIdentifyType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="appliInsuredNature" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="appliInsuredType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="appliLinkerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="appliMobile" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="appliNAge" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="appliNBirthday" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="appliNLocalPoliceStation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="appliNRoomAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="appliNRoomPostCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="appliNSex" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="appliNUnit" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="appliNUnitAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="appliNUnitPostCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="appliPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="appliPostAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="appliPostCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="appliPrpInsuredInsuredCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="appliPrpInsuredInsuredName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="appliStature" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="appliWeight" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="bank" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="healthAddress1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="healthAddress2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="healthAddress3" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="healthAddress4" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="industry" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="industryInfor" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="nationFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paymentMethord" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="socialSecurityNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="specificBusiness" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="waysOfPayment" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "prptApplicantDto", propOrder = {
    "account",
    "accountName",
    "appliBlackStateName",
    "appliIdentifyNumber",
    "appliIdentifyType",
    "appliInsuredNature",
    "appliInsuredType",
    "appliLinkerName",
    "appliMobile",
    "appliNAge",
    "appliNBirthday",
    "appliNLocalPoliceStation",
    "appliNRoomAddress",
    "appliNRoomPostCode",
    "appliNSex",
    "appliNUnit",
    "appliNUnitAddress",
    "appliNUnitPostCode",
    "appliPhoneNumber",
    "appliPostAddress",
    "appliPostCode",
    "appliPrpInsuredInsuredCode",
    "appliPrpInsuredInsuredName",
    "appliStature",
    "appliWeight",
    "bank",
    "healthAddress1",
    "healthAddress2",
    "healthAddress3",
    "healthAddress4",
    "industry",
    "industryInfor",
    "nationFlag",
    "paymentMethord",
    "socialSecurityNo",
    "specificBusiness",
    "waysOfPayment"
})
public class PrptApplicantDto implements Serializable{

    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

    protected String account;
    protected String accountName;
    protected String appliBlackStateName;
    protected String appliIdentifyNumber;
    protected String appliIdentifyType;
    protected String appliInsuredNature;
    protected String appliInsuredType;
    protected String appliLinkerName;
    protected String appliMobile;
    protected String appliNAge;
    protected String appliNBirthday;
    protected String appliNLocalPoliceStation;
    protected String appliNRoomAddress;
    protected String appliNRoomPostCode;
    protected String appliNSex;
    protected String appliNUnit;
    protected String appliNUnitAddress;
    protected String appliNUnitPostCode;
    protected String appliPhoneNumber;
    protected String appliPostAddress;
    protected String appliPostCode;
    protected String appliPrpInsuredInsuredCode;
    protected String appliPrpInsuredInsuredName;
    protected String appliStature;
    protected String appliWeight;
    protected String bank;
    protected String healthAddress1;
    protected String healthAddress2;
    protected String healthAddress3;
    protected String healthAddress4;
    protected String industry;
    protected String industryInfor;
    protected String nationFlag;
    protected String paymentMethord;
    protected String socialSecurityNo;
    protected String specificBusiness;
    protected String waysOfPayment;

    /**
     * Gets the value of the account property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccount() {
        return account;
    }

    /**
     * Sets the value of the account property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccount(String value) {
        this.account = value;
    }

    /**
     * Gets the value of the accountName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountName() {
        return accountName;
    }

    /**
     * Sets the value of the accountName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountName(String value) {
        this.accountName = value;
    }

    /**
     * Gets the value of the appliBlackStateName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliBlackStateName() {
        return appliBlackStateName;
    }

    /**
     * Sets the value of the appliBlackStateName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliBlackStateName(String value) {
        this.appliBlackStateName = value;
    }

    /**
     * Gets the value of the appliIdentifyNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliIdentifyNumber() {
        return appliIdentifyNumber;
    }

    /**
     * Sets the value of the appliIdentifyNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliIdentifyNumber(String value) {
        this.appliIdentifyNumber = value;
    }

    /**
     * Gets the value of the appliIdentifyType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliIdentifyType() {
        return appliIdentifyType;
    }

    /**
     * Sets the value of the appliIdentifyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliIdentifyType(String value) {
        this.appliIdentifyType = value;
    }

    /**
     * Gets the value of the appliInsuredNature property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliInsuredNature() {
        return appliInsuredNature;
    }

    /**
     * Sets the value of the appliInsuredNature property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliInsuredNature(String value) {
        this.appliInsuredNature = value;
    }

    /**
     * Gets the value of the appliInsuredType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliInsuredType() {
        return appliInsuredType;
    }

    /**
     * Sets the value of the appliInsuredType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliInsuredType(String value) {
        this.appliInsuredType = value;
    }

    /**
     * Gets the value of the appliLinkerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliLinkerName() {
        return appliLinkerName;
    }

    /**
     * Sets the value of the appliLinkerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliLinkerName(String value) {
        this.appliLinkerName = value;
    }

    /**
     * Gets the value of the appliMobile property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliMobile() {
        return appliMobile;
    }

    /**
     * Sets the value of the appliMobile property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliMobile(String value) {
        this.appliMobile = value;
    }

    /**
     * Gets the value of the appliNAge property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliNAge() {
        return appliNAge;
    }

    /**
     * Sets the value of the appliNAge property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliNAge(String value) {
        this.appliNAge = value;
    }

    /**
     * Gets the value of the appliNBirthday property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliNBirthday() {
        return appliNBirthday;
    }

    /**
     * Sets the value of the appliNBirthday property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliNBirthday(String value) {
        this.appliNBirthday = value;
    }

    /**
     * Gets the value of the appliNLocalPoliceStation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliNLocalPoliceStation() {
        return appliNLocalPoliceStation;
    }

    /**
     * Sets the value of the appliNLocalPoliceStation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliNLocalPoliceStation(String value) {
        this.appliNLocalPoliceStation = value;
    }

    /**
     * Gets the value of the appliNRoomAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliNRoomAddress() {
        return appliNRoomAddress;
    }

    /**
     * Sets the value of the appliNRoomAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliNRoomAddress(String value) {
        this.appliNRoomAddress = value;
    }

    /**
     * Gets the value of the appliNRoomPostCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliNRoomPostCode() {
        return appliNRoomPostCode;
    }

    /**
     * Sets the value of the appliNRoomPostCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliNRoomPostCode(String value) {
        this.appliNRoomPostCode = value;
    }

    /**
     * Gets the value of the appliNSex property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliNSex() {
        return appliNSex;
    }

    /**
     * Sets the value of the appliNSex property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliNSex(String value) {
        this.appliNSex = value;
    }

    /**
     * Gets the value of the appliNUnit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliNUnit() {
        return appliNUnit;
    }

    /**
     * Sets the value of the appliNUnit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliNUnit(String value) {
        this.appliNUnit = value;
    }

    /**
     * Gets the value of the appliNUnitAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliNUnitAddress() {
        return appliNUnitAddress;
    }

    /**
     * Sets the value of the appliNUnitAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliNUnitAddress(String value) {
        this.appliNUnitAddress = value;
    }

    /**
     * Gets the value of the appliNUnitPostCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliNUnitPostCode() {
        return appliNUnitPostCode;
    }

    /**
     * Sets the value of the appliNUnitPostCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliNUnitPostCode(String value) {
        this.appliNUnitPostCode = value;
    }

    /**
     * Gets the value of the appliPhoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliPhoneNumber() {
        return appliPhoneNumber;
    }

    /**
     * Sets the value of the appliPhoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliPhoneNumber(String value) {
        this.appliPhoneNumber = value;
    }

    /**
     * Gets the value of the appliPostAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliPostAddress() {
        return appliPostAddress;
    }

    /**
     * Sets the value of the appliPostAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliPostAddress(String value) {
        this.appliPostAddress = value;
    }

    /**
     * Gets the value of the appliPostCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliPostCode() {
        return appliPostCode;
    }

    /**
     * Sets the value of the appliPostCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliPostCode(String value) {
        this.appliPostCode = value;
    }

    /**
     * Gets the value of the appliPrpInsuredInsuredCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliPrpInsuredInsuredCode() {
        return appliPrpInsuredInsuredCode;
    }

    /**
     * Sets the value of the appliPrpInsuredInsuredCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliPrpInsuredInsuredCode(String value) {
        this.appliPrpInsuredInsuredCode = value;
    }

    /**
     * Gets the value of the appliPrpInsuredInsuredName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliPrpInsuredInsuredName() {
        return appliPrpInsuredInsuredName;
    }

    /**
     * Sets the value of the appliPrpInsuredInsuredName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliPrpInsuredInsuredName(String value) {
        this.appliPrpInsuredInsuredName = value;
    }

    /**
     * Gets the value of the appliStature property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliStature() {
        return appliStature;
    }

    /**
     * Sets the value of the appliStature property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliStature(String value) {
        this.appliStature = value;
    }

    /**
     * Gets the value of the appliWeight property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAppliWeight() {
        return appliWeight;
    }

    /**
     * Sets the value of the appliWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAppliWeight(String value) {
        this.appliWeight = value;
    }

    /**
     * Gets the value of the bank property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBank() {
        return bank;
    }

    /**
     * Sets the value of the bank property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBank(String value) {
        this.bank = value;
    }

    /**
     * Gets the value of the healthAddress1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHealthAddress1() {
        return healthAddress1;
    }

    /**
     * Sets the value of the healthAddress1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHealthAddress1(String value) {
        this.healthAddress1 = value;
    }

    /**
     * Gets the value of the healthAddress2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHealthAddress2() {
        return healthAddress2;
    }

    /**
     * Sets the value of the healthAddress2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHealthAddress2(String value) {
        this.healthAddress2 = value;
    }

    /**
     * Gets the value of the healthAddress3 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHealthAddress3() {
        return healthAddress3;
    }

    /**
     * Sets the value of the healthAddress3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHealthAddress3(String value) {
        this.healthAddress3 = value;
    }

    /**
     * Gets the value of the healthAddress4 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHealthAddress4() {
        return healthAddress4;
    }

    /**
     * Sets the value of the healthAddress4 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHealthAddress4(String value) {
        this.healthAddress4 = value;
    }

    /**
     * Gets the value of the industry property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndustry() {
        return industry;
    }

    /**
     * Sets the value of the industry property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndustry(String value) {
        this.industry = value;
    }

    /**
     * Gets the value of the industryInfor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIndustryInfor() {
        return industryInfor;
    }

    /**
     * Sets the value of the industryInfor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIndustryInfor(String value) {
        this.industryInfor = value;
    }

    /**
     * Gets the value of the nationFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNationFlag() {
        return nationFlag;
    }

    /**
     * Sets the value of the nationFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNationFlag(String value) {
        this.nationFlag = value;
    }

    /**
     * Gets the value of the paymentMethord property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentMethord() {
        return paymentMethord;
    }

    /**
     * Sets the value of the paymentMethord property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentMethord(String value) {
        this.paymentMethord = value;
    }

    /**
     * Gets the value of the socialSecurityNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSocialSecurityNo() {
        return socialSecurityNo;
    }

    /**
     * Sets the value of the socialSecurityNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSocialSecurityNo(String value) {
        this.socialSecurityNo = value;
    }

    /**
     * Gets the value of the specificBusiness property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSpecificBusiness() {
        return specificBusiness;
    }

    /**
     * Sets the value of the specificBusiness property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSpecificBusiness(String value) {
        this.specificBusiness = value;
    }

    /**
     * Gets the value of the waysOfPayment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWaysOfPayment() {
        return waysOfPayment;
    }

    /**
     * Sets the value of the waysOfPayment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWaysOfPayment(String value) {
        this.waysOfPayment = value;
    }

}
