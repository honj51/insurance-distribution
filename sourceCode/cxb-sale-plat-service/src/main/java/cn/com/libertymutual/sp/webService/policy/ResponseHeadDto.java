
package cn.com.libertymutual.sp.webService.policy;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for responseHeadDto complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="responseHeadDto">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="bussinessServiceMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="bussinessServiceResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="CAppNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="comCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="consumerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="count" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="operatorCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="osbMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="osbResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="proxyServiceMessage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="proxyServiceResult" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sequenceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "responseHeadDto", propOrder = {
    "bussinessServiceMessage",
    "bussinessServiceResult",
    "cAppNo",
    "comCode",
    "consumerId",
    "count",
    "operatorCode",
    "osbMessage",
    "osbResult",
    "proxyServiceMessage",
    "proxyServiceResult",
    "sequenceId"
})
public class ResponseHeadDto implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7134978976866545925L;
	protected String bussinessServiceMessage;
    protected String bussinessServiceResult;
    @XmlElement(name = "CAppNo")
    protected String cAppNo;
    protected String comCode;
    protected String consumerId;
    protected int count;
    protected String operatorCode;
    protected String osbMessage;
    protected String osbResult;
    protected String proxyServiceMessage;
    protected String proxyServiceResult;
    protected String sequenceId;

    /**
     * Gets the value of the bussinessServiceMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBussinessServiceMessage() {
        return bussinessServiceMessage;
    }

    /**
     * Sets the value of the bussinessServiceMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBussinessServiceMessage(String value) {
        this.bussinessServiceMessage = value;
    }

    /**
     * Gets the value of the bussinessServiceResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBussinessServiceResult() {
        return bussinessServiceResult;
    }

    /**
     * Sets the value of the bussinessServiceResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBussinessServiceResult(String value) {
        this.bussinessServiceResult = value;
    }

    /**
     * Gets the value of the cAppNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCAppNo() {
        return cAppNo;
    }

    /**
     * Sets the value of the cAppNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCAppNo(String value) {
        this.cAppNo = value;
    }

    /**
     * Gets the value of the comCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getComCode() {
        return comCode;
    }

    /**
     * Sets the value of the comCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setComCode(String value) {
        this.comCode = value;
    }

    /**
     * Gets the value of the consumerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConsumerId() {
        return consumerId;
    }

    /**
     * Sets the value of the consumerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConsumerId(String value) {
        this.consumerId = value;
    }

    /**
     * Gets the value of the count property.
     * 
     */
    public int getCount() {
        return count;
    }

    /**
     * Sets the value of the count property.
     * 
     */
    public void setCount(int value) {
        this.count = value;
    }

    /**
     * Gets the value of the operatorCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperatorCode() {
        return operatorCode;
    }

    /**
     * Sets the value of the operatorCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperatorCode(String value) {
        this.operatorCode = value;
    }

    /**
     * Gets the value of the osbMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOsbMessage() {
        return osbMessage;
    }

    /**
     * Sets the value of the osbMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOsbMessage(String value) {
        this.osbMessage = value;
    }

    /**
     * Gets the value of the osbResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOsbResult() {
        return osbResult;
    }

    /**
     * Sets the value of the osbResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOsbResult(String value) {
        this.osbResult = value;
    }

    /**
     * Gets the value of the proxyServiceMessage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProxyServiceMessage() {
        return proxyServiceMessage;
    }

    /**
     * Sets the value of the proxyServiceMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProxyServiceMessage(String value) {
        this.proxyServiceMessage = value;
    }

    /**
     * Gets the value of the proxyServiceResult property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProxyServiceResult() {
        return proxyServiceResult;
    }

    /**
     * Sets the value of the proxyServiceResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProxyServiceResult(String value) {
        this.proxyServiceResult = value;
    }

    /**
     * Gets the value of the sequenceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequenceId() {
        return sequenceId;
    }

    /**
     * Sets the value of the sequenceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequenceId(String value) {
        this.sequenceId = value;
    }

}
