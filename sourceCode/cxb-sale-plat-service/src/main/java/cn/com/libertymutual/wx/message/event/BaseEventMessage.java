package cn.com.libertymutual.wx.message.event;

import cn.com.libertymutual.wx.message.BaseMessage;

public class BaseEventMessage extends BaseMessage {
	private String EventType;

	public String getEventType() {
		return this.EventType;
	}

	public void setEventType(String eventType) {
		this.EventType = eventType;
	}
}
