package cn.com.libertymutual.wx.message.requestdto;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class ImageMessage extends RequestBaseMessage {
	@XStreamAlias("MediaId")
	private String mediaId;
	@XStreamAlias("PicUrl")
	private String picUrl;

	public String getMediaId() {
		return this.mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}
}
