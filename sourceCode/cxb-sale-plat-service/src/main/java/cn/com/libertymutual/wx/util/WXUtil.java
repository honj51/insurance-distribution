package cn.com.libertymutual.wx.util;

import org.springframework.stereotype.Component;

@Component
public class WXUtil {

	public static final String ACCESSTOKEN = "accessToken";// 全局的access_token在Redis中存储名,该token每天限2000次，每2小时失效一次
	public static final String TICKET = "ticket";// ticket

	public static final String SNSAPI_BASE = "snsapi_base";// 获取微信用户基本信息的方式,静默授权获取，必须关注公众号
	public static final String SNSAPI_USERINFO = "snsapi_userinfo";// 获取微信用户基本信息的方式,网页授权获取，无须关注

	/**重试次数(例如获取token/发送消息等)*/
	public static final int RETRYTIMES = 3;
	/**缓存accesstoken等信息的100分钟时间，单位：毫秒*/
	public static final int REDIS_TIME_6000 = 100 * 60;

	/** 国家地区语言版本 */
	public static final String LANG = "zh_CN";

	/** 通信编码 */
	public static final String ENCODING = "utf-8";

	/**获取全局access_token  限2000（次/天）*/
	public static final String GET_ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";

	/**获取JSSDK ticket，本平台主用于分享*/
	public static final String GET_JSSDK_TICKET_URL = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=ACCESS_TOKEN&type=jsapi";

	/**获取code*/
	public static final String GET_CODE_URL = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=%s&redirect_uri=%s&response_type=code&scope=%s&state=STATE#wechat_redirect";

	/**获取用户基本信息的AccessToken*/
	public static final String GET_USER_ACCESS_TOKEN_URL = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code";

	/**获取用户基本信息——静默*/
	public static final String POST_USER_INFO_URL_SILENT = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=%s&openid=%s&lang=zh_CN";

	/**获取用户基本信息——网页授权*/
	public static final String GET_USER_INFO_URL_OAUTH = "https://api.weixin.qq.com/sns/userinfo?access_token=%s&openid=%s&lang=zh_CN";

	/** 取access_token地址 */
	public static final String GET_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token";
	/** 取关注者列表 */
	public static final String GET_USER_URL = "https://api.weixin.qq.com/cgi-bin/user/get";
	/** 取关注者信息 */
	public static final String GET_USER_INFO_URL = "https://api.weixin.qq.com/cgi-bin/user/info";
	/** 取分组列表 */
	public static final String GET_USER_GROUP_URL = "https://api.weixin.qq.com/cgi-bin/groups/get";
	/** 根据openid取分组ID */
	public static final String POST_USER_GROUP_ID_URL = "https://api.weixin.qq.com/cgi-bin/groups/getid";
	/** 创建用户分组 */
	public static final String CREATE_USER_GROUP_URL = "https://api.weixin.qq.com/cgi-bin/groups/create";
	/** 修改用户分组名称 */
	public static final String UPDATE_USER_GROUP_URL = "https://api.weixin.qq.com/cgi-bin/groups/update";
	/** 移动用户分组 */
	public static final String MOVE_USER_TO_GROUP_URL = "https://api.weixin.qq.com/cgi-bin/groups/members/update";
	/** 创建自定义菜单 */
	public static final String CREATE_MENU_URL = "https://api.weixin.qq.com/cgi-bin/menu/create";
	/** 上传多媒体文件 */
	public static final String UPLOAD_MEDIA_URL = "http://file.api.weixin.qq.com/cgi-bin/media/upload";
	/** 上传图文消息*/
	public static final String UPLOAD_ARTICLE_URL = "https://api.weixin.qq.com/cgi-bin/media/uploadnews";
	/** 发送消息 */
	public static final String SEND_MESSAGE_URL = "https://api.weixin.qq.com/cgi-bin/message/custom/send";
}
