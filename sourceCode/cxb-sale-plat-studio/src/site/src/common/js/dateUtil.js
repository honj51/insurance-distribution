export default {
    /**
     * 时间 / 时间戳 转yyyy-MM-dd 
     */
    getDateyMd(val) {
        let d = new Date(val);
        let curr_date = d.getDate();
        let curr_month = d.getMonth() + 1;
        let curr_year = d.getFullYear();
        String(curr_month).length < 2 ? (curr_month = "0" + curr_month) : curr_month;
        String(curr_date).length < 2 ? (curr_date = "0" + curr_date) : curr_date;
        let yyyyMMdd = curr_year + "-" + curr_month + "-" + curr_date;
        return yyyyMMdd;
    },
    /**
     * 获得当前时间时分秒
     */
    getNowTimeHMS() {
        let date = new Date();
        let hour = date.getHours();
        let minute = date.getMinutes();
        let second = date.getSeconds();
        return hour + ":" + minute + ":" + second;
    }

}