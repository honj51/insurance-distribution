import HighCharts from 'highcharts';
import dateUtil from './dateUtil';

//  demo :   https://www.hcharts.cn/demo/highcharts
export default {
    /**
     * 加载图表
     * @param id  绑定的id
     * @param chartsData 画图的数据
     * @param chartType 图标类型
     * @param titleText 图标名字
     */
    getChartColumn(id, chartsData, chartType, titleText) {
        let _this = this;
        HighCharts.chart(id, {
            chart: {
                type: chartType
            },
            title: {
                text: titleText
            },
            subtitle: {
                text: ''
            },
            xAxis: {
                type: 'category',
                labels: {
                    rotation: -45,
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            },
            yAxis: {
                min: 0,
                title: {
                    text: '条数'
                }
            },
            credits: {
                enabled: false,
            },
            legend: {
                enabled: false
            },
            tooltip: {
                pointFormat: '条数: <b>{point.y:.1f} 条 </b>'
            },
            series: [{
                name: '总条数',
                data: chartsData,
                dataLabels: {
                    enabled: true,
                    rotation: -90,
                    color: '#FFFFFF',
                    align: 'right',
                    // format: '{point.y:.1f}', // one decimal
                    y: 10, // 10 pixels down from the top
                    style: {
                        fontSize: '13px',
                        fontFamily: 'Verdana, sans-serif'
                    }
                }
            }]
        });
    },
    /**
     * 加载折线图表
     * @param id  绑定的id
     * @param titleText 标题
     * @param subtitleText 子标题
     * @param xData x轴数据
     * @param yDataText y轴说明
     * @param yData y轴数据
     */
    getLineChart(id, titleText, subtitleText, xData, yDataText, yData) {
        let _this = this;
        HighCharts.chart(id, {
            chart: {
                type: 'line'
            },
            title: {
                text: titleText
            },
            subtitle: {
                text: subtitleText
            },
            xAxis: {
                categories: xData
            },
            yAxis: {
                title: {
                    text: yDataText
                }
            },
            credits: {
                enabled: false,
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true          // 开启数据标签
                    },
                    enableMouseTracking: true // 关闭鼠标跟踪，对应的提示框、点击事件会失效
                }
            },
            series: yData
        });
    },
    /**
     * 生成动态图表
     * @param  绑定的id
     * @param  图表类型
     * 
     */
    getDynamicChart(id, type, yDataUrl, urlParm) {
        HighCharts.setOptions({ global: { useUTC: false } });
        HighCharts.chart(id, {
            chart: {
                type: type,
                animation: HighCharts.svg, // don't animate in old IE
                marginRight: 10,
                events: {
                    load: function() {

                        // set up the updating of the chart each second
                        var series = this.series[0];
                        setInterval(function() {
                            var x = (new Date()).getTime(), // current time
                                // y = Math.random();
                                y;
                            if (sessionStorage.getItem("dynaChartState") == 'true') {
                                $.ajax({
                                    type: "POST",
                                    url: yDataUrl,
                                    sync: true,
                                    data: urlParm,
                                    success: function(data, status, xhr) {
                                        let usemen = 1 - (data.result.menfree['mem.free'] / data.result.mem.mem);
                                        y = usemen;

                                        series.addPoint([x, y], true, true);
                                    }
                                });
                            }
                        }, 2000);
                    }
                }
            },
            title: {
                text: "服务器CPU使用情况"
            },
            xAxis: {
                type: 'datetime',
                tickPixelInterval: 100,
                labels: {
                    format: '{value: %H:%M:%S}',
                    align: 'right',

                }
            },
            credits: {
                enabled: false,
            },
            yAxis: {
                title: {
                    text: '百分比/%'
                },
                plotLines: [{
                    value: 0,
                    width: 1,
                    color: '#808080'
                }]
            },
            tooltip: {
                formatter: function() {
                    return '<b>' + this.series.name + '</b><br/> 时间：' +
                        HighCharts.dateFormat('%Y-%m-%d %H:%M:%S', this.x) + '<br/> 使用率：' +
                        HighCharts.numberFormat(this.y, 2);
                }
            },
            legend: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            series: [{
                name: '详细信息',
                data: (function() {
                    // generate an array of random data
                    var data = [],
                        time = (new Date()).getTime(),
                        i;

                    for (i = -10; i <= 0; i += 1) {
                        data.push({
                            x: time + i * 2000,
                            y: 0
                        });
                    }
                    return data;
                } ())
            }]
        });
    }
}