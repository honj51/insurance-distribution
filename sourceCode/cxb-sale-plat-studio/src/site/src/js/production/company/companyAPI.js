/*****************************************************************************************************
 * DESC			:	API函数定义
 * Author		:	Steven.Li
 * Datetime		:	2017-07-06
 * ***************************************************************************************************
 * 函数组			函数名称			函数作用
 *
 * 接口函数
 * 			           queryKindData		           按条件获取条款信息
 *                                             initRiskSelect                            初始化险种下拉框信息
 * ***************************************************************************************************
 */

import Constant from "../constant";
import Validations from "../validations";

const CompanyAPI = {

           queryCompanys(_this) {
                $.ajax({
                    type: 'POST',
                    url: Constant.urls.queryCompanys,
                    dataType: 'json',
                    success: function(data) {
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                            _this.comOptions.push({
                                    branchNo: "ALL",
                                    branchCname: "全部公司",
                                    branchEname: "All Companies",
                            })
                            for(let index in data.result) {
                                     // _this.comOptions.push(data.result[index]);
                                    _this.comOptions.push({
                                        branchNo: data.result[index].branchNo,
                                        branchCname: data.result[index].branchCname,
                                        branchEname: data.result[index].branchEname,
                                    });
                            }
                        } else {
                           _this.$message({type: 'error',message: '调用接口错误！'});
                        }
                    }
                });
            },
             queryComs(_this) {
                $.ajax({
                    type: 'POST',
                    url: Constant.urls.queryCompanys,
                    dataType: 'json',
                    success: function(data) {
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                          // _this.comOptions = data.result;
                          for(let index in data.result) {
                                 // _this.comOptions.push(data.result[index]);
                                _this.comOptions.push({
                                    branchNo: data.result[index].branchNo,
                                    branchCname: data.result[index].branchCname,
                                    branchEname: data.result[index].branchEname,
                                });
                            }
                        } else {
                           _this.$message({type: 'error',message: '调用接口错误！'});
                        }
                    }
                });
            },
            queryComsByCodes(_this) {
                $.ajax({
                    type: 'POST',
                    url: Constant.urls.queryCompanysBycodes,
                    data: {
                      comcodes: _this.$parent.defaultComOptions
                    },
                    dataType: 'json',
                    success: function(data) {
                        if(data.state === 3) {
                            _this.$router.push('/login');
                            _this.$message({type: 'error',message: data.result});
                            return false;
                        }
                        if(data.state == 0) {
                          // _this.comOptions = data.result;
                            for(let index in data.result) {
                                // _this.comOptions.push(data.result[index]);
                                _this.comOptions.push({
                                    branchNo: data.result[index].branchNo,
                                    branchCname: data.result[index].branchCname,
                                    branchEname: data.result[index].branchEname,
                                });
                            }
                        } else {
                           _this.$message({type: 'error',message: '调用接口错误！'});
                        }
                    }
                });
            },
}

export default CompanyAPI
