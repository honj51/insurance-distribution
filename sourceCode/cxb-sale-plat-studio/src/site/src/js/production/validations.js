/***********************************************************************************************************************
 * DESC			:	常用功能函数定义
 * Author		:	Steven.Li
 * Datetime		:	2017-07-06
 * *********************************************************************************************************************
 * 函数组				函数名称					函数作用
 * 
 * 日期处理
 * 						bindDatePicker			      给输入框[class='bindDate']绑定日期插件
 * 						getNowFormatDate		返回当前日期YYYY-MM-DD
 * 						getAnyDate				返回N天后日期
 * 						getNextYearToday		      返回下一年今天的日期
 *
 * 其他
 *                                    getScreenHeight                 返回屏幕高度
 * *********************************************************************************************************************
 */	

const Validations = {
          
          required(obj,_this) {
                  for(let i = 0;i<obj.length;i++) {
                      if(obj[i] === null || typeof obj[i] === 'undefined' || obj[i] === '') {
                          _this.$message({type: 'error',message: '带 “ * ” 号字段为必填项！'});
                          return false;
                      }
                  }
                  return true;
          },

          /**
           * 只能输入整数及小数
           * @param  {[type]} intLength     整数部分长度
           * @param  {[type]} digitalLength 小数保留位数
           * @param  {[type]} obj           表单对象
           * @param  {[type]} field           字段
           */
          checkDecimal(intLength, digitalLength, obj, field) {
			obj[field] = obj[field].replace(/[^\d.]/g,""); //只能输入数字及小数点
			obj[field] = obj[field].replace(/^\./g,"");  //以数字开始

			if (obj[field].length > 1) {	//排除0开始的长整数
				let firstInt = obj[field].slice(0,1);
				if (firstInt === '0' && obj[field].slice(1,2) !== '.') {
					obj[field] = '0';
				}
			}

			if (obj[field].length > intLength && obj[field].indexOf(".") < 0) {  //整数长度
				let dot = obj[field].slice(intLength,intLength + 1);
				if (dot !== '.') {
					obj[field] = obj[field].slice(0,intLength); 
				}
			}

			obj[field] = obj[field].replace(/\.{2,}/g,"."); //只能输入一个小数点
			obj[field] = obj[field].replace(".","$#$").replace(/\./g,"").replace("$#$",".");

			let regular;
			switch (digitalLength) {
				case 1: {
					regular = /^(\-)*(\d+)\.(\d).*$/;
					break;
				}
				case 2: {
					regular = /^(\-)*(\d+)\.(\d\d).*$/;
					break;
				}
				case 3: {
					regular = /^(\-)*(\d+)\.(\d\d\d).*$/;
					break;
				}
				case 4: {
					regular = /^(\-)*(\d+)\.(\d\d\d\d).*$/;
					break;
				}
				default : {
					regular = /^(\-)*(\d+)\.(\d\d\d\d\d\d\d\d).*$/;
				}
			}
			obj[field] = obj[field].replace(regular,'$1$2.$3');  //保留4位小数
	},

	/**
           * 只能输入整数
           * @param  {[type]} intLength     整数长度
           * @param  {[type]} obj           表单对象
           * @param  {[type]} field           字段
           */
	checkNumber(intLength, obj, field) {
		obj[field] = obj[field].replace(/[^\d]/g,""); //只能输入数字

		if (obj[field].length > 1) {	//排除0开始的长整数
				let firstInt = obj[field].slice(0,1);
				if (firstInt === '0' && obj[field].slice(1,2) !== '.') {
					obj[field] = '0';
				}
		}

		if (obj[field].length > intLength) {  //整数长度
			obj[field] = obj[field].slice(0,intLength); 
		}
	}
}

export default Validations